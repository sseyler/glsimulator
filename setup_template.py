"""setuptools installation of GLSimulator
Copyright (c) 2018 Sean L Seyler <slseyler@asu.com>
Released under the GNU Public License 3 (or higher, your choice)

For a basic installation just type the command::
    python setup.py install
See the files INSTALL and README for details
"""
from __future__ import with_statement
try:
    from setuptools import setup, find_packages
    from setuptools.extension import Extension
except ImportError:
    from distutils.core import setup
    from distutils.extension import Extension

from setuptools import find_packages
from Cython.Build import cythonize
# from Cython.Distutils import build_ext

import os

with open("README.rst") as readme:
    long_description = readme.read()


# See: https://github.com/encore-similarity/encore/blob/master/setup.py
ext_modules = [
    Extension('glsim0',
              sources=[
                  '**/glsim0.pyx',
              ],
              include_dirs=[
                  '.',
                  '...',
                  '/home/seanseyler/Library/miniconda2/include',
                  os.path.join(os.getcwd(), 'include'),
              ],
              libraries=['m', 'gsl', 'gslcblas'],
              library_dirs=[os.getcwd(), ],  # path to .a or .so file(s)
              extra_compile_args=['-std=c++11', '-march=native', '-Ofast',
                                  '-ffast-math', '-fopenmp', '-Wall', '-Wextra'],
              extra_link_args=['-fopenmp'],
              language='c++'
              ),
    Extension()
]

setup(name='GLSimulator',
      version='0.1.0.dev',
      description='A simulation engine for generalized Langevin dynamics.',
      long_description=open('README.rst').read(),
      author='Sean L Seyler',
      author_email='slseyler@asu.com',
      license='GPLv3',
      url='http://glsimulator.readthedocs.org/',
      project_urls={
          'https://github.com/sseyler/generalized_langevin_simulator',
      },
      keywords="langevin simulation stochastic SDE 'generalized langevin'",
      classifiers=[
          'Development Status :: 3 - Alpha',
          'Environment :: Console',
          'Intended Audience :: Science/Research',
          'License :: OSI Approved :: GNU General Public License (GPL)',
          'Operating System :: POSIX',
          'Programming Language :: Python :: 2.7',
          'Programming Language :: Cython :: 0.28.2',
          'Topic :: Scientific/Engineering :: Physics',
      ],

      packages=['glsimulator', 'glsimulator.core',
                'glsimulator.manager', 'glsimulator.utils'],
      package_dir={
        'glsimulator' : 'glsimulator'
      },
      scripts=['scripts/testrun.py'],

      # package_dir={'glsimulator': 'src/glsimulator'},
      # cmdclass = {"build_ext": build_ext},
      ext_modules=cythonize(ext_modules),
      # ext_modules = cythonize("**/*.pyx"),

      # Package requirements
      install_requires=['cython>=0.25',
                        'numpy>=1.12',
                        'scipy>=1.0'],
      tests_require=['numpy', 'pandas'],
      )


# # build script for 'dvedit' - Python libdv wrapper
#
# # change this as needed
# libdvIncludeDir = "/usr/include/libdv"
#
# import sys, os
# from distutils.core import setup
# from distutils.extension import Extension
#
# # we'd better have Cython installed, or it's a no-go
# try:
#     from Cython.Distutils import build_ext
# except:
#     print("You don't seem to have Cython installed. Please get a")
#     print("copy from www.cython.org and install it")
#     sys.exit(1)
#
#
# # scan the 'dvedit' directory for extension files, converting
# # them to extension names in dotted notation
# def scandir(dir, files=[]):
#     for file in os.listdir(dir):
#         path = os.path.join(dir, file)
#         if os.path.isfile(path) and path.endswith(".pyx"):
#             files.append(path.replace(os.path.sep, ".")[:-4])
#         elif os.path.isdir(path):
#             scandir(path, files)
#     return files
#
#
# # generate an Extension object from its dotted name
# def makeExtension(extName):
#     extPath = extName.replace(".", os.path.sep)+".pyx"
#     return Extension(
#         extName,
#         [extPath],
#         include_dirs = [libdvIncludeDir, "."],   # adding the '.' to include_dirs is CRUCIAL!!
#         extra_compile_args = ["-O3", "-Wall"],
#         extra_link_args = ['-g'],
#         libraries = ["dv",],
#         )
#
# # get the list of extensions
# extNames = scandir("dvedit")
#
# # and build up the set of Extension objects
# extensions = [makeExtension(name) for name in extNames]
#
# # finally, we can pass all this to distutils
# setup(
#   name="dvedit",
#   packages=["dvedit", "dvedit.filters"],
#   ext_modules=extensions,
#   cmdclass = {'build_ext': build_ext},
# )
