# from __future__ import division, unicode_literals, print_function#, absolute_imports
import os, sys, time
import itertools as it
import pprint; pp = pprint.PrettyPrinter(indent=4)
# from contextlib import ContextDecorator  # Only available in Python3

import numpy as np
import pandas as pd

import copy
from dotmap import DotMap
from collections import OrderedDict

from glsimulator.utils.misc import (Bunch, round_to_int, module_exists, matches_any,
                                    matches_none, rename_existing_file, whatisthis,
                                    make_iterable, isiterable, to_unicode)
from glsimulator.utils.physical_quantities import PI, kBol, Navo, Re_s
from glsimulator.utils.physical_quantities import (mass_e, mass_p, gamma_s, gamma_0,
                                                   gamma_b, nu_b)
#################################################################################

SEED = 23061989
KEYSET = 1

#################################################################################
# Class to enable easier simulation post-processing
#################################################################################
class SimCapsule(object):
    trj_col_map = {'pos': 'X',
                   'vel': 'V',
                   'fex': 'FE',
                   'ths': 'NS',
                   'thb': 'NB',
                   'aux': 'S'}

    def __init__(self, inputs, directories, kT_REF=303.15*kBol, verbose=False):
        #=======================================================================
        self.verbose = verbose
        self.kT_REF = kT_REF
        #------------------------------------
        self.inputs = copy.deepcopy(inputs)  # OrderedDict(sorted(inputs.items()))
        self.force = DotMap()
        self.dirs = copy.deepcopy(directories)
        #=======================================================================

        #------------------------------------
        self.sid = '{:04d}'.format(int(inputs.sid)) # min 4-char numerical string
        self.units    = inputs.units
        self.unitless = inputs.unitless
        self.dynamics = inputs.dynamics
        self.method   = inputs.method
        self.dt    = inputs.dt
        self.tsim  = inputs.tsim
        self.dtout = inputs.dtout
        #------------------------------------
        self.NAUX  = inputs.NAUX
        self.NPAR  = inputs.NPAR
        self.NDIM  = inputs.NDIM
        self.NPAR  = inputs.NPAR
        self.NDIM  = inputs.NDIM
        self.NSETS = inputs.NSETS
        self.NWORK = inputs.meta.NWORK

        self.NRUN   = self.NPAR * self.NWORK * self.NSETS
        self.NTASKS = self.NWORK * self.NSETS

        #=======================================================================
        self.Q = DotMap()   # store unitful quantities
        self.QD = DotMap()  # store dimensionless quantities
        #------------------------------------
        self.Q.R = inputs.R
        self.Q.Te = inputs.Te
        self.Q.mu = inputs.mu
        self.Q.rhf = inputs.rhf
        self.Q.rhs = inputs.rhs
        self.QD.X0 = inputs.X0
        self.QD.V0 = inputs.V0
        #------------------------------------
        self.rng = DotMap() # store rng info
        self.io = DotMap()  # WARN: confusing; it's for I/O-related vars, not files...
        self.meta = DotMap()
        self.files = DotMap()
        self.trj = DotMap()

        self.rng.lib = inputs.rng.lib
        self.rng.name = inputs.rng.name

        #=== FORCE PARAMS ======================================================
        #------------------------------------
        self.fpar_abbr = {
            'style': self.inputs['force.style'],
            'shape': self.inputs['force.shape'],
            'amplitude': 'A',
            'slope': 'F0',
            'F0': 'F0',
            'F1': 'F1',
            'wavelength': 'L',
            'period': 'tau',
            'phase': 'phi',
            'bias': 'B',
            'duration': 't0',
            'gap_frac': 'gf',
            'sharpness': 'S',
            'n_cycles': 'NCYC'
        }

        ### TEMPORARY FIX FOR SCREWED-UP NAMING OF TRAJ VIA FORCE PARAM ORDERING ###
        if inputs['force.style'] == 'space-periodic':
            temp_fparam_ordered = ['style', 'shape', 'amplitude', 'slope', 'wavelength',
                                   'phase', 'bias', 'duration', 'gap_frac', 'sharpness']
        elif inputs['force.style'] == 'time-periodic':
            temp_fparam_ordered = ['style', 'shape', 'duration', 'amplitude',
                                   'period', 'n_cycles']
        elif matches_any(inputs['force.style'], ['potential']):
            if matches_any(inputs['force.shape'], ['harmonic']):
                temp_fparam_ordered = ['style', 'shape', 'F0', 'F1', 'duration']
            elif matches_any(inputs['force.shape'], ['linear']):
                temp_fparam_ordered = ['style', 'shape', 'F0', 'duration']
            else:
                print('WARNING: problem in SimCapsule constructor with potential force')
        else:
            print('WARNING: force style wasn\'t "space-periodic" or "time-periodic"...')
        # ['phi','B','t0','L','A','S','r']


        ##################################################################################

        self.force.style = self.inputs.pop('force.style')
        self.force.shape = self.inputs.pop('force.shape')
        params_force = '{}-{}'.format(self.force.style, self.force.shape)

        for par, val in copy.deepcopy(self.inputs).items():
            if par.startswith('force.'):
                _, name = par.split('force.')
                self.force.Q[name] = val
                # print('Adding parameter \"{}\" with value \"{}\" and id \"{}\" to self.force.Q'.format(name, self.force.Q[name], id(self.force.Q[name])))
                #-----------------------------
                if isinstance(val, float):
                    params_force += '/{}={:.1e}'.format(self.fpar_abbr[name], val)
                else:
                    params_force += '/{}={}'.format(self.fpar_abbr[name], val)

        # print('---------------------')
        # for par, val in self.inputs.items():
        #     print('id of parameter \"{}\" with value \"{}\" from self.inputs: \"{}\"'.format(par, val, id(val)))

        # for k, v in self.force.items():
        #     print(k, v, type(v), v == False)

        # for par in inputs.keys():
        #     if par.startswith('force.'):
        #         _, name = par.split('force.')
        #         self.force[name] = inputs.get(par)

        # force_ordered = OrderedDict((k, self.force[k]) for k in temp_fparam_ordered)
        # force_ordered = copy.deepcopy(force_ordered)

        # params_force = '{}-{}'.format(force_ordered['style'], force_ordered['shape'])
        # for par, value in force_ordered.items():
        #     if matches_none(par, ['style', 'shape', 'Q', 'QD']):
        #         self.force.Q[par] = value
        #         if isinstance(value, float):
        #             params_force += '/{}={:.1e}'.format(self.fpar_abbr[par], value)
        #         else:
        #             params_force += '/{}={}'.format(self.fpar_abbr[par], value)

        ##################################################################################

        #=== SIMULATION PARAMS =================================================
        params_sim = 'dt={}'.format(self.fmt_time(self.dt))
        if self.is_bbo_dynamics():
            self.NU0 = self.inputs.NU0
            params_sim += '_nu0={:.1e}_NAUX={}'.format(self.NU0, self.NAUX)
        else:
            self.NU0 = 1
            self.NAUX = 1
            if self.is_langevin_dynamics():
                self.dynamics = 'Langevin'
            elif self.is_brownian_dynamics():
                self.dynamics = 'Brownian'

        #=======================================================================
        if verbose:
            print('self.dynamics', self.dynamics, 'self.sid', self.sid)
            print('params_force', params_force)
            print('params_sim', params_sim)
        if verbose:
            print('\nForce parameters:')
            pprint.pprint(self.force.toDict())

        # NOTE: trj path used to have self.sid as bottommost dir; sid now in filename
        self.dirs.trj.relpath = '/'.join([params_force, self.dynamics, params_sim])
        self.dirs.trj.fullpath = '/'.join([self.dirs.trj.WORK, self.dirs.trj.relpath])
        self.dirs.analysis = None  # specified during postprocessing step
        self.dirs.figs     = None  # specified during postprocessing step
        #------------------------------------
        self.files.trj.basename = '_'.join([self.method, self.rng.name])
        self.files.trj.fullname = '/'.join([self.dirs.trj.fullpath,
                                            self.files.trj.basename])
        #=======================================================================
        if self.is_deterministic():
            print('NOTE: deterministic dynamics selected. Setting Te to 0.')
            self.Q.Te = 0.0
            self.equilibrate = False
        else:
            self.equilibrate = True

        #=======================================================================

    def add_simulation(self, sim):
        self.sim = sim

    def init_force_info(self, force_data_dict):
        raise NotImplementedError

    def check_file_exists(self, fullname):
        return os.path.exists(fullname)

    def has_existing_data(self, ext='h5'):
        fname_phys = '.'.join([self.files.trj.fullname, 'phys', ext])
        fname_dims = '.'.join([self.files.trj.fullname, 'dims', ext])
        return os.path.exists(fname_phys) and os.path.exists(fname_dims)

    def setup(self, force_eff_mass=False, n_chunk=10, T_eq_fracs=1.0, nstep_eq_frac=0.1,
              verbose=True):
        self.T_eq_fracs = make_iterable(T_eq_fracs)
        self.nstep_eq_frac = nstep_eq_frac
        self.force_eff_mass = force_eff_mass
        #-----------------------------------------------------------
        self.populate_physical_params()
        self.populate_force_params()
        self.non_dimensionalize()
        self.set_run_params(n_chunk=n_chunk, verbose=verbose)
        self.store_physical_metadata(verbose=verbose)
        self.store_simulation_metadata(verbose=verbose)

    def load(self, force_params=None):
        '''Alias for load_physical_metadata() and load_simulation_metadata().
        '''
        self.files.meta = Bunch()
        self.load_physical_metadata()
        self.load_simulation_metadata()
        self.load_force_metadata(force_params)

    def populate_physical_params(self):
        # WARNING: This code is fucky. It works, but you better know what you're doing..
        Te  = self.Q.Te
        mu  = self.Q.mu
        rhf = self.Q.rhf
        rhs = self.Q.rhs
        R   = self.Q.R
        #------------------------------------
        kTe = Te if self.unitless else kBol*Te
        knu = mu/rhf
        Me  = mass_e(R, rhf, rhs)
        Ms  = mass_p(R, rhs)
        if self.is_bbo_dynamics() or self.force_eff_mass:
            dyn_flag = 1
            M = Me
            if self.force_eff_mass:
                print('NOTE: this simulation is set up to use the effective' + \
                      ' mass instead of the particle mass.')
        elif self.is_langevin_dynamics() or self.is_brownian_dynamics():
            dyn_flag = 0
            M = Ms
        else:
            print('Error: the selected dynamics "{}" are unknown.'
                  ' Exiting.'.format(self.dynamics))
            sys.exit(-1)

        if self.units == 'density':
            M = Me
        #------------------------------------
        self.Q.update({'Me': Me, 'Ms': Ms, 'M': M, 'kTe': kTe, 'knu': knu})

        # if self.force_eff_mass:
        #     self._set_characteristic_scales(R, Me, rhs, rhf, Te, mu)
        # else:
        #     self._set_characteristic_scales(R, M, rhs, rhf, Te, mu)
        if Te == 0:
            print('NOTE: zero-temperature run. Using Te = 1 for non-dimensionalization')
            self._set_characteristic_scales(R, M, rhs, rhf, 1, mu)
        else:
            self._set_characteristic_scales(R, M, rhs, rhf, Te, mu)

        #------------------------------------
        self.Q.gams = gamma_s(R, knu, rhf, rhs, M)
        self.Q.gam0 = gamma_0(R, knu, rhf, rhs, M, self.NAUX, self.NU0, self.Tc)
        self.Q.gamb = dyn_flag*gamma_b(R, knu, rhf, rhs, M, self.NAUX, self.NU0, self.Tc)
        self.Q.nub  = dyn_flag*nu_b(self.NAUX, self.NU0, self.Tc)

    def _set_characteristic_scales(self, R, M, rhs, rhf, Te, mu):
        # WARNING: This code is fucky. It works, but you better know what you're doing..
        RH_o_BETA = (2*rhs + rhf) / 9.0
        RH   = rhs/rhf if rhf else np.inf
        BETA = 9 / (2*rhs/rhf + 1) if rhf else 0

        if self.units == 'arminski':
            self.Tc = (RH_o_BETA*R**2) / mu
            self.Vc = 1.0
            self.Ec = 6*PI*R*mu*(self.Tc*self.Vc**2)
            #------------------------------------
            self.Lc = self.Vc*self.Tc
            self.Fc = 6*PI*R*mu*(self.Vc)  # self.Ec/self.Lc
            self.Mc = self.Ec/self.Vc**2
            self.Pc = self.Ec/self.Tc
        elif self.units == 'thermal':
            self.Tc = (RH_o_BETA*R**2) / mu
            self.Mc = mass_e(R, rhf, rhs)
            self.Ec = Te
            self.Vc = ( self.Ec/self.Mc )**0.5
            self.Lc = self.Vc*self.Tc
            self.Fc = self.Ec/self.Lc
            self.Pc = self.Ec/self.Tc
        elif self.units == 'density':
            self.Tc = (RH_o_BETA*R**2) / mu
            self.Vc = ( 3.0 / (rhf/rhs + 2) )**0.5  # ( 1.5*rhs / (rhs + 0.5*rhf) )**0.5
            self.Ec = 6*PI*R*mu*(self.Tc*self.Vc**2)
            #------------------------------------
            self.Lc = self.Vc*self.Tc
            self.Fc = self.Ec/self.Lc
            self.Mc = self.Ec/self.Vc**2
            self.Pc = self.Ec/self.Tc
        else:  # like 'thermal' except uses input param M instead of effective mass
            self.Tc = (RH_o_BETA*R**2) / mu
            self.Ec = 1.0
            self.Vc = (self.Ec/M)**0.5
            #------------------------------------
            self.Lc = self.Vc*self.Tc
            self.Fc = self.Ec/self.Lc
            self.Mc = self.Ec/self.Vc**2
            self.Pc = self.Ec/self.Tc
        #----------------------------------------
        self.QD.update({'RH': RH, 'BETA': BETA})

        # print('self.Tc',self.Tc)
        # print('self.Vc',self.Vc)
        # print('self.Fc',self.Fc)
        # print('self.Lc',self.Lc)
        # print('self.Ec',self.Ec)
        # print('self.Mc',self.Mc)
        # print('self.Pc',self.Pc)
        # print('M',M)

    def populate_force_params(self):
        DURA_STEP = 0
        PERI_STEP = 0
        NCYC_TOTAL = 0
        f_style = self.force.style
        f_shape = self.force.shape

        if f_style == 'free':
            self.force.Q.update({'amplitude': 0, 'duration': 0,
                                 'period': 0, 'wavelength': 0})
            self.force.QD.update({'DURA_STEP': 0, 'PERI_STEP': 0,
                                  'NCYC_TOTAL': 0})
        elif f_style == 'potential':
            # self.force.Q.update({'F0': self.force.F0, 'F1': self.force.F1})
            self.force.QD.update({'DURA_STEP': DURA_STEP})
        elif matches_any(f_style, ['time-periodic','pulse_delay',
                                   'freq_doubling','burst']):
            DURA_STEP = round_to_int(self.force.Q.duration / self.dt)
            PERI_STEP = round_to_int(self.force.Q.period / self.dt)
            if f_style == 'burst':
                # DELAY_STEP = round_to_int(f.burst_delay / self.dt)
                BPERI_STEP = round_to_int(self.force.Q.burst_period / self.dt)
                N_BURSTS = round_to_int(self.force.Q.N_BURSTS / self.dt)
                self.force.Q.update({'burst_period': self.force.Q.burst_period})
                self.force.QD.update({'BPERI_STEP': BPERI_STEP, 'N_BURSTS': N_BURSTS})
            if matches_any(f_style, ['pulse_delay', 'freq_doubling']):
                NCYC_TOTAL = self.force.Q.n_cycles*self.force.Q.FMUL #FMUL pulses/(1 NCYC)
                DURA_STEP *= 1./self.force.Q.FMUL
                PERI_STEP *= 1./self.force.Q.FMUL
            else:
                NCYC_TOTAL = self.force.Q.n_cycles
            self.force.Q.update({'amplitude': self.force.Q.amplitude,
                                 'duration': self.force.Q.duration,
                                 'period': self.force.Q.period})
            self.force.QD.update({'DURA_STEP': DURA_STEP, 'PERI_STEP': PERI_STEP,
                                  'NCYC_TOTAL': NCYC_TOTAL})

        elif matches_any(f_style, ['space-periodic']):
            if matches_any(f_shape, ['butterworth', 'tilted', 'washboard']):
                DURA_STEP = round_to_int(self.force.Q.duration / self.dt)
                self.force.QD.update({'DURA_STEP': DURA_STEP})
            else:
                print('ERROR: Undefined force style in \"populate_force_params()\"!')

    def non_dimensionalize(self):
        self.QD.TSIM   = self.tsim   / self.Tc
        self.QD.DT     = self.dt     / self.Tc
        self.QD.DTOUT  = self.dtout  / self.Tc
        self.QD.MASSs  = self.Q.Ms   / self.Mc
        self.QD.MASSe  = self.Q.Me   / self.Mc
        self.QD.MASS   = self.Q.M    / self.Mc
        self.QD.RADIUS = self.Q.R    / self.Lc
        self.QD.GAMMAs = self.Q.gams * self.Tc
        self.QD.GAMMA0 = self.Q.gam0 * self.Tc
        self.QD.GAMMAb = self.Q.gamb * self.Tc
        self.QD.NUb    = self.Q.nub  * self.Tc
        self.QD.kT     = self.Q.kTe  / self.Ec
        #------------------------------------
        for par, value in copy.deepcopy(self.force.Q).items():
            if matches_none(par, ['style', 'shape', 'n_cycles', 'Q', 'QD']):
                par_abbr = self.fpar_abbr[par]
                value_nd = None
                if par == 'amplitude':
                    value_nd = value / self.Fc
                elif par == 'slope':
                    value_nd = value / self.Fc
                elif par == 'wavelength':
                    value_nd = value / self.Lc
                elif par == 'bias':
                    value_nd = value / self.Fc
                elif par == 'phase':
                    # value_nd = value / self.Lc
                    if value != 0:
                        raise NotImplementedError
                elif par == 'duration':
                    value_nd = value / self.Tc
                elif par == 'period':
                    value_nd = value / self.Tc
                elif par == 'F0':
                    value_nd = value / self.Fc
                elif par == 'F1':
                    value_nd = value / (self.Fc / self.Lc)
                #------------------------------------------
                # self.force.Q[par] = value
                if isinstance(value_nd, float):
                    self.force.QD[par_abbr] = value_nd

    def set_run_params(self, n_chunk=10, verbose=False):
        #--- output frame every NTOUT steps ---------------
        ntout       = round_to_int(self.QD.DTOUT / self.QD.DT)
        n_step_mult = n_chunk * ntout
        n_step      = int(np.around(self.QD.TSIM / self.QD.DT, decimals=-3))
        #------------------------------------
        self.io.NSTEP = n_step + n_step_mult - (n_step % n_step_mult)  # make divisible
        self.io.NTDMP = round_to_int(self.io.NSTEP / n_chunk)
        #------------------------------------
        self.io.NTOUT  = ntout
        self.io.NCHUNK = n_chunk
        self.io.NSTEP_EQ = round_to_int(self.io.NSTEP * self.nstep_eq_frac)
        self.io.NTOUT_EQ = ntout
        self.io.NTDMP_EQ = self.io.NTDMP
        #------------------------------------
        if (self.io.NTDMP % self.io.NTOUT) != 0:
            print('WARNING: NTDMP is not a divisor of NTOUT')
            sys.exit(-1)
        if verbose:
            print('TSIM:', self.QD.TSIM)
            print('NTOUT:', self.io.NTOUT)
            print('NSTEP:', self.io.NSTEP)
            print('NCHUNK:', self.io.NCHUNK)
            print('NTDMP:', self.io.NTDMP)
            print('dump size:', self.io.NTDMP//self.io.NTOUT)
            print('NSTEP_EQ:', self.io.NSTEP_EQ)
            print('NTOUT_EQ:', self.io.NTOUT_EQ)

    def store_physical_metadata(self, postfix='phys', ext='h5', verbose=False):
        #=======================================================================
        fname = '.'.join([self.files.trj.basename, postfix, ext])
        self.files.meta[postfix] = fname
        if not os.path.exists(self.dirs.trj.fullpath):
            os.makedirs(self.dirs.trj.fullpath)

        fullname = '/'.join([self.dirs.trj.fullpath, fname])
        self.files.meta.phys = fullname
        if self.check_file_exists(fullname):
            rename_existing_file(fullname)
        #=======================================================================
        Q, QD = self.Q, self.QD
        fields = DotMap()
        data = DotMap()
        #------------------------------------
        fields.phys = ['R', 'M', 'Me', 'Ms', 'Te', 'rhs', 'rhf', 'mu', 'gamS', 'gam0']
        fields.char  = ['Lc', 'Tc', 'Ec', 'Mc', 'Vc', 'Fc', 'Pc']
        fields.diml = ['beta', 'rhs_rel', 'Re_s']
        #------------------------------------
        data.phys = [Q.R, Q.M, Q.Me, Q.Ms, Q.Te, Q.rhs, Q.rhf, Q.mu, Q.gams, Q.gam0]
        data.char = [self.Lc, self.Tc, self.Ec, self.Mc, self.Vc, self.Fc, self.Pc]
        data.diml = [QD.BETA, QD.RH, Re_s(self.Vc, Q.R, Q.mu/Q.rhf)]
        #=======================================================================
        index = self.generate_metadata_index(fancy=False)
        d_phys = dict(zip(fields.phys, data.phys))
        d_char = dict(zip(fields.char, data.char))
        d_diml = dict(zip(fields.diml, data.diml))
        df_phys = pd.DataFrame(data=d_phys, columns=fields.phys, index=index)
        df_char = pd.DataFrame(data=d_char, columns=fields.char, index=index)
        df_diml = pd.DataFrame(data=d_diml, columns=fields.diml, index=index)
        df_phys.to_hdf(fullname, 'phys', mode='w', format='table', append=True)
        df_char.to_hdf(fullname, 'char', mode='a', format='table', append=True)
        df_diml.to_hdf(fullname, 'dimless', mode='a', format='table', append=True)
        #------------------------------------
        d_fex  = {self.fpar_abbr[k]:v for k, v in self.force.items()
                    if not any(k in s for s in ['Q','QD','style','shape'])}
        df_fex = pd.DataFrame(data=d_fex, index=index)
        df_fex.to_hdf(fullname, 'fex', mode='a', format='table', append=True)

        #------------------------------------
        if verbose:
            from IPython.display import display
            pd.set_option('display.max_rows', 25)
            pd.options.display.float_format = '{:9.2e}'.format
            display(df_phys)
            display(df_fex)
            display(df_char)
            display(df_diml)
            #------------------------------------
            return df_phys, df_fex, df_char, df_diml

    def store_simulation_metadata(self, postfix='dims', ext='h5', seed=SEED,
                                  keyset=KEYSET, verbose=False):
        #=======================================================================
        fname = '.'.join([self.files.trj.basename, postfix, ext])
        self.files.meta[postfix] = fname
        if not os.path.exists(self.dirs.trj.fullpath):
            os.makedirs(self.dirs.trj.fullpath)

        fullname = '/'.join([self.dirs.trj.fullpath, fname])
        self.files.meta.dims = fullname
        if self.check_file_exists(fullname):
            rename_existing_file(fullname)
        #=======================================================================
        fields = DotMap()
        data = DotMap()
        index = self.generate_metadata_index(fancy=False)
        #=======================================================================
        fields.dims.run = ['n_aux', 'n_dim', 'n_par', 'n_steps',  'ntout',
                           'ntdump', 'n_steps_eq', 'ntout_eq', 'ntdump_eq']
        data.dims.run = [self.NAUX, self.NDIM, self.NPAR, self.io.NSTEP, self.io.NTOUT,
                         self.io.NTDMP, self.io.NSTEP_EQ, self.io.NTOUT_EQ,
                         self.io.NTDMP_EQ]
        d_run = dict(zip(fields.dims.run, data.dims.run))
        df_run = pd.DataFrame(data=d_run, columns=fields.dims.run, index=index)
        df_run.to_hdf(fullname, 'run', mode='w', format='table', append=True)
        #------------------------------------
        fields.dims.batch = ['n_runs', 'n_sets', 'n_tasks', 'n_work', 'seed', 'keyset']
        data.dims.batch = [self.NRUN, self.NSETS, self.NTASKS, self.NWORK, seed, keyset]
        d_batch = dict(zip(fields.dims.batch, data.dims.batch))
        df_batch = pd.DataFrame(data=d_batch, columns=fields.dims.batch, index=index)
        df_batch.to_hdf(fullname, 'batch', mode='a', format='table', append=True)
        #------------------------------------
        if verbose:
            from IPython.display import display
            pd.set_option('display.max_rows', 25)
            pd.options.display.float_format = '{:9.2e}'.format
            display(df_run)
            display(df_batch)
            #------------------------------------
            return df_run, df_batch

    def generate_metadata_index(self, fancy=False):
        #=======================================================================
        def elements_to_str(data):
            for datum in data:
                if isinstance(datum, (str, int, bool)):
                    if isinstance(datum, basestring):
                        datum = datum.capitalize()
                    yield '{}'.format(datum)
                elif isinstance(datum, float):
                    yield '{:.1e}'.format(datum)
        #------------------------------------
        def encode_elements(data):
            for datum in data:
                yield u'{}'.format(datum).encode('utf-8')
        #=======================================================================
        if not fancy:
            return [0]
        id_names = ['Dynamics', 'Method', 'Units',
                    'dt', 'nu0', 'NP', 'ND']
        id_values = [self.dynamics, self.method, not self.unitless,
                     self.dt, self.NU0, self.NPAR, self.NDIM]
        # for name, value in self.force.items():
        #     if not any(name in s for s in ['Q','QD']):
        #         id_names.append('{}'.format(name.capitalize()))
        #         id_values.append(value)

        f_items = {k:v for k, v in self.force.items()
                    if not matches_any(k, ['Q','QD','style'])}
        id_names  += ['{}'.format(k) for k in f_items.keys()]
        id_values += f_items.values()
        #------------------------------------
        id_names = encode_elements(id_names)
        id_values = tuple([d for d in elements_to_str(id_values)])
        return pd.MultiIndex.from_tuples([id_values], names=id_names)

    def load_physical_metadata(self, postfix='phys', ext='h5', col='Units'):
        self.files.meta.phys = '.'.join([self.files.trj.fullname, postfix, ext])
        self.meta.phys = pd.read_hdf(self.files.meta.phys, 'phys')
        self.meta.char = pd.read_hdf(self.files.meta.phys, 'char')
        self.meta.diml = pd.read_hdf(self.files.meta.phys, 'dimless')
        #-----------------------------------------------------------------
        self.R    = self.extract_param(self.meta.phys, 'R')
        self.M    = self.extract_param(self.meta.phys, 'M')
        self.Me   = self.extract_param(self.meta.phys, 'Me')
        self.Ms   = self.extract_param(self.meta.phys, 'Ms')
        self.Te   = self.extract_param(self.meta.phys, 'Te')
        self.rhs  = self.extract_param(self.meta.phys, 'rhs')
        self.rhf  = self.extract_param(self.meta.phys, 'rhf')
        self.mu   = self.extract_param(self.meta.phys, 'mu')
        self.gamS = self.extract_param(self.meta.phys, 'gamS')
        self.gam0 = self.extract_param(self.meta.phys, 'gam0')
        #-----------------------------------------------------------------
        self.Lc = self.extract_param(self.meta.char, 'Lc')
        self.Tc = self.extract_param(self.meta.char, 'Tc')
        self.Ec = self.extract_param(self.meta.char, 'Ec')
        self.Mc = self.extract_param(self.meta.char, 'Mc')
        self.Vc = self.extract_param(self.meta.char, 'Vc')
        self.Fc = self.extract_param(self.meta.char, 'Fc')
        self.Pc = self.extract_param(self.meta.char, 'Pc')
        #-----------------------------------------------------------------
        self.beta    = self.extract_param(self.meta.diml, 'beta')
        self.rhs_rel = self.extract_param(self.meta.diml, 'rhs_rel')
        self.Re_s    = self.extract_param(self.meta.diml, 'Re_s')

    def load_simulation_metadata(self, postfix='dims', ext='h5'):
        self.files.meta.dims = '.'.join([self.files.trj.fullname, postfix, ext])
        self.meta.run   = pd.read_hdf(self.files.meta.dims, 'run')
        self.meta.batch = pd.read_hdf(self.files.meta.dims, 'batch')
        #-----------------------------------------------------------------
        self.n_aux      = self.extract_param(self.meta.run, 'n_aux')
        self.n_dim      = self.extract_param(self.meta.run, 'n_dim')
        self.n_par      = self.extract_param(self.meta.run, 'n_par')
        self.n_steps    = self.extract_param(self.meta.run, 'n_steps')
        self.ntout      = self.extract_param(self.meta.run, 'ntout')
        self.ntdump     = self.extract_param(self.meta.run, 'ntdump')
        self.n_steps_eq = self.extract_param(self.meta.run, 'n_steps_eq')
        self.ntout_eq   = self.extract_param(self.meta.run, 'ntout_eq')
        self.ntdump_eq  = self.extract_param(self.meta.run, 'ntdump_eq')
        #-----------------------------------------------------------------
        self.n_runs  = self.extract_param(self.meta.batch, 'n_runs')
        self.n_sets  = self.extract_param(self.meta.batch, 'n_sets')
        self.n_tasks = self.extract_param(self.meta.batch, 'n_tasks')
        self.n_work  = self.extract_param(self.meta.batch, 'n_work')
        self.seed    = self.extract_param(self.meta.batch, 'seed')
        self.keyset  = self.extract_param(self.meta.batch, 'keyset')
        #-----------------------------------------------------------------
        self.tsim = self.n_steps*self.dt
        self.QD.TSIM = self.tsim / self.Tc
        self.QD.DT = self.dt / self.Tc

    def load_force_metadata(self, force_params=None, postfix='phys', ext='h5',
                            col='Units'):
        self.meta.fex = pd.read_hdf(self.files.meta.phys, 'fex')
        #-----------------------------------------------------------------
        try:
            for name in force_params:
                self.force[name] = self.extract_param(self.meta.fex, name)
        except e:
            print('Error: {}\n  >>> No force parameters were loaded.'.format(e))

    def extract_param(self, df, param):
        return df[param][0]

    def load_full_trajectories(self, var_list, ext='h5', reindex=True, format='long',
                               combine=True, quiet=True, hack=False):
        for var in var_list:
            df_list = []
            for i in xrange(self.NTASKS):
                trj_name = '.'.join([self.files.trj.fullname,
                                     '{:04d}'.format(i), var, ext])
                df = pd.read_hdf(trj_name, var)
                df_list.append(df)
            #--------------------------------
            df = self.combine_trajectories(df_list)
            if reindex:
                print('  >>> Re-indexing trajectories...', end='')
                with Timer() as timer:
                    df = self.reindex_parallel_trj(df, var)
            if format == 'long':
                print('  >>> Converting trajectory DataFrames to long format...', end='')
                with Timer() as timer:
                    if var == 'aux':
                        df = self.wide_to_long(df, var=var, quiet=quiet, hack=hack)
                    else:
                        df = self.wide_to_long(df, var=var, quiet=quiet)

            self.trj[var] = df.copy()
        #------------------------------------
        if combine:
            print('  >>> Consolidating variables in single DataFrame...', end='')
            with Timer() as timer:
                for i, var in enumerate(var_list):
                    col = self.trj_col_map[var]
                    if i == 0:
                        df_combined = self.trj[var].copy()
                    else:
                        df_combined[col] = self.trj[var][col]
            return df_combined.copy()

    def combine_trajectories(self, df_list):
        '''Take a list of pandas DataFrames and return a combined one'''
        return pd.concat(df_list, axis=1)

    def reindex_parallel_trj(self, df, var):
        runs = ['Run{:04d}'.format(r+1) for r in xrange(self.NRUN)]
        if self.NDIM == 1:
            coords = ['x']
        elif self.NDIM == 2:
            coords = ['x', 'y']
        elif self.NDIM == 3:
            coords = ['x', 'y', 'z']
        if (var == 'pos' or var == 'vel' or var == 'fex'
                or var == 'ths' or var == 'thb'):
            names = ['Run', 'Coordinate']
            headr = list(it.product(runs, coords))
        elif var == 'aux':
            names = ['Run', 'Coordinate', 'S']
            index = ['S{:02d}'.format(i+1) for i in xrange(self.NAUX)]
            headr = list(it.product(runs, coords, index))
        elif var == 'noi':
            names = ['Run', 'Coordinate', 'G']
            index = ['G{:02d}'.format(i+1) for i in xrange(self.NAUX)]
            headr = list(it.product(runs, coords, index))
        new_columns = pd.MultiIndex.from_tuples(headr, names=names)
        df.columns = new_columns
        return df

    def wide_to_long(self, df, var='pos', dt=None, quiet=False, hack=False):
        if var == 'aux':
            DT = self.QD.DT
            # Insert a 'Time' column before all aux variable values
            df.insert(0, 'Time', DT * df.index.values)
            # Append the 'Time' column to the index (which has 'Step' already)
            df = df.set_index('Time', append=True)

            # WARN: TEMPORARY HACK TO GET AUX VAR CALCS FAST; NEED FULL REFACTOR
            if hack:
                S_dict_map = {'S{:02d}'.format(i+1): i+1 for i in xrange(self.NAUX)}
                R_dict_map = {'Run{:04d}'.format(i+1): i+1 for i in xrange(self.NRUN)}
                df = df.rename(columns=S_dict_map)
                df = df.rename(columns=R_dict_map)
                # print(df)
                df_sum = df.sum(axis=1)
                # print(df_sum)
                # Move 'Step' and 'Time' indexes in Series to columns
                df_sum = df_sum.reset_index(inplace=False)
                # Rename the column
                df_sum = df_sum.rename(columns={0: 'S_sum'})
                return df_sum  # Return the stacked DataFrame

            # WARN: THE CODE BELOW IS REALLY SLOW (HENCE THE HACK ABOVE)
            else:
                df = df.stack()
                # Move 'Step' and 'Time' indexes in stacked df to columns
                df.reset_index(inplace=True)
                #  Melt the DataFrame to put to stack the values for different Runs
                df = df.melt(id_vars=['Step','Time','S'])
                # Rename the aux variable labeling column to 'i' for the 'i'th aux var
                df.rename(columns={'S': 'i'}, inplace=True)
                # Rename the 'value' column to S
                df.rename(columns={'value': 'S'}, inplace=True)
                # Drop the coordinate column
                df = df.drop(columns=['Coordinate'])
                # Replace 'RunXXXX' with the integer form of XXXX
                df['Run'] = df['Run'].map(lambda s: int(s[3:]))
                # Replace 'SXX' with the integer form of XX
                df['i'] = df['i'].map(lambda s: int(s[1:]))
                return df
        else:
            if 'Time' not in df.columns:
                if not isinstance(dt, float):
                    if not quiet:
                        print('Warning: timestep not specified or isn\'t type' + \
                              ' "from contextlib import ContextDecoratorfloat".')
                        print(' --> setting dt to the dimensionless timestep')
                    dt = self.QD.DT
                columns = ['Time'] + [i+1 for i in xrange(df.shape[1])]
                data = np.concatenate((dt * df.index.values[:, np.newaxis], df.values),
                                      axis=1)
            else:
                df['Time'] *= self.QD.DT
                columns = df.columns
                data = df.values
            # Specify 'identifier' variables; all unspecified columns will be unpivoted
            df_flat = pd.DataFrame(data=data, columns=columns)  # Removes MultiIndexing
            df_flat['Step'] = df.index  # New col 'Step' via old index (before melting)
            df_flat = df_flat.melt(id_vars=['Step', 'Time'],
                                   var_name='Run',
                                   value_name=self.trj_col_map[var]).infer_objects()
            df_flat['Run'] = df_flat['Run'].astype('category')
            return df_flat

    def is_brownian_dynamics(self):
        return matches_any(self.dynamics, ['BD', 'Brownian', 'overdamped'])

    def is_langevin_dynamics(self):
        return matches_any(self.dynamics, ['LE', 'Langevin', 'Stokes', 'underdamped'])

    def is_bbo_dynamics(self):
        return matches_any(self.dynamics, ['FBBO', 'BBO'])

    def is_deterministic(self):
        return matches_any(self.dynamics, ['BBO', 'Stokes'])

    def is_euler_integrator(self):
        return matches_any(self.method, ['EM', 'Euler', 'Euler-Maruyama'])

    def is_impulse_integrator(self):
        return matches_any(self.method, ['LI', 'Impulse', 'Langevin-Impulse'])

    def fmt_time(self, time):
        if self.unitless:
            return '{:.2e}u'.format(round_to_int(time))
        elif time < 1e-12:
            return '{}fs'.format(round_to_int(time*1e15))
        elif time < 1e-9:
            return '{}ps'.format(round_to_int(time*1e12))
        elif time < 1e-6:
            return '{}ns'.format(round_to_int(time*1e9))
        elif time < 1e-3:
            return '{} microseconds'.format(round_to_int(time*1e6))
        elif time < 1e0:
            return '{}ms'.format(round_to_int(time*1e3))
        else:
            return '{}s'.format(round_to_int(time))


#################################################################################
# Function for selecting a different 64-bit keyset
#################################################################################
def select_keyset(keyset, i):
    if   keyset == 1:
        KEYS = np.array([i, 14702096011762217365, 14636641319537817119,
                         3351689648718967237], dtype=np.uint64)
    elif keyset == 2:
        KEYS = np.array([i, 14559322926498757700, 3632739549066318548,
                         10755600028138268439], dtype=np.uint64)
    elif keyset == 3:
        KEYS = np.array([i, 5533886795991833713, 4758535347328213817,
                         11818842473267243275], dtype=np.uint64)
    elif keyset == 4:
        KEYS = np.array([i, 2462089494503843304, 12584081913381477329,
                         834753877517719638], dtype=np.uint64)
    elif keyset == 5:
        KEYS = np.array([i, 2289790832165283079, 3697694132300942476,
                         15101728960709935653], dtype=np.uint64)
    return list(KEYS.astype(str))

def printlist(lst):
    print(*('{}: {}'.format(*i) for i in enumerate(lst)), sep='\n')

# def isiterable(item):
#     try:
#         iter(item)
#     except TypeError as e:
#         return False
#     else:
#         return not isinstance(item, (str, unicode))
#
# def make_iterable(item):
#     return item if isiterable(item) else [item]

class Timer():
    def __enter__(self):
        self.start = time.time()
        return self

    def __exit__(self, *args):
        self.end = time.time()
        self.interval = self.end - self.start
        print(' ({:.2f} s)'.format(self.interval))
        return False
