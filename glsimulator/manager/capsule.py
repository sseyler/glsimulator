# from __future__ import division, unicode_literals, print_function#, absolute_imports
import os, sys, time
import itertools as it
import pprint; pp = pprint.PrettyPrinter(indent=4)
# from contextlib import ContextDecorator  # Only available in Python3

from dataclasses import dataclass, field, fields, asdict
from typing import List
from dotmap import DotMap

import numpy as np
import pandas as pd
import xarray as xr

import copy
from collections import OrderedDict

from glsimulator.core.integrator import (EulerIntegratorLE, EulerIntegratorFBBO,
                                         HeunIntegratorLE, HeunIntegratorFBBO)
from glsimulator.utils.misc import (Bunch, round_to_int, module_exists, matches_any,
                                    matches_none, rename_existing_file, whatisthis,
                                    make_iterable, isiterable, to_unicode, any_in)
from glsimulator.utils.physical_quantities import PI, kBol, Navo, Re_s
from glsimulator.utils.physical_quantities import (mass_e, mass_p, gamma_s, gamma_0,
                                                   gamma_b, nu_b)
#################################################################################

basedir = '/home/sseyler/Projects/hydrobm'
SEED = 23061989
KEYSET = 1

#################################################################################
def make_named_outdir(dir, outdir, postfix=''):
    return f'{dir}/{outdir}{postfix}' if outdir else f'{dir}{postfix}'

def get_euler_integrator(system, sc, seed, keys):
    if sc.is_langevin_dynamics():
        return EulerIntegratorLE(system, sc.rng.lib, sc.rng.name, sc.QD.dt,
                                   seed=seed, keys=keys)
    elif sc.is_bbo_dynamics():
        return EulerIntegratorFBBO(system, sc.rng.lib, sc.rng.name, sc.QD.dt,
                                     seed=seed, keys=keys)

def get_heun_integrator(system, sc, seed, keys):
    if sc.is_langevin_dynamics():
        return HeunIntegratorLE(system, sc.rng.lib, sc.rng.name, sc.QD.dt,
                                  seed=seed, keys=keys)
    elif sc.is_bbo_dynamics():
        return HeunIntegratorFBBO(system, sc.rng.lib, sc.rng.name, sc.QD.dt,
                                  seed=seed, keys=keys)

@dataclass
class BaseParameters:
    def __repr__(self):
        # params = '\n'.join(f'   {f.name} ({f.metadata["unit"]}): {f.type}'
        #                         for f in fields(self))
        params = '\n'.join(f'   {f.name}: {asdict(self)[f.name]}' for f in fields(self))
        return f'{self.__class__.__name__}:\n{params}'

    def __str__(self):
        return repr(self)

@dataclass(repr=False)
class TrajFileParameters(BaseParameters):
    rpath: str
    fullpath: str
    basename: str
    fullname: str

def init_traj_file_params():
    return TrajFileParameters('', '', '', '')

FILE_PARAMETERS = {'base', 'outdir', 'sims', 'analysis', 'figs'}

@dataclass(repr=False)
class FileParameters(BaseParameters):
    base: str = field(default=basedir)
    outdir: str = f'temp'
    sims: str = f'sims'
    analysis: str = f'analysis'
    figs: str = f'figs'
    traj: TrajFileParameters = field(default_factory=init_traj_file_params)

    def _outdir(self):
        return f'/{self.outdir}' if self.outdir else f''

    def get_trjdir(self):
        return '/'.join([f'{self.base}', f'{self.sims}{self._outdir()}'])

    def get_workdir(self):
        return '/'.join([f'{self.base}', f'{self.sims}{self._outdir()}/WORK'])

    def get_analysisdir(self):
        return '/'.join([f'{self.base}', f'{self.analysis}{self._outdir()}'])

    def get_figdir(self):
        return '/'.join([f'{self.base}', f'{self.figs}{self._outdir()}'])

def default_files():
    return FileParameters()

@dataclass(repr=False)
class RNGParameters(BaseParameters):
    lib: str = field(default='randomgen')
    name: str = field(default='threefry')

def default_rng(lib='randomgen', name='threefry'):
    return RNGParameters(lib, name)

@dataclass(repr=False)
class ForceParameters(BaseParameters):
    style: str
    shape: str = field(default='none')
    #---------------------------
    # slope:      float = field(default=1.0, metadata={'unit': 'force/length'})
    tilt:       float = field(default=1.0, metadata={'unit': 'force'})
    amplitude:  float = field(default=1.0, metadata={'unit': 'force'})
    wavelength: float = field(default=1.0, metadata={'unit': 'length'})
    phase:      float = field(default=0.0, metadata={'unit': 'nd'})
    bias:       float = field(default=0.0, metadata={'unit': 'force'})
    gap_frac:   float = field(default=0.0, metadata={'unit': 'nd'})
    duration:   float = field(default=-1, metadata={'unit': 'time'})
    sharpness:  float = field(default=0.0, metadata={'unit': 'nd'})
    F1:         float = field(default=1.0, metadata={'unit': 'force/length**2'})
    n_cycles:   float = field(default=-1, metadata={'unit': 'nd'})

def default_force(style='free'):
    return ForceParameters(style)

@dataclass
class RunParameters(BaseParameters):
    tsim:  float = field(default=1.0, metadata={'unit': 'time'})
    dt:    float = field(default=1e-5, metadata={'unit': 'time'})
    dtout: float = field(default=1e-2, metadata={'unit': 'time'})
    nu0:   float = field(default=1e4, metadata={'unit': 'time'})
    #---------------------------
    n_aux:   int = field(default=1, metadata={'unit': 'nd'})
    n_pars:  int = field(default=1, metadata={'unit': 'nd'})
    n_sets:  int = field(default=1, metadata={'unit': 'nd'})
    n_dims:  int = field(default=1, metadata={'unit': 'nd'})
    n_dumps: int = field(default=5, metadata={'unit': 'nd'})
    #---------------------------
    Te_ref: float = field(default=1.0, metadata={'unit': 'temperature'})
    #---------------------------
    Te:  float = field(default=1.0, metadata={'unit': 'temperature'})
    rhf: float = field(default=1.0, metadata={'unit': 'mass/length**3'})
    rhs: float = field(default=1.0, metadata={'unit': 'mass/length**3'})
    mu:  float = field(default=1.0, metadata={'unit': 'mass/length/time'})
    R:   float = field(default=1.0, metadata={'unit': 'length'})
    #---------------------------
    X0:  float = field(default=0.0, metadata={'unit': 'length'})
    V0:  float = field(default=0.0, metadata={'unit': 'length/time'})
    #---------------------------
    rng: RNGParameters = field(default_factory=default_rng)
    force: ForceParameters = field(default_factory=default_force)

    def __repr__(self):
        field_list = [f for f in fields(self)
                        if f.name not in ('force', 'equilibration')]
        #---------------------------
        params = '\n'.join(f'   {f.name}: {asdict(self)[f.name]}' for f in field_list)
        return (f'{self.__class__.__name__}:\n{params}' + f'\n{repr(self.force)}')

def default_run():
    return RunParameters()

@dataclass(repr=False)
class EQStageParameters(RunParameters):
    '''Parameters for a single equilibration stage.'''
    stage: int = 1

def default_equilibration(style='free', n_stages=2):
    return [EQStageParameters(stage=i+1) for i in range(n_stages)]

@dataclass
class EQParameters(BaseParameters):
    '''Parameters for a set of equilibration stages.
    '''
    stages: List[EQStageParameters] = field(default_factory=default_equilibration)

    def __repr__(self):
        stages = '\n'.join(f'\n{repr(s)}' for s in self.stages)
        return f'{self.__class__.__name__}:\n{stages}'

GLSIM_PARAMETERS = {'name', 'dynamics', 'sid', 'method', 'units', 'unitless'}

@dataclass
class GLSimulationParameters(BaseParameters):
    name:     str
    dynamics: str
    #---------------------------
    sid:      str  = field(default='0000')
    method:   str  = field(default='Euler-Maruyama')
    units:    str  = field(default='thermal')
    unitless: bool = field(default=True)
    #---------------------------
    equilibration: EQParameters = field(default_factory=default_equilibration)
    production: RunParameters = field(default_factory=default_run)
    #---------------------------
    files: FileParameters = field(default_factory=default_files)

    def __repr__(self):
        field_list = [f for f in fields(self)
                        if f.name not in ('production', 'equilibration')]
        #---------------------------
        params = '\n'.join(f'   {f.name}: {asdict(self)[f.name]}' for f in field_list)
        return (f'{self.__class__.__name__}:\n{params}'
                + '\n' + repr(self.production)
                + '\n' + repr(self.equilibration)
                + '\n' + repr(self.files))

IO_PARAMETERS = {'n_step', 'n_chunk', 'ntout', 'ntdump', 'n_step_eq', 'ntout_eq',
                 'ntdump_eq'}

@dataclass
class IOParameters(BaseParameters):
    n_step: int
    n_chunk: int
    ntout: int
    ntdump: int
    #---------------------------
    n_step_eq: int
    ntout_eq: int
    ntdump_eq: int

@dataclass
class Quantities(BaseParameters):
    Te_ref: float  = field(default=-1)
    kTe_ref: float = field(default=-1)
    #---------------------------
    R: float = field(default=-1)
    Te: float = field(default=-1)
    mu: float = field(default=-1)
    rhf: float = field(default=-1)
    rhs: float = field(default=-1)
    #---------------------------
    knu: float = field(default=-1)
    Ms: float = field(default=-1)
    Me: float = field(default=-1)
    M: float = field(default=-1)
    kTe: float = field(default=-1)
    #---------------------------
    gams: float = field(default=-1)
    gam0: float = field(default=-1)
    gamb: float = field(default=-1)
    nub: float = field(default=-1)

@dataclass
class DimensionlessQuantities(BaseParameters):
    RADIUS: float = field(default=-1)
    RH: float = field(default=-1)
    BETA: float = field(default=-1)
    #---------------------------
    MASSs: float = field(default=-1)
    MASSe: float = field(default=-1)
    MASS: float = field(default=-1)
    kT: float = field(default=-1)
    #---------------------------
    X0: float = field(default=-1)
    V0: float = field(default=-1)
    #---------------------------
    tsim: float = field(default=-1)
    dt: float = field(default=-1)
    dtout: float = field(default=-1)
    #---------------------------
    GAMMAs: float = field(default=-1)
    GAMMA0: float = field(default=-1)
    GAMMAb: float = field(default=-1)
    NUb: float = field(default=-1)

@dataclass
class Trajectory():
    position: float = field(default=-1)
    RH: float = field(default=-1)
    BETA: float = field(default=-1)
    #---------------------------

# %% Module-level functions for constructing parameters-type dataclasses ******
def extract_params(d, param_class):
    return {k.split('.')[1]: d[k] for k in d.keys() if f'{param_class}.' in k}

def extract_file_params(d):
    return {k: d[k] for k in d.keys() if (k in FILE_PARAMETERS)}

def extract_force_params(d):
    return extract_params(d, 'force')

def extract_equilibration_params(d):
    return extract_params(d, 'eq')

def extract_production_params(d):
    exclude = lambda k: ((k in GLSIM_PARAMETERS) or (k in FILE_PARAMETERS) or
                         (k in IO_PARAMETERS) or any_in(['eq','force','rng'], k))
    return {k: d[k] for k in d.keys() if not exclude(k)}

def extract_glsim_params(d):
    return {k: d[k] for k in d.keys() if (k in GLSIM_PARAMETERS)}
#------------------------------------------------------------------------------
def get_FileParameters(d):
    return FileParameters(**extract_file_params(d))

def get_ForceParameters(d):
    return ForceParameters(**extract_force_params(d))

def get_EQParameters(d):
    return EQParameters(**extract_equilibration_params(d))

def get_ProductionParameters(d):
    return RunParameters(**extract_production_params(d), force=get_ForceParameters(d))
# %end ************************************************************************

#################################################################################
# Class to enable easier simulation post-processing
#################################################################################
class GLSimulatorCapsule(object):
    trj_col_map = {'pos': 'X',
                   'vel': 'V',
                   'fex': 'FE',
                   'uex': 'UE',
                   'ths': 'NS',
                   'thb': 'NB',
                   'aux': 'S'}

    def __init__(self, input_param_dict, n_workers, verbose=False):
        self.verbose = verbose
        #------------------------------------
        # self.inputs = inputs  # a GLSimulationParameters dataclass object!
        d = input_param_dict
        self.param_dict = copy.deepcopy(d)
        self.inputs = GLSimulationParameters('Test', d['dynamics'],
                                             sid=d['sid'],
                                             method=d['method'],
                                             production=get_ProductionParameters(d),
                                             equilibration=get_EQParameters(d),
                                             files=get_FileParameters(d))
        self.equilibration = self.inputs.equilibration
        self.production    = self.inputs.production
        self.files         = self.inputs.files

        self.rng   = self.production.rng
        self.force = self.production.force
        self.FQ = {}
        self.FQD = {}
        #=======================================================================
        self.name     = self.inputs.name
        self.dynamics = self.inputs.dynamics
        #------------------------------------
        self.sid = '{:04d}'.format(int(self.inputs.sid)) # min 4-char numerical string
        self.method   = self.inputs.method
        self.units    = self.inputs.units
        self.unitless = self.inputs.unitless
        #------------------------------------
        self.dt    = self.production.dt
        self.tsim  = self.production.tsim
        self.dtout = self.production.dtout
        #------------------------------------
        self.n_aux  = self.production.n_aux
        self.n_pars = self.production.n_pars
        self.n_sets = self.production.n_sets
        self.n_dims = self.production.n_dims
        self.n_workers = n_workers

        self.n_runs  = self.n_pars * self.n_workers * self.n_sets
        self.n_tasks = self.n_workers * self.n_sets
        #=======================================================================
        self.QD = DimensionlessQuantities(
            X0 = self.production.X0,
            V0 = self.production.V0
        )  # store dimensionless quantities
        #=======================================================================
        self.meta  = {}
        self.trj   = {}
        self.metafiles = {}

        #=== FORCE PARAMS ======================================================
        self.fpar_abbr = {
            'style': self.force.style,
            'shape': self.force.shape,
            'amplitude': 'A',
            'tilt': 'tilt',
            'slope': 'F0',
            'F0': 'F0',
            'F1': 'F1',
            'wavelength': 'L',
            'period': 'tau',
            'phase': 'phi',
            'bias': 'B',
            'duration': 't0',
            'gap_frac': 'gf',
            'sharpness': 'sharpness',
            'n_cycles': 'NCYC'
        }

        ### TEMPORARY FIX FOR SCREWED-UP NAMING OF TRAJ VIA FORCE PARAM ORDERING ###
        if self.force.style == 'space-periodic':
            temp_fparam_ordered = ['style', 'shape', 'amplitude', 'tilt', 'wavelength',
                                   'phase', 'bias', 'duration', 'gap_frac', 'sharpness']
        elif self.force.style == 'time-periodic':
            temp_fparam_ordered = ['style', 'shape', 'duration', 'amplitude',
                                   'period', 'n_cycles']
        elif matches_any(self.force.style, ['potential']):
            if matches_any(self.force.shape, ['harmonic']):
                temp_fparam_ordered = ['style', 'shape', 'F0', 'F1', 'duration']
            elif matches_any(self.force.shape, ['linear']):
                temp_fparam_ordered = ['style', 'shape', 'F0', 'duration']
            else:
                print('WARNING: problem in SimCapsule constructor with potential force')
        else:
            print('WARNING: force style wasn\'t "space-periodic" or "time-periodic"...')

        #################################################################################
        params_force = '{}-{}'.format(self.force.style, self.force.shape)

        for f in fields(self.force):
            if f.name not in ('force', 'equilibration', 'style', 'shape'):
                name = f.name
                val = asdict(self.force)[f.name]
                self.FQ[name] = val
                # print('Adding parameter \"{}\" with value \"{}\" and id \"{}\" to
                # self.FQ'.format(name, self.FQ[name], id(self.FQ[name])))
                #-----------------------------
                if isinstance(val, float):
                    params_force += '/{}={:.1e}'.format(self.fpar_abbr[name], val)
                else:
                    params_force += '/{}={}'.format(self.fpar_abbr[name], val)

        #=== SIMULATION PARAMS =================================================
        params_sim = 'Te={:.1e}_dt={}'.format(self.production.Te, self.fmt_time(self.dt))
        if self.is_bbo_dynamics():
            self.nu0 = self.production.nu0
            params_sim += '_nu0={:.1e}_NAUX={}'.format(self.nu0, self.n_aux)
        else:
            self.nu0 = 1
            self.n_aux = 1
            if self.is_langevin_dynamics():
                self.dynamics = 'Langevin'
            elif self.is_brownian_dynamics():
                self.dynamics = 'Brownian'

        #=======================================================================
        if verbose:
            print('self.dynamics', self.dynamics, 'self.sid', self.sid)
            print('params_force', params_force)
            print('params_sim', params_sim)
        if verbose:
            print('\nForce parameters:')
            pprint.pprint(self.force.toDict())

        # NOTE: trj path used to have self.sid as bottommost dir; sid now in filename
        self.files.traj.rpath = '/'.join([params_force, self.dynamics, params_sim])
        self.files.traj.fullpath = '/'.join([self.files.get_workdir(),
                                             self.files.traj.rpath])
        #------------------------------------
        self.files.traj.basename = '_'.join([self.method, self.rng.name])
        self.files.traj.fullname = '/'.join([self.files.traj.fullpath,
                                             self.files.traj.basename])

        #=======================================================================
        if self.is_deterministic():
            print('NOTE: deterministic dynamics selected. Setting Te to 0.')
            # self.Q.Te = 0.0
            self.equilibrate = False
        else:
            self.equilibrate = True
        #=======================================================================

    def add_simulation(self, sim):
        self.sim = sim

    def check_file_exists(self, fullname):
        return os.path.exists(fullname)

    def has_existing_data(self, ext='h5'):
        fname_phys = '.'.join([self.files.traj.fullname, 'phys', ext])
        fname_dims = '.'.join([self.files.traj.fullname, 'dims', ext])
        return os.path.exists(fname_phys) and os.path.exists(fname_dims)

    def setup(self, force_eff_mass=False, n_chunk=10, T_eq_fracs=1.0, nstep_eq_frac=0.1,
              verbose=True):
        self.T_eq_fracs = make_iterable(T_eq_fracs)
        self.nstep_eq_frac = nstep_eq_frac
        self.force_eff_mass = force_eff_mass
        #-----------------------------------------------------------
        self.populate_physical_params()
        self.populate_force_params()
        self.non_dimensionalize()
        self.set_run_params(n_chunk=n_chunk, verbose=verbose)
        self.store_physical_metadata(verbose=verbose)
        self.store_simulation_metadata(verbose=verbose)

    def load(self, force_params=None):
        '''Alias for load_physical_metadata() and load_simulation_metadata().
        '''
        self.load_physical_metadata()
        self.load_simulation_metadata()
        self.load_force_metadata(force_params)

    def populate_physical_params(self):
        # WARNING: This code is fucky. It works, but you better know what you're doing..
        Te_ref = self.production.Te_ref
        Te  = self.production.Te
        mu  = self.production.mu
        rhf = self.production.rhf
        rhs = self.production.rhs
        R   = self.production.R
        #------------------------------------
        kTe_ref = Te_ref if self.unitless else kBol*Te_ref
        kTe = Te if self.unitless else kBol*Te
        knu = mu/rhf
        Me  = mass_e(R, rhf, rhs)
        Ms  = mass_p(R, rhs)
        if self.is_bbo_dynamics() or self.force_eff_mass:
            dyn_flag = 1
            M = Me
            if self.force_eff_mass:
                print('NOTE: this simulation is set up to use the effective' + \
                      ' mass instead of the particle mass.')
        elif self.is_langevin_dynamics() or self.is_brownian_dynamics():
            dyn_flag = 0
            M = Ms
        else:
            print('Error: the selected dynamics "{}" are unknown.'
                  ' Exiting.'.format(self.dynamics))
            sys.exit(-1)

        if self.units == 'density':
            M = Me
        #------------------------------------
        # if self.force_eff_mass:
        #     self._set_characteristic_scales(R, Me, rhs, rhf, Te, mu)
        # else:
        #     self._set_characteristic_scales(R, M, rhs, rhf, Te, mu)

        if Te == 0:
            print('NOTE: zero-temperature run. Using Te = 1 for non-dimensionalization')
            self._set_characteristic_scales(R, M, rhs, rhf, Te_ref, mu)
        else:
            self._set_characteristic_scales(R, M, rhs, rhf, Te_ref, mu)

        #------------------------------------
        self.Q  = Quantities(
            kTe_ref = kTe_ref,
            Te_ref = Te_ref,
            R   = self.production.R,
            Te  = self.production.Te,
            mu  = self.production.mu,
            rhf = self.production.rhf,
            rhs = self.production.rhs,
            knu = knu,
            Ms = Ms,
            Me = Me,
            M = M,
            kTe = kTe,
            gams = gamma_s(R, knu, rhf, rhs, M),
            gam0 = gamma_0(R, knu, rhf, rhs, M, self.n_aux, self.nu0, self.Tc),
            gamb = dyn_flag*gamma_b(R, knu, rhf, rhs, M, self.n_aux, self.nu0, self.Tc),
            nub  = dyn_flag*nu_b(self.n_aux, self.nu0, self.Tc)
        )  # store unitful quantities
        # print(f'Here is the mf value of Te = {self.Q.Te}')
        # print(f'Here is the mf value of kTe = {self.Q.kTe}')
        # print(f'Here is the mf value of Te = {self.Q.Te_ref}')
        # print(f'Here is the mf value of kTe = {self.Q.kTe_ref}')

    def _set_characteristic_scales(self, R, M, rhs, rhf, Te, mu):
        # WARNING: This code is fucky. It works, but you better know what you're doing..
        RH_o_BETA = (2*rhs + rhf) / 9.0
        RH   = rhs/rhf if rhf else np.inf
        BETA = 9 / (2*rhs/rhf + 1) if rhf else 0

        if self.units == 'arminski':
            self.Tc = (RH_o_BETA*R**2) / mu
            self.Vc = 1.0
            self.Ec = 6*PI*R*mu*(self.Tc*self.Vc**2)
            #------------------------------------
            self.Lc = self.Vc*self.Tc
            self.Fc = 6*PI*R*mu*(self.Vc)  # self.Ec/self.Lc
            self.Mc = self.Ec/self.Vc**2
            self.Pc = self.Ec/self.Tc
        elif self.units == 'thermal':
            self.Tc = (RH_o_BETA*R**2) / mu
            self.Mc = mass_e(R, rhf, rhs)
            self.Ec = Te
            self.Vc = ( self.Ec/self.Mc )**0.5
            self.Lc = self.Vc*self.Tc
            self.Fc = self.Ec/self.Lc
            self.Pc = self.Ec/self.Tc
        elif self.units == 'thermal2':
            self.Tc = (RH_o_BETA*R**2) / mu
            self.Mc = mass_e(R, rhf, rhs)
            self.Ec = 1.0
            self.Vc = ( self.Ec/self.Mc )**0.5
            self.Lc = self.Vc*self.Tc
            self.Fc = self.Ec/self.Lc
            self.Pc = self.Ec/self.Tc
        elif self.units == 'density':
            self.Tc = (RH_o_BETA*R**2) / mu
            self.Vc = ( 3.0 / (rhf/rhs + 2) )**0.5  # ( 1.5*rhs / (rhs + 0.5*rhf) )**0.5
            self.Ec = 6*PI*R*mu*(self.Tc*self.Vc**2)
            #------------------------------------
            self.Lc = self.Vc*self.Tc
            self.Fc = self.Ec/self.Lc
            self.Mc = self.Ec/self.Vc**2
            self.Pc = self.Ec/self.Tc
        else:  # like 'thermal' except uses input param M instead of effective mass
            self.Tc = (RH_o_BETA*R**2) / mu
            self.Ec = 1.0
            self.Vc = (self.Ec/M)**0.5
            #------------------------------------
            self.Lc = self.Vc*self.Tc
            self.Fc = self.Ec/self.Lc
            self.Mc = self.Ec/self.Vc**2
            self.Pc = self.Ec/self.Tc
        #----------------------------------------
        self.QD.RH = RH
        self.QD.BETA = BETA

    def populate_force_params(self):
        DURA_STEP = 0
        PERI_STEP = 0
        NCYC_TOTAL = 0
        f_style = self.force.style
        f_shape = self.force.shape

        if f_style == 'free':
            self.FQ.update({'amplitude': 0, 'duration': 0,
                                 'period': 0, 'wavelength': 0})
            self.FQD.update({'DURA_STEP': 0, 'PERI_STEP': 0,
                                  'NCYC_TOTAL': 0})
        elif f_style == 'potential':
            # self.FQ.update({'F0': self.force.F0, 'F1': self.force.F1})
            self.FQD.update({'DURA_STEP': DURA_STEP})
        elif matches_any(f_style, ['time-periodic']):
            DURA_STEP = round_to_int(self.force.duration / self.dt)
            PERI_STEP = round_to_int(self.force.period / self.dt)
            NCYC_TOTAL = self.FQ['n_cycles']
            self.FQ.update({'amplitude': self.force.amplitude,
                                 'duration': self.force.duration,
                                 'period': self.force.period})
            self.FQD.update({'DURA_STEP': DURA_STEP, 'PERI_STEP': PERI_STEP,
                                  'NCYC_TOTAL': NCYC_TOTAL})
        elif matches_any(f_style, ['space-periodic']):
            if matches_any(f_shape, ['butterworth', 'tilted', 'gapped',
                                     'washboard', 'modified-washboard']):
                DURA_STEP = round_to_int(self.force.duration / self.dt)
                self.FQD.update({'DURA_STEP': DURA_STEP})
            else:
                print('ERROR: Undefined force style in \"populate_force_params()\"!')

    def non_dimensionalize(self):
        self.QD.tsim   = self.tsim   / self.Tc
        self.QD.dt     = self.dt     / self.Tc
        self.QD.dtout  = self.dtout  / self.Tc
        self.QD.MASSs  = self.Q.Ms   / self.Mc
        self.QD.MASSe  = self.Q.Me   / self.Mc
        self.QD.MASS   = self.Q.M    / self.Mc
        self.QD.RADIUS = self.Q.R    / self.Lc
        self.QD.GAMMAs = self.Q.gams * self.Tc
        self.QD.GAMMA0 = self.Q.gam0 * self.Tc
        self.QD.GAMMAb = self.Q.gamb * self.Tc
        self.QD.NUb    = self.Q.nub  * self.Tc
        self.QD.kT     = self.Q.kTe  / self.Ec
        #------------------------------------
        for par, value in copy.deepcopy(self.FQ).items():
            if matches_none(par, ['style', 'shape', 'n_cycles', 'Q', 'QD']):
                par_abbr = self.fpar_abbr[par]
                value_nd = None
                if par == 'amplitude':
                    value_nd = value / self.Fc
                elif par == 'tilt':
                    value_nd = value / self.Fc
                elif par == 'wavelength':
                    value_nd = value / self.Lc
                elif par == 'bias':
                    value_nd = value / self.Fc
                elif par == 'phase':
                    # value_nd = value / self.Lc
                    if value != 0:
                        raise NotImplementedError
                elif par == 'duration':
                    value_nd = value / self.Tc
                elif par == 'period':
                    value_nd = value / self.Tc
                elif par == 'F0':
                    value_nd = value / self.Fc
                elif par == 'F1':
                    value_nd = value / (self.Fc / self.Lc)
                elif par == 'sharpness':
                    value_nd = value
                #------------------------------------------
                # self.FQ[par] = value
                if isinstance(value_nd, float):
                    self.FQD[par_abbr] = value_nd

    def set_run_params(self, n_chunk=10, verbose=False):
        #--- output frame every ntout steps ---------------
        ntout       = round_to_int(self.QD.dtout / self.QD.dt)
        n_step_mult = n_chunk * ntout
        n_step      = int(np.around(self.QD.tsim / self.QD.dt, decimals=-3))
        #------------------------------------
        n_step_adj = n_step + n_step_mult - (n_step % n_step_mult)  # make divisible
        ntdump = round_to_int(n_step_adj / n_chunk)
        self.io = IOParameters(
            n_step = n_step_adj,
            ntdump = ntdump,
            ntout = ntout,
            n_chunk = n_chunk,
            n_step_eq = round_to_int(n_step_adj * self.nstep_eq_frac),
            ntout_eq = ntout,
            ntdump_eq = ntdump
        )
        #------------------------------------
        if (self.io.ntdump % self.io.ntout) != 0:
            print('WARNING: ntdump is not a divisor of ntout')
            sys.exit(-1)
        if verbose:
            print('tsim:', self.QD.tsim)
            print('ntout:', self.io.ntout)
            print('n_step:', self.io.n_step)
            print('n_chunk:', self.io.n_chunk)
            print('ntdump:', self.io.ntdump)
            print('dump size:', self.io.ntdump//self.io.ntout)
            print('n_step_eq:', self.io.n_step_eq)
            print('ntout_eq:', self.io.ntout_eq)

    def store_physical_metadata(self, postfix='phys', ext='h5', verbose=False):
        #=======================================================================
        fname = '.'.join([self.files.traj.basename, postfix, ext])
        self.metafiles[postfix] = fname
        if not os.path.exists(self.files.traj.fullpath):
            os.makedirs(self.files.traj.fullpath)

        fullname = '/'.join([self.files.traj.fullpath, fname])
        self.metafiles['phys'] = fullname
        if self.check_file_exists(fullname):
            rename_existing_file(fullname)
        #=======================================================================
        Q, QD = self.Q, self.QD
        fields = DotMap()
        data = DotMap()
        #------------------------------------
        fields.phys = ['R', 'M', 'Me', 'Ms', 'Te', 'rhs', 'rhf', 'mu', 'gamS', 'gam0']
        fields.char  = ['Lc', 'Tc', 'Ec', 'Mc', 'Vc', 'Fc', 'Pc']
        fields.diml = ['beta', 'rhs_rel', 'Re_s']
        #------------------------------------
        data.phys = [Q.R, Q.M, Q.Me, Q.Ms, Q.Te, Q.rhs, Q.rhf, Q.mu, Q.gams, Q.gam0]
        data.char = [self.Lc, self.Tc, self.Ec, self.Mc, self.Vc, self.Fc, self.Pc]
        data.diml = [QD.BETA, QD.RH, Re_s(self.Vc, Q.R, Q.mu/Q.rhf)]
        #=======================================================================
        index = self.generate_metadata_index(fancy=False)
        d_phys = dict(zip(fields.phys, data.phys))
        d_char = dict(zip(fields.char, data.char))
        d_diml = dict(zip(fields.diml, data.diml))
        df_phys = pd.DataFrame(data=d_phys, columns=fields.phys, index=index)
        df_char = pd.DataFrame(data=d_char, columns=fields.char, index=index)
        df_diml = pd.DataFrame(data=d_diml, columns=fields.diml, index=index)
        df_phys.to_hdf(fullname, 'phys', mode='w', format='table', append=True)
        df_char.to_hdf(fullname, 'char', mode='a', format='table', append=True)
        df_diml.to_hdf(fullname, 'dimless', mode='a', format='table', append=True)
        #------------------------------------
        d_fex  = {self.fpar_abbr[k]:v for k, v in asdict(self.force).items()
                    if not any(k in s for s in ['Q','QD','style','shape'])}
        df_fex = pd.DataFrame(data=d_fex, index=index)
        df_fex.to_hdf(fullname, 'fex', mode='a', format='table', append=True)

        #------------------------------------
        if verbose:
            from IPython.display import display
            pd.set_option('display.max_rows', 25)
            pd.options.display.float_format = '{:9.2e}'.format
            display(df_phys)
            display(df_fex)
            display(df_char)
            display(df_diml)
            #------------------------------------
            return df_phys, df_fex, df_char, df_diml

    def store_simulation_metadata(self, postfix='dims', ext='h5', seed=SEED,
                                  keyset=KEYSET, verbose=False):
        #=======================================================================
        fname = '.'.join([self.files.traj.basename, postfix, ext])
        self.metafiles[postfix] = fname
        if not os.path.exists(self.files.traj.fullpath):
            os.makedirs(self.files.traj.fullpath)

        fullname = '/'.join([self.files.traj.fullpath, fname])
        self.metafiles['dims'] = fullname
        if self.check_file_exists(fullname):
            rename_existing_file(fullname)
        #=======================================================================
        fields = DotMap()
        data = DotMap()
        index = self.generate_metadata_index(fancy=False)
        #=======================================================================
        fields.dims.run = ['n_aux', 'n_dim', 'n_par', 'n_steps',  'ntout',
                           'ntdump', 'n_steps_eq', 'ntout_eq', 'ntdump_eq']
        data.dims.run = [self.n_aux, self.n_dims, self.n_pars, self.io.n_step,
                         self.io.ntout, self.io.ntdump, self.io.n_step_eq,
                         self.io.ntout_eq, self.io.ntdump_eq]
        d_run = dict(zip(fields.dims.run, data.dims.run))
        df_run = pd.DataFrame(data=d_run, columns=fields.dims.run, index=index)
        df_run.to_hdf(fullname, 'run', mode='w', format='table', append=True)
        #------------------------------------
        fields.dims.batch = ['n_runs', 'n_sets', 'n_tasks', 'n_work', 'seed', 'keyset']
        data.dims.batch = [self.n_runs, self.n_sets, self.n_tasks, self.n_workers, seed,
                           keyset]
        d_batch = dict(zip(fields.dims.batch, data.dims.batch))
        df_batch = pd.DataFrame(data=d_batch, columns=fields.dims.batch, index=index)
        df_batch.to_hdf(fullname, 'batch', mode='a', format='table', append=True)
        #------------------------------------
        if verbose:
            from IPython.display import display
            pd.set_option('display.max_rows', 25)
            pd.options.display.float_format = '{:9.2e}'.format
            display(df_run)
            display(df_batch)
            #------------------------------------
            return df_run, df_batch

    def generate_metadata_index(self, fancy=False):
        #=======================================================================
        def elements_to_str(data):
            for datum in data:
                if isinstance(datum, (str, int, bool)):
                    if isinstance(datum, basestring):
                        datum = datum.capitalize()
                    yield '{}'.format(datum)
                elif isinstance(datum, float):
                    yield '{:.1e}'.format(datum)
        #------------------------------------
        def encode_elements(data):
            for datum in data:
                yield u'{}'.format(datum).encode('utf-8')
        #=======================================================================
        if not fancy:
            return [0]
        id_names = ['Dynamics', 'Method', 'Units',
                    'dt', 'nu0', 'NP', 'ND']
        id_values = [self.dynamics, self.method, not self.unitless,
                     self.dt, self.nu0, self.n_pars, self.n_dims]
        # for name, value in self.force.items():
        #     if not any(name in s for s in ['Q','QD']):
        #         id_names.append('{}'.format(name.capitalize()))
        #         id_values.append(value)

        f_items = {k:v for k, v in self.force.items()
                    if not matches_any(k, ['Q','QD','style'])}
        id_names  += ['{}'.format(k) for k in f_items.keys()]
        id_values += f_items.values()
        #------------------------------------
        id_names = encode_elements(id_names)
        id_values = tuple([d for d in elements_to_str(id_values)])
        return pd.MultiIndex.from_tuples([id_values], names=id_names)

    def load_physical_metadata(self, postfix='phys', ext='h5', col='Units'):
        self.metafiles['phys'] = '.'.join([self.files.traj.fullname, postfix, ext])
        self.meta['phys'] = pd.read_hdf(self.metafiles['phys'], 'phys')
        self.meta['char'] = pd.read_hdf(self.metafiles['phys'], 'char')
        self.meta['diml'] = pd.read_hdf(self.metafiles['phys'], 'dimless')
        #-----------------------------------------------------------------
        self.R    = self.extract_param(self.meta['phys'], 'R')
        self.M    = self.extract_param(self.meta['phys'], 'M')
        self.Me   = self.extract_param(self.meta['phys'], 'Me')
        self.Ms   = self.extract_param(self.meta['phys'], 'Ms')
        self.Te   = self.extract_param(self.meta['phys'], 'Te')
        self.rhs  = self.extract_param(self.meta['phys'], 'rhs')
        self.rhf  = self.extract_param(self.meta['phys'], 'rhf')
        self.mu   = self.extract_param(self.meta['phys'], 'mu')
        self.gamS = self.extract_param(self.meta['phys'], 'gamS')
        self.gam0 = self.extract_param(self.meta['phys'], 'gam0')
        #-----------------------------------------------------------------
        self.Lc = self.extract_param(self.meta['char'], 'Lc')
        self.Tc = self.extract_param(self.meta['char'], 'Tc')
        self.Ec = self.extract_param(self.meta['char'], 'Ec')
        self.Mc = self.extract_param(self.meta['char'], 'Mc')
        self.Vc = self.extract_param(self.meta['char'], 'Vc')
        self.Fc = self.extract_param(self.meta['char'], 'Fc')
        self.Pc = self.extract_param(self.meta['char'], 'Pc')
        #-----------------------------------------------------------------
        self.beta    = self.extract_param(self.meta['diml'], 'beta')
        self.rhs_rel = self.extract_param(self.meta['diml'], 'rhs_rel')
        self.Re_s    = self.extract_param(self.meta['diml'], 'Re_s')

    def load_simulation_metadata(self, postfix='dims', ext='h5'):
        self.metafiles['dims'] = '.'.join([self.files.traj.fullname, postfix, ext])
        self.meta['run']   = pd.read_hdf(self.metafiles['dims'], 'run')
        self.meta['batch'] = pd.read_hdf(self.metafiles['dims'], 'batch')
        #-----------------------------------------------------------------
        self.n_aux      = self.extract_param(self.meta['run'], 'n_aux')
        self.n_dims     = self.extract_param(self.meta['run'], 'n_dim')
        self.n_pars     = self.extract_param(self.meta['run'], 'n_par')
        self.n_steps    = self.extract_param(self.meta['run'], 'n_steps')
        self.ntout      = self.extract_param(self.meta['run'], 'ntout')
        self.ntdump     = self.extract_param(self.meta['run'], 'ntdump')
        self.n_steps_eq = self.extract_param(self.meta['run'], 'n_steps_eq')
        self.ntout_eq   = self.extract_param(self.meta['run'], 'ntout_eq')
        self.ntdump_eq  = self.extract_param(self.meta['run'], 'ntdump_eq')
        #-----------------------------------------------------------------
        self.n_runs  = self.extract_param(self.meta['batch'], 'n_runs')
        self.n_sets  = self.extract_param(self.meta['batch'], 'n_sets')
        self.n_tasks = self.extract_param(self.meta['batch'], 'n_tasks')
        self.n_work  = self.extract_param(self.meta['batch'], 'n_work')
        self.seed    = self.extract_param(self.meta['batch'], 'seed')
        self.keyset  = self.extract_param(self.meta['batch'], 'keyset')
        #-----------------------------------------------------------------
        self.tsim = self.n_steps*self.dt
        self.QD.tsim = self.tsim / self.Tc
        self.QD.dt = self.dt / self.Tc

    def load_force_metadata(self, force_params=None, postfix='phys', ext='h5',
                            col='Units'):
        self.meta['fex'] = pd.read_hdf(self.metafiles['phys'], 'fex')
        #-----------------------------------------------------------------
        try:
            for name in force_params:
                self.force[name] = self.extract_param(self.meta['fex'], name)
        except e:
            print('Error: {}\n  >>> No force parameters were loaded.'.format(e))

    def extract_param(self, df, param):
        return df[param][0]

    def load_full_trajectories(self, var_list, ext='h5', reindex=True, long=True,
                               combine=True, quiet=True, shrink=True, hack=False):
        '''Will probably be deprecated at some point.

        This old method was used for dealing with trajectory data stored as pandas
        `DataFrame`s. This approach also tended to rely on conversion to the long rather
        than wide format, which not only is relatively memory inefficient, calculations
        done on the trajectories require groupby operations, which seem to be relatively
        expensive. The new approach is to leverage xarray Datasets, which is handled by
        the new `GLSimulatorCapsule.load_traj_dataset` method (and helpers).
        '''
        for v in var_list:
            df = self._df_cat([self.load_traj(v,i,ext) for i in range(self.n_tasks)])
            #--------------------------------
            if shrink:
                for c in df.columns:
                    df[c] = pd.to_numeric(df[c], downcast='float')
            if reindex:
                print('  >>> Re-indexing trajectories...', end='')
                with Timer() as timer:
                    df = self.reindex_parallel_trj(df, v)
            if long:
                print('  >>> Converting trajectory DataFrames to long format...', end='')
                with Timer() as timer:
                    if v == 'aux':
                        df = self.wide_to_long(df, var=v, quiet=quiet, hack=hack)
                    else:
                        df = self.wide_to_long(df, var=v, quiet=quiet)
                if shrink:
                    df['Step'] = pd.to_numeric(df['Step'], downcast='unsigned')
                    df['Time'] = pd.to_numeric(df['Time'], downcast='float')
            self.trj[v] = copy.deepcopy(df)
        #------------------------------------
        if combine and long:
            print('  >>> Consolidating variables in single DataFrame...', end='')
            with Timer() as timer:
                for i, var in enumerate(var_list):
                    col = self.trj_col_map[var]
                    if i == 0:
                        df = copy.deepcopy(self.trj[var])
                    else:
                        df[col] = self.trj[var][col]
                return copy.deepcopy(df)
        return {v: self.trj[v] for v in var_list}

    def load_traj_dataset(self, var_list, ext='h5', shrink=True, dtype='float32',
                          ts_idx=slice(None), p_idx=slice(None), verbose=True):
        '''Converts a dictionary of trajectories into an `xarray.Dataset()`.'''
        kwargs = {'ext': ext, 'shrink': shrink, 'dtype': dtype, 'verbose': verbose}
        return self.trajs_to_dataset(
                    self.load_multi_traj(var_list, **kwargs), ts_idx=ts_idx, p_idx=p_idx)

    def trajs_to_dataset(self, traj_dict, ts_idx=slice(None), p_idx=slice(None)):
        return self._niceify_dataset( xr.Dataset(traj_dict), ts_idx=ts_idx, p_idx=p_idx )

    def load_multi_traj(self, var_list, **kwargs):
        '''Returns a dictionary of trajectories for each variable in `var_list`.

          This method currently reads in data using the `GLSimulatorCapsule.load_traj`
          method, which somewhat relies on the trajectory data on disk's being stored
          as h5 files that can be read (and decompressed) using the pandas `load_hdf`
          function.
        '''
        return {self.trj_col_map[v]: self._df_cat(
                  [self.load_traj(v, i, **kwargs) for i in range(self.n_tasks)])
                for v in var_list}

    @staticmethod
    def _df_cat(df_list):
        '''Take a list of pandas DataFrames and return a combined one'''
        return pd.concat(df_list, axis=1)

    def load_traj(self, var, task_id=0, ext='h5', shrink=True, dtype='float32',
                  verbose=True):
        if verbose:
            print(f'{srp(2)}>>> Loading {var} trajectory...', end=srp(8))
            with Timer(verbose=False) as timer:
                df = pd.read_hdf(self.get_traj_filepath(var, task_id, ext), var)
            print(f' ({timer.elapsed:.2f} s)', end='\n')
        else:
            df = pd.read_hdf(self.get_traj_filepath(var, task_id, ext), var)
        #------------------------------------
        if shrink:
            mem_raw = 1e-6 * sys.getsizeof(df)
            print(f'{srp(6)}* Shrinking memory footprint...', end=srp(2))
            #--------------------------------
            with Timer(verbose=False) as timer:
                df = df.astype(dtype)
            print(f' ({timer.elapsed:.2f} s)', end='\n')
            #--------------------------------
            mem_shrink = 1e-6 * sys.getsizeof(df)
            mem_saved_rel = int(100 * (mem_raw - mem_shrink) / mem_raw)
            print(f'{srp(6)}* {mem_raw:.0f} MB --> {mem_shrink:.0f} MB'
                  f' ({mem_saved_rel:d}% saved)', end='\n')
        return df

    def load_traj_shrink(self, var, task_id=0, ext='h5', shrink=True, dtype='float32'):
        print(f'{srp(2)}>>> Loading {var} trajectory...', end='\n')
        print(f'{srp(2)}>>> Shrinking memory footprint...', end=srp(2))
        with Timer(verbose=True) as timer:
            return pd.read_hdf(self.get_traj_filepath(var,task_id,ext),var).astype(dtype)

    def load_traj_old(self, var, task_id=0, ext='h5', shrink=True, dtype='float'):
        print(f'{srp(2)}>>> Loading {var} trajectory...', end=srp(8))
        with Timer(verbose=False) as timer:
            df = pd.read_hdf(self.get_traj_filepath(var, task_id, ext), var)
        print(f' ({timer.elapsed:.2f} s)', end='\n')
        #------------------------------------
        if shrink:
            mem_raw = 1e-6 * sys.getsizeof(df)
            print(f'{srp(6)}* Shrinking memory footprint...', end=srp(2))
            #--------------------------------
            with Timer(verbose=False) as timer:
                for c in df.columns:
                    df[c] = pd.to_numeric(df[c], downcast=dtype)
            print(f' ({timer.elapsed:.2f} s)', end='\n')
            #--------------------------------
            mem_shrink = 1e-6 * sys.getsizeof(df)
            mem_saved_rel = int(100 * (mem_raw - mem_shrink) / mem_raw)
            print(f'{srp(6)}* {mem_raw:.0f} MB --> {mem_shrink:.0f} MB'
                  f' ({mem_saved_rel:d}% saved)', end='\n')
        return df

    def _niceify_dataset(self, ds, **kwargs):
        ts_idx  = kwargs.pop('ts_idx', slice(None))
        p_idx   = kwargs.pop('p_idx',  slice(None))
        ts_old  = kwargs.pop('ts_old', 'Step')
        ts_new  = kwargs.pop('ts_new', 'ts')
        par_old = kwargs.pop('par_old', 'dim_1')
        par_new = kwargs.pop('par_new', 'p')
        time    = kwargs.pop('time', 't')
        #-----------------------------------
        dt = self.QD.dt
        ds.coords.update({time: ds[ts_old] * dt})  # add non-dimension coord for time
        #--- add some attributes -----------
        ds.attrs['name'] = self.name
        ds.attrs['dynamics'] = self.dynamics
        ds.attrs['sid'] = self.sid
        ds.attrs['method'] = self.method
        ds.attrs['temperature'] = self.Q.Te  # careful! Acct for Boltz const. + dimension
        ds.attrs['timestep'] = dt  # dimensionless timestep
        ds.attrs['param_dict'] = self.param_dict
        ds.attrs['ts_idx'] = ts_idx
        ds.attrs['p_idx']  = p_idx
        ds.attrs['force'] = self.force  # ForceParameters object (not sure prefer dict)
        ds.attrs['capsule'] = self
        #-----------------------------------
        return ds.rename({ts_old: ts_new, par_old: par_new}).isel(ts=ts_idx, p=p_idx)

    def reindex_parallel_trj(self, df, var):
        runs = ['Run{:04d}'.format(r+1) for r in range(self.n_runs)]
        if self.n_dims == 1:
            coords = ['x']
        elif self.n_dims == 2:
            coords = ['x', 'y']
        elif self.n_dims == 3:
            coords = ['x', 'y', 'z']
        if (var == 'pos' or var == 'vel' or var == 'fex'
                or var == 'ths' or var == 'thb'):
            names = ['Run', 'Coordinate']
            headr = list(it.product(runs, coords))
        elif var == 'aux':
            names = ['Run', 'Coordinate', 'S']
            index = ['S{:02d}'.format(i+1) for i in range(self.n_aux)]
            headr = list(it.product(runs, coords, index))
        elif var == 'noi':
            names = ['Run', 'Coordinate', 'G']
            index = ['G{:02d}'.format(i+1) for i in range(self.n_aux)]
            headr = list(it.product(runs, coords, index))
        new_columns = pd.MultiIndex.from_tuples(headr, names=names)
        df.columns = new_columns
        return df

    def wide_to_long(self, df, var='pos', dt=None, quiet=False, hack=False):
        if var == 'aux':
            dt = self.QD.dt
            # Insert a 'Time' column before all aux variable values
            df.insert(0, 'Time', dt * df.index.values)
            # Append the 'Time' column to the index (which has 'Step' already)
            df = df.set_index('Time', append=True)

            # WARN: TEMPORARY HACK TO GET AUX VAR CALCS FAST; NEED FULL REFACTOR
            if hack:
                S_dict_map = {'S{:02d}'.format(i+1): i+1 for i in range(self.n_aux)}
                R_dict_map = {'Run{:04d}'.format(i+1): i+1 for i in range(self.n_runs)}
                df = df.rename(columns=S_dict_map)
                df = df.rename(columns=R_dict_map)
                # print(df)
                df_sum = df.sum(axis=1)
                # print(df_sum)
                # Move 'Step' and 'Time' indexes in Series to columns
                df_sum = df_sum.reset_index(inplace=False)
                # Rename the column
                df_sum = df_sum.rename(columns={0: 'S_sum'})
                return df_sum  # Return the stacked DataFrame

            # WARN: THE CODE BELOW IS REALLY SLOW (HENCE THE HACK ABOVE)
            else:
                df = df.stack()
                # Move 'Step' and 'Time' indexes in stacked df to columns
                df.reset_index(inplace=True)
                #  Melt the DataFrame to put to stack the values for different Runs
                df = df.melt(id_vars=['Step','Time','S'])
                # Rename the aux variable labeling column to 'i' for the 'i'th aux var
                df.rename(columns={'S': 'i'}, inplace=True)
                # Rename the 'value' column to S
                df.rename(columns={'value': 'S'}, inplace=True)
                # Drop the coordinate column
                df = df.drop(columns=['Coordinate'])
                # Replace 'RunXXXX' with the integer form of XXXX
                df['Run'] = df['Run'].map(lambda s: int(s[3:]))
                # Replace 'SXX' with the integer form of XX
                df['i'] = df['i'].map(lambda s: int(s[1:]))
                return df
        else:
            if 'Time' not in df.columns:
                if not isinstance(dt, float):
                    if not quiet:
                        print('Warning: timestep not specified or isn\'t type' + \
                              ' "from contextlib import ContextDecoratorfloat".')
                        print(' --> setting dt to the dimensionless timestep')
                    dt = self.QD.dt
                columns = ['Time'] + [i+1 for i in range(df.shape[1])]
                data = np.concatenate((dt * df.index.values[:, np.newaxis], df.values),
                                      axis=1)
            else:
                df['Time'] *= self.QD.dt
                columns = df.columns
                data = df.values
            # Specify 'identifier' variables; all unspecified columns will be unpivoted
            df_flat = pd.DataFrame(data=data, columns=columns)  # Removes MultiIndexing
            df_flat['Step'] = df.index  # New col 'Step' via old index (before melting)
            df_flat = df_flat.melt(id_vars=['Step', 'Time'],
                                   var_name='Run',
                                   value_name=self.trj_col_map[var]).infer_objects()
            df_flat['Run'] = df_flat['Run'].astype('category')
            return df_flat

    def get_traj_filepath(self, var, task_id=0, ext='h5'):
        return '.'.join([self.files.traj.fullname, f'{task_id:04d}', var, ext])

    def shrink_traj(self, df, var):
        # Consider using pd.to_numeric(arg, downcast={'integer','unsigned','float'})
        uint32max = (2**32 - 2)
        VAR_MAP = {'pos': 'X', 'vel': 'V', 'fex': 'F'}
        #---------------------------------------------
        step_type = 'uint32' if df['Step'].max() < uint32max else 'category'
        new_types_dict = {'Step': step_type, 'Time': 'float32'}
        # new_types_dict.update({VAR_MAP[var]: 'float32' for var in var_list})
        new_types_dict.update({VAR_MAP[var]: 'float32'}) # for var in var_list})
        return df.astype(new_types_dict)

    def is_brownian_dynamics(self):
        return matches_any(self.dynamics, ['BD', 'Brownian', 'overdamped'])

    def is_langevin_dynamics(self):
        return matches_any(self.dynamics, ['LE', 'Langevin', 'Stokes', 'underdamped'])

    def is_bbo_dynamics(self):
        return matches_any(self.dynamics, ['FBBO', 'BBO'])

    def is_deterministic(self):
        return matches_any(self.dynamics, ['BBO', 'Stokes'])

    def is_euler_integrator(self):
        return matches_any(self.method, ['EM', 'Euler', 'Euler-Maruyama'])

    def is_heun_integrator(self):
        return matches_any(self.method, ['SH', 'Heun', 'Stochastic-Heun'])

    def is_impulse_integrator(self):
        return matches_any(self.method, ['LI', 'Impulse', 'Langevin-Impulse'])

    def fmt_time(self, time):
        if self.unitless:
            return f'{time:.4e}u'
        elif time < 1e-12:
            return f'{round_to_int(time*1e15)}fs'
        elif time < 1e-9:
            return f'{round_to_int(time*1e12)}ps'
        elif time < 1e-6:
            return f'{round_to_int(time*1e9)}ns'
        elif time < 1e-3:
            return f'{round_to_int(time*1e6)} microseconds'
        elif time < 1e0:
            return f'{round_to_int(time*1e3)}ms'
        else:
            return f'{round_to_int(time)}s'


#################################################################################
# Function for selecting a different 64-bit keyset
#################################################################################
def select_keyset(keyset, i):
    if   keyset == 1:
        KEYS = np.array([i, 14702096011762217365, 14636641319537817119,
                         3351689648718967237], dtype=np.uint64)
    elif keyset == 2:
        KEYS = np.array([i, 14559322926498757700, 3632739549066318548,
                         10755600028138268439], dtype=np.uint64)
    elif keyset == 3:
        KEYS = np.array([i, 5533886795991833713, 4758535347328213817,
                         11818842473267243275], dtype=np.uint64)
    elif keyset == 4:
        KEYS = np.array([i, 2462089494503843304, 12584081913381477329,
                         834753877517719638], dtype=np.uint64)
    elif keyset == 5:
        KEYS = np.array([i, 2289790832165283079, 3697694132300942476,
                         15101728960709935653], dtype=np.uint64)
    return list(KEYS.astype(str))

def printlist(lst):
    print(*('{}: {}'.format(*i) for i in enumerate(lst)), sep='\n')

# def isiterable(item):
#     try:
#         iter(item)
#     except TypeError as e:
#         return False
#     else:
#         return not isinstance(item, (str, unicode))
#
# def make_iterable(item):
#     return item if isiterable(item) else [item]

def srp(l, c=' '):
    '''Return a string with character `c` repeated `l` times.'''
    return c.join(['' for _ in range(l)])

class Timer():
    def __init__(self, verbose=True):
        self.verbose = verbose

    def print_elapsed(self):
        print(f' ({self.elapsed:.2f} s)')

    def __enter__(self):
        self.start = time.time()
        return self

    def __exit__(self, *args):
        self.end = time.time()
        if self.verbose:
            self.print_elapsed()
        return False

    @property
    def elapsed(self):
        return self.end - self.start
