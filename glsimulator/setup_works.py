# Set this to True to enable building extensions using Cython.
# Set it to False to build extensions from the C file (that
# was previously created using Cython).
# Set it to 'auto' to build with Cython if available, otherwise
# from the C file.
# rm -rf ./*.so ./*.c && python setup_default.py build_ext --inplace --force
USE_CYTHON = True
CYTHON_LINETRACE = False
CYTHON_ANNOTATE = True

import numpy as np
from scipy._build_utils import numpy_nodepr_api  # for silencing the numpy API warnings...
import os, sys
from os.path import join
import platform, socket
import cython_gsl

from distutils.core import setup
# from setuptools import setup, find_packages
from distutils.extension import Extension

################################################################################
## Check whether Cython compilation step is needed
#--------------------------------------
if USE_CYTHON:
    try:
        from Cython.Build import cythonize
        # from Cython.Distutils import build_ext

        global_compiler_directives = {}
        global_macros = []

        # global_compiler_directives['boundscheck'] = False
        # global_compiler_directives['wraparound'] = False
        # global_compiler_directives['initializedcheck'] = False
        # global_compiler_directives['nonecheck'] = False
        # global_compiler_directives['cdivision'] = False

        # global_compiler_directives['language_level'] = 2

        if CYTHON_LINETRACE:
            # directive_defaults['linetrace'] = True
            global_compiler_directives['profile'] = True
            global_compiler_directives['linetrace'] = True
            global_compiler_directives['binding'] = True
            global_macros += ('CYTHON_TRACE', '1')

            # For nogil functions too:
            ## distutils: define_macros=CYTHON_TRACE_NOGIL=1

            # from Cython.Compiler.Options import get_directive_defaults
            # directive_defaults = get_directive_defaults()
            # directive_defaults['profile'] = True
            # directive_defaults['linetrace'] = True
            # directive_defaults['binding'] = True
        if CYTHON_ANNOTATE:
            global_compiler_directives['annotate'] = True
    except:
        print("You don't seem to have Cython installed. Please get a")
        print("copy from www.cython.org and install it")
        sys.exit(1)

cmdclass = { }
ext_modules = [ ]
#-------------------------------------------------------------------------------


################################################################################
## Remove REALLY irritating -Wstrict-prototypes warnings for C++ compiling
##   See: https://tinyurl.com/y7tbdj6u
#--------------------------------------
import distutils.sysconfig
cfg_vars = distutils.sysconfig.get_config_vars()
for key, value in cfg_vars.items():
    if type(value) == str:
        cfg_vars[key] = value.replace("-Wstrict-prototypes", "")
#-------------------------------------------------------------------------------


################################################################################
## Set up compiler, environment, libraries, etc.
#--------------------------------------
COMPILER = 'gnu'
HOSTNAME = platform.node()  # socket.hostname() -- slower

if HOSTNAME == 'seylermoon':
    if COMPILER == 'gnu':
        os.environ["CC"] = "gcc-7"
        os.environ["CXX"] = "g++-7"
        COMPILE_ARGS = [
            '-march=native', '-O3', '-mavx', '-mprefer-avx128', '-ffast-math',
            '-ftree-vectorizer-verbose=2', '-Wno-cpp', '-Wno-unused-variable',
            '-Wno-unused-function', '-Wno-discarded-qualifiers']
    MINICONDA_INCLUDE_DIR = '/home/sseyler/Library/miniconda2/include'
elif HOSTNAME == 'seylerpluto':
    if COMPILER == 'gnu':
        os.environ["CC"] = "gcc"
        os.environ["CXX"] = "g++"
        COMPILE_ARGS = [
            '-march=native', '-O3', '-mavx', '-mprefer-avx128', '-ffast-math',
            '-ftree-vectorizer-verbose=2', '-Wno-cpp', '-Wno-unused-variable',
            '-Wno-unused-function', '-Wno-discarded-qualifiers']
    if COMPILER == 'intel':
        os.environ["CC"] = "icc"
        os.environ["CXX"] = "icpc"
        os.environ["LINKCC"] = "icc"
        os.environ["LDSHARED"] = "icc -shared"
        COMPILE_ARGS = ['-fast', '-qopt-report=4']
    MINICONDA_INCLUDE_DIR = '/home/sseyler/Library/miniconda2/include'
#'-std=c++11', -fopenmp, -shared -pthread -fPIC -fwrapv -fno-strict-aliasing


################################################################################
## Specify sources
#--------------------------------------
module_dir = './glsimulator'

## Specify module names (names of *.so files)
mod_glsim      = 'glsimulator.glsim'
mod_integrator = 'glsimulator.integrator'
mod_system     = 'glsimulator.system'
mod_sim_state  = 'glsimulator.sim_state'
mod_force      = 'glsimulator.force'
mod_glrandom   = 'glsimulator.glrandom'
mod_types      = 'glsimulator.types'
mod_timers     = 'glsimulator.utils.timers'
mod_progressmeter = 'glsimulator.utils.progressmeter'


RANDOMGEN_INCLUDE_DIRS = [
        join('/home', 'sseyler', 'Repositories', 'python', 'randomgen'),
        join('/home', 'sseyler', 'Repositories', 'python', 'randomgen', 'randomgen')]
src_randomgen = join(RANDOMGEN_INCLUDE_DIRS[1], 'src', 'distributions', 'distributions.c')

def pyx_name_from_mod_name(mod_name):
    return [join(*mod_name.split('.')) + '.pyx']

src_glsim      = pyx_name_from_mod_name(mod_glsim)
src_integrator = pyx_name_from_mod_name(mod_integrator)
src_system     = pyx_name_from_mod_name(mod_system)
src_sim_state  = pyx_name_from_mod_name(mod_sim_state)
src_force      = pyx_name_from_mod_name(mod_force)
src_glrandom   = pyx_name_from_mod_name(mod_glrandom) + [src_randomgen]
src_types      = pyx_name_from_mod_name(mod_types)
src_timers     = pyx_name_from_mod_name(mod_timers)
src_progressmeter = pyx_name_from_mod_name(mod_progressmeter)

sources = {mod_glsim : src_glsim,
           mod_integrator : src_integrator,
           mod_system : src_system,
           mod_sim_state : src_sim_state,
           mod_force : src_force,
           mod_glrandom : src_glrandom,
           mod_types : src_types,
           mod_timers : src_timers,
           mod_progressmeter : src_progressmeter}

GENERAL_INCLUDE_DIRS = ['.','...', os.path.join(os.getcwd(), 'include'), np.get_include(),
                        MINICONDA_INCLUDE_DIR] + [cython_gsl.get_include()]
GENERAL_LIBRARY_DIRS = [os.getcwd(),]  # path to .a or .so file(s   )
GENERAL_LIBRARIES = ['m']  # compile modules


################################################################################
## Set up and build extensions
#--------------------------------------
def ensure_list(str_or_list):
    if str_or_list is None:
        return []
    elif isinstance(str_or_list, list):
        return str_or_list
    elif isinstance(str_or_list, str):
        return [str_or_list]


def generate_extension(module,
                       extra_libs=None, extra_lib_dirs=None, extra_inc_dirs=None,
                       extra_comp_args=None, macros=global_macros, language='c'):
    source = sources[module]
    # Default include directories, libraries, library directories, and compiler arguments
    inc_dirs  = GENERAL_INCLUDE_DIRS
    libs      = GENERAL_LIBRARIES
    lib_dirs  = GENERAL_LIBRARY_DIRS
    comp_args = COMPILE_ARGS

    inc_dirs += ensure_list(extra_inc_dirs)
    libs += ensure_list(extra_libs)
    lib_dirs += ensure_list(extra_lib_dirs)
    comp_args += ensure_list(extra_comp_args)

    ext = Extension(module,
        sources = source,
        include_dirs = inc_dirs,
        libraries    = libs,
        library_dirs = lib_dirs,
        extra_compile_args = comp_args,
        language = language,
        define_macros=macros
    )
    return [ext]


EXTENSIONS = []
EXTENSIONS += generate_extension(mod_progressmeter, language='c')
EXTENSIONS += generate_extension(mod_timers)
EXTENSIONS += generate_extension(mod_types)
EXTENSIONS += generate_extension(
    mod_glrandom,
    extra_inc_dirs=[cython_gsl.get_cython_include_dir()] + RANDOMGEN_INCLUDE_DIRS,
    extra_libs=cython_gsl.get_libraries(),
    extra_lib_dirs=cython_gsl.get_library_dir()
)
EXTENSIONS += generate_extension(mod_force)
EXTENSIONS += generate_extension(mod_sim_state)
EXTENSIONS += generate_extension(mod_system)
EXTENSIONS += generate_extension(
    mod_integrator,
    extra_inc_dirs=cython_gsl.get_cython_include_dir(),
    extra_libs=cython_gsl.get_libraries(),
    extra_lib_dirs=cython_gsl.get_library_dir()
)
EXTENSIONS += generate_extension(
    mod_glsim,
    extra_inc_dirs=cython_gsl.get_cython_include_dir(),
    extra_libs=cython_gsl.get_libraries(),
    extra_lib_dirs=cython_gsl.get_library_dir()
)


print '------------------------------------------------------------------------'
print('Running setup...')


setup(
    name = 'glsimulator',
    packages = ["glsimulator", "glsimulator.utils"],
    package_data = {'': ['*.pyx', '*.pxd'],
                   },
    include_dirs = GENERAL_INCLUDE_DIRS,
    ext_modules = cythonize(EXTENSIONS, nthreads=4, **global_compiler_directives),
    description='Generalized Langevin equation simulator'
        # compiler_directives = global_compiler_directives,
        # cmdclass = {"build_ext": build_ext},
    # zip_safe = False,  # something about not egg with setuptools
)
