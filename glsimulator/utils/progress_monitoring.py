import time
from ipywidgets import FloatProgress
from IPython.display import display


###############################################################################
# Progress monitoring for ipyparallel
def monitor_stdout_async(ar, monitor_id=0, dt=0.5, truncate=1000, start=0, stop=1,
                         skip=1, disp_monitor_id=False):
    stdout0 = ar.stdout  # initialize a stdout0 array for comparison
    while not ar.ready():
        if ar.stdout != stdout0:  # check if stdout changed for any kernel
            for i in range(len(ar.stdout)):
                if i == monitor_id and ar.stdout[i] != stdout0[i]:
                    # print only new stdout's without previous message and
                    #   remove '\n' at the end
                    if disp_monitor_id:
                        print(f'kernel {str(i)}: ', end='')
                    print(f'{ar.stdout[i][len(stdout0[i]):-1]}', end=''),
                    stdout0 = ar.stdout   # update stdout0 to current output
                else:
                    continue
        else:
            continue


def float_pbar(ar, n_tasks=None):
    n_tasks = n_tasks if n_tasks else len(ar.msg_ids)
    w = FloatProgress(min=0, max=n_tasks, step=0.1,
                      description='Progress:', bar_style='info')
    w.max = n_tasks
    display(w)
    while not ar.ready():
        w.value = ar.progress  # Update widget's value w/ # of finished tasks
        time.sleep(0.1)
    w.value = w.max
    return n_tasks
