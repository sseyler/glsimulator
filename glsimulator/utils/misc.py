import os
import shutil
import datetime
from copy import deepcopy


#################################################################################
# Class to enable direct access of variables with dot operator
#################################################################################
class Bunch(object):
    def __init__(self, d=dict(), keys=None):
        self.__dict__.update(d)
        if isinstance(keys, list):
            self.update({str(key): Bunch() for key in keys})

    def to_dict(self):
        return self.__dict__

    def extend(self, **kwargs):
        for key, value in kwargs.iteritems():
            self.__dict__[key] = value

    def copy(self):
        return Bunch(deepcopy(self.__dict__))

    def update(self, d):
        self.__dict__.update(d)

    def items(self):
         return self.__dict__.items()

    def keys(self):
         return self.__dict__.keys()

    def values(self):
         return self.__dict__.values()

    def popitem(self):
        return self.__dict__.popitem()

    # def iteritems(self):
    #     return self.__dict__.iteritems()
    #
    # def iterkeys(self):
    #     return self.__dict__.iterkeys()
    #
    # def itervalues(self):
    #     return self.__dict__.itervalues()

    def grab(self, keys, default=None):
        return [self.__dict__.get(k, default) for k in keys]

    def __len__(self):
         return self.__dict__.__len__()

    def __sizeof__(self):
        return self.__dict__.__sizeof__()

    def __getitem__(self, key):
        return self.__dict__.__getitem__(key)

    def __setitem__(self, key, value):
        self.__dict__.__setitem__(key, value)

    def __delitem__(self, key):
        self.__dict__.__delitem__()

    def __iter__(self):
        return self.__dict__.__iter__()

    def __next__(self):
        return self.__dict__.__next__()

    def __contains__(self, key):
        return self.__dict__.__contains__(key)

    def __clear__(self):
        print('Cleared contents of Bunch object')
        self.__dict__.__clear__()

    def __str__(self):
        return 'Bunch(' + self.__dict__.__str__() + ')'

    def __repr__(self):
        return 'Bunch(' + self.__dict__.__repr__() + ')'

    # WARNING: these built-ins don't work; they cause infinite recursion
    # def __getattribute__(self, key):
    #     return self.__dict__.__getattribute__(key)
    #
    # def __setattr__(self, key, value):
    #     self.__dict__.__setattr__(key, value)
    #
    # def __delattr__(self, key):
    #     self.__dict__.__delattr__(key)
    #
    # def __delete__(self, instance):
    #     print "Deleted contents of Bunch object"
    #     del self.__dict__.__delete__(instance)

def bunchify(x):
    """ Recursively transforms a dictionary into a Bunch via copy.

        >>> b = bunchify({'urmom': {'sez': {'what': 'what'}}})
        >>> b.urmom.sez.what
        'what'

        bunchify can handle intermediary dicts, lists and tuples (as well as
        their subclasses), but ymmv on custom datatypes.

        >>> b = bunchify({ 'lol': ('cats', {'hah':'i win again'}),
        ...         'hello': [{'french':'salut', 'german':'hallo'}] })
        >>> b.hello[0].french
        'salut'
        >>> b.lol[1].hah
        'i win again'

        nb. As dicts are not hashable, they cannot be nested in sets/frozensets.
    """
    if isinstance(x, dict):
        return Bunch( (k, bunchify(v)) for k,v in iteritems(x) )
    elif isinstance(x, (list, tuple)):
        return type(x)( bunchify(v) for v in x )
    else:
        return x

def unbunchify(x):
    """ Recursively converts a Bunch into a dictionary.

        >>> b = Bunch(foo=Bunch(lol=True), hello=42, ponies='are pretty!')
        >>> unbunchify(b)
        {'ponies': 'are pretty!', 'foo': {'lol': True}, 'hello': 42}

        unbunchify will handle intermediary dicts, lists and tuples (as well as
        their subclasses), but ymmv on custom datatypes.

        >>> b = Bunch(foo=['bar', Bunch(lol=True)], hello=42,
        ...         ponies=('are pretty!', Bunch(lies='are trouble!')))
        >>> unbunchify(b) #doctest: +NORMALIZE_WHITESPACE
        {'ponies': ('are pretty!', {'lies': 'are trouble!'}),
         'foo': ['bar', {'lol': True}], 'hello': 42}

        nb. As dicts are not hashable, they cannot be nested in sets/frozensets.
    """
    if isinstance(x, dict):
        return dict( (k, unbunchify(v)) for k,v in iteritems(x) )
    elif isinstance(x, (list, tuple)):
        return type(x)( unbunchify(v) for v in x )
    else:
        return x


# class Bunch(object):
#     def __init__(self, *args, **kwds):
#         super(Bunch, self).__init__(*args, **kwds)
#         self.__dict__ = self
#
#     def extend(self, **kwargs):
#         for key, value in kwargs.iteritems():
#             self.__dict__[key] = value


# class Bunch(dict):
#
#     def __contains__(self, k):
#         try:
#             return dict.__contains__(self, k) or hasattr(self, k)
#         except:
#             return False
#
#     def __getattr__(self, k):
#         try:
#             # Throws exception if not in prototype chain
#             return object.__getattribute__(self, k)
#         except AttributeError:
#             try:
#                 return self[k]
#             except KeyError:
#                 raise AttributeError(k)
#
#     def __setattr__(self, k, v):
#         try:
#             # Throws exception if not in prototype chain
#             object.__getattribute__(self, k)
#         except AttributeError:
#             try:
#                 self[k] = v
#             except:
#                 raise AttributeError(k)
#         else:
#             object.__setattr__(self, k, v)
#
#
#     def __delattr__(self, k):
#         try:
#             # Throws exception if not in prototype chain
#             object.__getattribute__(self, k)
#         except AttributeError:
#             try:
#                 del self[k]
#             except KeyError:
#                 raise AttributeError(k)
#         else:
#             object.__delattr__(self, k)
#
#     def toDict(self):
#         return unbunchify(self)
#
#     def __repr__(self):
#         keys = list(iterkeys(self))
#         keys.sort()
#         args = ', '.join(['%s=%r' % (key, self[key]) for key in keys])
#         return '%s(%s)' % (self.__class__.__name__, args)
#
#     @staticmethod
#     def fromDict(d):
#         return bunchify(d)
#
#
# def bunchify(x):
#     """ Recursively transforms a dictionary into a Bunch via copy.
#
#         >>> b = bunchify({'urmom': {'sez': {'what': 'what'}}})
#         >>> b.urmom.sez.what
#         'what'
#
#         bunchify can handle intermediary dicts, lists and tuples (as well as
#         their subclasses), but ymmv on custom datatypes.
#
#         >>> b = bunchify({ 'lol': ('cats', {'hah':'i win again'}),
#         ...         'hello': [{'french':'salut', 'german':'hallo'}] })
#         >>> b.hello[0].french
#         'salut'
#         >>> b.lol[1].hah
#         'i win again'
#
#         nb. As dicts are not hashable, they cannot be nested in sets/frozensets.
#     """
#     if isinstance(x, dict):
#         return Bunch( (k, bunchify(v)) for k,v in iteritems(x) )
#     elif isinstance(x, (list, tuple)):
#         return type(x)( bunchify(v) for v in x )
#     else:
#         return x
#
# def unbunchify(x):
#     """ Recursively converts a Bunch into a dictionary.
#
#         >>> b = Bunch(foo=Bunch(lol=True), hello=42, ponies='are pretty!')
#         >>> unbunchify(b)
#         {'ponies': 'are pretty!', 'foo': {'lol': True}, 'hello': 42}
#
#         unbunchify will handle intermediary dicts, lists and tuples (as well as
#         their subclasses), but ymmv on custom datatypes.
#
#         >>> b = Bunch(foo=['bar', Bunch(lol=True)], hello=42,
#         ...         ponies=('are pretty!', Bunch(lies='are trouble!')))
#         >>> unbunchify(b) #doctest: +NORMALIZE_WHITESPACE
#         {'ponies': ('are pretty!', {'lies': 'are trouble!'}),
#          'foo': ['bar', {'lol': True}], 'hello': 42}
#
#         nb. As dicts are not hashable, they cannot be nested in sets/frozensets.
#     """
#     if isinstance(x, dict):
#         return dict( (k, unbunchify(v)) for k,v in iteritems(x) )
#     elif isinstance(x, (list, tuple)):
#         return type(x)( unbunchify(v) for v in x )
#     else:
# return x


def whatisthis(s):
    print('\nInput \'{}\' is:'.format(s))
    if isinstance(s, str):
        print('  >>> an ordinary string')
    # elif isinstance(s, unicode):         # py2
    #     print('  >>> a unicode string')  # py2
    else:
        print('  >>> not a string')

def to_unicode(item):
    return u'{}'.format(item)

def matches_any(o, items):
    '''Return True if `o` matches at least one of the `items`'''
    try:
        return any(o == s for s in items)
    except TypeError as e:
        print('Error: the input \'items\' are (probably) not iterable.')
        return False

def matches_none(o, items):
    '''Return True if `o` matches none of the `items`'''
    return not matches_any(o, items)

def any_in(items, o):
    '''Return True if any of the `items` are in `o`'''
    return any(item in o for item in items)

def isiterable(item):
    try:
        iter(item)
    except TypeError as e:
        return False
    else:
        # return not isinstance(item, (str, unicode))  # py2
        return not isinstance(item, str)

def make_iterable(item):
    return item if isiterable(item) else [item]

def round_to_int(number):
    return int(round(number))

def rename_existing_file(fullpath, create_separate_dir=True, log_ext='log'):
    new_filepath = fullpath
    bdir, fname = os.path.split(fullpath)
    log_fname = fullpath.split('.', 1)[0] + '.' + log_ext
    f = open(log_fname, 'a+')
    f.write('[{}]\n'.format(datetime.datetime.now()))
    f.write('  >>> {} already exists in {}\n'.format(fname, bdir))

    i = 1
    if create_separate_dir:
        def _new_dir(i, bdir=bdir, name=fname.split('.', 1)[0]):
            return os.path.join(bdir, '#{}-{}#'.format(i, name))

        while os.path.exists(new_filepath):
            new_filepath = os.path.join(_new_dir(i), fname)
            i += 1
        new_dir, _ = os.path.split(new_filepath)
        if not os.path.isdir(new_dir):
            f.write('  >>> Making backup directory: {}\n'.format(new_dir))
            os.makedirs(new_dir)
        f.write('  >>> Moving file to {}\n'.format(new_dir))
        shutil.move(fullpath, new_dir)
    else:
        while os.path.exists(new_filepath):
            new_fname = '#{}-{}#'.format(i, fname)
            new_filepath = os.path.join(bdir, new_fname)
            i += 1
        f.write('  >>> Renaming file to {}\n'.format(new_fname))
        os.rename(fullpath, new_filepath)
    f.write('\n')
    f.close()

def module_exists(module_name):
    try:
        __import__(module_name)
    except ImportError:
        return False
    else:
        return True
    #
    # import imp
    # try:
    #     imp.find_module(module_name)
    #     return True
    # except ImportError:
    #     return False
