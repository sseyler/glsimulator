# from __future__ import print_function

import numpy as np
from scipy.special import gamma as gamma_fcn
from collections import OrderedDict

'''See following Wikipedia entries for more information on definitions:

    * https://en.wikipedia.org/wiki/Taylor_microscale
    * https://en.wikipedia.org/wiki/Turbulence_kinetic_energy
    * https://en.wikipedia.org/wiki/Kolmogorov_microscales
'''

# Define global constants
PI = np.pi
kBol = 1.3806485e-23
Navo = 6.0221409e23

a = 1.5       # Exponent for BBO-type superdiffusive GLE
b = 5.0       # Temporal scaling (spacing) parameter for Prony series terms
C = 1.78167   # Fitting parameter for Prony series
nu0 = 1.0e6   # Dimensionless base frequency (scales the Prony series)

def C_a(a, b):
    '''Fitting parameter for Prony series memory kernel approximation.

        * a = anomalous diffusion exponent
        * b = temporal scaling parameter for Prony series terms
    '''
    return 1.78167

def compute_characteristic_scales(rhs, rhf, R, eta, Te, preset=None, verbose=True):
    RH_o_BETA = (2*rhs + rhf) / 9.0

    if preset == 'arminski':
        Tc = 0.3333333333333333
        Ec = 6.283185307179586
        Vc = 1.0
        Fc = 18.84955592153876
        Lc = 0.3333333333333333
        Mc = 6.283185307179586
    elif preset == 'thermal':
        Tc = (RH_o_BETA*R**2) / eta
        Mc = mass_e(R, rhf, rhs)
        Ec = Te
        Vc = ( Ec/Mc )**0.5
        Lc = Vc*Tc
        Fc = Ec/Lc
    elif preset == 'thermal2':
        Tc = (RH_o_BETA*R**2) / eta
        Mc = mass_e(R, rhf, rhs)
        Ec = 1.0
        Vc = ( Ec/Mc )**0.5
        Lc = Vc*Tc
        Fc = Ec/Lc
    elif preset is None:
        Tc = (RH_o_BETA*R**2) / eta
        Vc = ( 3.0 / (rhf/rhs + 2) )**0.5
        Ec = 6*PI*R*eta*(Tc*Vc**2)
        Lc = Vc*Tc
        Fc = Ec/Lc
        Mc = Ec/Vc**2
    beta = 9 / (2*rhs/rhf + 1) if rhf else 0

    # if force_eff_mass:
    #     Tc = 0.3333333333333333
    #     Ec = 1.0
    #     Vc = 0.398942280401
    #     #----------------------
    #     Fc = 7.51988482389
    #     Lc = 0.132980760134
    #     Mc = 6.283185307179586
    # else:
    #     Tc = 0.3333333333333333
    #     Ec = 1.0
    #     Vc = 0.48860251190292
    #     #----------------------
    #     Fc = 6.13996024767893
    #     Lc = 0.16286750396764
    #     Mc = 4.18879020478639

    scales = OrderedDict({
        'time': Tc,
        'velocity': Vc,
        'energy': Ec,
        'length': Lc,
        'force': Fc,
        'mass': Mc,
        'beta': beta
    })
    nicknames = {
        'time': 'Tc',
        'velocity': 'Vc',
        'energy': 'Ec',
        'length': 'Lc',
        'force': 'Fc',
        'mass': 'Mc',
        'beta': 'B'
    }

    if verbose:
        for name, value in scales.items():
            unit_string = f'{name} ({nicknames[name]}): {value:.4f}'
            print(unit_string)
    return scales

###########################################################################
# General
#---------
# TODO: many of the functions use the kinematic viscosity "knu" as an
#       input parameter; there should also be a way to just put in the
#       dynamic viscosity directly. This allows the user to avoid having
#       to define the fluid density, which may not be physically clear
#       in the case of conventional Langevin dynamics. Possibilities:
#         * define separate functions that use eta
#         * have the functions take either viscosity and somehow
#           make the user input a (fluid) density if kinematic used
#         * maybe even define a whole class for all of these functions?
######################################

def volume_sphere(R):
    '''Volume of a sphere computed as a function of radius.

        * R = radius [L]
    '''
    return (4/3.)*PI*R**3

def mass_p(R, rhs):
    '''Particle mass calculated as a function of particle radius and density.

        * R = particle radius [L]
        * rhs = particle density [M/L^3]
    '''
    return rhs * volume_sphere(R)

def mass_s(R, rhs):
    '''Sphere mass calculated as a function of radius and density.

        * R = sphere radius [L]
        * rhs = sphere density [M/L^3]
    '''
    return rhs * volume_sphere(R)

def mass_e(R, rhf, rhs):
    '''Effective mass computed as a function of particle radius and the fluid
    and particle densities.

    This is the particle mass plus one-half of the displace fluid mass.

        * R = particle radius [L]
        * rhf = fluid density [M/L^3]
        * rhs = particle density [M/L^3]
    '''
    return (rhs + 0.5*rhf) * volume_sphere(R)

def nu_i(i, nu0=nu0, Tc=1, b=b):
    return (nu0/Tc) / b**i

def eta_a(R, knu, rhf, a=a):
    return 6*PI*(R**2)*rhf*(knu**0.5)

def eta_i(i, R, knu, rhf, nu0=nu0, Tc=1, a=a, b=b, C=C):
    return C*eta_a(R, knu, rhf)*nu_i(i, nu0, Tc)**a / abs(gamma_fcn(1-a))

def eta_hat_i(i, R, knu, rhf, nu0=nu0, Tc=1, a=a, b=b, C=C):
    return eta_i(i, R, knu, rhf, nu0, Tc) / nu_i(i, nu0, Tc)

def eta_0(R, knu, rhf, N, nu0=nu0, Tc=1, a=a, b=b, C=C):
    return sum([eta_hat_i(i+1, R, knu, rhf, nu0, Tc) for i in range(N)])
    # return C(a,b)*eta_a(R, knu, rhf, a=a) * nu_i(i)**(a-1) / abs(G(1-a))
    # return  sum([eta_i(i,R,knu,rhf)/nu_i(i) for i in range(1,N+1)])

def gamma_0(R, knu, rhf, rhs, mass, N, nu0=nu0, Tc=1):
    return eta_0(R, knu, rhf, N, nu0, Tc) / mass

def gamma_i(i, R, knu, rhf, rhs, mass, nu0=nu0, Tc=1):
    return eta_hat_i(i, R, knu, rhf, nu0, Tc) / mass

def gamma_b(R, knu, rhf, rhs, mass, N, nu0=nu0, Tc=1):
    return np.array([gamma_i(i+1, R, knu, rhf, rhs, mass, nu0, Tc)
                     for i in range(N)])

def nu_b(N, nu0=nu0, Tc=1, b=b):
    return np.array([nu_i(i+1, nu0, Tc) for i in range(N)])

def tau_0(R, knu, rhf, rhs, mass, a=a):
    return (eta_a(R, knu, rhf) / mass)**(1./(a-2))

def zeta_b(R, knu, rhf):
    '''Coefficient of Basset force term.

        * R   = particle radius [L]
        * knu = fluid kinematic viscosity [L**2/T]
        * rhf = fluid density [M/L**3]
    '''
    return 6*R**2*rhf*(PI*knu)**0.5

def zeta_s(R, knu, rhf):
    '''Coefficient of Stokes drag (friction) term.

        * R   = particle radius [L]
        * knu = fluid kinematic viscosity [L**2/T]
        * rhf = fluid density [M/L**3]
    '''
    return 6*PI*knu*rhf*R

def gamma_s(R, knu, rhf, rhs, mass):
    '''Corrected Stokes (Brownian) collision frequency.

    This collision frequency is calculated using the (no-slip) Stokes drag
    divided by the effective mass (corrected for displaced fluid inertia).

        * R = particle radius [L]
        * knu = fluid kinematic viscosity [L**2/T]
        * rhf = fluid density [M/L^3]
        * rhs = particle density [M/L^3]
    '''
    return 6*PI*knu*rhf*R / mass

def tau_s(R, knu, rhf, rhs, mass):
    '''Stokes (Brownian) relaxation time.

    This quantity is calculated using the (no-slip) Stokes drag divided by the
    effective mass (corrected for displaced fluid inertia). It is the inverse
    of `gamma_s`.

        * R = particle radius [L]
        * knu = fluid kinematic viscosity [L**2/T]
        * rhf = fluid density [M/L^3]
        * rhs = particle density [M/L^3]
    '''
    return mass / (6*PI*knu*rhf*R)

def tau_h(R, knu):
    '''Vorticity diffusion or hydrodynamic timescale.

    This quantity can also be interpreted as the timescale of deviatoric
    (shear or off-diagonal) momentum transport.

        * R = particle radius [L]
        * knu = fluid kinematic viscosity [L**2/T]
    '''
    return R**2 / knu

def Re_s(W_s, R, knu):
    '''(Spherical) particle Reynolds number.

        * W_s = relative sphere velocity [L/T]
        * R = particle radius [L]
        * knu = fluid kinematic viscosity [L**2/T]
    '''
    return 2*R*abs(W_s) / knu

def basset_stokes_ratio(R, knu, rhf):
    '''Ratio of Basset force and Stokes drag coefficients.

        * R   = particle radius [L]
        * knu = fluid kinematic viscosity [L**2/T]
        * rhf = fluid density [M/L**3]
    '''
    return ((R**2)/(PI*knu))**0.5  # zeta_b(R,knu,rhf) / zeta_s(R,knu,rhf)

def specific_energy_dissipation_rate(Eij, knu):
    '''Turbulence specific kinetic energy dissipation rate (per unit mass).

        * Eij = fluid velocity [L**2/T**2]
        * knu = fluid kinematic viscosity [L**2/T]
    '''
    return 2*knu * np.sum(Eij**2)

def turbulence_kinetic_energy(v):
    '''Turbulence kinetic energy (per unit mass).

        * v = fluid velocity [L/T]
    '''
    return 0.5 * np.mean(v**2)

def taylor_microscale(eps, TKE, knu):
    '''Taylor microscale (turbulence length scale).

        * eps = Turbulence specific kinetic energy dissipation rate [L**2/T]
        * k   = Mean turbulence kinetic energy per unit mass [L**2/T**2]
        * knu = fluid kinematic viscosity [L**2/T]
    '''
    return (10*knu*TKE/eps)**0.5

def kolmogorov_length(eps, knu):
    '''Kolmogorov length scale.

        * eps = Turbulence specific kinetic energy dissipation rate [L**2/T]
        * knu = fluid kinematic viscosity [L**2/T]
    '''
    return (knu**3 / eps) ** 0.25

def kolmogorov_time(eps, knu):
    '''Kolmogorov time scale.

        * eps = Turbulence specific kinetic energy dissipation rate [L**2/T]
        * knu = fluid kinematic viscosity [L**2/T]
    '''
    return (knu / eps) ** 0.5

def kolmogorov_velocity(eps, knu):
    '''Kolmogorov velocity scale.

        * eps = Turbulence specific kinetic energy dissipation rate [L**2/T]
        * knu = fluid kinematic viscosity [L**2/T]
    '''
    return (knu * eps) ** 0.25

def timestep_to_time(step, dt, Tc=None):
    '''Convert a timestep value to the corresponding time.

       Note: `Tc` is the characteristic time scale in physical units
       of time. `Tc = None` returns the nondimensional time.
    '''
    time = step * dt
    if Tc is not None:
        time *= Tc
    return time
