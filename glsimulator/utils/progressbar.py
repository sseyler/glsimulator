'''
Created on Nov 20, 2012

@author: Sean Lee Seyler
'''
from __future__ import division
import time
import sys
import numpy as np


class Printer():
    """
    Print things to stdout on one line dynamically
    """

    def __init__(self, data):

        sys.stdout.write("\r\x1b[K"+data.__str__())
        sys.stdout.flush()


class ProgressBar(object):
    def __init__(self, DATASIZE, print_interval=0.05, unit='full', \
                 width=50, eta_print_interval=0.5, verbose=True):
        # Initialize global features
        self.DATASIZE = DATASIZE
        self.status = ""
        # Initialize progress bar features
        self.bar = None
        self.width = width
        self.print_interval = print_interval
        # Initialize eta features
        self.eta_print_interval = eta_print_interval
        self.etatxt = "ETA: ..."
        # Initialize wall time features
        self.end = None
        self.hours = None
        self.secs  = None
        self.msecs = None
        self.mins  = None
        self.unit  = unit
        self.verbose = verbose

    def __enter__(self):
        # Set up toolbar
        sys.stdout.write("Progress:\n")
        sys.stdout.write("[%s]" % ("-" * self.width))
        sys.stdout.flush()
        sys.stdout.write("\b" * (self.width+1)) # return to start of line, after '['
        sys.stdout.flush()

        # Start timing
        self.start = time.time()
        self.t_nextout     = self.start
        self.t_eta_nextout = self.start
        self.frac_intvl = 0
        return self

    def update(self, step, curr_time=None):
        curr_time = time.time() if curr_time is None else curr_time
        try:
            frac_done = float(step)/self.DATASIZE
            if frac_done < 0:
                frac_done = 0
                self.status = "Halt...\r\n"
            if frac_done >= 1:
                frac_done = 1
                self.status = "Done...\r\n"
        except ValueError:
            frac_done = 0
            self.status = "Error: progress variable must be float or int\r\n"
        self._regenerate_bar(frac_done, curr_time)

    def _regenerate_bar(self, frac_done, t_now):
        if t_now >= self.t_eta_nextout:
            try:
                frac_left = 1.0 - frac_done
                t_elap = t_now - self.start
                avg_rate = t_elap / frac_done
                t_left = frac_left * avg_rate
                mins = t_left // 60
                secs = t_left % 60
                self.etatxt = "ETA: {:02d}:{:02d}".format(int(mins), int(secs))
                # self.etatxt = "ETA: {:02d}:{:05.2f}".format(int(mins), secs)
            except ZeroDivisionError:
                self.etatxt = "ETA: ..."
            finally:
                self.t_eta_nextout += self.eta_print_interval
        block = int(np.rint(self.width*frac_done))
        args = ( "#"*block + "-"*(self.width-block),  \
                 frac_done*100, self.etatxt, self.status )

        bar = "\r[{:s}] {:4.1f}% | {:s} {:s}".format(*args)
        Printer(bar)

    def __exit__(self, *args):
        self.end   = time.time()
        self.secs  = self.end - self.start
        self.msecs = self.secs * 1000  # millisecs
        self.mins  = self.secs / 60  # minutes
        self.hours = self.mins / 60

        sys.stdout.write("\n") # Add newline after progress bar
        sys.stdout.flush() # Reset line

        if self.verbose:
            summary_str = '  --->  Elapsed time: '
            if   self.unit == 'msec':
                details_str = '{:.0f} ms'.format(self.msecs)
            elif self.unit == 'sec':
                details_str = '{:.2f} s'.format(self.secs)
            elif self.unit == 'min':
                isec = int(self.secs%60)
                imin = int(self.mins%60)
                details_str = '{:d} min {:d} sec'.format(imin, isec)
            elif self.unit == 'hour':
                isec = int(self.secs%60)
                imin = int(self.mins%60)
                ihr  = int(self.hours)
                details_str = '{:02d}:{:02d}:{:02d}'.format(ihr, imin, isec)
            elif self.unit == 'full':
                isec = self.secs%60
                imin = int(self.mins%60)
                ihr  = int(self.hours)
                details_str = '{:02d}:{:02d}:{:05.2f}'.format(ihr, imin, isec)
            print('{:s}{:s}\n'.format(summary_str, details_str))
