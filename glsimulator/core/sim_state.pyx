#cython: boundscheck=False, wraparound=False, initializedcheck=False, nonecheck=False
#cython: cdivision=True, profile=False, linetrace=False, binding=True, language_level=3

import numpy as np
cimport numpy as np
cimport cython
from cython cimport view

# from libcpp.string cimport string
import os
import itertools as it
import pandas as pd

from glsimulator.core.types cimport *
#############################################################


################################################################################
## State
################################################################################
cdef class State:

    def __cinit__(self, unsigned long NPAR, unsigned long NAUX,
                       unsigned long NDIM, unsigned long long NSTEPS,
                       unsigned long long NTDUMP, unsigned long NTOUT,
                       str filename, str outdir):
        self.__TMB = TrajMemBuffer(NPAR, NAUX, NDIM, NTDUMP, NSTEPS, NTOUT)
        self.TrjW  = TrajWriter(self.__TMB, NPAR, NAUX, NDIM, NTDUMP, NSTEPS, NTOUT,
                                filename, outdir)

        self.NPAR    = NPAR     # number of simulation replicas
        self.NAUX    = NAUX     # number of auxiliary variables
        self.NDIM    = NDIM     # number of spatial dimensions
        self.NTDUMP  = NTDUMP   # number of steps between traj dumps
        self.NSTEPS  = NSTEPS   # total number of simulation steps
        self.NTOUT   = NTOUT    # number of steps between traj saves

        self.DUMPSIZE = NTDUMP//NTOUT
        self.NALLDIM  = NPAR * self.DUMPSIZE * NDIM

    def __init__(self, unsigned long NPAR, unsigned long NAUX,
                       unsigned long NDIM, unsigned long long NSTEPS,
                       unsigned long long NTDUMP, unsigned long NTOUT,
                       str filename, str outdir):
        self._filename  = filename
        self._outdir    = outdir

    def get_state(self, real_t[:,:,::1] Q):
        return np.asarray(Q)

    cdef save_state(self, real_t[:,:,::1] Q, real_t[:,:,::1] QF, real_t[:,::1] QU,
                     unsigned long long ts):
        cdef unsigned long i, ia, ias, iaw, p, d

        i = (ts//self.NTOUT - 1) % self.DUMPSIZE if ts != 0 else 0

        self.__TMB._X[i,...]  = Q[XP,...]
        self.__TMB._V[i,...]  = Q[VP,...]
        self.__TMB._F[i,...]  = QF[FP,...]
        self.__TMB._U[i,...]  = QU[UP,...]
        self.__TMB._GS[i,...] = QF[GSP,...]
        self.__TMB._GB[i,...] = QF[G0P,...]

        for ia in range(self.NAUX):
            ias = SP + ia
            iaw = GP + ia
            self.__TMB._S[ia,i,...] =  Q[ias,...]
            # self.__TMB._GS[i,ia,...] = QF[iaw,...]
            # self.__TMB._GB[i,ia,...] = QF[iaw,...]

        if ts % self.NTDUMP == 0:
            self.TrjW.dataframes_to_disk(ts)


    ######################################################################
    # properties
    #----------------------------------
    property filename:
        def __get__(self):
            return self._filename
        def __set__(self, name):
            self._filename = name

    property outdir:
        def __get__(self):
            return self._outdir
        def __set__(self, name):
            self._outdir = name

    property position:
        def __get__(self):
            return np.ascontiguousarray(self.__TMB._X)
        def __set__(self, arr):
            self.__TMB.position = arr

    property velocity:
        def __get__(self):
            return np.ascontiguousarray(self.__TMB._V)
        def __set__(self, arr):
            self.__TMB.velocity = arr

    property auxiliary:
        def __get__(self):
            return np.ascontiguousarray(self.__TMB._S)
        def __set__(self, arr):
            self.__TMB.auxiliary = arr

    property force:
        def __get__(self):
            return np.ascontiguousarray(self.__TMB._F)
        def __set__(self, arr):
            self.__TMB.force = arr

    property potential:
        def __get__(self):
            return np.ascontiguousarray(self.__TMB._U)
        def __set__(self, arr):
            self.__TMB.potential = arr

    property stokes_noise:
        def __get__(self):
            return np.ascontiguousarray(self.__TMB._GS)
        def __set__(self, arr):
            self.__TMB.stokes_noise = arr

    property basset_noise:
        def __get__(self):
            return np.ascontiguousarray(self.__TMB._GB)
        def __set__(self, arr):
            self.__TMB.basset_noise = arr

    property noise:
        def __get__(self):
            return np.ascontiguousarray(self.__TMB._G)
        def __set__(self, arr):
            self.__TMB.noise = arr


################################################################################
## TrajWriter
################################################################################
cdef class TrajWriter:

    def __cinit__(self, TrajMemBuffer TMB,
                 unsigned long NPAR, unsigned long NAUX,
                 unsigned long NDIM, unsigned long long NTDUMP,
                 unsigned long long NSTEPS, unsigned long NTOUT,
                 str filename, str outdir, **kwargs):
        self.__TMB  = TMB
        self.NPAR   = NPAR     # number of simulation replicas
        self.NAUX   = NAUX     # number of auxiliary variables
        self.NDIM   = NDIM     # number of spatial dimensions
        self.NTDUMP = NTDUMP   # number of steps between traj dumps
        self.NSTEPS = NSTEPS   # total number of simulation steps
        self.NTOUT  = NTOUT    # number of steps between traj saves

        self.DUMPSIZE = NTDUMP//NTOUT
        self.NALLDIM  = NPAR * self.DUMPSIZE * NDIM

        self.parquet_kwargs = {'engine':      'pyarrow',
                               'compression': 'snappy'}

        # NOTE: format=fixed seems to be REQUIRED when storing DataFrames with
        #       both axes MultiIndex'd (i.e. the DataFrame for aux variables);
        #       Unfortunately, this prevents the use of 'append' mode...
        # NOTE: if `mode = a` instead of `w`, an error is raised when attempting
        #       to append to the auxiliary trajectory file...
        self.hdf_kwargs     = {'complib':   'blosc:lz4',
                               'complevel': 9,
                               'format':    'table',
                               'append':    True,
                               'mode':      'a'}
        self.filename = filename
        self.outdir   = outdir
        self.ext      = 'h5'

        # IDEA: allow selection of keys by user at runtime
        traj_keys = ['pos', 'vel', 'fex', 'uex', 'ths', 'thb']
        traj_keys += [f'aux.{i+1}' for i in range(self.NAUX)]
        self.Qt_fnames = {
            k: os.path.join(outdir, '.'.join([filename, k, self.ext]))
            for k in traj_keys
        }

        for traj_name in self.Qt_fnames.itervalues():
            if os.path.exists(traj_name):
                TrajWriter._back_up_trajectory(traj_name)

        self.step_name = 'Step'
        self._generate_df_column_names()

    @staticmethod
    def _back_up_trajectory(filepath, create_separate_dir=True, log_ext='log'):
        import datetime
        import shutil  # copy2 copies to a dir and preserves metadata

        new_filepath = filepath
        bdir, fname = os.path.split(filepath)
        log_fname = filepath.split('.', 1)[0] + '.' + log_ext
        f = open(log_fname, 'a+')
        f.write('[{}]\n'.format(datetime.datetime.now()))
        f.write('  >>> {} already exists in {}\n'.format(fname, bdir))

        i = 1
        if create_separate_dir:
            def _new_dir(i, bdir=bdir, name=fname.split('.', 1)[0]):
                return os.path.join(bdir, '#{}-{}#'.format(i, name))

            while os.path.exists(new_filepath):
                new_filepath = os.path.join(_new_dir(i), fname)
                i += 1
            new_dir, _ = os.path.split(new_filepath)
            if not os.path.isdir(new_dir):
                f.write('  >>> Making backup directory: {}\n'.format(new_dir))
                os.makedirs(new_dir)
            f.write('  >>> Moving trajectory to {}\n'.format(new_dir))
            shutil.move(filepath, new_dir)
        else:
            while os.path.exists(new_filepath):
                new_fname = '#{}-{}#'.format(i, fname)
                new_filepath = os.path.join(bdir, new_fname)
                i += 1
            f.write('  >>> Renaming trajectory to {}\n'.format(new_fname))
            os.rename(filepath, new_filepath)
        f.write('\n')
        f.close()

    def _generate_df_column_names(self):
        # import itertools as it
        cdef unsigned long p

        if self.NDIM == 1:
            coord_vars = ['x']
        elif self.NDIM == 2:
            coord_vars = ['x','y']
        elif self.NDIM == 3:
            coord_vars = ['x','y','z']
        runs = ['Run{:03d}'.format(p+1) for p in range(self.NPAR)]

        x_headr  = list(it.product(runs, coord_vars))
        v_headr  = list(it.product(runs, coord_vars))
        f_headr  = list(it.product(runs, coord_vars))
        gs_headr = list(it.product(runs, coord_vars))
        gb_headr = list(it.product(runs, coord_vars))
        s_headr  = list(it.product(runs, coord_vars))

        self.x_cols  = pd.MultiIndex.from_tuples(x_headr)
        self.v_cols  = pd.MultiIndex.from_tuples(v_headr)
        self.f_cols  = pd.MultiIndex.from_tuples(f_headr)
        self.u_cols  = runs
        self.gs_cols = pd.MultiIndex.from_tuples(gs_headr)
        self.gb_cols = pd.MultiIndex.from_tuples(gb_headr)
        self.s_cols  = pd.MultiIndex.from_tuples(s_headr)

        self.df_pos = pd.DataFrame(columns=self.x_cols)
        self.df_vel = pd.DataFrame(columns=self.v_cols)
        self.df_fex = pd.DataFrame(columns=self.f_cols)
        self.df_uex = pd.DataFrame(columns=self.u_cols)
        self.df_ths = pd.DataFrame(columns=self.gs_cols)
        self.df_thb = pd.DataFrame(columns=self.gb_cols)
        self.df_aux = [pd.DataFrame(columns=self.s_cols) for _ in range(self.NAUX)]

        # self.df_pos.columns = self.x_cols
        # self.df_vel.columns = self.v_cols
        # self.df_fex.columns = self.f_cols
        # self.df_ths.columns = self.gs_cols
        # self.df_thb.columns = self.gb_cols
        # for i in range(self.NAUX):
        #     self.df_aux[i].columns = self.s_cols

    cpdef memory_to_dataframes_old(self, unsigned long long ts):
        # WARN: this implementation is probably VERY slow due to poor
        #   C array indexing and also unnecessary transposing as a result
        # Is probably faster to create/modify a dict then use df.replace or
        # df.map to update the DataFrames. (This is untested.)
        #   See question 47329860 and 42012339 on StackOverflow
        cdef:
            unsigned long start, stop, step, p
            unsigned long[::1] idx
            unsigned long n_pardim = self.NPAR*self.NDIM

        if ts == 0:
            start = 0
            stop = 1
            step = 1
            x_data_2d  = self.__TMB.position[0,:,:].reshape((1, n_pardim))
            v_data_2d  = self.__TMB.velocity[0,:,:].reshape((1, n_pardim))
            f_data_2d  = self.__TMB.force[0,:,:].reshape((1, n_pardim))
            gs_data_2d = self.__TMB.stokes_noise[0,:,:].reshape((1, n_pardim))
            gb_data_2d = self.__TMB.basset_noise[0,:,:].reshape((1, n_pardim))
            s_data_2d = self.__TMB.auxiliary[0,:,:,:].reshape((self.NAUX, n_pardim))
        else:
            start = ts - self.NTDUMP + self.NTOUT
            stop  = ts + self.NTOUT
            step  = self.NTOUT
            x_data_2d  = self.__TMB.position.reshape((self.DUMPSIZE, n_pardim))
            v_data_2d  = self.__TMB.velocity.reshape((self.DUMPSIZE, n_pardim))
            f_data_2d  = self.__TMB.force.reshape((self.DUMPSIZE, n_pardim))
            gs_data_2d = self.__TMB.stokes_noise.reshape((self.DUMPSIZE, n_pardim))
            gb_data_2d = self.__TMB.basset_noise.reshape((self.DUMPSIZE, n_pardim))
            s_data_2d = self.__TMB.auxiliary.reshape((self.DUMPSIZE*self.NAUX, n_pardim))

        x_indx = pd.RangeIndex(start, stop, step, name=self.step_name)
        idx = np.arange(start=start, stop=stop, step=step, dtype=np.dtype('u8')) #'uint64'

        index = list(it.product(idx, ['S{:02d}'.format(p) for p in
                                      range(1, self.NAUX+1)]))
        s_indx = pd.MultiIndex.from_tuples(index, names=[self.step_name, 'Si'])

        # self.df_pos = pd.DataFrame(data=x_data_2d, index=x_indx, columns=self.x_cols)
        # self.df_vel = pd.DataFrame(data=v_data_2d, index=x_indx, columns=self.v_cols)
        # self.df_fex = pd.DataFrame(data=f_data_2d, index=x_indx, columns=self.f_cols)
        # self.df_ths = pd.DataFrame(data=gs_data_2d, index=x_indx, columns=self.gs_cols)
        # self.df_thb = pd.DataFrame(data=gb_data_2d, index=x_indx, columns=self.gb_cols)
        # self.df_aux = pd.DataFrame(data=s_data_2d, index=s_indx, columns=self.s_cols)

        # Slightly faster to assign cols after vs constructing with columns
        self.df_pos = pd.DataFrame(data=x_data_2d, index=x_indx)
        self.df_vel = pd.DataFrame(data=v_data_2d, index=x_indx)
        self.df_fex = pd.DataFrame(data=f_data_2d, index=x_indx)
        self.df_ths = pd.DataFrame(data=gs_data_2d, index=x_indx)
        self.df_thb = pd.DataFrame(data=gb_data_2d, index=x_indx)
        self.df_aux = pd.DataFrame(data=s_data_2d, index=s_indx)
        self.df_pos.columns = self.x_cols
        self.df_vel.columns = self.v_cols
        self.df_fex.columns = self.f_cols
        self.df_ths.columns = self.gs_cols
        self.df_thb.columns = self.gb_cols
        self.df_aux.columns = self.s_cols

        self.df_aux = self.df_aux.unstack('Si')

    cpdef memory_to_dataframes(self, unsigned long long ts):
        cdef:
            unsigned long start, stop, step, p
            unsigned long[::1] idx
            unsigned long n_pardim = self.NPAR*self.NDIM
            (unsigned long, unsigned long) dump_x_pardim = (self.DUMPSIZE,
                                                            self.NPAR*self.NDIM)
            list s_data_2d = []

        if ts == 0:
            start = 0
            stop = 1
            step = 1
            x_data_2d  = self.__TMB.position[0,:,:].reshape((1, n_pardim))
            v_data_2d  = self.__TMB.velocity[0,:,:].reshape((1, n_pardim))
            f_data_2d  = self.__TMB.force[0,:,:].reshape((1, n_pardim))
            u_data_2d  = self.__TMB.potential[0,:].reshape((1, self.NPAR))
            gs_data_2d = self.__TMB.stokes_noise[0,:,:].reshape((1, n_pardim))
            gb_data_2d = self.__TMB.basset_noise[0,:,:].reshape((1, n_pardim))
            for i in range(self.NAUX):
                s_data_2d.append(self.__TMB.auxiliary[i,0,:,:].reshape((1, n_pardim)))
        else:
            start = ts - self.NTDUMP + self.NTOUT
            stop  = ts + self.NTOUT
            step  = self.NTOUT
            x_data_2d  = self.__TMB.position.reshape(dump_x_pardim)
            v_data_2d  = self.__TMB.velocity.reshape(dump_x_pardim)
            f_data_2d  = self.__TMB.force.reshape(dump_x_pardim)
            u_data_2d  = self.__TMB.potential.reshape((self.DUMPSIZE, self.NPAR))
            gs_data_2d = self.__TMB.stokes_noise.reshape(dump_x_pardim)
            gb_data_2d = self.__TMB.basset_noise.reshape(dump_x_pardim)
            for i in range(self.NAUX):
                s_data_2d.append(self.__TMB.auxiliary[i,:,:,:].reshape(dump_x_pardim))

        index = pd.RangeIndex(start, stop, step, name=self.step_name)

        # Slightly faster to assign cols after vs constructing with columns
        self.df_pos = pd.DataFrame(data=x_data_2d, index=index)
        self.df_vel = pd.DataFrame(data=v_data_2d, index=index)
        self.df_fex = pd.DataFrame(data=f_data_2d, index=index)
        self.df_uex = pd.DataFrame(data=u_data_2d, index=index)
        self.df_ths = pd.DataFrame(data=gs_data_2d, index=index)
        self.df_thb = pd.DataFrame(data=gb_data_2d, index=index)
        for i in range(self.NAUX):
            self.df_aux[i] = pd.DataFrame(data=s_data_2d[i], index=index)

        # self.df_pos = self.df_pos.append(data=x_data_2d, index=index)
        # self.df_vel = self.df_vel.append(data=v_data_2d, index=index)
        # self.df_fex = self.df_fex.append(data=f_data_2d, index=index)
        # self.df_ths = self.df_ths.append(data=gs_data_2d, index=index)
        # self.df_thb = self.df_thb.append(data=gb_data_2d, index=index)
        # for i in range(self.NAUX):
        #     self.df_aux[i] = pd.DataFrame(data=s_data_2d[i], index=index)

    # cpdef dataframes_to_disk(self, unsigned long long ts):
    #     self.memory_to_dataframes(ts)
    #     # TODO: code ability to just loop through the trajs to output
    #     # for traj_key, traj_fname in self.Qt_fnames:
    #     #     self.df_pos.to_hdf(self.Xt_fname,  'position',     **self.hdf_kwargs)
    #
    #     self.df_pos.to_hdf(self.Qt_fnames['pos'], 'pos', **self.hdf_kwargs)
    #     self.df_vel.to_hdf(self.Qt_fnames['vel'], 'vel', **self.hdf_kwargs)
    #     self.df_fex.to_hdf(self.Qt_fnames['fex'], 'fex', **self.hdf_kwargs)
    #     self.df_ths.to_hdf(self.Qt_fnames['ths'], 'ths', **self.hdf_kwargs)
    #     self.df_thb.to_hdf(self.Qt_fnames['thb'], 'thb', **self.hdf_kwargs)
    #     self.df_aux.to_hdf(self.Qt_fnames['aux'], 'aux', **self.hdf_kwargs)

    cpdef dataframes_to_disk(self, unsigned long long ts):
        self.memory_to_dataframes(ts)

        self.df_pos.to_hdf(self.Qt_fnames['pos'], 'pos', **self.hdf_kwargs)
        self.df_vel.to_hdf(self.Qt_fnames['vel'], 'vel', **self.hdf_kwargs)
        self.df_fex.to_hdf(self.Qt_fnames['fex'], 'fex', **self.hdf_kwargs)
        self.df_uex.to_hdf(self.Qt_fnames['uex'], 'uex', **self.hdf_kwargs)
        self.df_ths.to_hdf(self.Qt_fnames['ths'], 'ths', **self.hdf_kwargs)
        self.df_thb.to_hdf(self.Qt_fnames['thb'], 'thb', **self.hdf_kwargs)
        for i in range(self.NAUX):
            self.df_aux[i].to_hdf(self.Qt_fnames[f'aux.{i+1}'], 'aux', **self.hdf_kwargs)

################################################################################
## TrajMemBuffer
################################################################################
cdef class TrajMemBuffer:

    def __cinit__(self, unsigned long NPAR, unsigned long NAUX,
                        unsigned long NDIM, unsigned long long NTDUMP,
                        unsigned long long NSTEPS, unsigned long NTOUT):
        self.NPAR    = NPAR     # number of simulation replicas
        self.NAUX    = NAUX     # number of auxiliary variables
        self.NDIM    = NDIM     # number of spatial dimensions
        self.NTDUMP  = NTDUMP   # number of steps between traj dumps
        self.NSTEPS  = NSTEPS   # total number of simulation steps
        self.NTOUT   = NTOUT    # number of steps between traj saves

        self.DUMPSIZE = NTDUMP//NTOUT
        self.NALLDIM  = NPAR * self.DUMPSIZE * NDIM

        self._X  = np.zeros((self.DUMPSIZE, NPAR, NDIM), dtype=np.floating)
        self._V  = np.zeros((self.DUMPSIZE, NPAR, NDIM), dtype=np.floating)
        self._F  = np.zeros((self.DUMPSIZE, NPAR, NDIM), dtype=np.floating)
        self._U  = np.zeros((self.DUMPSIZE, NPAR), dtype=np.floating)
        self._GS = np.zeros((self.DUMPSIZE, NPAR, NDIM), dtype=np.floating)
        self._GB = np.zeros((self.DUMPSIZE, NPAR, NDIM), dtype=np.floating)
        self._S  = np.zeros((NAUX, self.DUMPSIZE, NPAR, NDIM), dtype=np.floating)
        # self._G  = np.zeros((self.DUMPSIZE, NAUX, NPAR, NDIM), dtype=np.floating)
        # if not self._X or not self._V or not self._F or not self._S or not self._G:
        #     raise MemoryError("Cannot allocate memory.")

    property position:
        def __get__(self):
            return np.ascontiguousarray(self._X)
        def __set__(self, arr):
            cdef unsigned long ds, p, d
            for ds in range(self.DUMPSIZE):
                for p in range(self.NPAR):
                    for d in range(self.NDIM):
                        self._X[ds,p,d] = arr[ds,p,d]

    property velocity:
        def __get__(self):
            return np.ascontiguousarray(self._V)
        def __set__(self, arr):
            cdef unsigned long ds, p, d
            for ds in range(self.DUMPSIZE):
                for p in range(self.NPAR):
                    for d in range(self.NDIM):
                        self._V[ds,p,d] = arr[ds,p,d]

    property auxiliary:
        def __get__(self):
            return np.ascontiguousarray(self._S)
        def __set__(self, arr):
            cdef unsigned long ia, ds, p, d, idx
            for ia in range(self.NAUX):
                for ds in range(self.DUMPSIZE):
                    for p in range(self.NPAR):
                        for d in range(self.NDIM):
                            self._S[ia,ds,p,d] = arr[ia,ds,p,d]

    property force:
        def __get__(self):
            return np.ascontiguousarray(self._F)
        def __set__(self, arr):
            cdef unsigned long ds, p, d
            for ds in range(self.DUMPSIZE):
                for p in range(self.NPAR):
                    for d in range(self.NDIM):
                        self._F[ds,p,d] = arr[ds,p,d]

    property potential:
        def __get__(self):
            return np.ascontiguousarray(self._U)
        def __set__(self, arr):
            cdef unsigned long ds, p
            for ds in range(self.DUMPSIZE):
                for p in range(self.NPAR):
                    self._U[ds,p] = arr[ds,p]

    property stokes_noise:
        def __get__(self):
            return np.ascontiguousarray(self._GS)
        def __set__(self, arr):
            cdef unsigned long ds, p, d
            for ds in range(self.DUMPSIZE):
                for p in range(self.NPAR):
                    for d in range(self.NDIM):
                        self._GS[ds,p,d] = arr[ds,p,d]

    property basset_noise:
        def __get__(self):
            return np.ascontiguousarray(self._GB)
        def __set__(self, arr):
            cdef unsigned long ds, p, d
            for ds in range(self.DUMPSIZE):
                for p in range(self.NPAR):
                    for d in range(self.NDIM):
                        self._GB[ds,p,d] = arr[ds,p,d]

    property noise:
        def __get__(self):
            return np.ascontiguousarray(self._G)
        def __set__(self, arr):
            cdef unsigned long ds, ia, p, d, idx
            for ds in range(self.DUMPSIZE):
                for ia in range(self.NAUX):
                    for p in range(self.NPAR):
                        for d in range(self.NDIM):
                            self._G[ds,ia,p,d] = arr[ds,ia,p,d]
