#cython: boundscheck=False, wraparound=False, initializedcheck=False, nonecheck=False
#cython: cdivision=True, profile=False, linetrace=False, binding=True, language_level=3

from libc.time cimport time, time_t
from posix.time cimport clock_gettime, timespec, CLOCK_REALTIME

cpdef int time_i():
    '''Cast type time_t to int'''
    return <int>time(NULL)

cpdef float time_f():
    cdef timespec ts
    clock_gettime(CLOCK_REALTIME, &ts)
    return (<double> ts.tv_sec) + (<double> ts.tv_nsec) * 1.0e-9

cpdef double time_d():
    cdef timespec ts
    clock_gettime(CLOCK_REALTIME, &ts)
    return (<double> ts.tv_sec) + (<double> ts.tv_nsec) * 1.0e-9
