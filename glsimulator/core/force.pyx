#cython: boundscheck=False, wraparound=False, initializedcheck=False, nonecheck=False
#cython: cdivision=True, profile=False, linetrace=False, binding=True, language_level=3

cimport cython

import numpy as np
cimport numpy as np
cimport scipy.special.cython_special as cysp

from libc.math cimport sin, cos, asin, acos, sqrt, M_PI

from glsimulator.core.types cimport *

cdef real_t TWO_PI = 2*M_PI

#########################################################################################
## ExternalForce
#########################################################################################
cdef class ExtForce:
    cdef real_t apply(self, real_t X, unsigned long ts):
        return 0

    #WARN: 1D only
    cdef inline real_t potential(self, real_t X):
        return 0

    cpdef real_t F(self, real_t X):
        return self.apply(X, 0)

    cpdef real_t U(self, real_t X):
        return self.potential(X)

#########################################################################################
## Unsorted forcing functions
#########################################################################################
@cython.final
cdef class ConstantForce(ExtForce):
    def __cinit__(self, real_t F0, unsigned long max_steps=-1):
        self.F0 = F0
        self.max_steps = max_steps  # number of steps over which const force is applied

    @cython.final
    cdef inline real_t apply(self, real_t X, unsigned long ts):
        if ts < self.max_steps:
            return self.F0
        else:
            return 0.0

        #WARN: 1D only
    cdef inline real_t potential(self, real_t X):
        return -self.F0*X

@cython.final
cdef class LinearForce(ExtForce):
    def __cinit__(self, real_t F0, real_t F1, unsigned long max_steps=-1):
        self.F0 = F0
        self.F1 = F1
        self.max_steps = max_steps  # number of steps over which linear force is applied

    @cython.final
    cdef inline real_t apply(self, real_t X, unsigned long ts):
        return self.F0 + self.F1*X

    #WARN: 1D only
    cdef inline real_t potential(self, real_t X):
        return -(self.F0 + 0.5*self.F1*X)*X

@cython.final
cdef class LinearlyGrowingForce(ExtForce):
    def __cinit__(self, real_t F0, real_t F1, unsigned long max_steps=-1):
        self.F0 = F0
        self.F1 = F1
        self.max_steps = max_steps  # number of steps over which linear force is applied

    @cython.final
    cdef inline real_t apply(self, real_t X, unsigned long ts):
        if ts < self.max_steps:
            return self.F0 + self.F1*ts
        else:
            return 0.0

@cython.final
cdef class PeriodicSquareWaveForce(ExtForce):
    def __cinit__(self, real_t A, unsigned long period, unsigned long duration,
                  int n_cycles):
        self.A = A
        self.period = period       # period expressed in number of time steps
        self.duration = duration   # "on-time" of force in num time steps
        self.n_cycles = n_cycles   # number of cycles (i.e. square pulses)

    @cython.final
    cdef inline real_t apply(self, real_t X, unsigned long ts):
        if ts % self.period < self.duration and ts / self.period < self.n_cycles:
            return self.A
        else:
            return 0.0

@cython.final
cdef class BurstingSquareWaveForce(ExtForce):
    def __cinit__(self, real_t F0, unsigned long period, unsigned long duration,
                  int n_cycles, unsigned long burst_period, int n_bursts):
        self.F0 = F0
        self.period = period       # period expressed in number of time steps
        self.duration = duration   # duration over which force is applied in # timesteps
        self.n_cycles = n_cycles   # number of cycles (i.e. square pulses)
        self.burst_period = burst_period   # time delay in # time steps between bursts
        self.n_bursts = n_bursts   # number of times to repeat bursts of n_cycles

        self.current_burst = 1
        self.burst_duration = self.period*self.n_cycles

    @cython.final
    cdef inline real_t apply(self, real_t X, unsigned long ts):
        if ( ts % (self.current_burst*self.burst_period) < self.burst_duration \
                and ts % self.period < self.duration ):
            return self.F0
        else:
            if ts >= self.current_burst*self.burst_period:
                # Once the simulation reaches the next burst, increment the burst count
                self.current_burst += 1
            return 0.0

@cython.final
cdef class SinusoidalForce(ExtForce):
    def __cinit__(self, real_t U0, unsigned long T0, real_t F0, unsigned long phi=0):
        self.U0 = U0
        self.F0 = F0    # (positive) constant energy offset
        self.T0 = T0    # period expressed in number of time steps
        self.phi = phi  # phase expressed in number of time steps

        self.omega = TWO_PI/T0

    @cython.final
    cdef inline real_t apply(self, real_t X, unsigned long ts):
        return self.U0 * sin(self.omega*(ts + self.phi)) + self.F0

@cython.final
cdef class TranslatingSpringForce(ExtForce):
    def __cinit__(self, real_t ks, real_t Vs, real_t dt, real_t X0=0,
                  unsigned long max_steps=-1):
        self.ks = ks      # (harmonic) spring constant
        self.Vs = Vs      # translational speed (of moving end of spring)
        self.Xs = X0      # current position (of moving end of spring)
        self.dt = dt      # simulation timestep (needed for translating the spring's end)
        self.dXs = Vs*dt  # step size (for moving end of spring)
        self.ts = 0       # current timestep
        self.max_steps = max_steps  # number of steps over which linear force is applied

    @cython.final
    cdef inline real_t apply(self, real_t X, unsigned long ts):
        cdef real_t Xs = self.Xs
        if ts < self.max_steps:
            # For preventing multiple particles in a sim from updating spring position
            if ts > self.ts:
                self.ts = ts   # update current timestep (particle 1 ends up doing this)
                self.Xs += self.dXs
            return self.ks*(Xs - X)
        else:
            return 0.0

    # May also want to try a one-sided spring so only forward forces applied to particles

    property ks:
        def __get__(self):
            return self.ks

    property Xs:
        def __get__(self):
            return self.Xs

    property Vs:
        def __get__(self):
            return self.Vs

    property dXs:
        def __get__(self):
            return self.dXs

    property ts:
        def __get__(self):
            return self.ts

@cython.final
cdef class PeriodicTranslatingSpringForce(ExtForce):
    def __cinit__(self, real_t ks, real_t Vmax, real_t dt, unsigned long duration,
                  unsigned long period, int n_cycles, real_t X0=0):
        self.duration = duration       # duration (in number timesteps) of pulling phase
        self.period   = period         # overall period (in number timesteps) of tugging
        self.n_cycles = n_cycles       # number of tugs to perform
        self.omega    = M_PI/duration  # angular frequency for pulling phase

        self.ks = ks      # (harmonic) spring constant
        self.Vmax = Vmax  # maximum spring velocity
        self.Xs = X0      # current position (of moving end of spring)
        self.dt = dt      # simulation timestep (needed for translating the spring's end)
        self.ts = 0       # current timestep; avoid multiple updates in parallel sims

    @cython.final
    cdef inline real_t apply(self, real_t X, unsigned long ts):
        self.Xs += self.get_spring_increment(ts)
        if self.Xs > X:
            return self.ks*(self.Xs - X)
        return 0.0

    @cython.final
    cdef inline real_t get_spring_increment(self, unsigned long ts):
        if ts / self.period < self.n_cycles:
            # allow only first particle in parallel sim to update spring position (for
            #   rest of particles)
            if ts > self.ts:
                self.ts = ts   # update current timestep (particle 1 ends up doing this)
                return self.get_spring_velocity(ts) * self.dt
        return 0.0

    @cython.final
    cdef inline real_t get_spring_velocity(self, unsigned long ts):
        cdef long ts_c = ts % self.period
        if ts_c < self.duration:
            return self.Vmax * sin( self.omega*ts_c )
        else:
            return 0.0

    # May also want to try a one-sided spring so only forward forces applied to particles

    property ks:
        def __get__(self):
            return self.ks

    property Xs:
        def __get__(self):
            return self.Xs

    property duration:
        def __get__(self):
            return self.duration

    property period:
        def __get__(self):
            return self.period

    property n_cycles:
        def __get__(self):
            return self.n_cycles

    property ts:
        def __get__(self):
            return self.ts

#########################################################################################
## Spatially periodic forcing functions
#########################################################################################
cdef class BiasedPeriodicForce(ExtForce):
    def __init__(self, real_t tilt=1.0, real_t wavelength=1.0, real_t phase=0.0,
                 real_t U0=0.0, unsigned long max_steps=-1):
        self.tilt = tilt            # avg force over one period (tilted potential slope)
        self.L    = wavelength      # spatial wavelength of potential
        self.phi  = phase           # phase in radians (pi --> x=0 local min; 0, x=0 max)
        self.U0   = U0              # reference value of potential
        self.max_steps = max_steps  # number of steps over which linear force is applied

        self.dU = self.tilt * self.L  # delta U (potential drop) over 1 wavelength
        self.iL = 1.0 / self.L
        self.phio2PI = self.phi / TWO_PI

    @cython.final
    cdef inline real_t periodize(self, real_t X):
        return (self.iL*X + self.phio2PI) % 1.0

cdef class WashboardForce(BiasedPeriodicForce):
    def __init__(self, real_t tilt=1.0, real_t wavelength=1.0, real_t phase=0.0,
                 real_t U0=0.0, unsigned long max_steps=-1, real_t amplitude=1.0):
        super(WashboardForce, self).__init__(tilt=tilt, wavelength=wavelength,
                                             phase=phase, U0=U0, max_steps=max_steps)
        self.A  = amplitude         # amplitude of sinusoidal force component
        self.k   = TWO_PI/self.L    # wavenumber
        self.Aok = self.A/self.k    # amplitude of derivative

    cdef inline real_t apply(self, real_t X, unsigned long ts):
        return self.A*sin(self.k*X + self.phi) + self.tilt

    #WARN: 1D only
    cdef inline real_t potential(self, real_t X):
        return self.Aok*cos(self.k*X + self.phi) - self.tilt*X

    cpdef real_t F(self, real_t X):
        return self.apply(X, 0)

    cpdef real_t U(self, real_t X):
        return self.potential(X)

cdef class ModifiedWashboard(BiasedPeriodicForce):
    def __init__(self, real_t tilt=1.0, real_t wavelength=1.0, real_t phase=0.0,
                 real_t U0=0.0, unsigned long max_steps=-1, real_t amplitude=1.0,
                 real_t sharpness=0.9):
        super(ModifiedWashboard, self).__init__(tilt=tilt, wavelength=wavelength,
                                                phase=phase, U0=U0, max_steps=max_steps)
        self.k   = TWO_PI/self.L         # wavenumber
        self.s   = 1 - (1-sharpness)**4  # 0: soft (washboard), 1: sharp (triangle wave)
        self.phi = phase - TWO_PI/4  # shift right by 1/4 wavelength to match washboard
        #----------------------------------------
        self.A   = amplitude            # F-matched scaling (U ampl incr w/ sharpness)
        # self.A  *= self.s/asin(self.s)  # U-matched scaling (F ampl decr w/ sharpness)
        self.AU  = -self.A / ( self.k*self.s )
        self.U0  = -self.AU * asin( self.s*sin(self.phi) )


    cdef inline real_t apply(self, real_t X, unsigned long ts):
        cdef real_t kX_phi = self.k*X + self.phi
        return self.A * cos(kX_phi) * ( 1-(self.s*sin(kX_phi))**2 )**(-0.5) + self.tilt

    #WARN: 1D only
    cdef inline real_t potential(self, real_t X):
        return self.AU*asin( self.s*sin(self.k*X + self.phi) ) - self.tilt*X + self.U0

    cpdef real_t F(self, real_t X):
        return self.apply(X, 0)

    cpdef real_t U(self, real_t X):
        return self.potential(X)

cdef class GapPotentialForce(BiasedPeriodicForce):
    def __init__(self, real_t tilt=1.0, real_t wavelength=1.0, real_t phase=0.0,
                 real_t U0=0.0, unsigned long max_steps=-1, real_t gap_frac=0.5,
                 real_t bias=0.0):
        super(GapPotentialForce, self).__init__(tilt=tilt, wavelength=wavelength,
                                                phase=phase, U0=U0, max_steps=max_steps)
        self.fr_gap = gap_frac   # extent of gap region as fraction of wavelength
        self.B = bias           # (constant) force bias (sets lowest force value)

        self.fr_slope = 1.0 - self.fr_gap  # sloped portion as fraction of wavelength
        self.Fslope   = self.tilt / self.fr_slope + self.B
        self.Fgap     = self.B

    cdef inline real_t apply(self, real_t X, unsigned long ts):
        return self.Fslope if self.periodize(X) < self.fr_slope else self.Fgap

    #WARN: 1D only
    cdef inline real_t potential(self, real_t X):
        n_wav, fr_wav = divmod(X, self.L)
        fr_dU = fr_wav * self.Fslope if fr_wav < self.fr_slope else self.dU
        return self.U0 - n_wav * self.dU - fr_dU

    cpdef real_t F(self, real_t X):
        return self.apply(X, 0)

    cpdef real_t U(self, real_t X):
        return self.potential(X)

cdef class AsymmetricGapPotentialForce(GapPotentialForce):
    def __init__(self, real_t tilt=1.0, real_t wavelength=1.0, real_t phase=0.0,
                 real_t U0=0.0, unsigned long max_steps=-1, real_t gap_frac=0.5,
                 real_t bias=0.0, real_t X0=0.0):
        super(AsymmetricGapPotentialForce, self).__init__(tilt=tilt, wavelength=wavelength,
                                                phase=phase, U0=U0, max_steps=max_steps,
                                                gap_frac=gap_frac, bias=bias)
        self.X0 = X0   # position below which potential is just average slope

    cdef inline real_t apply(self, real_t X, unsigned long ts):
        if X < self.X0:
            return self.tilt
        else:
            return self.Fslope if self.periodize(X) < self.fr_slope else self.Fgap

        #WARN: 1D only
    cdef inline real_t potential(self, real_t X):
        if X < self.X0:
            return self.U0 - self.tilt*X
        else:
            n_wav, fr_wav = divmod(X, self.L)
            fr_dU = fr_wav * self.Fslope if fr_wav < self.fr_slope else self.dU
            return self.U0 - n_wav * self.dU - fr_dU

    cpdef real_t F(self, real_t X):
        return self.apply(X, 0)

    cpdef real_t U(self, real_t X):
        return self.potential(X)

# cdef class SkewedWashboardForce(WashboardForce):
#     def __init__(self, real_t tilt=1.0, real_t wavelength=1.0, real_t phase=0.0,
#                  real_t U0=0.0, unsigned long max_steps=-1, real_t amplitude=1.0,
#                  unsigned long N):
#         cdef unsigned long n, i
#
#         super(WashboardForce, self).__init__(tilt=tilt, wavelength=wavelength,
#                                              phase=phase, U0=U0, max_steps=max_steps)
#         self.N  = N         # number of sinusoidal components -- alters skewness
#         self.C  = np.zeros((N,), dtype=np.floating)
#         self.nk = np.zeros((N,), dtype=np.floating)
#
#         for i in range(self.N):
#             n = i + 1
#             self.nk[i] = n * self.k
#             self.C[i] = self.A * cysp.binom(2*N, N-n) / ( n*cysp.binom(2*N, N) )
#
#     cdef inline real_t apply(self, real_t X, unsigned long ts):
#         cdef unsigned long n, i
#         f_total = 0
#         for i in range(self.N):
#             f_total += self.C[i] * sin(self.kn[i]*X + self.phi)
#         return f_total + self.tilt
#
#     #WARN: 1D only
#     cdef inline real_t potential(self, real_t X):
#         return self.Aok*cos(self.k*X + self.phi) - self.tilt*X

@cython.final
cdef class ButterworthForce(BiasedPeriodicForce):
    def __init__(self, real_t tilt=1.0, real_t wavelength=1.0, real_t phase=0.0,
                 real_t U0=0.0, unsigned long max_steps=-1, real_t gap_frac=0.5,
                 real_t bias=0.0, real_t sharpness=100.0):
        super(ButterworthForce, self).__init__(tilt, wavelength, phase, U0, max_steps)
        # Set force parameters
        self.fr_gap = gap_frac          # gap portion as fraction of wavelength [0,1]
        self.B = bias               # (constant) force bias (sets lowest force value)
        self.S = sharpness          # sharpness of potential; large S approaches square

        # Set derived parameters
        # self.W = self.r*self.L      # Width of square shape about its "center"
        # self.k = TWO_PI / self.L    # wavenumber

        self.iL = 1.0 / self.L
        self.fr_slope = 1.0 - self.fr_gap  # sloped portion as fraction of wavelength
        self.ifr_slope = 1.0 / self.fr_slope
        self.tiltosf = self.A * self.ifr_slope  # peak amplitude of Butterworth force

        self.phio2PI = self.phi / TWO_PI

    @cython.final
    cdef inline real_t apply(self, real_t X, unsigned long ts):
        if ts < self.max_steps:
            return self.tiltosf / (1 + (self.periodize(X)*self.ifr_slope)**self.S) + self.B
        else:
            return 0.0

        #WARN: 1D only
    cdef inline real_t potential(self, real_t X):
        return 0.0

    cpdef real_t F(self, real_t X):
        return self.apply(X, 0)

    cpdef real_t U(self, real_t X):
        return self.potential(X)
