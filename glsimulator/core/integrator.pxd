#cython: boundscheck=False, wraparound=False, initializedcheck=False, nonecheck=False
#cython: cdivision=True, profile=False, linetrace=False, binding=True, language_level=3

cimport cython

from glsimulator.core.types cimport *
from glsimulator.core.glrandom cimport RNGContainer
from glsimulator.core.system cimport System, Particle
from glsimulator.core.force cimport ExtForce
#############################################################

cdef real_t sum_c(real_t[:] arr) nogil
cdef real_t wsum_c(real_t[:] arr, real_t[::1] wgt) nogil

################################################################################
## Integrator base extension type
################################################################################
cdef class Integrator:
    cdef:
        unsigned long NPAR, NAUX, NDIM, NQ, NQF, NQU
        real_t _dt, _m, _kT
        real_t gams, thts, sigs, tsm1        # impulse velocity (tsm1 = thetas-1)
        real_t gam0, tht0, sig0, t0m1        # impulse velocity (t0m1 = theta0-1)
        real_t[::1] gam, nu, tht, sig, tm1g  # auxiliary advance (tm1g = (theta-1)*gam0)
        real_t[::1] sqgig0, dtnu, dti2nu     # noise weights: sqrt(gam[i]/gam0)
        str _name
        Particle _particle
        ExtForce _force
        RNGContainer _rngc
        object prng
    cpdef set_seed(self, seed)
    cpdef set_jump(self, jump)
    cdef void advance(self, real_t[:,:,::1] Q, real_t[:,:,::1] QF, real_t[:,::1] QU,
                      unsigned long ts)

#########################################################################################
#########################################################################################
# NOTE: Working integrators
#########################################################################################
#########################################################################################

################################################################################
## Stochastic integrators for conventional (underdamped) Langevin dynamics
################################################################################
cdef class EulerIntegratorLE(Integrator):
    cdef void advance(self, real_t[:,:,::1] Q, real_t[:,:,::1] QF, real_t[:,::1] QU,
                      unsigned long ts)

cdef class HeunIntegratorLE(Integrator):
    cdef void advance(self, real_t[:,:,::1] Q, real_t[:,:,::1] QF, real_t[:,::1] QU,
                      unsigned long ts)

cdef class ImpulseIntegratorLE(Integrator):
    cdef void advance(self, real_t[:,:,::1] Q, real_t[:,:,::1] QF, real_t[:,::1] QU,
                      unsigned long ts)

################################################################################
## Stochastic integrators for fluctuating BBO dynamics
################################################################################
cdef class EulerIntegratorFBBO(Integrator):
    cdef gam0s, im
    cdef real_t[::1] S, W, G
    cdef void advance(self, real_t[:,:,::1] Q, real_t[:,:,::1] QF, real_t[:,::1] QU,
                      unsigned long ts)

cdef class HeunIntegratorFBBO(Integrator):
    cdef gam0s, dti2, im
    cdef real_t[::1] S, Sh, Sm, W, G
    cdef void advance(self, real_t[:,:,::1] Q, real_t[:,:,::1] QF, real_t[:,::1] QU,
                      unsigned long ts)

################################################################################
## Stochastic integrator for overdamped Langevin dynamics (Brownian dynamics)
################################################################################
cdef class EulerIntegratorBD(Integrator):
    cdef real_t igams, izetas
    cdef void advance(self, real_t[:,:,::1] Q, real_t[:,:,::1] QF, real_t[:,::1] QU,
                      unsigned long ts)


#########################################################################################
#########################################################################################
# WARN: Untested integrators
#########################################################################################
#########################################################################################

################################################################################
## Stochastic integrators for conventional (underdamped) Langevin dynamics
################################################################################
cdef class LangevinOVRVO(Integrator):
    cdef real_t b, bdt, coef
    cdef void advance(self, real_t[:,:,::1] Q, real_t[:,:,::1] QF, real_t[:,::1] QU,
                      unsigned long ts)

################################################################################
## Stochastic integrators for fluctuating BBO dynamics
################################################################################
cdef class ImpulseIntegratorFBBO(Integrator):
    cdef void advance(self, real_t[:,:,::1] Q, real_t[:,:,::1] QF, real_t[:,::1] QU,
                      unsigned long ts)

cdef class ImpulseIntegratorStableFBBO(Integrator):
    cdef void advance(self, real_t[:,:,::1] Q, real_t[:,:,::1] QF, real_t[:,::1] QU,
                      unsigned long ts)

cdef class ImpulseIntegratorFBBOMod(Integrator):
    cdef void advance(self, real_t[:,:,::1] Q, real_t[:,:,::1] QF, real_t[:,::1] QU,
                      unsigned long ts)

cdef class GeneralizedOVRVO(Integrator):
    cdef real_t b, bdt, coef
    cdef void advance(self, real_t[:,:,::1] Q, real_t[:,:,::1] QF, real_t[:,::1] QU,
                      unsigned long ts)


#########################################################################################
#########################################################################################
#########################################################################################
## Unsorted miscellaneous Integrators
#########################################################################################
#########################################################################################
#########################################################################################

#================================================
## EulerIntegratorFBBO_Old
#================================================
cdef class EulerIntegratorFBBO_Old(Integrator):
    cdef gam0s, im
    cdef real_t[::1] S, W, G
    cdef void advance(self, real_t[:,:,::1] Q, real_t[:,:,::1] QF, real_t[:,::1] QU,
                      unsigned long ts)

#================================================
## ImpulseIntegratorLEV2 (conventional Langevin)
#================================================
cdef class ImpulseIntegratorLEV2(Integrator):
    cdef void advance(self, real_t[:,:,::1] Q, real_t[:,:,::1] QF, real_t[:,::1] QU,
                      unsigned long ts)

#================================================
## EulerIntegrator1AUXFast
#================================================
cdef class EulerIntegrator1AUXFast(Integrator):
    cdef void advance(self, real_t[:,:,::1] Q, real_t[:,:,::1] QF, real_t[:,::1] QU,
                      unsigned long ts)

#================================================
## EulerIntegratorBasset
#================================================
cdef class EulerIntegratorBasset(Integrator):
    cdef void advance(self, real_t[:,:,::1] Q, real_t[:,:,::1] QF, real_t[:,::1] QU,
                      unsigned long ts)

cdef class EulerIntegratorBassetV2(Integrator):
    cdef void advance(self, real_t[:,:,::1] Q, real_t[:,:,::1] QF, real_t[:,::1] QU,
                      unsigned long ts)

cdef class EulerIntegratorBassetV3(Integrator):
    cdef void advance(self, real_t[:,:,::1] Q, real_t[:,:,::1] QF, real_t[:,::1] QU,
                      unsigned long ts)

#================================================
## ImpulseIntegratorBasset
#================================================
cdef class ImpulseIntegratorBasset(Integrator):
    cdef void advance(self, real_t[:,:,::1] Q, real_t[:,:,::1] QF, real_t[:,::1] QU,
                      unsigned long ts)

cdef class ImpulseIntegratorBassetV2(Integrator):
    cdef void advance(self, real_t[:,:,::1] Q, real_t[:,:,::1] QF, real_t[:,::1] QU,
                      unsigned long ts)

cdef class ImpulseIntegratorBassetV3(Integrator):
    cdef void advance(self, real_t[:,:,::1] Q, real_t[:,:,::1] QF, real_t[:,::1] QU,
                      unsigned long ts)

cdef class ImpulseIntegratorBassetV4(Integrator):
    cdef void advance(self, real_t[:,:,::1] Q, real_t[:,:,::1] QF, real_t[:,::1] QU,
                      unsigned long ts)

cdef class ImpulseIntegratorBassetBac(Integrator):
    cdef void advance(self, real_t[:,:,::1] Q, real_t[:,:,::1] QF, real_t[:,::1] QU,
                      unsigned long ts)
