#cython: boundscheck=False, wraparound=False, initializedcheck=False, nonecheck=False
#cython: cdivision=True, profile=False, linetrace=False, binding=True, language_level=3

import numpy as np
cimport numpy as np
from cython cimport view

from glsimulator.core.types      cimport *
# from glsimulator.core.glrandom   cimport *  #RNGContainer
from glsimulator.core.sim_state  cimport *  #State
from glsimulator.core.system     cimport *  #System
from glsimulator.core.force      cimport *  #ExtForce
from glsimulator.core.integrator cimport *  #Integrator
#############################################################

# cdef struct Q:
#     real_t[::1] X
#     real_t[::1] V
#     real_t[:,::1] S
#     real_t[::1] F
#     real_t[::1] U

################################################################################
## Simulation
################################################################################
cdef class Simulation:
    cdef:
        System _sys
        Integrator _integrator
        State _state

        unsigned long NQ, NQF, NQU
        real_t[:,:,::1] _Q   # array of (extended) phase space vars (X, V, S_i)
        real_t[:,:,::1] _QF  # array of (external) forces and thermal noises
        real_t[:,::1]   _QU  # array of internal and external potential energies

        str filename, outdir

    cpdef run(self, unsigned long long NSTEPS, unsigned long NTOUT,
              bint equilibration=*, long long NTDUMP=*, int print_step_delay=*,
              double print_interval=*, bint store=*)

    cpdef save_state(self, real_t[:,:,::1] _Q, real_t[:,:,::1] _QF, real_t[:,::1] _QU,
                     unsigned long ts)

    # def initialize(self, X=*, V=*, F=*, U=*, S=*, G=*, bint equilibration=*,
    #                  bint continuation=*, real_t kT=*, real_t mass=*,
    #                  bint init_aux=*, unsigned long long seed=*,
    #                  unsigned long long jump=*)

    cpdef init_positions(self, X=*)
    cpdef init_velocities(self, V=*, real_t kT=*, real_t mass=*,
                          unsigned long long seed=*, unsigned long long jump=*)
    cpdef init_forces(self, F=*, U=*)
    cpdef init_auxiliaries(self, S=*, real_t kT=*)
    cpdef init_noises(self, G=*, unsigned long long seed=*, unsigned long long jump=*)
    cpdef maxwellian_velocities(self, real_t kT, real_t mass)
    cpdef init_state(self, unsigned long long NSTEPS, unsigned long NTOUT,
                     unsigned long long NTDUMP, bint equilibration=*)
