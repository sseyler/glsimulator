#cython: boundscheck=False, wraparound=False, initializedcheck=False, nonecheck=False
#cython: cdivision=True, profile=False, linetrace=False, binding=True, language_level=3

from glsimulator.core.types cimport *

################################################################################
## Force
################################################################################
cdef class ExtForce:
    cdef:
        unsigned long max_steps
    cdef real_t apply(self, real_t X, unsigned long ts)
    cdef real_t potential(self, real_t X)
    cpdef real_t F(self, real_t X)
    cpdef real_t U(self, real_t X)

cdef class ConstantForce(ExtForce):
    cdef:
        real_t F0
    cdef real_t apply(self, real_t X, unsigned long ts)
    cdef real_t potential(self, real_t X)

cdef class LinearForce(ExtForce):
    cdef:
        real_t F0, F1
    cdef real_t apply(self, real_t X, unsigned long ts)
    cdef real_t potential(self, real_t X)

###########################################################
## Time-dependent forcing functions
###########################################################
cdef class LinearlyGrowingForce(ExtForce):
    cdef:
        real_t F0, F1
    cdef real_t apply(self, real_t X, unsigned long ts)

cdef class PeriodicSquareWaveForce(ExtForce):
    cdef:
        real_t A
        unsigned long period, duration
        int n_cycles
    cdef real_t apply(self, real_t X, unsigned long ts)

cdef class BurstingSquareWaveForce(ExtForce):
    cdef:
        real_t F0
        unsigned long period, duration, burst_period, burst_duration
        int n_cycles, n_bursts, current_burst
    cdef real_t apply(self, real_t X, unsigned long ts)

cdef class SinusoidalForce(ExtForce):
    cdef:
        real_t U0, F0, omega
        unsigned long T0, phi
    cdef real_t apply(self, real_t X, unsigned long ts)

cdef class TranslatingSpringForce(ExtForce):
    cdef:
        real_t ks, Vs, Xs, dt, dXs
        unsigned long ts
    cdef real_t apply(self, real_t X, unsigned long ts)

cdef class PeriodicTranslatingSpringForce(ExtForce):
    cdef:
        real_t ks, Vmax, Xs, dt, omega
        unsigned long ts, duration, period, n_cycles
    cdef real_t apply(self, real_t X, unsigned long ts)
    cdef real_t get_spring_increment(self, unsigned long ts)
    cdef real_t get_spring_velocity(self, unsigned long ts)

###########################################################
## Spatially periodic forcing functions
###########################################################
cdef class BiasedPeriodicForce(ExtForce):
    cdef:
        real_t tilt, U0, L, phi, dU, iL, phio2PI
    cdef real_t apply(self, real_t X, unsigned long ts)
    cdef real_t potential(self, real_t X)
    cdef real_t periodize(self, real_t)
    cpdef real_t F(self, real_t X)
    cpdef real_t U(self, real_t X)

cdef class WashboardForce(BiasedPeriodicForce):
    cdef:
        real_t A, k, Aok

cdef class ModifiedWashboard(BiasedPeriodicForce):
    cdef:
        real_t A, k, s, AU

cdef class GapPotentialForce(BiasedPeriodicForce):
    cdef:
        real_t fr_gap, fr_slope, B, Fslope, Fgap

cdef class AsymmetricGapPotentialForce(GapPotentialForce):
    cdef:
        real_t X0

# cdef class SkewedWashboardForce(WashboardForce):
#     cdef:
#         unsigned long N
#         real_t[::1] C, nk

cdef class ButterworthForce(BiasedPeriodicForce):
    cdef:
        real_t fr_gap, fr_slope, ifr_slope, B, S, tiltosf
