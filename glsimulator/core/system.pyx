#cython: boundscheck=False, wraparound=False, initializedcheck=False, nonecheck=False
#cython: cdivision=True, profile=False, linetrace=False, binding=True, language_level=3

import numpy as np
cimport numpy as np
cimport cython
from cython cimport floating
from libc.math cimport sqrt

from glsimulator.core.types cimport *
from glsimulator.core.force cimport ExtForce
#############################################################


################################################################################
## System
################################################################################
cdef class System:
    def __cinit__(self, unsigned long NDIM, unsigned long NAUX, real_t kT):
        self.NDIM = NDIM
        self.NPAR = 0
        self.NAUX = NAUX
        self.NQ  = SP + self.NAUX
        self.NQF = GP + self.NAUX
        self.NQU = UP + 1  # need to change if other forces added

    def __init__(self, unsigned long NDIM, unsigned long NAUX, real_t kT):
        self._kT = kT
        self.particle_list = []
        self.force_list = []

    cpdef add_N_particles(self, unsigned long N, real_t mass, real_t radius,
                          real_t gamma_s, real_t gamma_0,
                          real_t[::1] gamma, real_t[::1] nu):
        cdef unsigned long i
        cdef Particle p

        for i in range(N):
            p = Particle(self.NAUX)
            p.mass    = mass
            p.radius  = radius
            p.gamma_s = gamma_s
            p.gamma_0 = gamma_0
            p.gamma   = gamma
            p.nu      = nu
            self.add_particle(p)

    cpdef add_particle(self, Particle p):
        self.particle_list.append(p)
        self.NPAR = self.n_particles

    cpdef add_force(self, ExtForce f):
        self.force_list.append(f)

    cpdef remove_force(self, unsigned long i=0):
        self.force_list.pop(i)

    ######################################################################
    # properties
    #----------------------------------
    property force_list:
        def __get__(self):
            return self.force_list

    property particle_list:
        def __get__(self):
            return self.particle_list

    property n_particles:
        def __get__(self):
            return len(self.particle_list)

    property n_spatial_dimensions:
        def __get__(self):
            return self.NSIM
        def __set__(self, N):
            self.NDIM = N

    property n_aux_variables:
        def __get__(self):
            return self.NAUX
        def __set__(self, N):
            self.NAUX = N
            self.NQ = SP + N

    property n_variables:
        def __get__(self):
            return self.NQ
        def __set__(self, N):
            self.NQ   = N
            self.NAUX = N - SP

    property kT:
        def __get__(self):
            return self._kT
        def __set__(self, kT):
            self._kT = kT

# cdef class Particle:
#     def __cinit__(self, unsigned long NAUX):
#         self.NAUX = NAUX
#         self.mass = 0
#         self.radius = 0
#         self.gamma_s = 0
#         self.gamma_0 = 0
#         self.gamma = np.zeros((NAUX), dtype=np.floating)
#         self.nu    = np.zeros((NAUX), dtype=np.floating)

cdef class Particle:
    def __init__(self, unsigned long NAUX):
        self._NAUX = NAUX
        self._mass = 0
        self._radius = 0
        self._gamma_s = 0
        self._gamma_0 = 0
        self._gamma = np.zeros((NAUX), dtype=np.floating)
        self._nu    = np.zeros((NAUX), dtype=np.floating)

    property mass:
        def __get__(self):
            return self._mass
        def __set__(self, mass):
            self._mass = mass

    property radius:
        def __get__(self):
            return self._radius
        def __set__(self, radius):
            self._radius = radius

    property gamma_s:
        def __get__(self):
            return self._gamma_s
        def __set__(self, gamma_s):
            self._gamma_s = gamma_s

    property gamma_0:
        def __get__(self):
            return self._gamma_0
        def __set__(self, gamma_0):
            self._gamma_0 = gamma_0

    property gamma:
        def __get__(self):
            return np.ascontiguousarray(self._gamma)
        def __set__(self, arr):
            cdef unsigned long ia
            for ia in range(self._NAUX):
                self._gamma[ia] = arr[ia]

    property nu:
        def __get__(self):
            return np.ascontiguousarray(self._nu)
        def __set__(self, arr):
            cdef unsigned long ia
            for ia in range(self._NAUX):
                self._nu[ia] = arr[ia]
