#cython: boundscheck=False, wraparound=False, initializedcheck=False, nonecheck=False
#cython: cdivision=True, profile=False, linetrace=False, binding=True, language_level=3

cpdef int time_i()

cpdef float time_f()

cpdef double time_d()
