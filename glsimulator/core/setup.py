import sys
import numpy as np
# from scipy._build_utils import numpy_nodepr_api  # silence numpy API warnings

from distutils.core import setup
from distutils.extension import Extension
try:
    from Cython.Build import cythonize
    from Cython.Distutils import build_ext
except Exception as e:
    print("Caught exception: {}".format(e))
    print("You don't seem to have Cython installed. Please get a")
    print("copy from www.cython.org and install it")
    sys.exit(1)

import os
from os.path import join
import platform
# import socket
# import cython_gsl

###############################################################################
# Remove REALLY irritating -Wstrict-prototypes warnings for C++ compiling
#   See: https://tinyurl.com/y7tbdj6u
import distutils.sysconfig
cfg_vars = distutils.sysconfig.get_config_vars()
for key, value in cfg_vars.items():
    if type(value) == str:
        cfg_vars[key] = value.replace("-Wstrict-prototypes", "")
###############################################################################


###############################################################################
# Set up compiler, environment, libraries, etc.
#############################
NB_COMPILE_JOBS = 4
COMPILER = 'gnu'
HOSTNAME = platform.node()  # socket.hostname() -- slower

if HOSTNAME == 'seylermoon':
    if COMPILER == 'gnu':
        os.environ["CC"] = "gcc-7"
        os.environ["CXX"] = "g++-7"
        EXTRA_COMPILE_ARGS = ['-march=native', '-O3', '-ffast-math',
                              '-ftree-vectorizer-verbose=2', '-mavx',
                              '-mprefer-avx128', '-Wno-cpp']
    MINICONDA_INCLUDE_DIR = '/home/sseyler/Library/miniconda2/include'
elif HOSTNAME == 'seylerpluto':
    if COMPILER == 'gnu':
        os.environ["CC"] = "gcc"
        os.environ["CXX"] = "g++"
        EXTRA_COMPILE_ARGS = ['-march=native', '-O3', '-flto', '-Wall',
                              '-ffast-math']
        # '-mavx2', '-march=znver1',
        # '-ftree-vectorizer-verbose=2', '-mavx',
        # '-mprefer-avx128', '-flto=`nproc`',
        # '-fgraphite-identity',
        # '-ftree-loop-distribution',
        # '-floop-nest-optimize']
    if COMPILER == 'intel':
        os.environ["CC"] = "icc"    # "gcc"
        os.environ["CXX"] = "icpc"  # "g++"
        os.environ["LINKCC"] = "icc"
        os.environ["LDSHARED"] = "icc -shared"
        EXTRA_COMPILE_ARGS = ['-xHost', '-fast', '-qopt-report=4']
    MINICONDA_INCLUDE_DIR = '/home/sseyler/Library/miniconda/include'

# '-std=c++11', -fopenmp, -shared -pthread -fPIC -fwrapv -fno-strict-aliasing


###############################################################################
# Specify sources
#############################
src_glsim      = ['glsim.pyx']
src_integrator = ['integrator.pyx']
src_sim_system = ['sim_system.pyx']
src_sim_state  = ['sim_state.pyx']
src_force      = ['force.pyx']
src_glrandom   = ['glrandom.pyx']
src_types      = ['types.pyx']
# Specify module names (names of *.so files)
modname_glsim      = 'glsim'
modname_integrator = 'integrator'
modname_sim_system = 'sim_system'
modname_sim_state  = 'sim_state'
modname_force      = 'force'
modname_glrandom   = 'glrandom'
modname_types      = 'types'

RANDOMGEN_INCLUDE_DIR = join('/home', 'sseyler', 'Repositories', 'python',
                             'randomgen', 'randomgen')
src_randomgen = [join(RANDOMGEN_INCLUDE_DIR, 'src', 'distributions',
                      'distributions.c')]

GENERAL_INCLUDE_DIRS = ['.', '...', os.path.join(os.getcwd(), 'include'),
                        np.get_include(), MINICONDA_INCLUDE_DIR,
                        RANDOMGEN_INCLUDE_DIR]
GENERAL_LIBRARY_DIRS = [os.getcwd(), ]  # path to .a or .so file(s   )
GENERAL_LIBRARIES = ['m']  # compile modules

###############################################################################
# Set up and build extensions
#############################
N_ext = 7
iext = 1

print('[{}/{}] Generating Cython extension {}'.format(iext, N_ext,
                                                      src_glsim[0]))
ext_glsim = Extension(modname_glsim,
                      sources=src_glsim,
                      include_dirs=GENERAL_INCLUDE_DIRS,
                      libraries=GENERAL_LIBRARIES + ['gsl', 'gslcblas'],
                      library_dirs=GENERAL_LIBRARY_DIRS,
                      extra_compile_args=EXTRA_COMPILE_ARGS,
                      # language='c++'  # randomgen only compiles w/ gcc
                      )

iext += 1
print('[{}/{}] Generating Cython extension {}'.format(iext, N_ext,
                                                      src_integrator[0]))
ext_integrator = Extension(modname_integrator,
                           sources=src_integrator,
                           include_dirs=GENERAL_INCLUDE_DIRS,
                           libraries=GENERAL_LIBRARIES + ['gsl', 'gslcblas'],
                           library_dirs=GENERAL_LIBRARY_DIRS,
                           extra_compile_args=EXTRA_COMPILE_ARGS,
                           # language='c++'  # randomgen only compiles w/ gcc
                           )

iext += 1
print('[{}/{}] Generating Cython extension {}'.format(iext, N_ext,
                                                      src_sim_system[0]))
ext_sim_system = Extension(modname_sim_system,
                           sources=src_sim_system,
                           include_dirs=GENERAL_INCLUDE_DIRS,
                           libraries=GENERAL_LIBRARIES,
                           library_dirs=GENERAL_LIBRARY_DIRS,
                           extra_compile_args=EXTRA_COMPILE_ARGS,
                           # language='c++'
                           )

iext += 1
print('[{}/{}] Generating Cython extension {}'.format(iext, N_ext,
                                                      src_sim_state[0]))
ext_sim_state = Extension(modname_sim_state,
                          sources=src_sim_state,
                          include_dirs=GENERAL_INCLUDE_DIRS,
                          libraries=GENERAL_LIBRARIES,
                          library_dirs=GENERAL_LIBRARY_DIRS,
                          extra_compile_args=EXTRA_COMPILE_ARGS,
                          # language='c++'
                          )

iext += 1
print('[{}/{}] Generating Cython extension {}'.format(iext, N_ext,
                                                      src_force[0]))
ext_force = Extension(modname_force,
                      sources=src_force,
                      include_dirs=GENERAL_INCLUDE_DIRS,
                      libraries=GENERAL_LIBRARIES,
                      library_dirs=GENERAL_LIBRARY_DIRS,
                      extra_compile_args=EXTRA_COMPILE_ARGS,
                      # language='c++'
                      )

iext += 1
print('[{}/{}] Generating Cython extension {}'.format(iext, N_ext,
                                                      src_glrandom[0]))
ext_glrandom = Extension(modname_glrandom,
                         sources=src_glrandom + src_randomgen,
                         include_dirs=GENERAL_INCLUDE_DIRS,
                         libraries=GENERAL_LIBRARIES + ['gsl', 'gslcblas'],
                         library_dirs=GENERAL_LIBRARY_DIRS,
                         extra_compile_args=EXTRA_COMPILE_ARGS,
                         # language='c++'  # randomgen only compiles w/ gcc
                         )

iext += 1
print('[{}/{}] Generating Cython extension {}'.format(iext, N_ext,
                                                      src_types[0]))
ext_types = Extension(modname_types,
                      sources=src_types,
                      include_dirs=GENERAL_INCLUDE_DIRS,
                      libraries=GENERAL_LIBRARIES,
                      library_dirs=GENERAL_LIBRARY_DIRS,
                      extra_compile_args=EXTRA_COMPILE_ARGS,
                      # language='c++'
                      )

EXTENSIONS = [ext_types, ext_glrandom, ext_force, ext_sim_state,
              ext_sim_system, ext_integrator, ext_glsim]


def setup_given_extensions(extensions):
    setup(name='glsimulator',
          cmdclass={"build_ext":  build_ext},
          ext_modules=cythonize(extensions)
          )


def setup_extensions_in_sequential():
    setup_given_extensions(EXTENSIONS)


def setup_extensions_in_parallel():
    import multiprocessing as mp
    cythonize(EXTENSIONS, nthreads=NB_COMPILE_JOBS)
    pool = mp.Pool(processes=NB_COMPILE_JOBS)
    pool.map(setup_given_extensions, EXTENSIONS)
    pool.close()
    pool.join()


print('----------------------------------------------------------------------')
if "build_ext" in sys.argv:
    print('Generating Cython extensions in parallel...\n')
    setup_extensions_in_parallel()
else:
    print('Generating Cython extensions...\n')
    if False:
        setup(name='glsimulator',
              # packages = ["glsimulator", "glsimulator.core",
              # "glsimulator.utils"],
              cmdclass={"build_ext":  build_ext},
              ext_modules=EXTENSIONS
              # ext_modules = cythonize("**/*.pyx")
              )
    else:
        setup_extensions_in_sequential()
