#cython: boundscheck=False, wraparound=False, initializedcheck=False, nonecheck=False
#cython: cdivision=True, profile=False, linetrace=False, binding=True, language_level=3

# from libcpp.string cimport string

cdef class CProgressMeter:
    cdef:
        unsigned long DATASIZE
        int width
        double start, end, msecs, secs, mins, hours
        double t_eta_nextout, print_interval, eta_print_interval
        bint verbose
        str unit, bar, status, etatxt

cdef class ProgressMeter(CProgressMeter):
    cpdef update(self, unsigned long step, double curr_time=*)
    cpdef str _update_text(self, double frac_done, double t_now)
    cpdef printer(self, str data)
