#cython: boundscheck=False, wraparound=False, initializedcheck=False, nonecheck=False
#cython: cdivision=True, profile=False, linetrace=False, binding=True, language_level=3

import numpy as np
cimport numpy as np
cimport cython
from cython cimport floating

from libc.math cimport sqrt, exp, tanh
# cimport cython_gsl as cgsl
# from randomgen import RandomGenerator
# from randomgen import Xoroshiro128, MT19937

from glsimulator.core.types cimport *
from glsimulator.core.glrandom cimport get_rng, RNGContainer
from glsimulator.core.system cimport System, Particle
from glsimulator.core.force cimport ExtForce
#############################################################

DEF ISEED = 23061989

@cython.binding
@cython.final
cdef inline real_t sum_c(real_t[:] arr) nogil:
    cdef real_t s = 0
    cdef unsigned long i, N = arr.shape[0]
    for i in range(N):
        s += arr[i]
    return s

@cython.binding
@cython.final
cdef inline real_t wsum_c(real_t[:] arr, real_t[::1] wgt) nogil:
    '''Calculate weighted sum of array arr with weights wgt'''
    cdef real_t s = 0
    cdef unsigned long i, N = arr.shape[0]
    for i in range(N):
        s += wgt[i] * arr[i]
    return s


################################################################################
## Integrator
################################################################################
cdef class Integrator:
    def __cinit__(self, System sys, str rng_lib_name, str brng_name, real_t dt,
                        unsigned long long seed=ISEED, unsigned long long jump=0,
                        keys=None):
        ## system dimensions
        self.NDIM = sys.NDIM
        self.NPAR = sys.NPAR
        self.NAUX = sys.NAUX
        self.NQ   = sys.NQ
        self.NQF  = sys.NQF
        self.NQU  = sys.NQU
        ## shortcut parameters
        self.gam  = np.zeros((self.NAUX), dtype=np.floating)
        self.nu   = np.zeros((self.NAUX), dtype=np.floating)
        self.tht  = np.zeros((self.NAUX), dtype=np.floating)
        self.sig  = np.zeros((self.NAUX), dtype=np.floating)
        self.tm1g = np.zeros((self.NAUX), dtype=np.floating)
        self.sqgig0 = np.zeros((self.NAUX), dtype=np.floating)
        ## particle, forces, and PRNG
        self._particle = sys.particle_list[0]
        self._force = sys.force_list[0]
        self._rngc = get_rng(rng_lib_name, brng_name, seed=seed, jump=jump, keys=keys)
        # self._rngc = RandomGenRNG(brng_name, seed=seed, jump=jump)
        self.prng  = None  #RandomGenerator(Xoroshiro128())
        # self.W_s_p_d = np.zeros((self.NAUX, self.NPAR, self.NDIM), dtype=np.floating)
        self._name = 'N/A'
        self._dt = dt
        self._m  = self._particle.mass
        self._kT = sys._kT

    cpdef set_seed(self, seed):
        self._rngc.set_seed(seed)

    cpdef set_jump(self, jump):
        self._rngc.set_jump(jump)

    cdef void advance(self, real_t[:,:,::1] Q, real_t[:,:,::1] QF, real_t[:,::1] QU,
                      unsigned long ts):
        pass

    ######################################################################
    # properties
    #----------------------------------
    property name:
        def __get__(self):
            return self._name

    property force:
        def __get__(self):
            return self._force

    property particle:
        def __get__(self):
            return self._particle

    property dt:
        def __get__(self):
            return self._dt
        def __set__(self, dt):
            self._dt = dt

    property m:
        def __get__(self):
            return self._m

    property p:
        def __get__(self):
            return self._particle

    property kT:
        def __get__(self):
            return self._kT

    property seed:
        def __get__(self):
            return self._rngc._seed
        def __set__(self, seed):
            '''Alias for Integrator.set_seed().

              Note: reinitializes the RNG stream with the given seed.
            '''
            self.set_seed(seed)

    property jump:
        def __get__(self):
            return self._rngc._jump
        def __set__(self, jump):
            '''Alias for Integrator.set_jump().

              Note: reinitializes the RNG stream with the given jump.
            '''
            self.set_jump(jump)


#########################################################################################
#########################################################################################
# NOTE: Working integrators
#########################################################################################
#########################################################################################

################################################################################
## Stochastic integrators for conventional (underdamped) Langevin dynamics
################################################################################

#================================================
## EulerIntegratorLE (conventional Langevin)
#================================================
cdef class EulerIntegratorLE(Integrator):
    def __init__(self, System sys, str rng_lib_name, str brng_name, real_t dt,
                       unsigned long long seed=ISEED, unsigned long long jump=0,
                       keys=None):
        cdef:
            real_t kT, m, gam_s

        self._name = 'Euler-Maruyama integrator for underdamped Langevin dynamics'
        kT    = self._kT  # or self.kT (since cinit called first)
        m     = self._particle.mass
        gam_s = self._particle.gamma_s

        self._dt  = dt
        self._m   = m
        self.gams = gam_s
        self.thts = 1 - gam_s*dt
        self.sigs = sqrt(2*kT*gam_s*dt/m)
        self.tsm1 = -(1 - self.thts)

    cdef void advance(self, real_t[:,:,::1] Q, real_t[:,:,::1] QF, real_t[:,::1] QU,
                      unsigned long ts):
        cdef:
            unsigned long p, d
            real_t X, V, F, Gs
            real_t dt=self._dt, m=self._m
            real_t im=1./m, gams=self.gams, sigs=self.sigs

        for p in range(self.NPAR):
            for d in range(self.NDIM):
                X  = Q[XP,p,d]
                V  = Q[VP,p,d]
                F  = QF[FP,p,d]
                Gs = sigs*self._rngc.gauss_rv()

                Q[XP,p,d] += dt*V
                Q[VP,p,d] += dt*(im*F - gams*V) + Gs
                QF[FP,p,d] = self._force.apply(X, ts)
                QF[GSP,p,d] = Gs
            QU[UP,p] = self._force.potential(Q[XP,p,0])  #WARN: 1D only

#================================================
## HeunIntegratorLE (conventional Langevin)
#================================================
cdef class HeunIntegratorLE(Integrator):
    def __init__(self, System sys, str rng_lib_name, str brng_name, real_t dt,
                       unsigned long long seed=ISEED, unsigned long long jump=0,
                       keys=None):
        cdef:
            real_t kT, m, gam_s

        self._name = 'Stochastic Heun integrator for underdamped Langevin dynamics'
        kT    = self._kT  # or self.kT (since cinit called first)
        m     = self._particle.mass
        gam_s = self._particle.gamma_s

        self._dt  = dt
        self._m   = m
        self.gams = gam_s
        self.thts = 1 - gam_s*dt
        self.sigs = sqrt(2*kT*gam_s*dt/m)
        self.tsm1 = -(1 - self.thts)

    cdef void advance(self, real_t[:,:,::1] Q, real_t[:,:,::1] QF, real_t[:,::1] QU,
                      unsigned long ts):
        cdef:
            unsigned long p, d
            real_t X, V, F, Xh, Vh, Fh, Gs
            real_t dt=self._dt, m=self._m, dti2=0.5*dt, im=1./m
            real_t gams=self.gams, sigs=self.sigs

        for p in range(self.NPAR):
            for d in range(self.NDIM):
                X  = Q[XP,p,d]
                V  = Q[VP,p,d]
                F  = QF[FP,p,d]
                Gs = sigs*self._rngc.gauss_rv()

                Xh = X + dt*V
                Vh = V + dt*(im*F - gams*V) + Gs
                Fh = self._force.apply(Xh, ts)

                X += dti2*(V + Vh)
                Q[XP,p,d] = X
                Q[VP,p,d] += dti2*(im*(F + Fh) - gams*(V + Vh)) + Gs
                QF[FP,p,d] = self._force.apply(X, ts)
                QF[GSP,p,d] = Gs
            QU[UP,p] = self._force.potential(Q[XP,p,0])  #WARN: 1D only

#================================================
## ImpulseIntegratorLE (conventional Langevin)
#================================================
cdef class ImpulseIntegratorLE(Integrator):
    def __init__(self, System sys, str rng_lib_name, str brng_name, real_t dt,
                       unsigned long long seed=ISEED, unsigned long long jump=0,
                       keys=None):
        cdef:
            real_t kT, m, gam_s, gam_0

        self._name = 'Langevin-Impulse integrator for underdamped Langevin dynamics'
        kT    = self._kT  # or self.kT (since cinit called first)
        m     = self._particle.mass
        gam_s = self._particle.gamma_s

        self._dt  = dt
        self._m   = m
        self.gams = gam_s
        self.thts = exp(-gam_s*dt)
        self.sigs = sqrt( (1 - exp(-2*gam_s*dt)) * kT/m )
        self.tsm1 = -(1 - self.thts)

    cdef void advance(self, real_t[:,:,::1] Q, real_t[:,:,::1] QF, real_t[:,::1] QU,
                      unsigned long ts):
        cdef:
            unsigned long p, d
            real_t Vh, Vi, Gs
            real_t dt=self._dt, m=self._m
            real_t dtim=dt/m, tsm1=self.tsm1, sigs=self.sigs

        for p in range(self.NPAR):
            for d in range(self.NDIM):
                Gs = sigs*self._rngc.gauss_rv()
                Vh = Q[VP,p,d] + dtim*QF[FP,p,d]
                Vi = tsm1*Vh + Gs
                Q[XP,p,d] += dt * (Vh + 0.5*Vi)
                QF[FP,p,d] = self._force.apply(Q[XP,p,d], ts)
                Q[VP,p,d]  = Vh + Vi
                QF[GSP,p,d] = Gs


################################################################################
## Stochastic integrators for fluctuating BBO dynamics
################################################################################

#================================================
## EulerIntegratorFBBO
#================================================
cdef class EulerIntegratorFBBO(Integrator):
    def __init__(self, System sys, str rng_lib_name, str brng_name, real_t dt,
                       unsigned long long seed=ISEED, unsigned long long jump=0,
                       keys=None):
        cdef:
            unsigned long ia
            real_t kT, m, gam_s, gam_0, gam_i, nu_i

        self._name = 'Euler-Maruyama integrator for fluctuating BBO dynamics'
        kT    = self._kT  # or self.kT (since cinit called first)
        m     = self._particle.mass
        gam_s = self._particle.gamma_s
        gam_0 = self._particle.gamma_0

        self.S  = np.zeros((self.NAUX), dtype=np.floating)
        self.W  = np.zeros((self.NAUX), dtype=np.floating)
        self.G  = np.zeros((self.NAUX), dtype=np.floating)

        self._dt  = dt
        self._m   = m
        self.gams = gam_s
        self.gam0 = gam_0
        self.gam0s = gam_0 + gam_s
        self.im    = 1./m
        self.thts = 1 - gam_s*dt
        self.tht0 = 1 - gam_0*dt
        self.sigs = sqrt(2*kT*gam_s*dt/m)
        self.sig0 = sqrt(2*kT*gam_0*dt/m)
        self.tsm1 = -(1 - self.thts)
        self.t0m1 = -(1 - self.tht0)
        for ia in range(self.NAUX):
            gam_i = self._particle.gamma[ia]
            nu_i  = self._particle.nu[ia]
            self.gam[ia]  = gam_i
            self.nu[ia]   = nu_i
            self.tht[ia]  = 1 - nu_i*dt
            self.sig[ia]  = nu_i * sqrt(2*kT*gam_i*dt/m)
            self.tm1g[ia] = -(1 - self.tht[ia]) * gam_i
            self.sqgig0[ia] = sqrt(gam_i / gam_0)

    cdef void advance(self, real_t[:,:,::1] Q, real_t[:,:,::1] QF, real_t[:,::1] QU,
                      unsigned long ts):
        cdef:
            unsigned long p, d, i
            real_t X, V, F, S0, Ws, W0, Gs, G0
            real_t[::1] S=self.S, W=self.W, G=self.G
            real_t dt=self._dt, m=self._m
            real_t im=self.im, gam0s=self.gam0s
            real_t gams=self.gams, gam0=self.gam0, sigs=self.sigs, sig0=self.sig0
            real_t[::1] gam=self.gam, nu=self.nu, sig=self.sig, sqgig0=self.sqgig0

        for p in range(self.NPAR):
            for d in range(self.NDIM):
                G0 = 0
                S0 = 0
                #-------------
                X  = Q[XP,p,d]
                V  = Q[VP,p,d]
                F  = QF[FP,p,d]
                Gs = QF[GSP,p,d]
                for i in range(self.NAUX):
                    S[i] = Q[SP+i,p,d]
                    G[i] = QF[GP+i,p,d]
                    W[i] = sig[i]*G[i]
                    S0 += S[i]
                    G0 += sqgig0[i] * G[i]
                #-------------
                Ws = sigs*Gs
                W0 = sig0*G0

                Q[XP,p,d] += dt*V
                Q[VP,p,d] += dt*(im*F - gam0s*V - S0) + Ws + W0
                QF[FP,p,d] = self._force.apply(X, ts)
                QF[GSP,p,d] = self._rngc.gauss_rv()  # Noise for Stokes drag term
                QF[G0P,p,d] = G0
                for i in range(self.NAUX):
                    Q[SP+i,p,d] += -dt*nu[i]*(gam[i]*V + S[i]) + W[i]
                    QF[GP+i,p,d] = self._rngc.gauss_rv()
            QU[UP,p] = self._force.potential(Q[XP,p,0])  #WARN: 1D only

#================================================
## HeunIntegratorFBBO
#================================================
cdef class HeunIntegratorFBBO(Integrator):
    def __init__(self, System sys, str rng_lib_name, str brng_name, real_t dt,
                       unsigned long long seed=ISEED, unsigned long long jump=0,
                       keys=None):
        cdef:
            unsigned long ia
            real_t kT, m, gam_s, gam_0, gam_i, nu_i

        self._name = 'Stochastic Heun integrator for fluctuating BBO dynamics'
        kT    = self._kT  # or self.kT (since cinit called first)
        m     = self._particle.mass
        gam_s = self._particle.gamma_s
        gam_0 = self._particle.gamma_0

        self.S  = np.zeros((self.NAUX), dtype=np.floating)
        self.Sh = np.zeros((self.NAUX), dtype=np.floating)
        self.Sm = np.zeros((self.NAUX), dtype=np.floating)
        self.W  = np.zeros((self.NAUX), dtype=np.floating)
        self.G  = np.zeros((self.NAUX), dtype=np.floating)

        self._dt  = dt
        self._m   = m
        self.gams = gam_s
        self.gam0 = gam_0
        self.gam0s = gam_0 + gam_s
        self.dti2  = 0.5*dt
        self.im    = 1./m
        self.thts = 1 - gam_s*dt
        self.tht0 = 1 - gam_0*dt
        self.sigs = sqrt(2*kT*gam_s*dt/m)
        self.sig0 = sqrt(2*kT*gam_0*dt/m)
        self.tsm1 = -(1 - self.thts)
        self.t0m1 = -(1 - self.tht0)
        for ia in range(self.NAUX):
            gam_i = self._particle.gamma[ia]
            nu_i  = self._particle.nu[ia]
            self.gam[ia]  = gam_i
            self.nu[ia]   = nu_i
            self.tht[ia]  = 1 - nu_i*dt
            self.sig[ia]  = nu_i * sqrt(2*kT*gam_i*dt/m)
            self.tm1g[ia] = -(1 - self.tht[ia]) * gam_i
            self.sqgig0[ia] = sqrt(gam_i / gam_0)

    cdef void advance(self, real_t[:,:,::1] Q, real_t[:,:,::1] QF, real_t[:,::1] QU,
                      unsigned long ts):
        cdef:
            unsigned long p, d, i
            real_t X, V, F, S0, Gs, G0, WsW0
            real_t Xh, Vh, Fh, S0h, Vm, Fm, S0m
            real_t[::1] S=self.S, Sh=self.Sh, Sm=self.Sm, W=self.W, G=self.G
            real_t dt=self._dt, m=self._m, dti2=self.dti2, im=self.im
            real_t gams=self.gams, gam0=self.gam0, sigs=self.sigs, sig0=self.sig0
            real_t gam0s=self.gam0s
            real_t[::1] gam=self.gam, nu=self.nu, sig=self.sig, sqgig0=self.sqgig0

        for p in range(self.NPAR):
            for d in range(self.NDIM):
                S0 = 0
                S0h = 0
                G0 = 0
                #-------------
                X  = Q[XP,p,d]
                V  = Q[VP,p,d]
                F  = QF[FP,p,d]
                Gs = QF[GSP,p,d]
                for i in range(self.NAUX):
                    S[i] = Q[SP+i,p,d]
                    G[i] = QF[GP+i,p,d]
                    W[i] = sig[i]*G[i]
                    S0 += S[i]
                    G0 += sqgig0[i] * G[i]
                #-------------
                WsW0 = sigs*Gs + sig0*G0

                #--- (1) Predictor ----------------------------------
                Xh = X + dt*V
                Vh = V + dt*(im*F - gam0s*V - S0) + WsW0
                Fh = self._force.apply(Xh, ts)
                for i in range(self.NAUX):
                    Sh[i] = S[i] - dt*nu[i]*(gam[i]*V + S[i]) + W[i]
                    Sm[i] = S[i] + Sh[i]
                    S0h += Sh[i]

                #--- (2) Corrector ----------------------------------
                Vm  = V + Vh
                Fm  = F + Fh
                S0m = S0 + S0h
                Q[XP,p,d] += dti2*Vm
                Q[VP,p,d] += dti2*(im*Fm - gam0s*Vm - S0m) + WsW0
                QF[FP,p,d] = self._force.apply(X, ts)
                QF[GSP,p,d] = self._rngc.gauss_rv()  # Noise for Stokes drag term
                QF[G0P,p,d] = G0
                for i in range(self.NAUX):
                    Q[SP+i,p,d] += -dti2*nu[i]*(gam[i]*Vm + Sm[i]) + W[i]
                    QF[GP+i,p,d] = self._rngc.gauss_rv()
            QU[UP,p] = self._force.potential(Q[XP,p,0])  #WARN: 1D only

################################################################################
## Stochastic integrator for overdamped Langevin dynamics (Brownian dynamics)
################################################################################

#================================================
## EulerIntegratorBD (overdamped Langevin -- Brownian dynamics)
#================================================
cdef class EulerIntegratorBD(Integrator):
    def __init__(self, System sys, str rng_lib_name, str brng_name, real_t dt,
                       unsigned long long seed=ISEED, unsigned long long jump=0,
                       keys=None):
        cdef:
            real_t kT, m, gam_s

        self._name = 'Euler-Maruyama integrator for Brownian dynamics'
        kT    = self._kT  # or self.kT (since cinit called first)
        m     = self._particle.mass
        gam_s = self._particle.gamma_s

        self._dt   = dt
        self._m    = m
        self.gams  = gam_s
        self.igams = 1./gam_s
        self.izetas = 1./(m*gam_s)
        self.thts  = 1 - gam_s*dt
        self.sigs  = sqrt(2*kT/(dt*m*gam_s))
        self.tsm1  = -(1 - self.thts)

    cdef void advance(self, real_t[:,:,::1] Q, real_t[:,:,::1] QF, real_t[:,::1] QU,
                      unsigned long ts):
        cdef:
            unsigned long p, d
            real_t X, V, F, Gs
            real_t dt=self._dt, idt=1./dt, izetas=self.izetas, sigs=self.sigs

        for p in range(self.NPAR):
            for d in range(self.NDIM):
                X  = Q[XP,p,d]
                F  = QF[FP,p,d]
                Gs = sigs * self._rngc.gauss_rv()
                V  = izetas*F + Gs

                Q[XP,p,d]  += dt*V
                Q[VP,p,d]   = V
                QF[FP,p,d]  = self._force.apply(X, ts)
                QF[GSP,p,d] = Gs


#########################################################################################
#########################################################################################
# WARN: Untested integrators
#########################################################################################
#########################################################################################

################################################################################
## Stochastic integrators for conventional (underdamped) Langevin dynamics
################################################################################

#================================================
## LangevinOVRVO (conventional Langevin)
#================================================
cdef class LangevinOVRVO(Integrator):
    def __init__(self, System sys, str rng_lib_name, str brng_name, real_t dt,
                       unsigned long long seed=ISEED, unsigned long long jump=0,
                       keys=None):
        cdef:
            real_t kT, m, gam_s, gam_0

        self._name = 'Langevin-Impulse integrator for underdamped Langevin dynamics'
        kT    = self._kT  # or self.kT (since cinit called first)
        m     = self._particle.mass
        gam_s = self._particle.gamma_s

        self._dt  = dt
        self._m   = m
        self.b    = sqrt( ( 2/(gam_s*dt) )*tanh(gam_s*dt/2) )
        self.bdt  = self.b*dt
        self.gams = gam_s
        self.thts = exp(-gam_s*dt)
        self.sigs = sqrt( (1 - exp(-2*gam_s*dt)) * kT/m )
        self.tsm1 = -(1 - self.thts)
        self.coef = (1 + self.sigs)*self.bdt/(2*m)

    cdef void advance(self, real_t[:,:,::1] Q, real_t[:,:,::1] QF, real_t[:,::1] QU,
                      unsigned long ts):
        cdef:
            unsigned long p, d
            real_t Vh, Vi, Ws
            real_t dt=self._dt, m=self._m
            real_t dtim=dt/m

        for p in range(self.NPAR):
            for d in range(self.NDIM):
                Ws = self._rngc.gauss_rv()
                X  = Q[XP,p,d]
                V  = Q[VP,p,d]
                F  = QF[FP,p,d]

                V = self.thts*V + self.sigs*Ws + self.coef*F
                Q[XP,p,d] += self.bdt * V
                QF[FP,p,d] = self._force.apply(Q[XP,p,d], ts)
                Q[VP,p,d]  = V
                QF[GP,p,d] = Ws


################################################################################
## Stochastic integrators for fluctuating BBO dynamics
################################################################################

#================================================
## ImpulseIntegratorFBBO (ImpulseIntegratorBassetV4, but with Stokes drag+noise)
#================================================
cdef class ImpulseIntegratorFBBO(Integrator):
    def __init__(self, System sys, str rng_lib_name, str brng_name, real_t dt,
                       unsigned long long seed=ISEED, unsigned long long jump=0,
                       keys=None):
        cdef:
            unsigned long ia
            real_t kT, m, gam_s, gam_0, gam_i, nu_i

        self._name = 'Langevin-Impulse integrator for fluctuating BBO dynamics'
        kT    = self._kT  # or self.kT (since cinit called first)
        m     = self._particle.mass
        gam_s = self._particle.gamma_s
        gam_0 = self._particle.gamma_0

        self._dt  = dt
        self._m   = m
        self.gams = gam_s
        self.gam0 = gam_0
        self.thts = exp(-gam_s*dt)  # 1 - gam_s*dt
        self.tht0 = exp(-gam_0*dt)  # 1 - gam_0*dt
        self.sigs = sqrt( (1 - exp(-2*gam_s*dt)) * kT/m ) #sqrt(2*kT*gam_s*dt/m)
        self.sig0 = sqrt( (1 - exp(-2*gam_0*dt)) * kT/m ) #sqrt(2*kT*gam_0*dt/m)
        self.tsm1 = -(1 - self.thts)
        self.t0m1 = -(1 - self.tht0)
        for ia in range(self.NAUX):
            gam_i = self._particle.gamma[ia]
            nu_i  = self._particle.nu[ia]
            self.gam[ia]  = gam_i
            self.nu[ia]   = nu_i
            self.tht[ia]  = exp(-nu_i*dt/2)
            self.sig[ia]  = sqrt( (1 - exp(-nu_i*dt)) * nu_i*gam_i*kT/m )
            self.tm1g[ia] = -(1 - self.tht[ia]) * gam_i
            self.sqgig0[ia] = sqrt(gam_i / gam_0)

    cdef void advance(self, real_t[:,:,::1] Q, real_t[:,:,::1] QF, real_t[:,::1] QU,
                      unsigned long ts):
        cdef:
            unsigned long p, d, i
            real_t X, V, F, Vh, Vi, Vh_p_Vi, Ws, W0, Gs, G0, S_sum
            real_t[::1] Sh = np.zeros(self.NAUX, dtype=np.float64)
            real_t dt = self._dt, m=self._m
            real_t dti2=0.5*dt, im=1./m

        for p in range(self.NPAR):
            for d in range(self.NDIM):
                X  = Q[XP,p,d]
                V  = Q[VP,p,d]
                F  = QF[FP,p,d]

                Ws = self._rngc.gauss_rv()  # Noise for Stokes drag term
                W0 = 0
                for i in range(self.NAUX):
                    QF[GP+i,p,d] = self._rngc.gauss_rv()
                    W0 += self.sqgig0[i] * QF[GP+i,p,d]
                Gs = self.sigs*Ws
                G0 = self.sig0*W0

                S_sum = 0
                for i in range(SP, SP+self.NAUX):
                    S_sum += Q[i,p,d]

                Vh = V + dt*( im*F - S_sum )
                Vi = (self.tsm1 + self.t0m1)*Vh + Gs + G0
                X += dt * (Vh + 0.5*Vi)
                Vh_p_Vi = Vh + Vi
                for i in range(self.NAUX):
                    Sh[i] = self.tht[i]*Q[SP+i,p,d] + self.tm1g[i]*Vh #+self.sig[i]*W[i]
                    Q[SP+i,p,d] = (self.tht[i]*Sh[i] + self.tm1g[i]*Vh_p_Vi
                                + self.sig[i]*QF[GP+i,p,d])

                Q[XP,p,d]   = X
                Q[VP,p,d]   = Vh_p_Vi
                QF[FP,p,d]  = self._force.apply(X, ts)
                QF[GSP,p,d] = Gs
                QF[G0P,p,d] = G0

#================================================
## ImpulseIntegratorStableFBBO (ImpulseIntegratorBassetV4, but with Stokes drag+noise)
#================================================
cdef class ImpulseIntegratorStableFBBO(Integrator):
    def __init__(self, System sys, str rng_lib_name, str brng_name, real_t dt,
                       unsigned long long seed=ISEED, unsigned long long jump=0,
                       keys=None):
        cdef:
            unsigned long ia
            real_t kT, m, gam_s, gam_0, gam_i, nu_i

        self._name = 'Langevin-Impulse integrator for fluctuating BBO dynamics'
        kT    = self._kT  # or self.kT (since cinit called first)
        m     = self._particle.mass
        gam_s = self._particle.gamma_s
        gam_0 = self._particle.gamma_0

        self._dt  = dt
        self._m   = m
        self.gams = gam_s
        self.gam0 = gam_0
        self.thts = exp(-gam_s*dt)  # 1 - gam_s*dt
        self.tht0 = exp(-gam_0*dt)  # 1 - gam_0*dt
        self.sigs = sqrt(2*(1-exp(-gam_s*dt))**2 * kT/(m*gam_s*dt)) #sqrt(2*kT*gam_s*dt/m)
        self.sig0 = sqrt(2*(1-exp(-gam_0*dt))**2 * kT/(m*gam_0*dt)) #sqrt(2*kT*gam_0*dt/m)
        self.tsm1 = -(1 - self.thts)
        self.t0m1 = -(1 - self.tht0)
        for ia in range(self.NAUX):
            gam_i = self._particle.gamma[ia]
            nu_i  = self._particle.nu[ia]
            self.gam[ia]  = gam_i
            self.nu[ia]   = nu_i
            self.tht[ia]  = exp(-nu_i*dt/2)
            self.sig[ia]  = sqrt(2*(1-exp(-nu_i*dt))**2*nu_i*gam_i*kT/m) # exp(-nu_i*dt/2)
            self.tm1g[ia] = -(1 - self.tht[ia]) * gam_i
            self.sqgig0[ia] = sqrt(gam_i / gam_0)

    cdef void advance(self, real_t[:,:,::1] Q, real_t[:,:,::1] QF, real_t[:,::1] QU,
                      unsigned long ts):
        cdef:
            unsigned long p, d, i
            real_t X, V, F, Vh, Vi, Vh_p_Vi, Ws, W0, Gs, G0, S_sum
            real_t[::1] Sh = np.zeros(self.NAUX, dtype=np.float64)
            real_t dt = self._dt, m=self._m
            real_t dti2=0.5*dt, im=1./m

        for p in range(self.NPAR):
            for d in range(self.NDIM):
                X  = Q[XP,p,d]
                V  = Q[VP,p,d]
                F  = QF[FP,p,d]

                Ws = self._rngc.gauss_rv()  # Noise for Stokes drag term
                W0 = 0
                for i in range(self.NAUX):
                    QF[GP+i,p,d] = self._rngc.gauss_rv()
                    W0 += self.sqgig0[i] * QF[GP+i,p,d]
                Gs = self.sigs*Ws
                G0 = self.sig0*W0

                S_sum = 0
                for i in range(SP, SP+self.NAUX):
                    S_sum += Q[i,p,d]

                Vh = V + dt*( im*F - S_sum )
                Vi = (self.tsm1 + self.t0m1)*Vh + Gs + G0
                X += dt * (Vh + 0.5*Vi)
                Vh_p_Vi = Vh + Vi
                for i in range(self.NAUX):
                    Sh[i] = self.tht[i]*Q[SP+i,p,d] + self.tm1g[i]*Vh #+self.sig[i]*W[i]
                    Q[SP+i,p,d] = (self.tht[i]*Sh[i] + self.tm1g[i]*Vh_p_Vi
                                + self.sig[i]*QF[GP+i,p,d])

                Q[XP,p,d]   = X
                Q[VP,p,d]   = Vh_p_Vi
                QF[FP,p,d]  = self._force.apply(X, ts)
                QF[GSP,p,d] = Gs
                QF[G0P,p,d] = G0

#================================================
## ImpulseIntegratorFBBOMod (ImpulseIntegratorFBBO but impulsive step only for X *not* S)
##   --> S vars updated by full timestep in one go; X has half step, impulse, half step
#================================================
cdef class ImpulseIntegratorFBBOMod(Integrator):
    def __init__(self, System sys, str rng_lib_name, str brng_name, real_t dt,
                       unsigned long long seed=ISEED, unsigned long long jump=0,
                       keys=None):
        cdef:
            unsigned long ia
            real_t kT, m, gam_s, gam_0, gam_i, nu_i

        self._name = 'Langevin-Impulse integrator for fluctuating BBO dynamics'
        kT    = self._kT  # or self.kT (since cinit called first)
        m     = self._particle.mass
        gam_s = self._particle.gamma_s
        gam_0 = self._particle.gamma_0

        self._dt  = dt
        self._m   = m
        self.gams = gam_s
        self.gam0 = gam_0
        self.thts = exp(-gam_s*dt)  # 1 - gam_s*dt
        self.tht0 = exp(-gam_0*dt)  # 1 - gam_0*dt
        self.sigs = sqrt( (1 - exp(-2*gam_s*dt)) * kT/m ) #sqrt(2*kT*gam_s*dt/m)
        self.sig0 = sqrt( (1 - exp(-2*gam_0*dt)) * kT/m ) #sqrt(2*kT*gam_0*dt/m)
        self.tsm1 = -(1 - self.thts)
        self.t0m1 = -(1 - self.tht0)
        for ia in range(self.NAUX):
            gam_i = self._particle.gamma[ia]
            nu_i  = self._particle.nu[ia]
            self.gam[ia]  = gam_i
            self.nu[ia]   = nu_i
            self.tht[ia]  = exp(-nu_i*dt)
            self.sig[ia]  = sqrt( (1 - exp(-2*nu_i*dt)) * nu_i*gam_i*kT/m )
            self.tm1g[ia] = -(1 - self.tht[ia]) * gam_i
            self.sqgig0[ia] = sqrt(gam_i / gam_0)

    cdef void advance(self, real_t[:,:,::1] Q, real_t[:,:,::1] QF, real_t[:,::1] QU,
                      unsigned long ts):
        cdef:
            unsigned long p, d, i
            real_t X, V, F, Vh, Vi, Vhi, Ws, W0, Gs, G0, S_sum
            real_t[::1] Sh = np.zeros(self.NAUX, dtype=np.float64)
            real_t dt = self._dt, m=self._m
            real_t dti2=0.5*dt, im=1./m

        for p in range(self.NPAR):
            for d in range(self.NDIM):
                X  = Q[XP,p,d]
                V  = Q[VP,p,d]
                F  = QF[FP,p,d]

                Ws = self._rngc.gauss_rv()  # Noise for Stokes drag term
                W0 = 0
                for i in range(self.NAUX):
                    QF[GP+i,p,d] = self._rngc.gauss_rv()
                    W0 += self.sqgig0[i] * QF[GP+i,p,d]
                Gs = self.sigs*Ws
                G0 = self.sig0*W0

                S_sum = 0
                for i in range(SP, SP+self.NAUX):
                    S_sum += Q[i,p,d]

                Vh  = V + dt*( im*F - S_sum )
                Vi  = (self.tsm1 + self.t0m1)*Vh + Gs + G0
                Vhi = Vh + 0.5*Vi
                X  += dt * Vhi
                for i in range(self.NAUX):
                    Q[SP+i,p,d] = (self.tht[i]*Q[SP+i,p,d] + self.tm1g[i]*Vhi
                                + self.sig[i]*QF[GP+i,p,d])

                Q[XP,p,d]   = X
                Q[VP,p,d]   = Vh + Vi
                QF[FP,p,d]  = self._force.apply(X, ts)
                QF[GSP,p,d] = Gs
                QF[G0P,p,d] = G0

#================================================
## GeneralizedOVRVO (based on Sivak et al. and Baczewski)
#================================================
cdef class GeneralizedOVRVO(Integrator):
    def __init__(self, System sys, str rng_lib_name, str brng_name, real_t dt,
                       unsigned long long seed=ISEED, unsigned long long jump=0,
                       keys=None):
        cdef:
            unsigned long ia
            real_t kT, m, gam_s, gam_0, gam_i, nu_i

        self._name = 'Generalized OVRVO Integrator'
        kT    = self._kT  # or self.kT (since cinit called first)
        m     = self._particle.mass
        gam_s = self._particle.gamma_s
        gam_0 = self._particle.gamma_0

        self._dt  = dt
        self._m   = m
        self.b    = sqrt( ( 2/(gam_s*dt) )*tanh(gam_s*dt/2) )
        self.bdt  = self.b*dt
        self.gams = gam_s
        self.gam0 = gam_0
        self.thts = exp(-gam_s*dt)
        self.tht0 = exp(-gam_0*dt)
        self.tsm1 = -(1 - self.thts)
        self.t0m1 = -(1 - self.tht0)
        self.sigs = sqrt( (1 - self.thts**2) * kT/m )
        self.sig0 = sqrt( (1 - self.tht0**2) * kT/m )
        self.coef = (1 + self.sigs)*self.bdt/(2*m)
        for ia in range(self.NAUX):
            gam_i = self._particle.gamma[ia]
            nu_i  = self._particle.nu[ia]
            self.gam[ia]  = gam_i
            self.nu[ia]   = nu_i
            self.tht[ia]  = exp(-nu_i*dt)
            self.sig[ia]  = sqrt( (1 - exp(-2*nu_i*dt)) * nu_i*gam_i*kT/m )
            self.tm1g[ia] = -(1 - self.tht[ia]) * gam_i
            self.sqgig0[ia] = sqrt(gam_i / gam_0)

    cdef void advance(self, real_t[:,:,::1] Q, real_t[:,:,::1] QF, real_t[:,::1] QU,
                      unsigned long ts):
        cdef:
            unsigned long p, d, i
            real_t X, V, F, Vh, Vi, Vhi, Ws, W0, Gs, G0, S_sum
            real_t[::1] Sh = np.zeros(self.NAUX, dtype=np.float64)
            real_t[::1] tht = self.tht
            real_t[::1] tm1g = self.tm1g
            real_t[::1] sig = self.sig
            real_t dt = self._dt, m = self._m, bdt = self.bdt
            real_t tht0 = self.tht0, thts = self.thts, coef = self.coef

        for p in range(self.NPAR):
            for d in range(self.NDIM):
                W0 = 0
                S_sum = 0
                X  = Q[XP,p,d]
                V  = Q[VP,p,d]
                F  = QF[FP,p,d]
                Ws = self._rngc.gauss_rv()  # Noise for Stokes drag term
                for i in range(self.NAUX):
                    QF[GP+i,p,d] = self._rngc.gauss_rv()
                    W0 += self.sqgig0[i] * QF[GP+i,p,d]
                Gs = self.sigs*Ws
                G0 = self.sig0*W0

                for i in range(SP, SP+self.NAUX):
                    S_sum += Q[i,p,d]

                V  = (self.thts + self.tht0)*V + G0 + Gs + coef*(F + m*S_sum)
                X += self.bdt*V
                for i in range(self.NAUX):
                    Q[SP+i,p,d] = (self.tht[i]*Q[SP+i,p,d] + self.tm1g[i]*V
                                + self.sig[i]*QF[GP+i,p,d])

                Q[XP,p,d]   = X
                Q[VP,p,d]   = V
                QF[FP,p,d]  = self._force.apply(X, ts)
                QF[GSP,p,d] = Gs
                QF[G0P,p,d] = G0


#########################################################################################
#########################################################################################
#########################################################################################
## Unsorted miscellaneous Integrators
#########################################################################################
#########################################################################################
#########################################################################################

#================================================
## EulerIntegratorFBBO_Old
#================================================
cdef class EulerIntegratorFBBO_Old(Integrator):
    def __init__(self, System sys, str rng_lib_name, str brng_name, real_t dt,
                       unsigned long long seed=ISEED, unsigned long long jump=0,
                       keys=None):
        cdef:
            unsigned long ia
            real_t kT, m, gam_s, gam_0, gam_i, nu_i

        self._name = 'Euler-Maruyama integrator for fluctuating BBO dynamics'
        kT    = self._kT  # or self.kT (since cinit called first)
        m     = self._particle.mass
        gam_s = self._particle.gamma_s
        gam_0 = self._particle.gamma_0

        self.S  = np.zeros((self.NAUX), dtype=np.floating)
        self.W  = np.zeros((self.NAUX), dtype=np.floating)
        self.G  = np.zeros((self.NAUX), dtype=np.floating)

        self._dt  = dt
        self._m   = m
        self.gams = gam_s
        self.gam0 = gam_0
        self.gam0s = gam_0 + gam_s
        self.im    = 1./m
        self.thts = 1 - gam_s*dt
        self.tht0 = 1 - gam_0*dt
        self.sigs = sqrt(2*kT*gam_s*dt/m)
        self.sig0 = sqrt(2*kT*gam_0*dt/m)
        self.tsm1 = -(1 - self.thts)
        self.t0m1 = -(1 - self.tht0)
        for ia in range(self.NAUX):
            gam_i = self._particle.gamma[ia]
            nu_i  = self._particle.nu[ia]
            self.gam[ia]  = gam_i
            self.nu[ia]   = nu_i
            self.tht[ia]  = 1 - nu_i*dt
            self.sig[ia]  = nu_i * sqrt(2*kT*gam_i*dt/m)
            self.tm1g[ia] = -(1 - self.tht[ia]) * gam_i
            self.sqgig0[ia] = sqrt(gam_i / gam_0)

    cdef void advance(self, real_t[:,:,::1] Q, real_t[:,:,::1] QF, real_t[:,::1] QU,
                      unsigned long ts):
        cdef:
            unsigned long p, d, i
            real_t X, V, F, S0, Ws, W0, Gs, G0
            real_t[::1] S=self.S, W=self.W, G=self.G
            real_t dt=self._dt, m=self._m
            real_t im=self.im, gam0s=self.gam0s
            real_t gams=self.gams, gam0=self.gam0, sigs=self.sigs, sig0=self.sig0
            real_t[::1] gam=self.gam, nu=self.nu, sig=self.sig, sqgig0=self.sqgig0

        for p in range(self.NPAR):
            for d in range(self.NDIM):
                X  = Q[XP,p,d]
                V  = Q[VP,p,d]
                F  = QF[FP,p,d]
                Gs = self._rngc.gauss_rv()  # Noise for Stokes drag term
                G0 = 0
                for i in range(self.NAUX):
                    QF[GP+i,p,d] = self._rngc.gauss_rv()
                    G0 += self.sqgig0[i] * QF[GP+i,p,d]
                Ws = sigs*Gs
                W0 = sig0*G0

                S0 = 0
                for i in range(SP, SP+self.NAUX):
                    S0 += Q[i,p,d]

                Q[XP,p,d] += dt*V
                Q[VP,p,d] += dt*(im*F - S0 - (gams + gam0)*V) + Ws + W0
                for i in range(self.NAUX):
                    Q[SP+i,p,d] += (-dt*self.nu[i]*(self.gam[i]*V + Q[SP+i,p,d])
                                   + self.sig[i]*QF[GP+i,p,d])
                QF[FP,p,d] = self._force.apply(X, ts)
                QF[GSP,p,d] = Gs
                QF[G0P,p,d] = G0
            QU[UP,p] = self._force.potential(Q[XP,p,0])  #WARN: 1D only

#================================================
## ImpulseIntegratorLEV2 (conventional Langevin)
#================================================
cdef class ImpulseIntegratorLEV2(Integrator):
    def __init__(self, System sys, str rng_lib_name, str brng_name, real_t dt,
                       unsigned long long seed=ISEED, unsigned long long jump=0,
                       keys=None):
        cdef:
            real_t gam_0
        kT    = self._kT  # or self.kT (since cinit called first)
        m     = self._particle.mass
        gam_s = self._particle.gamma_s
        gam_0 = self._particle.gamma_0

        self._dt  = dt
        self._m   = m
        self.gams = gam_s
        self.gam0 = gam_0
        self.thts = exp(-gam_s*dt)
        self.tht0 = exp(-gam_0*dt)
        self.sigs = sqrt( (1 - exp(-2*gam_s*dt)) * kT/m )
        self.sig0 = sqrt( (1 - exp(-2*gam_0*dt)) * kT/m )
        self.tsm1 = -(1 - self.thts)
        self.t0m1 = -(1 - self.tht0)

    cdef void advance(self, real_t[:,:,::1] Q, real_t[:,:,::1] QF, real_t[:,::1] QU,
                      unsigned long ts):
        cdef:
            unsigned long p, d
            real_t X, V, F, Vh, Vi, Gs
            real_t dt=self._dt, m=self._m
            real_t im=1./m, dtim=dt*im, dti2=0.5*dt

        for p in range(self.NPAR):
            for d in range(self.NDIM):
                X  = Q[XP,p,d]
                V  = Q[VP,p,d]
                F  = QF[FP,p,d]
                Gs = self.sigs*self._rngc.gauss_rv()

                Vh = V + dti2*( im*F )
                Vi = self.tsm1*Vh + Gs
                X += dt * (Vh + 0.5*Vi)
                F  = self._force.apply(X, ts)  #self.apply_force(Q[XP,p,d])
                V  = Vh + Vi + dti2*( im*F )

                Q[XP,p,d]  = X
                Q[VP,p,d]  = V
                QF[FP,p,d] = F
                QF[GSP,p,d] = Gs

#================================================
## EulerIntegrator1AUXFast
#================================================
cdef class EulerIntegrator1AUXFast(Integrator):
    def __init__(self, System sys, str rng_lib_name, str brng_name, real_t dt,
                       unsigned long long seed=ISEED, unsigned long long jump=0,
                       keys=None):
        cdef:
            unsigned long ia
            real_t gam_0, gam_i, nu_i
        kT    = self._kT  # or self.kT (since cinit called first)
        m     = self._particle.mass
        gam_0 = self._particle.gamma_0

        self._dt  = dt
        self._m   = m
        self.gam0 = gam_0
        self.tht0 = 1 - gam_0*dt
        self.sig0 = sqrt(2*kT*gam_0*dt/m)
        self.t0m1 = -(1 - self.tht0)
        for ia in range(self.NAUX):
            gam_i = self._particle.gamma[ia]
            nu_i  = self._particle.nu[ia]
            self.gam[ia]  = gam_i
            self.nu[ia]   = nu_i
            self.tht[ia]  = 1 - nu_i*dt
            self.sig[ia]  = nu_i * sqrt(2*kT*gam_i*dt/m)
            self.tm1g[ia] = -(1 - self.tht[ia]) * gam_i
            self.sqgig0[ia] = sqrt(gam_i / gam_0)

    cdef void advance(self, real_t[:,:,::1] Q, real_t[:,:,::1] QF, real_t[:,::1] QU,
                      unsigned long ts):
        cdef:
            unsigned long p, d, i
            real_t X, V, F, W0
            real_t dt=self._dt, m=self._m
            real_t im=1./m, dtim=dt*im
            real_t W_temp

        for p in range(self.NPAR):
            for d in range(self.NDIM):
                X  = Q[XP,p,d]
                V  = Q[VP,p,d]
                F  = QF[FP,p,d]
                QF[GP,p,d] = self._rngc.gauss_rv()
                Q[XP,p,d] = X + dt*V
                Q[VP,p,d] = (V + dt*(im*F - Q[SP,p,d] - self.gam0*V)
                              + self.sig0*QF[GP,p,d])
                Q[SP,p,d] += (-dt*self.nu[0]*(self.gam[0]*V + Q[SP,p,d])
                             + self.sig[0]*QF[GP,p,d])
                QF[FP,p,d] = self._force.apply(X, ts)

#================================================
## EulerIntegratorBasset
#================================================
cdef class EulerIntegratorBasset(Integrator):
    def __init__(self, System sys, str rng_lib_name, str brng_name, real_t dt,
                       unsigned long long seed=ISEED, unsigned long long jump=0,
                       keys=None):
        cdef:
            unsigned long ia
            real_t gam_0, gam_i, nu_i
        kT    = self._kT  # or self.kT (since cinit called first)
        m     = self._particle.mass
        gam_0 = self._particle.gamma_0

        self._dt  = dt
        self._m   = m
        self.gam0 = gam_0
        self.tht0 = 1 - gam_0*dt
        self.sig0 = sqrt(2*kT*gam_0*dt/m)
        self.t0m1 = -(1 - self.tht0)
        for ia in range(self.NAUX):
            gam_i = self._particle.gamma[ia]
            nu_i  = self._particle.nu[ia]
            self.gam[ia]  = gam_i
            self.nu[ia]   = nu_i
            self.tht[ia]  = 1 - nu_i*dt
            self.sig[ia]  = nu_i * sqrt(2*kT*gam_i*dt/m)
            self.tm1g[ia] = -(1 - self.tht[ia]) * gam_i
            self.sqgig0[ia] = sqrt(gam_i / gam_0)

    cdef void advance(self, real_t[:,:,::1] Q, real_t[:,:,::1] QF, real_t[:,::1] QU,
                      unsigned long ts):
        cdef:
            unsigned long p, d, i
            real_t X, V, F
            real_t dt=self._dt, m=self._m
            real_t im=1./m, dtim=dt*im
            real_t S_sum, W0

        for p in range(self.NPAR):
            for d in range(self.NDIM):
                X  = Q[XP,p,d]
                V  = Q[VP,p,d]
                F  = QF[FP,p,d]

                W0 = 0
                for i in range(self.NAUX):
                    QF[GP+i,p,d] = self._rngc.gauss_rv()
                    W0 += self.sqgig0[i] * QF[GP+i,p,d]
                S_sum = 0
                for i in range(SP, SP+self.NAUX):
                    S_sum += Q[i,p,d]

                Q[XP,p,d] += dt*V
                Q[VP,p,d] += dt*(im*F - S_sum - self.gam0*V) + self.sig0*W0
                for i in range(self.NAUX):
                    Q[SP+i,p,d] += (-dt*self.nu[i]*(self.gam[i]*V + Q[SP+i,p,d])
                                   + self.sig[i]*QF[GP+i,p,d])
                QF[FP,p,d] = self._force.apply(X, ts)

cdef class EulerIntegratorBassetV2(Integrator):
    def __init__(self, System sys, str rng_lib_name, str brng_name, real_t dt,
                       unsigned long long seed=ISEED, unsigned long long jump=0,
                       keys=None):
        cdef:
            unsigned long ia
            real_t gam_0, gam_i, nu_i
        kT    = self._kT  # or self.kT (since cinit called first)
        m     = self._particle.mass
        gam_0 = self._particle.gamma_0

        self._dt  = dt
        self._m   = m
        self.gam0 = gam_0
        self.tht0 = 1 - gam_0*dt
        self.sig0 = sqrt(2*kT*gam_0*dt/m)
        self.t0m1 = -(1 - self.tht0)
        for ia in range(self.NAUX):
            gam_i = self._particle.gamma[ia]
            nu_i  = self._particle.nu[ia]
            self.gam[ia]  = gam_i
            self.nu[ia]   = nu_i
            self.tht[ia]  = 1 - nu_i*dt
            self.sig[ia]  = nu_i * sqrt(2*kT*gam_i*dt/m)
            self.tm1g[ia] = -(1 - self.tht[ia]) * gam_i
            self.sqgig0[ia] = sqrt(gam_i / gam_0)

    cdef void advance(self, real_t[:,:,::1] Q, real_t[:,:,::1] QF, real_t[:,::1] QU,
                      unsigned long ts):
        cdef:
            unsigned long p, d, i
            real_t X, V, F, W0
            real_t[:] S = np.zeros(self.NAUX, dtype=np.float64)
            real_t[:] W = np.zeros(self.NAUX, dtype=np.float64)
            real_t dt=self._dt, m=self._m
            real_t im=1./m, dtim=dt*im

        for p in range(self.NPAR):
            for d in range(self.NDIM):
                X  = Q[XP,p,d]
                V  = Q[VP,p,d]
                F  = QF[FP,p,d]

                for i in range(self.NAUX):
                    S[i] = Q[SP+i,p,d]
                    W[i] = self._rngc.gauss_rv()
                W0 = wsum_c(W, self.sqgig0)

                Q[XP,p,d] = X + dt*V
                Q[VP,p,d] = V + dt*(im*F - sum_c(S) - self.gam0*V) + self.sig0*W0
                for i in range(self.NAUX):
                    S[i] = S[i] - dt*self.nu[i]*(self.gam[i]*V + S[i]) + self.sig[i]*W[i]
                QF[FP,p,d] = self._force.apply(X, ts)

                for i in range(self.NAUX):
                    Q[SP+i,p,d]  = S[i]
                    QF[GP+i,p,d] = W[i]

cdef class EulerIntegratorBassetV3(Integrator):
    def __init__(self, System sys, str rng_lib_name, str brng_name, real_t dt,
                       unsigned long long seed=ISEED, unsigned long long jump=0,
                       keys=None):
        cdef:
            unsigned long ia
            real_t gam_0, gam_i, nu_i
        kT    = self._kT  # or self.kT (since cinit called first)
        m     = self._particle.mass
        gam_0 = self._particle.gamma_0

        self._dt  = dt
        self._m   = m
        self.gam0 = gam_0
        self.tht0 = 1 - gam_0*dt
        self.sig0 = sqrt(2*kT*gam_0*dt/m)
        self.t0m1 = -(1 - self.tht0)
        for ia in range(self.NAUX):
            gam_i = self._particle.gamma[ia]
            nu_i  = self._particle.nu[ia]
            self.gam[ia]  = gam_i
            self.nu[ia]   = nu_i
            self.tht[ia]  = 1 - nu_i*dt
            self.sig[ia]  = nu_i * sqrt(2*kT*gam_i*dt/m)
            self.tm1g[ia] = -(1 - self.tht[ia]) * gam_i
            self.sqgig0[ia] = sqrt(gam_i / gam_0)

    cdef void advance(self, real_t[:,:,::1] Q, real_t[:,:,::1] QF, real_t[:,::1] QU,
                      unsigned long ts):
        cdef:
            unsigned long p, d, i
            real_t X, V, F, W0
            real_t[:] S = np.zeros(self.NAUX, dtype=np.float64)
            real_t[:] W = np.zeros(self.NAUX, dtype=np.float64)
            real_t dt=self._dt, m=self._m
            real_t im=1./m, dtim=dt*im

        for p in range(self.NPAR):
            for d in range(self.NDIM):
                X  = Q[XP,p,d]
                V  = Q[VP,p,d]
                F  = QF[FP,p,d]

                for i in range(self.NAUX):
                    QF[GP+i,p,d] = self._rngc.gauss_rv()
                W0 = wsum_c(QF[GP:GP+self.NAUX,p,d], self.sqgig0)

                Q[XP,p,d] = X + dt*V
                Q[VP,p,d] = (V + dt*(im*F - sum_c(Q[SP:SP+self.NAUX,p,d]) - self.gam0*V)
                              + self.sig0*W0)
                for i in range(self.NAUX):
                    Q[SP+i,p,d] += (-dt*self.nu[i]*(self.gam[i]*V + Q[SP+i,p,d])
                                   + self.sig[i]*QF[GP+i,p,d])
                QF[FP,p,d] = self._force.apply(X, ts)

#================================================
## ImpulseIntegratorBasset (V1 -- half-step V update w/ true update of X, S)
#================================================
cdef class ImpulseIntegratorBasset(Integrator):
    def __init__(self, System sys, str rng_lib_name, str brng_name, real_t dt,
                       unsigned long long seed=ISEED, unsigned long long jump=0,
                       keys=None):
        cdef:
            unsigned long ia
            real_t gam_0, gam_i, nu_i
        kT    = self._kT  # or self.kT (since cinit called first)
        m     = self._particle.mass
        gam_0 = self._particle.gamma_0

        self._dt  = dt
        self._m   = m
        self.gam0 = gam_0
        self.tht0 = exp(-gam_0*dt)
        self.sig0 = sqrt( (1 - exp(-2*gam_0*dt)) * kT/m )
        self.t0m1 = -(1 - self.tht0)

        for ia in range(self.NAUX):
            gam_i = self._particle.gamma[ia]
            nu_i  = self._particle.nu[ia]
            self.gam[ia]  = gam_i
            self.nu[ia]   = nu_i
            self.tht[ia]  = exp(-nu_i*dt/2)
            self.sig[ia]  = sqrt( (1 - exp(-nu_i*dt)) * nu_i*gam_i*kT/m )
            self.tm1g[ia] = -(1 - self.tht[ia]) * gam_i
            self.sqgig0[ia] = sqrt(gam_i / gam_0)

    cdef void advance(self, real_t[:,:,::1] Q, real_t[:,:,::1] QF, real_t[:,::1] QU,
                      unsigned long ts):
        cdef:
            unsigned long p, d, i
            real_t X, V, F, Vh, Vi, Vh_p_Vi, W0
            real_t[::1] Sh = np.zeros(self.NAUX, dtype=np.float64)
            real_t dt=self._dt, m=self._m
            real_t im=1./m, dtim=dt*im, dti2=0.5*dt

        for p in range(self.NPAR):
            for d in range(self.NDIM):
                X  = Q[XP,p,d]
                V  = Q[VP,p,d]
                F  = QF[FP,p,d]
                for i in range(self.NAUX):
                    QF[GP+i,p,d] = self._rngc.gauss_rv()
                W0 = wsum_c(QF[GP:,p,d], self.sqgig0)

                Vh = V + dti2*( im*F - sum_c(Q[SP:,p,d]) )
                Vi = self.t0m1*Vh + self.sig0*W0
                Q[XP,p,d] = X + dt * (Vh + 0.5*Vi)
                QF[FP,p,d] = self._force.apply(Q[XP,p,d],ts) #self.apply_force(Q[XP,p,d])
                Vh_p_Vi = Vh + Vi
                for i in range(self.NAUX):
                    Sh[i] = self.tht[i]*Q[SP+i,p,d]  + self.tm1g[i]*Vh #+self.sig[i]*W[i]
                    Q[SP+i,p,d] = (self.tht[i]*Sh[i] + self.tm1g[i]*Vh_p_Vi
                                + self.sig[i]*QF[GP+i,p,d])
                Q[VP,p,d] = Vh_p_Vi + dti2*( im*QF[FP,p,d] - sum_c(Q[SP:,p,d]) )

#================================================
## ImpulseIntegratorBassetV2 (V1 -- half-step V update w/ true update of X, S)
#================================================
cdef class ImpulseIntegratorBassetV2(Integrator):
    def __init__(self, System sys, str rng_lib_name, str brng_name, real_t dt,
                       unsigned long long seed=ISEED, unsigned long long jump=0,
                       keys=None):
        cdef:
            unsigned long ia
            real_t gam_0, gam_i, nu_i
        kT    = self._kT  # or self.kT (since cinit called first)
        m     = self._particle.mass
        gam_0 = self._particle.gamma_0

        self._dt  = dt
        self._m   = m
        self.gam0 = gam_0
        self.tht0 = exp(-gam_0*dt)
        self.sig0 = sqrt( (1 - exp(-2*gam_0*dt)) * kT/m )
        self.t0m1 = -(1 - self.tht0)

        for ia in range(self.NAUX):
            gam_i = self._particle.gamma[ia]
            nu_i  = self._particle.nu[ia]
            self.gam[ia]  = gam_i
            self.nu[ia]   = nu_i
            self.tht[ia]  = exp(-nu_i*dt/2)
            self.sig[ia]  = sqrt( (1 - exp(-nu_i*dt)) * nu_i*gam_i*kT/m )
            self.tm1g[ia] = -(1 - self.tht[ia]) * gam_i
            self.sqgig0[ia] = sqrt(gam_i / gam_0)

    cdef void advance(self, real_t[:,:,::1] Q, real_t[:,:,::1] QF, real_t[:,::1] QU,
                      unsigned long ts):
        cdef:
            unsigned long p, d, i
            real_t X, V, F, Vh, Vi, Vh_p_Vi, W0
            real_t[::1] Sh = np.zeros(self.NAUX, dtype=np.float64)
            real_t dt=self._dt, m=self._m
            real_t im=1./m, dtim=dt*im, dti2=0.5*dt

        for p in range(self.NPAR):
            for d in range(self.NDIM):
                X  = Q[XP,p,d]
                V  = Q[VP,p,d]
                F  = QF[FP,p,d]
                for i in range(self.NAUX):
                    QF[GP+i,p,d] = self._rngc.gauss_rv()
                W0 = wsum_c(QF[GP:,p,d], self.sqgig0)

                Vh = V + dti2*( im*F - sum_c(Q[SP:,p,d]) )
                Vi = self.t0m1*Vh + self.sig0*W0
                X += dt * (Vh + 0.5*Vi)

                F = self._force.apply(X, ts)  #self.apply_force(Q[XP,p,d])
                Vh_p_Vi = Vh + Vi
                for i in range(self.NAUX):
                    Sh[i] = self.tht[i]*Q[SP+i,p,d] + self.tm1g[i]*Vh #+self.sig[i]*W[i]
                    Q[SP+i,p,d] = (self.tht[i]*Sh[i] + self.tm1g[i]*Vh_p_Vi
                                + self.sig[i]*QF[GP+i,p,d])
                V = Vh_p_Vi + dti2*( im*F - sum_c(Q[SP:,p,d]) )

                Q[XP,p,d]  = X
                Q[VP,p,d]  = V
                QF[FP,p,d] = F

#================================================
## ImpulseIntegratorBassetV3 (V1 -- half-step V update w/ true update of X, S)
#================================================
cdef class ImpulseIntegratorBassetV3(Integrator):
    def __init__(self, System sys, str rng_lib_name, str brng_name, real_t dt,
                       unsigned long long seed=ISEED, unsigned long long jump=0,
                       keys=None):
        cdef:
            unsigned long ia
            real_t gam_0, gam_i, nu_i
        kT    = self._kT  # or self.kT (since cinit called first)
        m     = self._particle.mass
        gam_0 = self._particle.gamma_0

        self._dt  = dt
        self._m   = m
        self.gam0 = gam_0
        self.tht0 = exp(-gam_0*dt)
        self.sig0 = sqrt( (1 - exp(-2*gam_0*dt)) * kT/m )
        self.t0m1 = -(1 - self.tht0)

        for ia in range(self.NAUX):
            gam_i = self._particle.gamma[ia]
            nu_i  = self._particle.nu[ia]
            self.gam[ia]  = gam_i
            self.nu[ia]   = nu_i
            self.tht[ia]  = exp(-nu_i*dt/2)
            self.sig[ia]  = sqrt( (1 - exp(-nu_i*dt)) * nu_i*gam_i*kT/m )
            self.tm1g[ia] = -(1 - self.tht[ia]) * gam_i
            self.sqgig0[ia] = sqrt(gam_i / gam_0)

    cdef void advance(self, real_t[:,:,::1] Q, real_t[:,:,::1] QF, real_t[:,::1] QU,
                      unsigned long ts):
        cdef:
            unsigned long p, d, i
            real_t X, V, F, Vh, Vi, Vh_p_Vi, W0, S_sum
            real_t[::1] Sh = np.zeros(self.NAUX, dtype=np.float64)
            real_t dt=self._dt, m=self._m
            real_t im=1./m, dtim=dt*im, dti2=0.5*dt

        for p in range(self.NPAR):
            for d in range(self.NDIM):
                X  = Q[XP,p,d]
                V  = Q[VP,p,d]
                F  = QF[FP,p,d]
                W0 = 0
                for i in range(self.NAUX):
                    QF[GP+i,p,d] = self._rngc.gauss_rv()
                    W0 += self.sqgig0[i] * QF[GP+i,p,d]
                S_sum = 0
                for i in range(SP, SP+self.NAUX):
                    S_sum += Q[i,p,d]

                Vh = V + dti2*( im*F - S_sum )
                Vi = self.t0m1*Vh + self.sig0*W0
                X += dt * (Vh + 0.5*Vi)

                F  = self._force.apply(X, ts)  #self.apply_force(Q[XP,p,d])
                Vh_p_Vi = Vh + Vi
                for i in range(self.NAUX):
                    Sh[i] = self.tht[i]*Q[SP+i,p,d] + self.tm1g[i]*Vh #+self.sig[i]*W[i]
                    Q[SP+i,p,d] = (self.tht[i]*Sh[i] + self.tm1g[i]*Vh_p_Vi
                                + self.sig[i]*QF[GP+i,p,d])
                V  = Vh_p_Vi + dti2*( im*F - S_sum )

                Q[XP,p,d]  = X
                Q[VP,p,d]  = V
                QF[FP,p,d] = F

#================================================
## ImpulseIntegratorBassetV4 (V3, but combines V half steps to avoid 2 F calc)
#================================================
cdef class ImpulseIntegratorBassetV4(Integrator):
    def __init__(self, System sys, str rng_lib_name, str brng_name, real_t dt,
                       unsigned long long seed=ISEED, unsigned long long jump=0,
                       keys=None):
        cdef:
            unsigned long ia
            real_t gam_0, gam_i, nu_i
        kT    = self._kT  # or self.kT (since cinit called first)
        m     = self._particle.mass
        gam_0 = self._particle.gamma_0

        self._dt  = dt
        self._m   = m
        self.gam0 = gam_0
        self.tht0 = exp(-gam_0*dt)
        self.sig0 = sqrt( (1 - exp(-2*gam_0*dt)) * kT/m )
        self.t0m1 = -(1 - self.tht0)

        for ia in range(self.NAUX):
            gam_i = self._particle.gamma[ia]
            nu_i  = self._particle.nu[ia]
            self.gam[ia]  = gam_i
            self.nu[ia]   = nu_i
            self.tht[ia]  = exp(-nu_i*dt/2)
            self.sig[ia]  = sqrt( (1 - exp(-nu_i*dt)) * nu_i*gam_i*kT/m )
            self.tm1g[ia] = -(1 - self.tht[ia]) * gam_i
            self.sqgig0[ia] = sqrt(gam_i / gam_0)

    cdef void advance(self, real_t[:,:,::1] Q, real_t[:,:,::1] QF, real_t[:,::1] QU,
                      unsigned long ts):
        cdef:
            unsigned long p, d, i
            real_t X, V, F, Vh, Vi, Vh_p_Vi, W0, S_sum
            real_t[::1] Sh = np.zeros(self.NAUX, dtype=np.float64)
            real_t dt=self._dt, m=self._m
            real_t im=1./m, dtim=dt*im, dti2=0.5*dt

        for p in range(self.NPAR):
            for d in range(self.NDIM):
                X  = Q[XP,p,d]
                V  = Q[VP,p,d]
                F  = QF[FP,p,d]
                W0 = 0
                for i in range(self.NAUX):
                    QF[GP+i,p,d] = self._rngc.gauss_rv()
                    W0 += self.sqgig0[i] * QF[GP+i,p,d]
                S_sum = 0
                for i in range(SP, SP+self.NAUX):
                    S_sum += Q[i,p,d]

                Vh = V + dt*( im*F - S_sum )
                Vi = self.t0m1*Vh + self.sig0*W0
                X += dt * (Vh + 0.5*Vi)
                Vh_p_Vi = Vh + Vi
                for i in range(self.NAUX):
                    Sh[i] = self.tht[i]*Q[SP+i,p,d] + self.tm1g[i]*Vh #+self.sig[i]*W[i]
                    Q[SP+i,p,d] = (self.tht[i]*Sh[i] + self.tm1g[i]*Vh_p_Vi
                                + self.sig[i]*QF[GP+i,p,d])

                Q[XP,p,d]  = X
                Q[VP,p,d]  = Vh_p_Vi
                QF[FP,p,d] = self._force.apply(X, ts)

#================================================
## ImpulseIntegratorBassetBac (V3, but Baczewski method 3 for larger dt when tau_0 ~ dt)
#================================================
cdef class ImpulseIntegratorBassetBac(Integrator):
    def __init__(self, System sys, str rng_lib_name, str brng_name, real_t dt,
                       unsigned long long seed=ISEED, unsigned long long jump=0,
                       keys=None):
        cdef:
            unsigned long ia
            real_t gam_0, gam_i, nu_i
        kT    = self._kT  # or self.kT (since cinit called first)
        m     = self._particle.mass
        gam_0 = self._particle.gamma_0

        self._dt  = dt
        self._m   = m
        self.gam0 = gam_0
        self.tht0 = exp(-gam_0*dt)
        self.sig0 = sqrt( (1 - exp(-2*gam_0*dt)) * kT/m )
        self.t0m1 = -(1 - self.tht0)

        for ia in range(self.NAUX):
            gam_i = self._particle.gamma[ia]
            nu_i  = self._particle.nu[ia]
            self.gam[ia]  = gam_i
            self.nu[ia]   = nu_i
            self.tht[ia]  = exp(-nu_i*dt/2)
            self.sig[ia]  = sqrt( (1 - exp(-nu_i*dt/2))**2 * (2*dt/2)*gam_i*kT/m )
            self.tm1g[ia] = -(1 - self.tht[ia]) * gam_i
            self.sqgig0[ia] = sqrt(gam_i / gam_0)

    cdef void advance(self, real_t[:,:,::1] Q, real_t[:,:,::1] QF, real_t[:,::1] QU,
                      unsigned long ts):
        cdef:
            unsigned long p, d, i
            real_t X, V, F, Vh, Vi, Vh_p_Vi, W0, S_sum
            real_t[::1] Sh = np.zeros(self.NAUX, dtype=np.float64)
            real_t dt=self._dt, m=self._m
            real_t im=1./m, dtim=dt*im, dti2=0.5*dt

        for p in range(self.NPAR):
            for d in range(self.NDIM):
                X  = Q[XP,p,d]
                V  = Q[VP,p,d]
                F  = QF[FP,p,d]
                W0 = 0
                for i in range(self.NAUX):
                    QF[GP+i,p,d] = self._rngc.gauss_rv()
                    W0 += self.sqgig0[i] * QF[GP+i,p,d]
                S_sum = 0
                for i in range(SP, SP+self.NAUX):
                    S_sum += Q[i,p,d]

                Vh = V + dti2*( im*F - S_sum )
                Vi = self.t0m1*Vh + self.sig0*W0
                X += dt * (Vh + 0.5*Vi)

                F  = self._force.apply(X, ts)  #self.apply_force(Q[XP,p,d])
                Vh_p_Vi = Vh + Vi
                for i in range(self.NAUX):
                    Sh[i] = self.tht[i]*Q[SP+i,p,d] + self.tm1g[i]*Vh #+self.sig[i]*W[i]
                    Q[SP+i,p,d] = (self.tht[i]*Sh[i] + self.tm1g[i]*Vh_p_Vi
                                + self.sig[i]*QF[GP+i,p,d])
                V  = Vh_p_Vi + dti2*( im*F - S_sum )

                Q[XP,p,d]  = X
                Q[VP,p,d]  = V
                QF[FP,p,d] = F
