#cython: boundscheck=False, wraparound=False, initializedcheck=False, nonecheck=False
#cython: cdivision=True, profile=False, linetrace=False, binding=True, language_level=3

from glsimulator.core.types cimport *
from glsimulator.core.force cimport ExtForce
#############################################################

#############################################################

cdef class System:
    cdef:
        real_t _kT
        unsigned long NDIM, NPAR, NAUX, NQ, NQF, NQU
        list particle_list
        list force_list

    cpdef add_N_particles(self, unsigned long N, real_t mass, real_t radius,
                          real_t gamma_s, real_t gamma_0, real_t[::1] gamma,
                          real_t[::1] nu)
    cpdef add_particle(self, Particle p)
    cpdef add_force(self, ExtForce f)
    cpdef remove_force(self, unsigned long i=*)


cdef class Particle:
    cdef:
        unsigned long _NAUX
        real_t _mass, _radius, _gamma_s, _gamma_0
        real_t[::1] _gamma, _nu
