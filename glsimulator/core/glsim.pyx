#cython: boundscheck=False, wraparound=False, initializedcheck=False, nonecheck=False
#cython: cdivision=True, profile=False, linetrace=False, binding=True, language_level=3
import os
import numpy as np
cimport numpy as np
from cython cimport view

from libc.math cimport sqrt
from posix.time cimport clock_gettime, timespec, CLOCK_REALTIME

# from core.utils.progressbar import ProgressBar
from glsimulator.core.progressmeter import ProgressMeter
from glsimulator.core.timers cimport time_d

from glsimulator.core.types      cimport XP, VP, SP, FP, GSP, G0P, GP, UP, real_t
from glsimulator.core.sim_state  cimport State
from glsimulator.core.system     cimport System
from glsimulator.core.force      cimport ExtForce
from glsimulator.core.integrator cimport Integrator
#############################################################

DEF ISEED = 23061989

################################################################################
## Simulation
################################################################################
cdef class Simulation:

    def __cinit__(self, System sys, Integrator intgr, str filename, str outdir):
        self._sys = sys
        self._integrator = intgr
        self._state = None

        self.NQ  = self._sys.NQ
        self.NQF = self._sys.NQF
        self.NQU = self._sys.NQU
        self._Q  = np.zeros((self._sys.NQ,  self._sys.NPAR, self._sys.NDIM),
                            dtype=np.floating)
        self._QF = np.zeros((self._sys.NQF, self._sys.NPAR, self._sys.NDIM),
                            dtype=np.floating)
        self._QU = np.zeros((self._sys.NQU, self._sys.NPAR), dtype=np.floating)

    def __init__(self, System sys, Integrator intgr, str filename, str outdir):
        self.filename = filename
        self.outdir   = outdir

    ######################################################################
    ## initialize
    #----------------------------------
    # WARNING: This sets the system temperature; might want to set temp differently later
    #----------------------------------
    def initialize(self, X=None, V=None, F=None, U=None, S=None, G=None,
                     bint equilibration=False, bint continuation=False, real_t kT=0,
                     real_t mass=0, unsigned long long seed=ISEED,
                     unsigned long long jump=0):

        self.system.kT = kT
        if equilibration:
            self.init_positions(X=X)
            self.init_velocities(V=V, kT=kT, mass=mass, seed=seed, jump=jump)
            self.init_forces(F=F, U=U)
            self.init_auxiliaries(S=S, kT=kT)
            self.init_noises(G=G)
        else:
            if continuation:
                # Enables a continuation run; should be used after equilibration
                #   Velocities/auxiliaries are preserved from previous equilibration
                #   run, but gives option to reinit particle positions and forces.
                self.init_positions(X=X)
                self.init_velocities(V=self.velocity)
                self.init_forces(F=F, U=U)
                self.init_auxiliaries(S=S, kT=0)
                self.init_noises(G=G)
            else:
                self.init_positions(X=X)
                self.init_velocities(V=V, kT=kT, mass=mass, seed=seed, jump=jump)
                self.init_forces(F=F, U=U)
                self.init_auxiliaries(S=S, kT=kT)
                self.init_noises(G=G)

    ######################################################################
    cpdef run(self, unsigned long long NSTEP, unsigned long NTOUT,
              bint equilibration=False, long long NTDUMP=-1,
              int print_step_delay=25000, double print_interval=0.05,
              bint store=True):
        cdef:
            real_t[:,:,::1] Q  = self._Q
            real_t[:,:,::1] QF = self._QF
            real_t[:,::1]   QU = self._QU
            unsigned long long ts, n
            double t_now = time_d()
            double t_nextout = t_now

        print('\n========================================================')
        if equilibration:
            head = 'Performing equilibration run using '
        else:
            head = 'Performing production run using '
        print(head + self._integrator.name)
        print('----------------------------')
        print('  Te     = {}'.format(self.system.kT))
        print('  NSTEP  = {}'.format(NSTEP))
        print('  dt     = {}'.format(self.dt))
        print('----------------------------')
        print('  NDIM   = {}'.format(self.n_dim))
        print('  NPAR   = {}'.format(self.n_par))
        print('  NAUX   = {}'.format(self.n_aux))
        print('----------------------------')
        print('  NTOUT  = {}'.format(NTOUT))
        print('  NTDUMP = {}'.format(NTDUMP))
        print('----------------------------')
        print('  output = {}'.format(self.outdir))
        print('  files = {}'.format(self.filename))
        print('--------------------------------------------------------\n')

        if NTDUMP <= 0:
            if equilibration:
                NTDUMP = NSTEP         # Default equilibration dump interval
            else:
                NTDUMP = int(NSTEP/4)  # Default production dump interval
        self.init_state(NSTEP, NTOUT, NTDUMP, equilibration=equilibration)

        with ProgressMeter(NSTEP, print_interval) as pb:
            for ts in range(NSTEP+1):
                n = (ts % NTOUT)
                if (n == 0 and store):
                    self.save_state(Q, QF, QU, ts)
                if (ts % print_step_delay) == 0:
                    t_now = time_d()
                    if (t_now > t_nextout):
                        pb.update(ts, t_now)
                        t_nextout += print_interval
                self._integrator.advance(Q, QF, QU, ts)

    ######################################################################
    cpdef save_state(self, real_t[:,:,::1] Q, real_t[:,:,::1] QF, real_t[:,::1] QU,
                     unsigned long ts):
        '''Alias for method State.save_state()'''
        # cdef real_t W_temp, W_temp_in
        # W_temp = np.asarray(self._QF[GP,...])[0]
        # W_temp_in = np.asarray(QF[GP,...])[0]
        self._state.save_state(Q, QF, QU, ts)
        # PASS: print W_temp, np.asarray(self._QF[GP,...])[0], ' | ',
        #     W_temp_in, np.asarray(QF[GP,...])[0]

    #----------------------------------
    cpdef init_positions(self, X=None):
        self.position = np.zeros((self._sys.NPAR, self._sys.NDIM), dtype=np.floating)
        if X is not None:
            if not isinstance(X, np.ndarray):
                for p in range(self._sys.NPAR):
                    for d in range(self._sys.NDIM):
                        self._Q[XP,p,d] = X
            else:
                self.position[:,:] = X[:,:]

    #----------------------------------
    cpdef init_velocities(self, V=None, real_t kT=-1, real_t mass=0,
                          unsigned long long seed=ISEED, unsigned long long jump=0):
        cdef unsigned long p, d
        if V is not None:
            if not isinstance(V, np.ndarray):
                for p in range(self._sys.NPAR):
                    for d in range(self._sys.NDIM):
                            self._Q[VP,p,d] = V
            else:
                self.velocity[:,:] = V[:,:]
        if kT >= 0 and mass != 0:
            print('Initializing velocities with seed = {}'.format(seed))
            advance = jump * (2**63)  # skip 2^128 numbers for MT19937
            np.random.seed(seed + advance)
            self.velocity += self.maxwellian_velocities(kT, mass)

    #----------------------------------
    cpdef init_forces(self, F=None, U=None):
        cdef:
            unsigned long p, d
            ExtForce force = self._sys.force_list[0]
        if F is not None:
            self.force = F
            self.potential = U
        for p in range(self._sys.NPAR):
            for d in range(self._sys.NDIM):
                self._QF[FP,p,d] += force.apply(self._Q[XP,p,d], 0)  # (time) step is 0
            self._QU[UP,p] += force.U(self._Q[XP,p,0])  #WARN: 1D only

    #----------------------------------
    cpdef init_auxiliaries(self, S=None, real_t kT=-1):
        cdef:
            real_t gam_i, nu_i
            unsigned long ia, p, d

        if S is not None:
            if not isinstance(S, np.ndarray):
                for ia in range(self._sys.NAUX):
                    for p in range(self._sys.NPAR):
                        for d in range(self._sys.NDIM):
                            self.auxiliary[ia,p,d] = S
            elif S.shape[0] == self._sys.NAUX:
                for ia in range(self._sys.NAUX):
                    self.auxiliary[ia,:,:] = S[ia]
            else:
                self.auxiliary[:,:,:] = S[:,:,:]
        if kT >= 0:
            mass = self._integrator.m
            sigma = np.ones((self._sys.NAUX, self._sys.NPAR, self._sys.NDIM),
                             dtype=np.floating)
            for ia in range(self._sys.NAUX):
                gam_i = self._integrator.gam[ia]
                nu_i  = self._integrator.nu[ia]
                sigma[ia,:,:] *= sqrt(nu_i * gam_i * kT / mass)
            self.auxiliary += np.random.normal(loc=0.0, scale=sigma)

    #----------------------------------
    cpdef init_noises(self, G=None, unsigned long long seed=ISEED,
                      unsigned long long jump=0):
        cdef:
            unsigned long ia, p, d
        if G is not None:
            if not isinstance(G, np.ndarray):
                for p in range(self._sys.NPAR):
                    for d in range(self._sys.NDIM):
                        self._QF[GSP,p,d] = G
                        for ia in range(self._sys.NAUX):
                            self._QF[GP+ia,p,d] = G
            else:
                self.noise[:,:,:] = G[:,:,:]
        else:
            for p in range(self._sys.NPAR):
                for d in range(self._sys.NDIM):
                    self._QF[GSP,p,d] = self._integrator._rngc.gauss_rv()
                    for ia in range(self._sys.NAUX):
                        self._QF[GP+ia,p,d] = self._integrator._rngc.gauss_rv()

    #----------------------------------
    cpdef maxwellian_velocities(self, real_t kT, real_t mass):
        cdef real_t[:,::1] sigma
        sigma = sqrt(kT/mass) * np.ones((self._sys.NPAR, self._sys.NDIM),
                                        dtype=np.floating)
        return np.random.normal(loc=0.0, scale=sigma)

    #----------------------------------
    cpdef init_state(self, unsigned long long NSTEP, unsigned long NTOUT,
                     unsigned long long NTDUMP, bint equilibration=False):
        '''Called within `Simulation.run()`;effectively an alias for
        constructor State.State() to initialize a production or equilibration
        run.

        Note: `equilibration` flag should be set to `True` to set the correct
        filenames for storing equilibration trajectories.
        '''
        cdef str filename, outdir

        if equilibration:
            base, suffix = self.filename.split('.', 1)
            filename = '.'.join([base, 'eq', suffix])
            outdir = '{}/equilibration'.format(self.outdir)
        else:
            filename = self.filename
            outdir = self.outdir
        if not os.path.exists(outdir):
            os.makedirs(outdir)

        self._state = State(self.n_par, self.n_aux, self.n_dim, NSTEP, NTDUMP,
                            NTOUT, filename, outdir)

    ######################################################################
    # properties
    #----------------------------------
    property Q:
        def __get__(self):
            return np.ascontiguousarray(self._Q[...])

    property QF:
        def __get__(self):
            return np.ascontiguousarray(self._QF[...])

    property QU:
        def __get__(self):
            return np.ascontiguousarray(self._QU[...])

    property n_dim:
        def __get__(self):
            return self._sys.NDIM

    property n_par:
        def __get__(self):
            return self._sys.NPAR

    property n_aux:
        def __get__(self):
            return self._sys.NAUX

    property system:
        def __get__(self):
            return self._sys

    property integrator:
        def __get__(self):
            return self._integrator

    property state:
        def __get__(self):
            return self._state

    property dt:
        def __get__(self):
            return self._integrator.dt

    property position:
        def __get__(self):
            return np.ascontiguousarray(self._Q[XP,:,:])
        def __set__(self, arr):
            cdef unsigned long p, d
            for p in range(self._sys.NPAR):
                for d in range(self._sys.NDIM):
                    self._Q[XP,p,d] = arr[p,d]

    property velocity:
        def __get__(self):
            return np.ascontiguousarray(self._Q[VP,:,:])
        def __set__(self, arr):
            cdef unsigned long p, d
            for p in range(self._sys.NPAR):
                for d in range(self._sys.NDIM):
                    self._Q[VP,p,d] = arr[p,d]

    property auxiliary:
        def __get__(self):
            return np.ascontiguousarray(self._Q[SP:,:,:])
        def __set__(self, arr):
            cdef unsigned long ia, p, d
            for ia in range(self._sys.NAUX):
                for p in range(self._sys.NPAR):
                    for d in range(self._sys.NDIM):
                        self._Q[SP+ia,p,d] = arr[ia,p,d]

    property force:
        def __get__(self):
            return np.ascontiguousarray(self._QF[FP,:,:])
        def __set__(self, arr):
            cdef unsigned long p, d
            for p in range(self._sys.NPAR):
                for d in range(self._sys.NDIM):
                    self._QF[FP,p,d] = arr[p,d]

    property potential:
        def __get__(self):
            return np.ascontiguousarray(self._QU[UP,:])
        def __set__(self, arr):
            cdef unsigned long p
            for p in range(self._sys.NPAR):
                self._QU[UP,p] = arr[p]

    property stokes_noise:
        def __get__(self):
            return np.ascontiguousarray(self._QF[GSP,:,:])
        def __set__(self, arr):
            cdef unsigned long p, d
            for p in range(self._sys.NPAR):
                for d in range(self._sys.NDIM):
                    self._QF[GSP,p,d] = arr[p,d]

    property basset_noise:
        def __get__(self):
            return np.ascontiguousarray(self._QF[G0P,:,:])
        def __set__(self, arr):
            cdef unsigned long p, d
            for p in range(self._sys.NPAR):
                for d in range(self._sys.NDIM):
                    self._QF[G0P,p,d] = arr[p,d]

    property noise:
        def __get__(self):
            return np.ascontiguousarray(self._QF[GP:,:,:])
        def __set__(self, arr):
            cdef unsigned long ia, p, d
            for ia in range(self._sys.NAUX):
                for p in range(self._sys.NPAR):
                    for d in range(self._sys.NDIM):
                        self._QF[GP+ia,p,d] = arr[ia,p,d]
