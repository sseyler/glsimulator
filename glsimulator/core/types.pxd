#cython: boundscheck=False, wraparound=False, initializedcheck=False, nonecheck=False
#cython: cdivision=True, profile=False, linetrace=False, binding=True, language_level=3

ctypedef double real_t
ctypedef unsigned long uint_t
ctypedef unsigned long long uint64_t

cdef uint_t XP = 0
cdef uint_t VP = 1
cdef uint_t SP = 2
cdef uint_t FP = 0
cdef uint_t GSP = 1
cdef uint_t G0P = 2
cdef uint_t GP = 3
cdef uint_t UP = 0

# DEF XP = 0
# DEF VP = 1
# DEF FP = 2
# DEF WP = 3
# DEF SP = 4

# cdef enum: XP = 0
# cdef enum: VP = 1
# cdef enum: SP = 2
# cdef enum: FP = 0
# cdef enum: WP = 1

# cdef unsigned int XP = 0
# cdef unsigned int VP = 1
# cdef unsigned int SP = 2
# cdef unsigned int FP = 0
# cdef unsigned int WP = 1
