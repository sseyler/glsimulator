#cython: boundscheck=False, wraparound=False, initializedcheck=False, nonecheck=False
#cython: cdivision=True, profile=False, linetrace=False, binding=True, language_level=3

import numpy as np
cimport numpy as np

from libc.stdlib cimport malloc, free
from cpython.mem cimport PyMem_Malloc, PyMem_Free

from glsimulator.core.types cimport *

from cpython.ref cimport PyObject
from cpython.pycapsule cimport (PyCapsule_IsValid, PyCapsule_GetPointer,
                                PyCapsule_GetDestructor, PyCapsule_SetDestructor)
from randomgen.distributions cimport brng_t, random_gauss_zig, random_gauss_zig_f
from randomgen.common cimport *

# from randomgen import MT19937, Xoroshiro128, Xorshift1024, PCG64, ThreeFry, DSFMT
from randomgen.mt19937 import MT19937
from randomgen.xoroshiro128 import Xoroshiro128
from randomgen.xorshift1024 import Xorshift1024
from randomgen.pcg64 import PCG64
from randomgen.philox import Philox
from randomgen.threefry import ThreeFry
from randomgen.dsfmt import DSFMT

from cython_gsl cimport *
from cython_gsl cimport gsl
# from cython_gsl.gsl_rng cimport gsl_rng_type
#############################################################

DEF ISEED = 23061989

cpdef RNGContainer get_rng(str lib_name, str brng_name,
        unsigned long long seed=ISEED, unsigned long long jump=0, keys=None):
    '''Return an RNGContainer objectself.

      Choose 'gsl' or 'randomgen' for `lib_name` to select the
      (Cython) GNU Scientific Library or the RandomGen package.
    '''
    cdef RNGContainer rngc
    if lib_name == 'gsl':
        rngc = CythonGSLRNG(brng_name, seed, jump)
    elif lib_name == 'randomgen':
        if keys is not None:
            rngc = RandomGenParallelRNG(brng_name, keys.astype(np.uint64))
        else:
            rngc = RandomGenRNG(brng_name, seed, jump)
    else:
        print('No RNGContainer available for \'{}\'; current valid options ' + \
               'are \'gsl\' and \'randomgen\'.')
        rngc = None
    return rngc

################################################################################
cdef class RNGContainer:
    cdef void set_seed(self, unsigned long long seed):
        pass
    cdef void set_jump(self, unsigned long long jump):
        pass
    cdef real_t gauss_rv(self):
        return 0

################################################################################
## CythonGSLRNG
#================================================
cdef class CythonGSLRNG(RNGContainer):
    def __cinit__(self, str brng_name, unsigned long long seed=ISEED,
                                       unsigned long long jump=0):
        self.__gsl_brng = NULL
        self.__gsl_state = NULL
        self._initialize(brng_name, seed, jump)

    cdef void _initialize(self, str brng_name, unsigned long long seed,
                                               unsigned long long jump):
        if brng_name == 'mt19937':
            self.__gsl_brng = gsl_rng_mt19937
        elif brng_name == 'gfsr4':
            self.__gsl_brng = gsl_rng_gfsr4
        elif brng_name == 'taus':
            self.__gsl_brng = gsl_rng_taus
        elif brng_name == 'taus2':
            self.__gsl_brng = gsl_rng_taus2
        elif brng_name == 'ranlxs0':
            self.__gsl_brng = gsl_rng_ranlxs0
        elif brng_name == 'ranlxs1':
            self.__gsl_brng = gsl_rng_ranlxs1
        else:
            print('Error: basic RNG \'{}\' not found.'.format(brng_name))
        self.brng_name = brng_name
        self.__gsl_state = gsl_rng_alloc(self.__gsl_brng)
        self.set_seed(seed)

    cdef void set_seed(self, unsigned long long seed):
        self._seed = seed
        gsl_rng_set(self.__gsl_state, self._seed)

    cdef void set_jump(self, unsigned long long jump):
        self._jump = jump
        pass # gsl_rng_set(self.__gsl_state, self._seed)

    cdef real_t gauss_rv(self):
        return gsl_ran_gaussian(self.__gsl_state, 1)

    def __dealloc__(self):
        if self.__gsl_state is not NULL:
            gsl_rng_free(self.__gsl_state)
            self.__gsl_state = NULL
        # if self.__gsl_brng is not NULL:
        #     PyMem_Free(self.__gsl_brng)
        #     self.__gsl_brng = NULL


################################################################################
## RandomGenRNG
#================================================
# WARN: apparently, making RandomGenRNG a subclass of the RNGContainer type makes it
#       such that the pointer self.__rg_state must be coerced to a Python object.
#  See https://bashtage.github.io/randomgen/parallel.html for more info

cdef class RandomGenRNG(RNGContainer):
    def __cinit__(self, str brng_name, unsigned long long seed=ISEED,
                                       unsigned long long jump=0):
        self.__rg_state = NULL
        self._basicrng = None
        self._initialize(brng_name, seed, jump)

    cdef void _initialize(self, str brng_name, unsigned long long seed,
                                               unsigned long long jump):
        if brng_name == 'mt19937':
            brng = MT19937(seed)
        elif brng_name == 'xoroshiro128':
            brng = Xoroshiro128(seed)
        elif brng_name == 'xorshift1024':
            brng = Xorshift1024(seed)
        elif brng_name == 'pcg64':
            brng = PCG64(seed)
        elif brng_name == 'philox':
            brng = Philox(seed)
        elif brng_name == 'threefry':
            brng = ThreeFry(seed)
        elif brng_name == 'dsfmt':
            brng = DSFMT(seed)
        else:
            print('Error: basic RNG \'{}\' not found.'.format(brng_name))
        self._basicrng = brng

        # self.set_seed(seed)
        # self.set_jump(jump)
        brng.seed(seed)
        brng.jump(jump)
        self._seed = seed
        self._jump = jump

        capsule = brng.capsule
        cdef const char *name = "BasicRNG"
        # Optional: check that capsule is from a Basic RNG, then cast ptr
        if not PyCapsule_IsValid(capsule, name):
            raise ValueError("Invalid brng. The brng must be instantized.")
        self.__rg_state = <brng_t *>PyCapsule_GetPointer(capsule, name)
        # self.__rg_state = <brng_t*>PyMem_Malloc(sizeof(rg_state[0]))
        # PyCapsule_SetDestructor(<void *>rg_state, &_free_pycapsule_ptr)
        # PyCapsule_SetDestructor(self.capsule, &_free_pycapsule_ptr)

    cdef void set_seed(self, unsigned long long seed):
        self._seed = seed
        self._basicrng.seed(self._seed)

    cdef void set_jump(self, unsigned long long jump):
        self._jump = jump
        self._basicrng.jump(self._jump)

    cdef real_t gauss_rv(self):
        return random_gauss_zig(self.__rg_state)

    def __dealloc__(self):
        pass
        # if self.__rg_state is not NULL:
        #     free(self.__rg_state)
        #     self.__rg_state = NULL

    @property
    def state(self):
        """
            Get or set the Basic RNG's state

            Returns
            -------
            state : dict
                Dictionary containing the information required to describe the
                state of the Basic RNG

            Notes
            -----
            This is a trivial pass-through function.  RandomGenerator does not
            directly contain or manipulate the basic RNG's state.
        """
        return self._basicrng.state

    @state.setter
    def state(self, value):
        self._basicrng.state = value

# cdef void _free_pycapsule_ptr(PyObject *capsule):  # object capsule
#     # This should probably have some error checking in
#     # or at very least clear any errors raised once it's done
#     cdef brng_t *rg_state
#     print("Entered destructor\n")
#     rg_state = <brng_t*>PyCapsule_GetPointer(capsule, "MAP_C_API");
#     if (rg_state == NULL):
#          return
#     free(rg_state)

# cdef void _free_pycapsule_ptr(object capsule):
#     # This should probably have some error checking in
#     # or at very least clear any errors raised once it's done
#     cdef brng_t *rg_state
#     print("Entered destructor\n")
#     rg_state = <brng_t*>PyCapsule_GetPointer(capsule, "MAP_C_API");
#     if (rg_state == NULL):
#          return
#     free(rg_state)


################################################################################
## RandomGenRNG
#================================================
# WARN: apparently, making RandomGenRNG a subclass of the RNGContainer type makes it
#       such that the pointer self.__rg_state must be coerced to a Python object.
#  See https://bashtage.github.io/randomgen/parallel.html for more info

cdef class RandomGenParallelRNG(RNGContainer):
    def __cinit__(self, str brng_name, uint64_t[::1] keys):
        self.__rg_state = NULL
        self._basicrng = None
        self._seed = 0
        self._jump = 0
        self._initialize(brng_name, keys)

    cdef void _initialize(self, str brng_name, uint64_t[::1] keys):
        if brng_name == 'threefry':
            brng = ThreeFry(key=keys)
            # print np.array(keys)
        else:
            print('Error: basic RNG \'{}\' not found.'.format(brng_name))
        self._basicrng = brng
        self._keys = keys

        capsule = brng.capsule
        cdef const char *name = "BasicRNG"
        # Optional: check that capsule is from a Basic RNG, then cast ptr
        if not PyCapsule_IsValid(capsule, name):
            raise ValueError("Invalid brng. The brng must be instantized.")
        self.__rg_state = <brng_t *>PyCapsule_GetPointer(capsule, name)

    cdef real_t gauss_rv(self):
        return random_gauss_zig(self.__rg_state)

    def __dealloc__(self):
        pass

    @property
    def state(self):
        """
            Get or set the Basic RNG's state

            Returns
            -------
            state : dict
                Dictionary containing the information required to describe the
                state of the Basic RNG

            Notes
            -----
            This is a trivial pass-through function.  RandomGenerator does not
            directly contain or manipulate the basic RNG's state.
        """
        return self._basicrng.state

    @state.setter
    def state(self, value):
        self._basicrng.state = value
