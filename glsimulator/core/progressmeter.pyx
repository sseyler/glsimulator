#cython: boundscheck=False, wraparound=False, initializedcheck=False, nonecheck=False
#cython: cdivision=True, profile=False, linetrace=False, binding=True, language_level=3

import numpy as np
cimport numpy as cnp
cimport cython

from libc.math cimport round, lround, trunc, rint, lrint
from libc.stdio cimport printf, sprintf
# from libcpp.string cimport string

import sys
from glsimulator.core.timers cimport time_d


cdef class CProgressMeter:
    def __cinit__(self, unsigned long long DATASIZE, double print_interval=0.05, str unit='full', \
                        int width=50, double eta_print_interval=0.5, bint verbose=True):
        if DATASIZE <= 0:
            raise ValueError('Error: DATASIZE must be a positive integer\r\n')
        else:
            self.DATASIZE = DATASIZE
        self.status = ''
        # Initialize progress bar features
        self.bar   = ''
        self.width = width
        self.print_interval = print_interval
        # Initialize eta features
        self.t_eta_nextout = 0.0
        self.eta_print_interval = eta_print_interval
        self.etatxt = 'ETA: ...'
        # Initialize wall time features
        self.hours = 0.0
        self.secs  = 0.0
        self.msecs = 0.0
        self.mins  = 0.0
        # Initialize output vars
        self.unit  = 'full'
        self.verbose = verbose
        # Initialize timing vars
        self.start = -1.0
        self.end   = -1.0


cdef class ProgressMeter(CProgressMeter):
    def __enter__(self):
        # Set up toolbar
        sys.stdout.write('Progress:\n')
        sys.stdout.write('[%s]' % ('-' * self.width))
        sys.stdout.flush()
        sys.stdout.write('\b' * (self.width+1)) # return to start of line, after '['
        sys.stdout.flush()

        # Start timing
        self.start = <double>time_d()
        # self.t_nextout     = self.start
        self.t_eta_nextout = self.start
        return self

    @cython.cdivision(True)
    def __exit__(self, *args):
        cdef:
            str summary_str, details_str
            int isec, imin, ihr
            double fsec

        self.end   = <double>time_d()
        self.secs  = self.end - self.start
        self.msecs = self.secs * 1000  # millisecs
        self.mins  = self.secs / 60  # minutes
        self.hours = self.mins / 60

        sys.stdout.write('\n') # Add newline after progress bar
        sys.stdout.flush() # Reset line

        fsec = (self.secs%60)
        imin = <int>(self.mins%60)
        ihr  = <int>(self.hours)
        summary_str = '  --->  Elapsed time: '
        details_str = '{:02d}:{:02d}:{:05.2f}'.format(ihr, imin, fsec)

        print('{:s}{:s}\n'.format(summary_str, details_str))

    @cython.cdivision(True)
    cpdef update(self, unsigned long step, double curr_time=-1.0):
        cdef:
             double frac_done
             str bar
        curr_time = time_d() if curr_time < 0 else curr_time
        frac_done = (<double> step) / self.DATASIZE  # NOTE: DATASIZE checked at cinit
        if frac_done < 0.0:
            frac_done = 0.0
            self.status = 'Halt...\r\n'
        elif frac_done >= 1.0:
            frac_done = 1.0
            self.status = 'Done...\r\n'
        bar = self._update_text(frac_done, curr_time)
        self.printer(bar)

    @cython.cdivision(True)
    cpdef str _update_text(self, double frac_done, double t_now):
        cdef:
            double frac_left, t_elap, avg_rate, t_left
            long mins, secs, block
            str bar_str

        if t_now >= self.t_eta_nextout:
            frac_left = 1.0 - frac_done
            t_elap = t_now - self.start  # WARN: subtracting two large numbers; need double
            if frac_done <= 0.0:
                avg_rate = 0.0
            else:
                avg_rate = t_elap / frac_done
            t_left = rint(frac_left * avg_rate)
            mins = <long>(t_left / 60)
            secs = <long>(t_left % 60)
            self.etatxt = 'ETA: {:02d}:{:02d}'.format(mins, secs)
            self.t_eta_nextout += self.eta_print_interval
        block = lround(self.width*frac_done)
        bar_str = ('#'*block + '-'*(self.width-block))
        return '\r[{:s}] {:4.1f}% | {:s} {:s}'.format(bar_str, frac_done*100, \
                                                    self.etatxt, self.status)

    cpdef printer(self, str data):
        sys.stdout.write('\r\x1b[K'+data.__str__())
        sys.stdout.flush()
