#cython: boundscheck=False, wraparound=False, initializedcheck=False, nonecheck=False
#cython: cdivision=True, profile=False, linetrace=False, binding=True, language_level=3

from cython cimport view
# from libcpp.string cimport string

from glsimulator.core.types cimport *

################################################################################
## State
################################################################################
cdef class State:
    cdef:
        TrajMemBuffer __TMB
        TrajWriter    TrjW
        unsigned long NPAR, NAUX, NDIM, NTOUT, DUMPSIZE, NALLDIM
        unsigned long long NSTEPS, NTDUMP
        str _filename, _outdir

    cdef save_state(self, real_t[:,:,::1] Q, real_t[:,:,::1] QF, real_t[:,::1] QU,
                     unsigned long long ts)

################################################################################
## TrajWriter
################################################################################
cdef class TrajWriter:
    cdef:
        TrajMemBuffer __TMB
        unsigned long NPAR, NAUX, NDIM, NTOUT, DUMPSIZE, NALLDIM
        unsigned long long NSTEPS, NTDUMP
        str filename, outdir, ext, step_name
        # str Xt_fname, Vt_fname, Ft_fname, GSt_fname, GBt_fname, St_fname
        dict Qt_fnames, parquet_kwargs, hdf_kwargs

        object x_cols
        object v_cols
        object f_cols
        object u_cols
        object gs_cols
        object gb_cols
        object s_cols

        object df_pos
        object df_vel
        object df_fex
        object df_uex
        object df_ths
        object df_thb
        list df_aux  # object df_aux

    cpdef memory_to_dataframes_old(self, unsigned long long ts)
    cpdef memory_to_dataframes(self, unsigned long long ts)
    cpdef dataframes_to_disk(self, unsigned long long ts)

################################################################################
## TrajMemBuffer
################################################################################
cdef class TrajMemBuffer:
    cdef:
        unsigned long NPAR, NAUX, NDIM, NTOUT, DUMPSIZE, NALLDIM
        unsigned long long NSTEPS, NTDUMP
        real_t[:,:,::1]   _X
        real_t[:,:,::1]   _V
        real_t[:,:,::1]   _F
        real_t[:,::1]     _U
        real_t[:,:,::1]   _GS
        real_t[:,:,::1]   _GB
        real_t[:,:,:,::1] _S
