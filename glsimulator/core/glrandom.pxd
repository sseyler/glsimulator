#cython: boundscheck=False, wraparound=False, initializedcheck=False, nonecheck=False
#cython: cdivision=True, profile=False, linetrace=False, binding=True, language_level=3

from glsimulator.core.types cimport *

from cpython.ref cimport PyObject

from randomgen.distributions cimport brng_t
from randomgen.common cimport *
from cython_gsl cimport *
# from cython_gsl cimport gsl_rng
# from cython_gsl.gsl_rng cimport gsl_rng_type
#############################################################

cpdef RNGContainer get_rng(str lib_name, str brng_name,
                           unsigned long long seed=*,
                           unsigned long long jump=*, keys=*)

################################################################################
cdef class RNGContainer:
    cdef:
        str brng_name
        unsigned long long _seed, _jump
    cdef void set_seed(self, unsigned long long seed)
    cdef void set_jump(self, unsigned long long jump)
    cdef real_t gauss_rv(self)

################################################################################
## CythonGSL-based generator for normals
################################################################################
cdef class CythonGSLRNG(RNGContainer):
    cdef:
        gsl_rng_type *__gsl_brng
        gsl_rng      *__gsl_state
    cdef void _initialize(self, str brng_name, unsigned long long seed,
                                unsigned long long jump)
    cdef void set_seed(self, unsigned long long seed)
    cdef void set_jump(self, unsigned long long jump)
    cdef real_t gauss_rv(self)

################################################################################
## RandomGen-based generator for normals
################################################################################
# cdef void _free_pycapsule_ptr(PyObject *capsule)
# cdef void _free_pycapsule_ptr(object capsule)

cdef class RandomGenRNG(RNGContainer):
    cdef:
        # unsigned long _seed
        brng_t     *__rg_state
        public object _basicrng
    cdef void _initialize(self, str brng_name, unsigned long long seed,
                                unsigned long long jump)
    cdef void set_seed(self, unsigned long long seed)
    cdef void set_jump(self, unsigned long long jump)
    # cdef get_pycapsule_destructor(self)
    cdef real_t gauss_rv(self)


################################################################################
## RandomGen-based generator for normals
################################################################################
cdef class RandomGenParallelRNG(RNGContainer):
    cdef:
        brng_t     *__rg_state
        public object _basicrng
        uint64_t[::1] _keys
    cdef void _initialize(self, str brng_name, uint64_t[::1] keys)
    cdef real_t gauss_rv(self)
