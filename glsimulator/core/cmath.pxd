#cython: boundscheck=False, wraparound=False, initializedcheck=False, nonecheck=False
#cython: cdivision=True, profile=False, linetrace=False, binding=True, language_level=3

from libc.stdlib cimport RAND_MAX
from libcpp.limits cimport numeric_limits

from libc.math cimport log, sqrt, sin, cos


cdef double iRAND_MAX = 1.0/RAND_MAX
cdef double epsilon = numeric_limits[double].min()
cdef double two_pi = 2.0*3.14159265358979323846


cdef extern from "math.h":
    float  sqrt(float m)
    double sqrt(double m)

    float  log(float m)
    double log(double m)

    float  sin(float m)
    double sin(double m)

    float  cos(float m)
    double cos(double m)
