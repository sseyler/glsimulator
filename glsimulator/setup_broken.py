"""distutils installation of GLSimulator
Copyright (c) 2018 Sean L Seyler <slseyler@asu.com>
Released under the GNU Public License 3 (or higher, your choice)

For a basic build, just type the command:
    python setup.py build_ext
See the files INSTALL and README for details
"""
import sys
import os
from os.path import join

from distutils.core import setup
from distutils.extension import Extension
from distutils.sysconfig import get_config_vars

# import socket
import platform
import numpy as np
# from scipy._build_utils import numpy_nodepr_api  # silence numpy API warnings
import cython_gsl


###############################################################################
# Remove REALLY irritating -Wstrict-prototypes warnings for C++ compiling
###############################################################################
#   See: https://tinyurl.com/y7tbdj6u
cfg_vars = get_config_vars()
for key, value in cfg_vars.items():
    if type(value) == str:
        cfg_vars[key] = value.replace("-Wstrict-prototypes", "")

DEFAULT_SETUP = True
NB_COMPILE_JOBS = 6
COMPILER = 'gnu'
HOSTNAME = platform.node()  # socket.hostname() -- slower

# If USE_CYTHON is set to:
#   * True: enable building extensions using Cython
#   * False: build extensions via C files previously created using Cython
USE_CYTHON = True
CYTHON_LINETRACE = False
CYTHON_ANNOTATE = True

###############################################################################
# Set up compiler, environment, libraries, etc.
###############################################################################
if HOSTNAME == 'seylermoon':
    if COMPILER == 'gnu':
        os.environ["CC"] = "gcc-7"
        os.environ["CXX"] = "g++-7"
        COMPILE_ARGS = [
            '-march=native', '-O3', '-mavx', '-mprefer-avx128', '-ffast-math',
            '-ftree-vectorizer-verbose=2', '-Wno-cpp', '-Wno-unused-variable',
            '-Wno-unused-function', '-Wno-discarded-qualifiers']
    MINICONDA_INC_DIR = '/home/sseyler/Library/miniconda2/include'
elif HOSTNAME == 'seylerpluto':
    if COMPILER == 'gnu':
        os.environ["CC"] = "gcc"
        os.environ["CXX"] = "g++"
        COMPILE_ARGS = [
            '-march=native', '-O3', '-mavx', '-mprefer-avx128', '-ffast-math',
            '-ftree-vectorizer-verbose=2', '-Wno-cpp', '-Wno-unused-variable',
            '-Wno-unused-function', '-Wno-discarded-qualifiers']
        # '-mavx2', '-mprefer-avx128', '-msse', '-msse2', '-msse3',
        # '-mfpmath=sse', '-march=znver1', '-flto=`nproc`', '-flto', '-fPIC',
        # '-Wall', '-static', '-fgraphite-identity',
        # '-ftree-loop-distribution', '-floop-nest-optimize'
    if COMPILER == 'intel':
        os.environ["CC"] = "icc"
        os.environ["CXX"] = "icpc"
        os.environ["LINKCC"] = "icc"
        os.environ["LDSHARED"] = "icc -shared"
        COMPILE_ARGS = ['-fast', '-qopt-report=4']
    MINICONDA_INC_DIR = '/home/sseyler/Library/miniconda2/include'
# '-std=c++11', -fopenmp, -shared -pthread -fPIC -fwrapv -fno-strict-aliasing

###############################################################################
# Check whether Cython compilation step is needed
###############################################################################
if USE_CYTHON:
    try:
        # from setuptools import setup
        from Cython.Build import cythonize
        # from Cython.Distutils.extension import Extension

        global_compiler_directives = {}
        global_macros = []
        global_compiler_directives['language_level'] = 2
        # global_compiler_directives['boundscheck'] = False
        # global_compiler_directives['wraparound'] = False
        # global_compiler_directives['initializedcheck'] = False
        # global_compiler_directives['nonecheck'] = False
        # global_compiler_directives['cdivision'] = False
        if CYTHON_LINETRACE:
            global_compiler_directives['profile'] = True
            global_compiler_directives['linetrace'] = True
            global_compiler_directives['binding'] = True
            global_macros += ('CYTHON_TRACE', '1')
            # For nogil functions too:
            # distutils: define_macros=CYTHON_TRACE_NOGIL=1
            # from Cython.Compiler.Options import get_directive_defaults
            # directive_defaults = get_directive_defaults()
            # directive_defaults['profile'] = True
            # directive_defaults['linetrace'] = True
            # directive_defaults['binding'] = True
        if CYTHON_ANNOTATE:
            global_compiler_directives['annotate'] = True
    except ImportError:
        print("You don't seem to have Cython installed. Please get a")
        print("copy from www.cython.org and install it")
        sys.exit(1)


###############################################################################
# Specify libraries, library dirs, include dirs
#   IMPORTANT: Specify full module names in terms of directory structure
###############################################################################
module_dir = './glsimulator'
MODULES = {
    'glsim': 'glsimulator.glsim',
    'integrator': 'glsimulator.integrator',
    'system': 'glsimulator.system',
    'sim_state': 'glsimulator.sim_state',
    'force': 'glsimulator.force',
    'glrandom': 'glsimulator.glrandom',
    'types': 'glsimulator.types',
    'timers': 'glsimulator.utils.timers',
    'progressmeter': 'glsimulator.utils.progressmeter',
}

CYTHON_INC_DIR = cython_gsl.get_cython_include_dir()
CYTHON_GSL_LIBS = cython_gsl.get_libraries()
CYTHON_GSL_LIB_DIR = cython_gsl.get_library_dir()
CYTHON_GSL_INC_DIR = cython_gsl.get_include()
RANDOMGEN_INC_DIRS = [
        join('/home', 'sseyler', 'Repositories', 'python', 'randomgen'),
        join('/home', 'sseyler', 'Repositories', 'python', 'randomgen',
             'randomgen')
]
GLRANDOM_INC_DIRS = RANDOMGEN_INC_DIRS + [CYTHON_INC_DIR]

GENERAL_INC_DIRS = ['.', '...', os.path.join(os.getcwd(), 'include'),
                    np.get_include(), MINICONDA_INC_DIR, CYTHON_GSL_INC_DIR]
GENERAL_LIB_DIRS = [os.getcwd(), ]  # path to .a or .so file(s   )
GENERAL_LIBS = ['m']  # compile modules


###############################################################################
# Specify source files
###############################################################################
def mod_to_src(name):
    return [join(*name.split('.')) + '.pyx']


src_randomgen = join(GLRANDOM_INC_DIRS[1], 'src', 'distributions',
                     'distributions.c')
SOURCES = {k: mod_to_src(v) for k, v in MODULES.iteritems()}


###############################################################################
# Set up and build extensions
###############################################################################
def ensure_list(str_or_list):
    if str_or_list is None:
        return []
    elif isinstance(str_or_list, list):
        return str_or_list
    elif isinstance(str_or_list, str):
        return [str_or_list]


def generate_extension(name, mod_names=MODULES, src_names=SOURCES,
                       extra_libs=None, extra_lib_dirs=None,
                       extra_inc_dirs=None, extra_comp_args=None,
                       macros=global_macros, depends='', language='c'):
    src_fname = src_names[name]
    inc_dirs = GENERAL_INC_DIRS
    libs = GENERAL_LIBS
    lib_dirs = GENERAL_LIB_DIRS
    comp_args = COMPILE_ARGS

    inc_dirs += ensure_list(extra_inc_dirs)
    libs += ensure_list(extra_libs)
    lib_dirs += ensure_list(extra_lib_dirs)
    comp_args += ensure_list(extra_comp_args)

    return Extension(
        name=mod_names[name],
        sources=src_fname,
        include_dirs=inc_dirs,
        define_macros=macros,
        library_dirs=lib_dirs,
        libraries=libs,
        extra_compile_args=comp_args,
        depends=depends,
        language=language
    )


def _setup_given_extensions(extensions):
    setup(
        name='glsimulator',
        version='0.1a',
        description='Fluctuating Basset-Boussinesq-Oseen Simulator',
        author='Sean L Seyler',
        author_email='slseyler@asu.edu',
        packages=["glsimulator", "glsimulator.utils"],
        package_data={'': ['*.pyx', '*.pxd'], },
        include_dirs=GENERAL_INC_DIRS,
        ext_modules=cythonize(extensions, **global_compiler_directives),
        classifiers=[
            'Development Status :: Alpha',
            'Programming Language :: Python :: 2.7',
            'Programming Language :: Cython',
            'Intended Audience :: Science/Research',
            'Operating System :: POSIX :: Linux',
            'License :: OSI Approved :: GNU Lesser General Public License v3 (LGPLv3)',
            'Topic :: Scientific/Engineering :: Physics',
            'Topic :: Scientific/Engineering :: Mathematics',
        ],
        keywords=['langevin', 'generalized langevin', 'simulation',
                  'basset', 'boussinesq', 'brownian motion',
                  'hydrodynamic memory', 'non-markovian',
                  'barkovian embedding'],
        # install_requires=[
        #     'Django==1.10.1',
        #     'djangorestframework==3.4.7',
        #     'Markdown==2.6.7',
        #     'django-filter==0.15.3',
        #     'django_parler==1.6.5',
        #     'django-parler-rest==1.4.2',
        #     'django-versatileimagefield==1.6',
        # ],
        # cmdclass={"build_ext": build_ext},
        # compiler_directives = global_compiler_directives,
        # zip_safe = False,  # something about not egg with setuptools
    )


def setup_extensions_serial(extensions):
    _setup_given_extensions(extensions)


def setup_extensions_parallel(extensions):
    import multiprocessing as mp
    cythonize(extensions, nthreads=NB_COMPILE_JOBS)
    pool = mp.Pool(processes=NB_COMPILE_JOBS)
    pool.map(_setup_given_extensions, extensions)
    pool.close()
    pool.join()


###############################################################################
# Generate list of extensions
###############################################################################
EXTENSIONS = []
EXTENSIONS.append(generate_extension('timers'))
EXTENSIONS.append(generate_extension('progressmeter'))
EXTENSIONS.append(generate_extension('types'))
EXTENSIONS.append(generate_extension('glrandom',
                                     extra_inc_dirs=GLRANDOM_INC_DIRS,
                                     extra_libs=CYTHON_GSL_LIBS,
                                     extra_lib_dirs=CYTHON_GSL_LIB_DIR))
EXTENSIONS.append(generate_extension('force'))
EXTENSIONS.append(generate_extension('sim_state'))
EXTENSIONS.append(generate_extension('system'))
EXTENSIONS.append(generate_extension('integrator',
                                     extra_inc_dirs=CYTHON_INC_DIR,
                                     extra_libs=CYTHON_GSL_LIBS,
                                     extra_lib_dirs=CYTHON_GSL_LIB_DIR))
EXTENSIONS.append(generate_extension('glsim',
                                     extra_inc_dirs=CYTHON_INC_DIR,
                                     extra_libs=CYTHON_GSL_LIBS,
                                     extra_lib_dirs=CYTHON_GSL_LIB_DIR))

###############################################################################
# Run setup
###############################################################################
print('----------------------------------------------------------------------')
if False: #{}"build_ext" in sys.argv:
    print('Generating Cython extensions in parallel...\n')
    setup_extensions_parallel(EXTENSIONS)
else:
    print('Generating Cython extensions sequentially...\n')
    setup_extensions_serial(EXTENSIONS)
