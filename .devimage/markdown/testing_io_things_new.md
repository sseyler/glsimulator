## DP, cmplvl=9, expectedrows

`NSTEPS=25000000`

### Reference: all steps (`NTOUT=1`)
* **No output**:              00:00:57.44
* **Full output** (blosc):    00:01:50.27
* **Full output** (no cmpr):  00:02:21.36

### `NTOUT=10`

* `NTDUMP=5000000` (blosc):  00:01:01.41
* `NTDUMP=500000` (blosc):   00:01:00.48
