﻿﻿## Window 1

* [Helpful Python Code Snippets for Data Exploration in Pandas](https://medium.com/@msalmon00/helpful-python-code-snippets-for-data-exploration-in-pandas-b7c5aed5ecb9)
* [12 Useful Pandas Techniques in Python for Data Manipulation](https://www.analyticsvidhya.com/blog/2016/01/12-pandas-techniques-python-data-manipulation/)
* [‎](chrome://newtab/)
* [Create and Store Dask DataFrames — Dask 0.17.2 documentation](http://dask.pydata.org/en/latest/dataframe-create.html)
* [Dask and Pandas and XGBoost: Playing nicely between distributed systems](https://www.kdnuggets.com/2017/04/dask-pandas-xgboost-playing-nicely-distributed-systems.html)
* [Distributed Pandas on a Cluster with Dask DataFrames](http://matthewrocklin.com/blog/work/2017/01/12/dask-dataframes)
* [‎](chrome://newtab/)
* [HDF5 Or How I Learned To Love Data Compression And Partial I/O - Standard Deviations](https://dziganto.github.io/out-of-core%20computation/HDF5-Or-How-I-Learned-To-Love-Data-Compression-And-Partial-Input-Output/)
* [h5py: reading and writing HDF5 files in Python](http://christopherlovell.co.uk/blog/2016/04/27/h5py-intro.html)
* [Comparison of compression libs on HDF in pandas](http://danielhnyk.cz/comparison-of-compression-libs-on-hdf-in-pandas/)
* [Feather format update: Whence and Whither? - Wes McKinney](http://wesmckinney.com/blog/feather-arrow-future/)
* [Compression — pandas-msgpack 0.1.0 documentation](http://pandas-msgpack.readthedocs.io/en/latest/compression.html)
* [1404.6383.pdf](https://arxiv.org/pdf/1404.6383.pdf)
* [Efficient DataFrame Storage with Apache Parquet - Blue Yonder Technology Blog](https://tech.blue-yonder.com/efficient-dataframe-storage-with-apache-parquet/)
* [Introduction — bcolz 1.2.0 documentation](http://bcolz.readthedocs.io/en/latest/intro.html)
* [‎](chrome://newtab/)
* [python - pandas, store multiple datasets in an h5 file with pd.to_hdf - Stack Overflow](https://stackoverflow.com/questions/38268599/pandas-store-multiple-datasets-in-an-h5-file-with-pd-to-hdf?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa)
* [python - On disk indexing of Pandas multiindexed HDFStore - Stack Overflow](https://stackoverflow.com/questions/40503724/on-disk-indexing-of-pandas-multiindexed-hdfstore?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa)
* [6.21. Reading trajectories from memory — MDAnalysis.coordinates.memory — MDAnalysis 0.17.0 documentation](https://www.mdanalysis.org/docs/documentation_pages/coordinates/memory.html)

## Window 2

* [Extension Types — Cython 0.28.1 documentation](http://cython.readthedocs.io/en/latest/src/userguide/extension_types.html)
* [Extension types (aka. cdef classes) — Cython 0.28.1 documentation](http://cython.readthedocs.io/en/latest/src/tutorial/cdef_classes.html)
* [Cython and Extension Types - Cython (2015)](http://apprize.info/python/cython/5.html)
* [How to optimize for speed — scikit-learn 0.19.1 documentation](http://scikit-learn.org/stable/developers/performance.html)
* [Cython and Numba: Compiling Cython code](https://neurohackweek.github.io/cython-tutorial/02-compiling/)