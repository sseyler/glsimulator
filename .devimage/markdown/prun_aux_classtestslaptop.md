        2857678 function calls (2857362 primitive calls) in 14.288 seconds

   Ordered by: internal time

   ncalls  tottime  percall  cumtime  percall filename:lineno(function)
   200000    2.431    0.000    9.997    0.000 GLE_integrator_v3.py:244(ELI_step)
        1    1.830    1.830    1.830    1.830 {method 'randn' of 'mtrand.RandomState' objects}
   200000    1.799    0.000    1.799    0.000 GLE_integrator_v3.py:227(ELI_auxiliary)
   200000    1.145    0.000    1.145    0.000 GLE_integrator_v3.py:236(ELI_velocity)
       14    1.111    0.079    1.111    0.079 {method '_g_flush' of 'tables.hdf5extension.Leaf' objects}
   200000    0.947    0.000    0.947    0.000 GLE_integrator_v3.py:198(ELI_half_velocity)
   200000    0.906    0.000    0.906    0.000 GLE_integrator_v3.py:219(ELI_position)
        1    0.784    0.784   14.288   14.288 GLE_simulation_v3.py:147(run)
   200000    0.592    0.000    0.592    0.000 GLE_integrator_v3.py:213(ELI_Stokes_drag)
   200001    0.574    0.000    1.315    0.000 force_v3.py:66(apply)
   200000    0.485    0.000    0.485    0.000 GLE_integrator_v3.py:216(ELI_noise)
   200004    0.321    0.000    0.321    0.000 {numpy.core.multiarray.copyto}
   200000    0.274    0.000   10.271    0.000 GLE_simulation_v3.py:259(update)
   200002    0.244    0.000    0.741    0.000 numeric.py:150(ones)
   200000    0.238    0.000    0.238    0.000 force_v3.py:143(GWN_prealloc)
   200178    0.176    0.000    0.176    0.000 {numpy.core.multiarray.empty}
   200001    0.141    0.000    1.456    0.000 GLE_integrator_v3.py:191(calc_single_force)


ELI_step
----------

         2857676 function calls (2857360 primitive calls) in 12.946 seconds

   Ordered by: internal time

   ncalls  tottime  percall  cumtime  percall filename:lineno(function)
        1    1.965    1.965    1.965    1.965 {method 'randn' of 'mtrand.RandomState' objects}
   200000    1.828    0.000    1.828    0.000 GLE_integrator_v3.py:227(ELI_auxiliary)
   200000    1.354    0.000    9.092    0.000 GLE_integrator_v3.py:244(ELI_step)
   200000    1.237    0.000    1.237    0.000 GLE_integrator_v3.py:236(ELI_velocity)
   200000    0.979    0.000    0.979    0.000 GLE_integrator_v3.py:198(ELI_half_velocity)
   200000    0.958    0.000    0.958    0.000 GLE_integrator_v3.py:219(ELI_position)
       14    0.709    0.051    0.709    0.051 {method '_g_flush' of 'tables.hdf5extension.Leaf' objects}
        1    0.690    0.690   12.946   12.946 GLE_simulation_v3.py:147(run)
   200000    0.585    0.000    0.585    0.000 GLE_integrator_v3.py:213(ELI_Stokes_drag)
   200001    0.558    0.000    1.285    0.000 force_v3.py:66(apply)
   200000    0.486    0.000    0.486    0.000 GLE_integrator_v3.py:216(ELI_noise)
   200004    0.312    0.000    0.312    0.000 {numpy.core.multiarray.copyto}
   200000    0.262    0.000    9.354    0.000 GLE_simulation_v3.py:261(update)
   200002    0.241    0.000    0.727    0.000 numeric.py:150(ones)
   200000    0.237    0.000    0.237    0.000 force_v3.py:143(GWN_prealloc)
   200178    0.174    0.000    0.174    0.000 {numpy.core.multiarray.empty}
   200001    0.144    0.000    1.429    0.000 GLE_integrator_v3.py:191(calc_single_force)


ELInc_step
----------

         2657670 function calls (2657354 primitive calls) in 12.119 seconds

   Ordered by: internal time

   ncalls  tottime  percall  cumtime  percall filename:lineno(function)
   200000    2.067    0.000    8.592    0.000 GLE_integrator_v3.py:315(ELInc_step)
        1    1.859    1.859    1.859    1.859 {method 'randn' of 'mtrand.RandomState' objects}
   200000    1.651    0.000    1.651    0.000 GLE_integrator_v3.py:298(ELInc_auxiliary)
   200000    1.187    0.000    1.187    0.000 GLE_integrator_v3.py:276(ELInc_impulse_velocity)
   200000    0.724    0.000    0.724    0.000 GLE_integrator_v3.py:269(ELInc_half_velocity)
       14    0.705    0.050    0.705    0.050 {method '_g_flush' of 'tables.hdf5extension.Leaf' objects}
   200000    0.687    0.000    0.687    0.000 GLE_integrator_v3.py:290(ELInc_position)
   200000    0.685    0.000    0.685    0.000 GLE_integrator_v3.py:307(ELInc_velocity)
        1    0.532    0.532   12.119   12.119 GLE_simulation_v3.py:147(run)
   200001    0.525    0.000    1.227    0.000 force_v3.py:66(apply)
   200004    0.308    0.000    0.308    0.000 {numpy.core.multiarray.copyto}
   200002    0.236    0.000    0.702    0.000 numeric.py:150(ones)
   200000    0.228    0.000    0.228    0.000 force_v3.py:143(GWN_prealloc)
   200000    0.178    0.000    8.770    0.000 GLE_simulation_v3.py:261(update)
   200178    0.159    0.000    0.159    0.000 {numpy.core.multiarray.empty}
   200001    0.137    0.000    1.364    0.000 GLE_integrator_v3.py:191(calc_single_force)


         2657684 function calls (2657368 primitive calls) in 12.699 seconds

   Ordered by: internal time

   ncalls  tottime  percall  cumtime  percall filename:lineno(function)
   200000    2.050    0.000    8.984    0.000 GLE_integrator_v3.py:314(ELInc_step)
        1    2.025    2.025    2.025    2.025 {method 'randn' of 'mtrand.RandomState' objects}
   200000    1.772    0.000    1.772    0.000 GLE_integrator_v3.py:298(ELInc_auxiliary)
   200000    1.248    0.000    1.248    0.000 GLE_integrator_v3.py:276(ELInc_impulse_velocity)
   200000    0.781    0.000    0.781    0.000 GLE_integrator_v3.py:269(ELInc_half_velocity)
   200000    0.738    0.000    0.738    0.000 GLE_integrator_v3.py:290(ELInc_position)
   200000    0.723    0.000    0.723    0.000 GLE_integrator_v3.py:306(ELInc_velocity)
       14    0.711    0.051    0.711    0.051 {method '_g_flush' of 'tables.hdf5extension.Leaf' objects}
   200001    0.563    0.000    1.291    0.000 force_v3.py:66(apply)
        1    0.559    0.559   12.699   12.699 GLE_simulation_v3.py:147(run)
   200006    0.325    0.000    0.325    0.000 {numpy.core.multiarray.copyto}
   200002    0.238    0.000    0.728    0.000 numeric.py:150(ones)
   200000    0.236    0.000    0.236    0.000 force_v3.py:143(GWN_prealloc)
   200000    0.187    0.000    9.172    0.000 GLE_simulation_v3.py:263(update)
   200178    0.166    0.000    0.166    0.000 {numpy.core.multiarray.empty}
   200001    0.146    0.000    1.437    0.000 GLE_integrator_v3.py:191(calc_single_force)
