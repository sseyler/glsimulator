## double precision, No compression

`NSTEPS = 25000000`

* **No output**:                 00:00:56.61
* `NTOUT=1 | NTDUMP = NSTEPS`:   00:02:03.53

* `NTOUT=2 | NTDUMP = NSTEPS`:   00:01:32.29
* `NTOUT=2 | NTDUMP = NSTEPS/2`: 00:01:29.52
* `NTOUT=2 | NTDUMP = NSTEPS/5`: 00:01:28.53


## double precision, bzip2, complevel = 5, expectedrows OFF

`NSTEPS = 25000000`

* **No output**:                 00:00:56.61

* `NTOUT=2 | NTDUMP = NSTEPS`:   00:02:41.06
* `NTOUT=2 | NTDUMP = NSTEPS/2`: 00:02:41.90
* `NTOUT=2 | NTDUMP = NSTEPS/5`: 00:02:40.37


## double precision, bzip2, complevel = 5, expectedrows ON

`NSTEPS = 25000000`

* **No output**:                 00:00:56.61

* `NTOUT=2 | NTDUMP = NSTEPS`:   00:03:32.76
* `NTOUT=2 | NTDUMP = NSTEPS/2`: 00:03:06.03
* `NTOUT=2 | NTDUMP = NSTEPS/5`: 00:02:53.77


## double precision, blosc, complevel = 5, expectedrows OFF

`NSTEPS = 25000000`

* **No output**:                 00:00:56.61

* `NTOUT=2 | NTDUMP = NSTEPS`:   
* `NTOUT=2 | NTDUMP = NSTEPS/2`:
* `NTOUT=2 | NTDUMP = NSTEPS/5`: 00:01:18.08

## double precision, blosc, complevel = 5, expectedrows ON

`NSTEPS = 25000000`

* **No output**:                 00:00:56.61
* `NTOUT=1 | NTDUMP = NSTEPS`:   

* `NTOUT=2 | NTDUMP = NSTEPS`:   00:01:17.40
* `NTOUT=2 | NTDUMP = NSTEPS/2`:
* `NTOUT=2 | NTDUMP = NSTEPS/5`: 00:01:18.94
