from __future__ import with_statement, division

import signal

import itertools as it
# import multiprocessing as mp
from pathos.pools import ProcessPool

from math import ceil
from collections import OrderedDict
from progressbar_simple import ProgressBar

import numpy as np



class ParallelRunIDK(object):
    def __init__(self, tasks, run_kwargs):
        self.tasks = tasks  # try inputting a generator first
        self.run_kwargs = run_kwargs

        self.N_objs = len(self.tasks)
        self._results = None



    def run(self, N_WORKERS=1, N_TASKS=None, interval=1):
        temp_results = {}
        processes = []


    try:
        for oid in xrange(N_WORKERS):
            p = mp.Process(target=self.tasks[oid], args=(task_q, done_q))


    @property
    def results(self):
        if self._results is None:
            err_str = "No results data; do 'Parallelizer.run()' first."
            raise ValueError(err_str)
        return self._results





class ParallelRun(object):

    def __init__(self, tasks, run_kwargs):
        self.tasks = tasks  # try inputting a generator first
        self.run_kwargs = run_kwargs

        self.num_objs = len(self.tasks)
        self.results = None


    def _run_map_async(self, map_async, pb, chunksize, **kwargs):
        def _callback_function(r):
            global temp_results
            global pbi
            pb.update(pbi)
            pbi += 1
            return temp_results.extend(r)

        callback = kwargs.pop('callback', _callback_function)
        while True:
            groups = [pairs for pairs in it.islice(self.data_it, chunksize)]
            if groups:
                rs = map_async(self.func, groups, callback=callback)
            else:
                break
        return rs

    def _run_imap_func(self, imap_func, pb, chunksize, **kwargs):
        global temp_results
        global pbi
        r_it = imap_func(self.func, self.data_it, chunksize=chunksize)
        for i, item in enumerate(r_it):
            temp_results.append(item)
            if i % chunksize == 0:
                pb.update(pbi)
                pbi += 1
        return r_it

    def _run(self, method, pool, pb, chunksize, **kwargs):
        if method is 'map_async':
            f = pool.map_async
            return self._run_map_async(f, pb, chunksize, **kwargs)
        elif method is 'imap':
            f = pool.imap
            return self._run_imap_func(f, pb, chunksize, **kwargs)
        elif method is 'imap_unordered':
            f = pool.imap_unordered
            return self._run_imap_func(f, pb, chunksize, **kwargs)
        elif method is None:
            print "method is 'None'; please specify a method."
        else:
            print "Don't know how we got here, but there's a problem"


    def run(self, N_PROCESSES=1, TASK_SIZE=1, interval=1, method='map_async', **kwargs):
        global temp_results
        global pbi

        temp_results = []
        pbi = 0

        orig_sigint_handler = signal.signal(signal.SIGINT, signal.SIG_IGN)  # init_worker(signal.SIG_IGN)
        pool = mp.ProcessPool(N_PROCESSES)
        signal.signal(signal.SIGINT, orig_sigint_handler)  # init_worker(orig_sigint_handler)

        N_GROUPS = int(ceil(self.data_size/TASK_SIZE))
        with ProgressBar(N_GROUPS, interval=interval, unit='msec') as pb:
            try:
                rs = self._run(method, pool, pb, TASK_SIZE, **kwargs)
            except KeyboardInterrupt:
                print '\nCaught KeyboardInterrupt; terminating workers...'
                pool.terminate()
            except Exception, e:
                print '\nCaught exception: {}; terminating workers...'.format(e)
                pool.terminate()
            else:
                pool.close()
            pool.join()

        self._results = np.array(temp_results)

    @property
    def results(self):
        if self._results is None:
            err_str = "No results data; do 'Parallelizer.run()' first."
            raise ValueError(err_str)
        return self._results
