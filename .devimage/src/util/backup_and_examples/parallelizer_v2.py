from __future__ import with_statement, division

import pathos.multiprocessing as mp
# import multiprocessing
# import multiprocessing as mp
from math import ceil

import itertools as it
from functools import partial

from collections import OrderedDict
from progressbar_simple import ProgressBar


class ParallelRun(object):
    def __init__(self, obj_list, run_kwargs):
        self.obj_list = obj_list  # try inputting a generator first
        self.run_kwargs = run_kwargs

        self.N_objs = len(self.obj_list)
        self.results = None
        self.global_vars = {}


    def _worker(self, task_q, done_q):
        for func, run_kwargs, pos in iter(task_q.get, 'STOP'):
            result = func(**self.run_kwargs)
            done_q.put({pos : result})


    def run(self, N_WORKERS=1, N_TASKS=None, interval=1):
        temp_results = {}
        processes = []
        task_q = multiprocessing.Queue()
        done_q = multiprocessing.Queue()

        TASKS = [(obj.run, self.run_kwargs, pos) for pos, obj in enumerate(self.obj_list)]

        for task in TASKS:
            task_q.put(task)

        self.N_objs = len(TASKS)
        if N_TASKS is None:
            N_TASKS = self.N_objs

        print("%i workers, %i total data elements...") % (N_WORKERS, self.N_objs)

        try:
            for jid in xrange(N_WORKERS):
                p = mp.Process(target=self._worker, args=(task_q, done_q))
                processes.append(p)
                p.start()

            with ProgressBar(N_TASKS, interval=interval, unit='msec') as pb:
                for i in xrange(pb.N):
                    temp_results.update(done_q.get(block=True, timeout=None))
                    pb.update(i)
        except Exception, e:  # (KeyboardInterrupt, SystemExit):
            print "**Caught exception: ", e, "\nTerminating workers...\n"
            for p in processes:
                p.terminate()
        finally:
            for jid in xrange(N_WORKERS):  # NOTE: this may be superfluous due to p.join() below
                task_q.put('STOP')  # Tell child processes to stop
            for p in processes:
                p.join()
            task_q.close()
            done_q.close()
            self.sort_results(temp_results)

    def task_generator(self):
        for obj in self.obj_list:
            yield obj

    def sort_results(self, temp):
        sorted_results = OrderedDict(sorted(temp.items()))
        self.results = sorted_results.values()



class Parallelizer(object):
    def __init__(self, func, data):
        self.func = func
        self.data = data  # try inputting a generator first
        self.N_objs = len(self.data)
        self.results = None
        self.global_vars = {}


    def _calculate(self, func, args):
        return func(*args)

    def _worker(self, task_q, done_q):
        for func, args, pos in iter(task_q.get, 'STOP'):
            result = self._calculate(func, args)
            done_q.put({pos : result})


    def run(self, N_WORKERS=1, N_TASKS=None, interval=1):
        temp_results = {}
        processes = []
        task_q = mp.Queue()
        done_q = mp.Queue()

        # Submit tasks
        TASKS = [(self.func, args, pos) for pos, args in enumerate(self.data)]

        self.N_objs = len(TASKS)
        if N_TASKS is None:
            N_TASKS = self.N_objs

        for task in TASKS:
            task_q.put(task)

        print("%i workers, %i total data elements...") % (N_WORKERS, self.N_objs)

        try:
            for jid in xrange(N_WORKERS):
                p = mp.Process(target=self._worker, args=(task_q, done_q))
                processes.append(p)
                p.start()

            with ProgressBar(N_TASKS, interval=interval, unit='msec') as pb:
                for i in xrange(pb.N):
                    temp_results.update(done_q.get(block=True, timeout=None))
                    pb.update(i)
        except Exception, e:  # (KeyboardInterrupt, SystemExit):
            print "**Caught exception: ", e, "\nTerminating workers...\n"
            for p in processes:
                p.terminate()
        finally:
            for jid in xrange(N_WORKERS):  # NOTE: this may be superfluous due to p.join() below
                task_q.put('STOP')  # Tell child processes to stop
            for p in processes:
                p.join()
            task_q.close()
            done_q.close()
            self.sort_results(temp_results)

    def task_generator(self):
        for datum in self.data:
            yield datum

    def sort_results(self, temp):
        sorted_results = OrderedDict(sorted(temp.items()))
        self.results = sorted_results.values()
