from __future__ import with_statement, division

import signal

import itertools as it
import multiprocessing as mp

from math import ceil
from collections import OrderedDict
from progressbar_simple import ProgressBar

import numpy as np

def init_worker(sigint_handler):
    signal.signal(signal.SIGINT, sigint_handler)


def flatten_list(l):
    return [item for sublist in l for item in sublist]


class Parallelizer(object):

    def __init__(self, func, data, data_size=None):
        self.func = func
        self.data = data  # try inputting a generator first
        self.data_it = iter(self.data)
        self.data_size = data_size or None

        self._results = None


    def run(self, N_WORKERS=1, TASK_SIZE=1, interval=1, method='map_async', **kwargs):
        global temp_results
        global pbi

        temp_results = []
        pbi = 0

        orig_sigint_handler = signal.signal(signal.SIGINT, signal.SIG_IGN)  # init_worker(signal.SIG_IGN)
        pool = mp.Pool(N_WORKERS)
        signal.signal(signal.SIGINT, orig_sigint_handler)  # init_worker(orig_sigint_handler)

        N_GROUPS = int(ceil(self.data_size/TASK_SIZE))
        with ProgressBar(N_GROUPS, interval=interval, unit='msec') as pb:
            try:
                rs = self._run(method, pool, pb, TASK_SIZE, **kwargs)
            except KeyboardInterrupt:
                print '\nCaught KeyboardInterrupt; terminating workers...'
                pool.terminate()
            except Exception, e:
                print '\nCaught exception: {}; terminating workers...'.format(e)
                pool.terminate()
            else:
                pool.close()
            pool.join()

        self._results = np.array(temp_results)


    def _run(self, method, pool, pb, chunksize, **kwargs):
        if method is 'map_async':
            f = pool.map_async
            return self._run_map_async(f, pb, chunksize, **kwargs)
        elif method is 'imap':
            f = pool.imap
            return self._run_imap_func(f, pb, chunksize, **kwargs)
        elif method is 'imap_unordered':
            f = pool.imap_unordered
            return self._run_imap_func(f, pb, chunksize, **kwargs)
        elif method is None:
            print "method is 'None'; please specify a method."
        else:
            print "Don't know how we got here, but there's a problem"


    def _run_imap_func(self, imap_func, pb, chunksize, **kwargs):
        global temp_results
        global pbi
        r_it = imap_func(self.func, self.data_it, chunksize=chunksize)
        for i, item in enumerate(r_it):
            temp_results.append(item)
            if i % chunksize == 0:
                pb.update(pbi)
                pbi += 1
        return r_it


    def _run_map_async(self, map_async, pb, chunksize, **kwargs):
        def _callback_function(r):
            global temp_results
            global pbi
            pb.update(pbi)
            pbi += 1
            return temp_results.extend(r)

        callback = kwargs.pop('callback', _callback_function)
        while True:
            groups = [pairs for pairs in it.islice(self.data_it, chunksize)]
            if groups:
                rs = map_async(self.func, groups, callback=callback)
            else:
                break
        return rs


    @property
    def results(self):
        if self._results is None:
            err_str = "No results data; do 'Parallelizer.run()' first."
            raise ValueError(err_str)
        return self._results
