"""General plotters.

"""

import seaborn.apionly as sns
import pandas as pd
import numpy as np
import os
import pdb

def plot_rolling(data, columns=None, colors=None, window=10, percentile=.95, ax=None, **kwargs):
    """Plot the columns of the dataframe as rolling means and percentile bands.

    This function plots a coarsened representation of the data, but with almost as many points
    as the original data. That is

        len(data) - window

    number of points.

    :Arguments:
        *data*
            DataFrame or Series to plot data from

    :Keywords:
        *columns*
            columns of dataset to plot; ``None`` means all will be plotted; only
            applies to DataFrames
        *colors*
            list of colors to use for plotting, in column order; ``None`` will
            use default color palette
        *window*
            number of rows to use for rolling window
        *percentile*
            percentile of points to include (e.g. .90 means 5% and 95% bars)
        *ax*
            axis to plot to

        **kwargs passed to plotter

    :Returns:
        *axis*
            axis used for plotting

    """
    mean = pd.rolling_mean(data, window)

    lower = (1 - percentile)/2
    upper = (1 - lower)

    low = pd.rolling_quantile(data, window, lower)
    high = pd.rolling_quantile(data, window, upper)

    if isinstance(data, pd.DataFrame):
        if not columns:
            columns = data.columns.values
        if not colors:
            colors = sns.color_palette()
        if not ax:
            fig = sns.plt.figure()
            ax = fig.add_subplot(1,1,1)

        for column, color in zip(columns, colors):
            mean[column].plot(color=color, ax=ax, **kwargs)
            ax.fill_between(low.index, low[column], high[column], alpha=.3, color=color)
    elif isinstance(data, pd.Series):
        if not colors:
            colors = sns.color_palette()[0]
        if not ax:
            fig = sns.plt.figure()
            ax = fig.add_subplot(1,1,1)
        mean.plot(color=colors, ax=ax, **kwargs)
        ax.fill_between(low.index, low, high, alpha=.3, color=colors)

    return ax

def plot_coarsened(data, columns=None, colors=None, window=10, percentile=.95, labels=None, ax=None, plot_bands=True, **kwargs):
    """Plot the columns of the dataframe as block means with percentile bands.

    This results in plots similar to those produced with plot_rolling, but with far fewer points
    needing to be plotted on the canvas. In other words, this function plots

        len(data)/window

    number of points.

    :Arguments:
        *data*
            DataFrame or Series to plot data from

    :Keywords:
        *columns*
            columns of dataset to plot; ``None`` means all will be plotted; only
            applies to DataFrames
        *colors*
            list of colors to use for plotting, in column order; ``None`` will
            use default color palette
        *window*
            number of rows to use for rolling window
        *percentile*
            percentile of points to include (e.g. .90 means 5% and 95% bars)
        *labels*
            labels to use for each column; if ``None`` column names will be
            used when available
        *ax*
            axis to plot to
        *plot_bands*
            if True, plot the bands; otherwise, plot only the block means

        **kwargs passed to plotter

    :Returns:
        *axis*
            axis used for plotting

    """
    numpoints = len(data)/window

    if isinstance(data, pd.DataFrame):
        if not columns:
            columns = data.columns.values

        positions = np.zeros((numpoints, len(columns)))
        means = np.zeros((numpoints, len(columns)))
        lows = np.zeros((numpoints, len(columns)))
        highs = np.zeros((numpoints, len(columns)))

        for i in xrange(numpoints):
            print "\r{} of {}".format(i, numpoints),
            start = i * window
            stop = start + window

            means[i] = data[columns].iloc[start:stop].mean()
            positions[i] = data[columns].iloc[start:stop].index.values.mean()

            lower = (1 - percentile)/2
            upper = (1 - lower)

            lows[i] = data[columns].iloc[start:stop].quantile(lower)
            highs[i] = data[columns].iloc[start:stop].quantile(upper)

        if not colors:
            colors = sns.color_palette()
        if not ax:
            fig = sns.plt.figure()
            ax = fig.add_subplot(1,1,1)
        if not labels:
            labels = columns

        for j, column, color in zip(xrange(len(columns)), columns, colors):
            ax.plot(positions[:,j], means[:,j], color=color, label=labels[j], **kwargs)
            if plot_bands:
                ax.fill_between(positions[:,j], lows[:,j], highs[:,j], alpha=.3, color=color)
    elif isinstance(data, pd.Series):
        positions = np.zeros((numpoints))
        means = np.zeros((numpoints))
        lows = np.zeros((numpoints))
        highs = np.zeros((numpoints))

        for i in xrange(numpoints):
            start = i * window
            stop = start + window

            means[i] = data.iloc[start:stop].mean()
            positions[i] = data.iloc[start:stop].index.values.mean()

            lower = (1 - percentile)/2
            upper = (1 - lower)

            lows[i] = data.iloc[start:stop].quantile(lower)
            highs[i] = data.iloc[start:stop].quantile(upper)

        if not colors:
            colors = sns.color_palette()
        if not ax:
            fig = sns.plt.figure()
            ax = fig.add_subplot(1,1,1)
        if not labels:
            labels = [None,]

        ax.plot(positions[:], means[:], color=colors[0], label=labels[0], **kwargs)
        if plot_bands:
            ax.fill_between(positions[:], lows[:], highs[:], alpha=.3, color=colors[0])

    return ax


def multiplot_coarsened(data, columns=None, colors=None, window=10, percentile=.95, labels=None, ax=None, plot_bands=True, **kwargs):
    """Plot the columns of the dataframe as block means with percentile bands.

    This results in plots similar to those produced with plot_rolling, but with far fewer points
    needing to be plotted on the canvas. In other words, this function plots

        len(data)/window

    number of points.

    :Arguments:
        *data*
            list of DataFrames or Series to plot

    :Keywords:
        *to_series*
            convert each DataFrame to a Series; assumes (for now) each has a
            single column of data
        *columns*
            columns of dataset to plot; ``None`` means all will be plotted; only
            applies to DataFrames
        *colors*
            list of colors to use for plotting, in column order; ``None`` will
            use default color palette
        *window*
            number of rows to use for rolling window
        *percentile*
            percentile of points to include (e.g. .90 means 5% and 95% bars)
        *labels*
            labels to use for each column; if ``None`` column names will be
            used when available
        *ax*
            axis to plot to
        *plot_bands*
            if True, plot the bands; otherwise, plot only the block means

        **kwargs passed to plotter

    :Returns:
        *axis*
            axis used for plotting

    """

    if isinstance(data, list):

        if not ax:
            fig = sns.plt.figure()
            ax = fig.add_subplot(1,1,1)
        if not colors:
            colors = sns.color_palette("Set1", n_colors=8)

        for i, datum in enumerate(data):

            if isinstance(datum, pd.DataFrame):

                df = datum
                columns = df.columns.values
                n_col = len(columns)
                # column = df.columns.values[0]

                numpoints = len(datum)/window

                # positions = np.zeros((numpoints))
                # means = np.zeros((numpoints))
                # lows = np.zeros((numpoints))
                # highs = np.zeros((numpoints))

                positions = np.zeros((numpoints, n_col))
                means = np.zeros((numpoints, n_col))
                lows = np.zeros((numpoints, n_col))
                highs = np.zeros((numpoints, n_col))

                for j in xrange(numpoints):
                    print "\r{} of {}".format(j, numpoints),
                    start = j * window
                    stop = start + window

                    means[j] = df[columns].iloc[start:stop].mean()
                    positions[j] = df[columns].iloc[start:stop].index.values.mean()

                    lower = (1 - percentile)/2
                    upper = (1 - lower)

                    lows[j] = df[columns].iloc[start:stop].quantile(lower)
                    highs[j] = df[columns].iloc[start:stop].quantile(upper)

                labels = columns

                # Plot the DataFrame
                # ax.plot(positions[:], means[:], color=color, label=label, **kwargs)
                # if plot_bands:
                #     ax.fill_between(positions[:], lows[:], highs[:], alpha=.3, color=color)
                for k, column in enumerate(columns):
                    color = colors[n_col*i+k]
                    ax.plot(positions[:,k], means[:,k], color=color, label=labels[k], **kwargs)
                    if plot_bands:
                        ax.fill_between(positions[:,k], lows[:,k], highs[:,k], alpha=.3, color=color)

    # for i, df in enumerate(data):
    #     if isinstance(df, pd.DataFrame):

    #         numpoints = len(data)/window

    #         if not columns:
    #             columns = df.columns.values

    #         positions = np.zeros((numpoints, len(columns)))
    #         means = np.zeros((numpoints, len(columns)))
    #         lows = np.zeros((numpoints, len(columns)))
    #         highs = np.zeros((numpoints, len(columns)))

    #         for j in xrange(numpoints):
    #             print "\r{} of {}".format(j, numpoints),
    #             start = j * window
    #             stop = start + window

    #             means[j] = df[columns].iloc[start:stop].mean()
    #             positions[j] = df[columns].iloc[start:stop].index.values.mean()

    #             lower = (1 - percentile)/2
    #             upper = (1 - lower)

    #             lows[j] = df[columns].iloc[start:stop].quantile(lower)
    #             highs[j] = df[columns].iloc[start:stop].quantile(upper)

    #         if not colors:
    #             colors = sns.color_palette()
    #         if not ax:
    #             fig = sns.plt.figure()
    #             ax = fig.add_subplot(1,1,1)
    #         if not labels:
    #             labels = columns

    #         for k, column, color in zip(xrange(len(columns)), columns, colors):
    #             ax.plot(positions[:,k], means[:,k], color=color, label=labels[k], **kwargs)
    #             if plot_bands:
    #                 ax.fill_between(positions[:,k], lows[:,k], highs[:,k], alpha=.3, color=color)

    #     elif isinstance(df, pd.Series):
    #         positions = np.zeros((numpoints))
    #         means = np.zeros((numpoints))
    #         lows = np.zeros((numpoints))
    #         highs = np.zeros((numpoints))

    #         for j in xrange(numpoints):
    #             start = j * window
    #             stop = start + window

    #             means[j] = data.iloc[start:stop].mean()
    #             positions[j] = data.iloc[start:stop].index.values.mean()

    #             lower = (1 - percentile)/2
    #             upper = (1 - lower)

    #             lows[j] = data.iloc[start:stop].quantile(lower)
    #             highs[j] = data.iloc[start:stop].quantile(upper)

    #         if not colors:
    #             colors = sns.color_palette()
    #         if not ax:
    #             fig = sns.plt.figure()
    #             ax = fig.add_subplot(1,1,1)
    #         if not labels:
    #             labels = [None,]

    #         ax.plot(positions[:], means[:], color=colors[i], label=labels[i], **kwargs)
    #         if plot_bands:
    #             ax.fill_between(positions[:], lows[:], highs[:], alpha=.3, color=colors[i])

    return ax


#TODO: NEED CIRCULAR MEAN!!!
def plot_coarsened_polar(data, columns=None, colors=None, window=10, percentile=.95, ax=None, **kwargs):
    """Plot the columns of the dataframe as block means with percentile bands
        on a polar grid.

    This results in plots similar to those produced with plot_rolling, but with far fewer points
    needing to be plotted on the canvas. In other words, this function plots

        len(data)/window

    number of points. Also, the plots are on a polar grid. Note that the fills
    don't work perfectly.

    :Arguments:
        *data*
            DataFrame or Series to plot data from

    :Keywords:
        *columns*
            columns of dataset to plot; ``None`` means all will be plotted; only
            applies to DataFrames
        *colors*
            list of colors to use for plotting, in column order; ``None`` will
            use default color palette
        *window*
            number of rows to use for rolling window
        *percentile*
            percentile of points to include (e.g. .90 means 5% and 95% bars)
        *ax*
            axis to plot to

        **kwargs passed to plotter

    :Returns:
        *axis*
            axis used for plotting

    """
    numpoints = len(data)/window

    if isinstance(data, pd.DataFrame):
        if not columns:
            columns = data.columns.values

        positions = np.zeros((numpoints, len(columns)))
        means = np.zeros((numpoints, len(columns)))
        lows = np.zeros((numpoints, len(columns)))
        highs = np.zeros((numpoints, len(columns)))

        for i in xrange(numpoints):
            start = i * window
            stop = start + window

            means[i] = data[columns].iloc[start:stop].mean()
            positions[i] = data[columns].iloc[start:stop].index.values.mean()

            lower = (1 - percentile)/2
            upper = (1 - lower)

            lows[i] = data[columns].iloc[start:stop].quantile(lower)
            highs[i] = data[columns].iloc[start:stop].quantile(upper)

        if not colors:
            colors = sns.color_palette()
        if not ax:
            fig = sns.plt.figure()
            ax = fig.add_subplot(111, polar=True)

        for j, column, color in zip(xrange(len(columns)), columns, colors):
            ax.plot(means[:,j], positions[:,j], color=color, **kwargs)
            c1 = ax.plot(lows[:,j], positions[:,j], alpha=.3, color=color)[0]
            y1 = c1.get_ydata()
            x1 = c1.get_xdata()
            c2 = ax.plot(highs[:,j], positions[:,j], alpha=.3, color=color)[0]
            y2 = c2.get_ydata()
            x2 = c2.get_xdata()
            ax.fill_betweenx(y1, x1, x2, color=color, alpha=.3)

    elif isinstance(data, pd.Series):
        positions = np.zeros((numpoints))
        means = np.zeros((numpoints))
        lows = np.zeros((numpoints))
        highs = np.zeros((numpoints))

        for i in xrange(numpoints):
            start = i * window
            stop = start + window

            means[i] = data.iloc[start:stop].mean()
            positions[i] = data.iloc[start:stop].index.values.mean()

            lower = (1 - percentile)/2
            upper = (1 - lower)

            lows[i] = data.iloc[start:stop].quantile(lower)
            highs[i] = data.iloc[start:stop].quantile(upper)

        if not colors:
            colors = sns.color_palette()[0]
        if not ax:
            fig = sns.plt.figure()
            ax = fig.add_subplot(111, polar=True)

        ax.plot(means[:,j], positions[:,j], color=colors, **kwargs)
        c1 = ax.plot(lows[:,j], positions[:,j], alpha=.3, color=colors)[0]
        y1 = c1.get_ydata()
        x1 = c1.get_xdata()
        c2 = ax.plot(highs[:,j], positions[:,j], alpha=.3, color=colors)[0]
        y2 = c2.get_ydata()
        x2 = c2.get_xdata()
        ax.fill_betweenx(y1, x1, x2, color=colors, alpha=.3)

    return ax

def plot_column_aggregates(data, columns=None, color=None, percentile=.95, ax=None, **kwargs):
    """Plot the mean and percentile bands for the selected columns of a dataframe.

    :Arguments:
        *data*
            aggregated dataframe to plot

    :Keywords:
        *columns*
            columns of dataset to plot on x-axis; ``None`` means all will be plotted
        *color*
            color to use for plotting; ``None`` will use default color palette
        *percentile*
            percentile of points to include (e.g. .90 means 5% and 95% bars)
        *ax*
            axis to plot to

        **kwargs passed to plotter

    :Returns:
        *axis*
            axis used for plotting

    """
    lower = (1 - percentile)/2
    upper = (1 - lower)

    low = data.quantile(lower, axis=0)
    high = data.quantile(upper, axis=0)

    if not columns:
        columns = data.columns.values
    if not color:
        color = sns.color_palette()[0]
    if not ax:
        fig = sns.plt.figure()
        ax = fig.add_subplot(1,1,1)

    data[columns].mean().plot(color=color, ax=ax, **kwargs)
    pdb.set_trace()
    ax.fill_between(low.index, low[columns], high[columns], alpha=.3, color=color)

    return ax


def aggregate(containers, dataset, columns=None):
    """Build an aggregate dataframe from a dataset from an ensemble of
        containers.

    :Arguments:
        *containers*
            containers to pull data from
        *dataset*
            name of datset to pull

    :Keywords:
        *columns*
            columns of dataset to plot; ``None`` means all will be plotted

    :Returns:
        *agg*
            aggregated dataframe
    """
    agg = None
    for container in containers:
        print "\rGrabbing data for '{}'".format(container.name),
        df = container.data.retrieve(dataset, columns=columns)
        label = len(df.index)*[container.name]
        index = pd.MultiIndex.from_arrays([label, df.index])
        df = df.set_index(index)

        if agg is not None:
            agg = agg.append(df)
        else:
            agg = df

    return agg

def plot_ensemble(aggdf, columns=None, colors=None, percentile=.95, ax=None, **kwargs):
    """Plot the mean and percentile bands for an aggregated dataframe.

    :Arguments:
        *aggdf*
            aggregated dataframe to plot

    :Keywords:
        *columns*
            columns of dataset to plot; ``None`` means all will be plotted
        *colors*
            list of colors to use for plotting, in column order; ``None`` will
            use default color palette
        *percentile*
            percentile of points to include (e.g. .90 means 5% and 95% bars)
        *ax*
            axis to plot to

        **kwargs passed to plotter

    :Returns:
        *axis*
            axis used for plotting

    """
    lower = (1 - percentile)/2
    upper = (1 - lower)

    g_agg = aggdf.groupby(level=1)
    low = g_agg.quantile(lower)
    high = g_agg.quantile(upper)

    if not columns:
        columns = aggdf.columns.values
    if not colors:
        colors = sns.color_palette()
    if not ax:
        fig = sns.plt.figure()
        ax = fig.add_subplot(1,1,1)

    for column, color in zip(columns, colors):
        g_agg[column].mean().plot(color=color, ax=ax, **kwargs)
        ax.fill_between(low.index, low[column], high[column], alpha=.3, color=color)

    return ax
