#cython: cdivision=True

import cython
cimport cython
from cython.parallel import prange
from libc.math cimport round
from libcpp.string cimport string
import numpy as np
cimport numpy as np

from c_gle_io import GLEInputOutput

import sys
sys.path.append('util')

from libc.time cimport time, time_t
from posix.time cimport clock_gettime, timespec, CLOCK_REALTIME


cdef int ctime_i():
    '''Cast type time_t to int'''
    return <int>time(NULL)

cdef float ctime_f():
    cdef timespec ts
    clock_gettime(CLOCK_REALTIME, &ts)
    return ts.tv_sec + (ts.tv_nsec / 1000000000.)


cdef class CProgressMeter:
    cdef int DATASIZE
    cdef int width, interval
    cdef float start, end, msecs, secs, mins, hours
    cdef bint verbose
    cdef string unit, bar, status, etatxt

    def __cinit__(self, int DATASIZE, int width=50, int interval=1, \
                string unit='full', bint verbose=True):
        self.DATASIZE = DATASIZE
        self.status = ''
        # Initialize progress bar features
        self.bar   = ''
        self.width = width
        # Initialize eta features
        self.interval = interval
        self.etatxt = 'ETA: ...'
        # Initialize wall time features
        self.hours = 0.0
        self.secs  = 0.0
        self.msecs = 0.0
        self.mins  = 0.0
        # Initialize output vars
        self.unit  = 'full'
        self.verbose = verbose
        # Initialize timing vars
        self.start = -1.0
        self.end   = -1.0


cdef class ProgressMeter(CProgressMeter):

    def __enter__(self):
        # Setup toolbar
        sys.stdout.write('Progress:\n')
        sys.stdout.write('[%s]' % ('-' * self.width))
        sys.stdout.flush()
        sys.stdout.write('\b' * (self.width+1)) # return to start of line, after '['
        sys.stdout.flush()

        # Start timing
        self.start = <float>ctime()
        return self

    @cython.cdivision(True)
    def __exit__(self, *args):
        cdef string summary_str
        cdef string details_str
        cdef int isec, imin, ihr
        cdef float fsec

        self.end   = <float>ctime()
        self.secs  = self.end - self.start
        self.msecs = self.secs * 1000  # millisecs
        self.mins  = self.secs / 60  # minutes
        self.hours = self.mins / 60

        sys.stdout.write('\n') # Add newline after progress bar
        sys.stdout.flush() # Reset line

        fsec = (self.secs%60)
        imin = <int>(self.mins%60)
        ihr  = <int>(self.hours)
        summary_str = '  --->  Elapsed time: '
        details_str = '{:02d}:{:02d}:{:05.2f}'.format(ihr, imin, fsec)
        print('{:s}{:s}\n'.format(summary_str, details_str))


    cpdef update(self, int step):
        cdef float frac_done
        cdef string bar

        frac_done = <float>(step) / self.DATASIZE
        if frac_done < 0.0:
            frac_done = 0.0
            self.status = 'Halt...\r\n'
        if frac_done >= 1.0:
            frac_done = 1.0
            self.status = 'Done...\r\n'
        bar = self._update_text(frac_done, ctime())
        self.printer(bar)

    @cython.cdivision(True)
    cpdef string _update_text(self, float frac_done, float t):
        cdef float frac_left, t_elap, avg_rate, t_left
        cdef int mins, secs, block
        cdef string bar_str

        if t > self.start:
            frac_left = 1.0 - frac_done
            t_elap = t - self.start
            if frac_done == 0:
                avg_rate = 0.0
            else:
                avg_rate = t_elap / frac_done
            t_left = np.rint(frac_left * avg_rate)
            mins = <int>(t_left / 60)
            secs = <int>(t_left % 60)
            self.etatxt = 'ETA: {:02d}:{:02d}'.format(mins, secs)
            self.start += self.interval
        block = <int>(round(self.width*frac_done))
        bar_str = ('#'*block + '-'*(self.width-block))
        return '\r[{:s}] {:4.1f}% | {:s} {:s}'.format(bar_str, frac_done*100, \
                                                    self.etatxt, self.status)

    cpdef printer(self, string data):
        sys.stdout.write('\r\x1b[K'+data.__str__())
        sys.stdout.flush()
