'''
Created on Nov 20, 2012

@author: Sean Lee Seyler
'''
import time

class Timer(object):
    def __init__(self, unit='msecs', verbose=False):
        self.unit = unit
        self.verbose = verbose

    def __enter__(self):
        self.start = time.time()
        return self

    def __exit__(self, *args):
        self.end = time.time()
        self.secs = self.end - self.start
        self.msecs = self.secs * 1000  # millisecs
        if self.verbose:
            if self.unit == 'msecs':
                print 'elapsed time: %.2f ms' % self.msecs
            elif self.unit == 'secs':
                print 'elapsed time: %.2f s' % self.secs

'''
with Timer() as t:
    blah blah blah
print " blah blah total elasped time: %s ms" % t.msecs
'''