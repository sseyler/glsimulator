'''
Created on Nov 20, 2012

@author: Sean Lee Seyler
'''
from __future__ import division
import time
import sys
import numpy as np


class Printer():
    """
    Print things to stdout on one line dynamically
    """
    def __init__(self, data):

        sys.stdout.write("\r\x1b[K"+data.__str__())
        sys.stdout.flush()

class ProgressBar(object):
    def __init__(self, N, width=50, interval=1, unit='sec', verbose=True):
        # initialize global features
        self.N = N
        self.status = ""

        # initialize progress bar features
        self.bar = None
        self.bar_width = width

        # initialize eta features
        self.tout_intvl = interval
        self.etatxt = "ETA: ..."

        # initialize wall time features
        self.end = None
        self.secs = None
        self.msecs = None
        self.min = None
        self.unit = unit
        self.verbose = verbose

    def __enter__(self):
        # setup toolbar
        sys.stdout.write("\nProgress:\n")
        sys.stdout.write("[%s]" % ("-" * self.bar_width))
        sys.stdout.flush()
        sys.stdout.write("\b" * (self.bar_width+1)) # return to start of line, after '['
        sys.stdout.flush()

        # start timing
        self.start = time.time()
        self.tout = self.start
        self.frac_intvl = 0
        return self

    # def update(self, progress):
    #     try:
    #         frac_done = float(progress)/self.N
    #         if frac_done < 0:
    #             frac_done = 0
    #             self.status = "Halt..."
    #         if frac_done >= 1:
    #             frac_done = 1
    #             self.status = "Done..."
    #     except ValueError:
    #         # if not isinstance(progress, int) or not isinstance(progress, float):
    #         frac_done = 0
    #         self.status = "Error: progress variable must be float or int"
    #     except ZeroDivisionError:
    #         frac_done = 0
    #         self.status = "Error: zero data size"
    #
    #     t = time.time()
    #     if t >= self.tout:  # check if past next output time
    #         self.bar = self._update_text(frac_done, t)
    #     sys.stdout.write(self.bar)
    #     sys.stdout.flush()

    def update(self, progress):
        try:
            frac_done = float(progress)/self.N
            if frac_done < 0:
                frac_done = 0
                self.status = "Halt...\r\n"
            if frac_done >= 1:
                frac_done = 1
                self.status = "Done...\r\n"
        except ValueError:
            # if not isinstance(progress, int) or not isinstance(progress, float):
            frac_done = 0
            self.status = "Error: progress variable must be float or int\r\n"
        bar = self._update_text(frac_done, time.time())
        # sys.stdout.write(bar)
        # sys.stdout.flush()
        Printer(bar)

    # Find the best estimator for remaining time. Simplest solution is to use the weighted
    # average of:
    #   1) the average overall completion rate
    #   2) the average completion rate over some moving interval (window), e.g., the
    #       the completion rate over the last x seconds.
    # Progress variable rescaled automatically w/ self.N (length of system to be monitored)

    # def _update_text(self, frac_done, t):
    #     try:
    #         frac_left = 1.0 - frac_done
    #         t_elap = t - self.start
    #         avg_rate = t_elap / frac_done
    #         t_left = np.rint(frac_left * avg_rate)
    #         mins = t_left / 60
    #         secs = t_left % 60
    #         self.etatxt = "ETA: %im%is" % (mins, secs)
    #     except ZeroDivisionError:
    #         self.etatxt = "ETA: ..."
    #     finally:
    #         self.tout += self.tout_intvl  # update next ouput time
    #     block = np.rint(self.bar_width*frac_done)
    #     return "\r[%s] %4.1f%% | %s %s\r\n" %                                       \
    #             ( "#"*block + "-"*(self.bar_width-block), frac_done*100,
    #               self.etatxt, self.status )

    def _update_text(self, frac_done, t):
        if t >= self.tout:
            try:
                frac_left = 1.0 - frac_done
                t_elap = t - self.start
                avg_rate = t_elap / frac_done
                t_left = np.rint(frac_left * avg_rate)
                mins = t_left / 60
                secs = t_left % 60
                self.etatxt = "ETA: %im%is" % (mins, secs)
            except ZeroDivisionError:
                self.etatxt = "ETA: ..."
            finally:
                self.tout += self.tout_intvl
        block = int(np.rint(self.bar_width*frac_done))
        return "\r[%s] %4.1f%% | %s %s" %                                       \
                ( "#"*block + "-"*(self.bar_width-block), frac_done*100,
                  self.etatxt, self.status )
        # return '\r[{:s}] {:4.1f}%% | {:s} {:s}'.format("#"*block + "-"*(self.bar_width-block),
        #                                        frac_done*100, self.etatxt, self.status)

    def __exit__(self, *args):
        self.end = time.time()
        self.secs = self.end - self.start
        self.msecs = self.secs * 1000  # millisecs
        self.min = self.secs / 60  # minutes

        sys.stdout.write("\n") # Add newline after progress bar
        sys.stdout.flush() # Reset line

        if self.verbose:
            if self.unit == 'msec':
                print '  --->  Elapsed time: %.0f ms\n' % self.msecs
            elif self.unit == 'sec':
                print '  --->  Elapsed time: %.2f s\n' % self.secs
            elif self.unit == 'min':
                print '  --->  Elapsed time: %i min %i sec\n' % (self.min, self.secs%60)
