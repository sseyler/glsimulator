%%cython --cplus -c=-march=native -c=-Ofast -c=-ffast-math -c=-fopenmp --link-args=-fopenmp -I/home/sseyler/Library/miniconda2/include -lgsl -lgslcblas
#cython: cdivision=True

import numpy as np
cimport numpy as cnp
cimport cython
from cython.parallel import prange, parallel
cimport openmp

from libcpp.vector cimport vector
from libcpp.string cimport string
from libc.math cimport round
from libc.time cimport time, time_t
from posix.time cimport clock_gettime, timespec, CLOCK_REALTIME

from cython_gsl cimport *

import sys
sys.path.append('util')
from progressbar_simple import ProgressBar
from c_gle_io import GLEInputOutput
#########################################################################################

cdef int c_time():
    '''Cast type time_t to int'''
    return <int>time(NULL)

cpdef double c_time_f():
    cdef timespec ts
    clock_gettime(CLOCK_REALTIME, &ts)
    return (<double> ts.tv_sec) + (<double> ts.tv_nsec) * 1.0e-9

#########################################################################################
cdef class Force:
    cpdef float evaluate(self, float X):
        return 0

cdef class ConstantForce(Force):
    cdef float C0

    def __cinit__(self, float C0):
        self.C0 = C0

    cpdef float evaluate(self, float X):
        return self.C0

cdef class LinearForce(Force):
    cdef float C0, C1

    def __cinit__(self, float C0, float C1):
        self.C0 = C0
        self.C1 = C1

    cpdef float evaluate(self, float X):
        return self.C0 + self.C1*X


#########################################################################################
cdef class Particle:
    cdef:
        float mass, radius
        float gamma, theta0, sigma0
        float theta1, sigma1
        int NDIM
        float X[3]
        float V[3]
        float F[3]
        float S[3]

    def __init__(self, ndim, m, r, gamma, theta0, sigma0, theta1, sigma1):
        self.NDIM = ndim
        self.mass = m
        self.radius = r
        self.gamma  = gamma
        self.theta0 = theta0
        self.sigma0 = sigma0
        self.theta1 = theta1
        self.sigma1 = sigma1

    def initialize_position(self):
        x_arr = np.zeros(self.NDIM, dtype=np.dtype("float"))
        for i in range(self.NDIM):
            self.X[i] = x_arr[i]

    def initialize_velocity(self):
        v_arr = np.zeros(self.NDIM, dtype=np.dtype("float"))
        for i in range(self.NDIM):
            self.V[i] = v_arr[i]

    def initialize_auxiliary(self):
        s_arr = np.zeros(self.NDIM, dtype=np.dtype("float"))
        for i in range(self.NDIM):
            self.S[i] = s_arr[i]


#########################################################################################
cdef class GLEIntegrator:
    cdef Particle P
    cdef Force force
    cdef float dt

    cdef float advance(self, gsl_rng * r, Particle P):
        return 0


cdef class GLEImpulseIntegrator(GLEIntegrator):
    cdef VelocityAdvance VA
    cdef ImpulseVelocityAdvance IVA
    cdef PositionAdvance PA
    cdef AuxiliaryAdvance SA

    cdef float dti2

    def __cinit__(self, Particle p, Force f, float dt):
        self.P     = p
        self.force = f
        self.dt    = dt
        self.dti2  = dt/2

        p.initialize_position()
        p.initialize_velocity()
        p.initialize_auxiliary()  # This isn't general. Use inheritance and overloading
        self.VA  = LIVelocityAdvance(p.mass)
        self.IVA = LIImpulseVelocityAdvance(p.theta0, p.sigma0)
        self.PA  = LIPositionAdvance()
        self.SA  = LIAuxiliaryAdvance(p.gamma, p.theta1, p.sigma1)


    cdef float advance(self, gsl_rng * r, Particle P):
        cdef float Vh, Vi, W

        for dim in range(P.NDIM):
            W = gsl_ran_gaussian(r, 1)
            Vh = P.V[dim] + self.dti2*self.VA.rhs(P.F[dim], P.S[dim])
            Vi = self.IVA.rhs(P.V[dim], W)
            P.X[dim] = P.X[dim] + self.dt*self.PA.rhs(Vh, Vi)
            P.S[dim] = self.SA.rhs(P.S[dim], Vh, W)
            P.F[dim] = self.force.evaluate(P.X[dim])
            P.V[dim] = Vh + Vi + self.dti2*self.VA.rhs(P.F[dim], P.S[dim])
        return 0


#########################################################################################
cdef class VelocityAdvance:
    cdef float mass
    cdef float imass

    def __cinit__(self, float mass):
        self.mass = mass
        self.imass = 1/mass

    cpdef float rhs(self, float F, float S):
        return 0

cdef class ImpulseVelocityAdvance:
    cdef float theta, sigma
    cdef float theta_minus_one

    def __cinit__(self, float theta, float sigma):
        self.theta = theta
        self.sigma = sigma
        self.theta_minus_one = -(1 - theta)

    cpdef float rhs(self, float V, float W):
        return 0

cdef class PositionAdvance:
    cpdef float rhs(self, float Vh, float Vi):
        return 0

cdef class AuxiliaryAdvance:
    cdef float gamma, theta, sigma
    cdef float one_m_theta_x_gamma

    def __cinit__(self, float gamma, float theta, float sigma):
        self.gamma = gamma
        self.theta = theta
        self.sigma = sigma
        self.one_m_theta_x_gamma = (1 - theta)*gamma

    cpdef float rhs(self, float S, float Vh, float W):
        return 0

#########################################################################################
cdef class LIVelocityAdvance(VelocityAdvance):
    cpdef float rhs(self, float F, float S):
        return self.imass*F - S

cdef class LIImpulseVelocityAdvance(ImpulseVelocityAdvance):
    cpdef float rhs(self, float V, float W):
        return self.theta_minus_one*V + self.sigma*W

cdef class LIPositionAdvance(PositionAdvance):
    cpdef float rhs(self, float Vh, float Vi):
        return Vh + 0.5*Vi

cdef class LIAuxiliaryAdvance(AuxiliaryAdvance):
    cpdef float rhs(self, float S, float Vh, float W):
        return self.theta*S - self.one_m_theta_x_gamma*Vh + self.sigma*W


#########################################################################################
cpdef run(particle, GLEIntegrator integrator, inout,         \
               int NREP, int NSTEPS, int NTDUMP, int NTOUT, int NDIM,  \
               int seed, int printdelay=100000, double print_interval=0.05, \
               int workers=1, no_store=False):

    cdef gsl_rng *r
    cdef int ts, n, dim, i, num_threads

    cdef Particle p = particle
    cdef double t_now = c_time_f()
    cdef double t_nextout = t_now
    cdef float dt = integrator.dt

    cdef float[:,:,:] traj = np.empty((4, NSTEPS+1, NDIM), dtype=np.dtype('f4'))

    inout.open_traj_files()      # open trajectory HDF5 files for writing
    inout.init_io_storage(NSTEPS, NTOUT, NTDUMP) # init trajs for temp storage pos/vel
    r = gsl_rng_alloc(gsl_rng_mt19937)
    gsl_rng_set(r, seed)

    for ts in range(NSTEPS+1):
        # for dim in range(NDIM):
        #     traj[0,ts,:]    = p.X
        #     traj[1,ts,:]    = p.V
        #     traj[2,ts,:]    = p.F
        #     traj[3,ts,:]    = p.S
        n = (ts % NTDUMP)
        if n == 0:
            # terminate = inout.dump_traj(ts, X, V, F, S, no_store=no_store)
            if ts == NSTEPS:
                break
            # for dim in range(NDIM):
            #     X[i,0,dim] = X[i,-1,dim]
            #     V[i,0,dim] = V[i,-1,dim]
            #     F[i,0,dim] = F[i,-1,dim]
            #     S[i,0,dim] = S[i,-1,dim]
        # if (ts % printdelay) == 0:
        #     t_now = c_time_f()
        #     if t_now >= t_nextout:
        #         pb.update(ts, t_now)
        #         t_nextout += print_interval
        integrator.advance(r, p)

    inout.close_traj_files()
    return traj
