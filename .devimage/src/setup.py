from distutils.core import setup
from Cython.Build import cythonize

setup(name="Generalized Langevin Dynamics Simulation",
    ext_modules = cythonize("glesim.pyx")
)
