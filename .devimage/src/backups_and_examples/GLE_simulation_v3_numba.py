import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

import numpy.random as npr
import time
import sympy
import pint
ur = pint.UnitRegistry()

import force_v3_numba as force         # NOT used explicitly; only to expose Force classes to user
import GLE_io_v3 as io           # used explicitly by GLESimulation TODO: changethis?
import GLE_integrator_v3_numba_update as gli  # used explicitly by GLESimulation TODO: changethis?

# from types import MethodType

PI = np.pi


def m_eff(r, rhp, rhf, unit=ur.kg):
    return (rhp+0.5*rhf)*(4./3.)*PI*r**3

def mass_particle(r, rhp, unit=ur.kg):
    return rhp*(4./3.)*PI*r**3

def effective_mass(r, rhp, rhf, unit=ur.kg):
    return (rhp+0.5*rhf)*(4./3.)*PI*r**3

def basset_coeff(r, eta, rhf, Cb):
    return Cb * 3*r**2*(PI*eta*rhf)**0.5


class GLESimulation(object):
    """Engine for simulating Langevin dynamics of a particle.

    """
    # Physical constants
    kBol = 1.3806485e-23              # Boltzmann constant (J/K)
    NAvo = 6.022e23                   # Avogadro's number: # particles/mole
    kB2 = 1e-3*kBol*NAvo              # Boltzmann constant (kJ/mol/K)

    def __init__(self, method='ELI', outname='traj', Nrep=1, Naux=1, D=3,
                    Te=303.15, L=1.0e-7, tau0=2.5e-9, R=1.0e-7,
                    rhp=1.0e3, rhf=1.0e3, eta=1.0e-3,
                    gam0=1.0e11, nu1=2.5e10, dt=1.0e-15,
                    xtrj=True, vtrj=True, strj=True, maxboltz=True,
                    seed=123456):
        """Generate system with custom parameters.

        :Arguments:
            *outfile*
                base name of file to output results to
            *dt*
                time step of integration (ps)
            *m*
                mass of particle (amu)
            *T*
                temperature (K)
            *gamma*
                collision frequency (1/ps)
            *k*
                spring constant (kJ/mol/nm^2)
        """
        # Define system dimensionality
        self.Nrep = Nrep                     # number of simulation replicas
        self.Naux = Naux                     # number of auxiliary variables
        self.D    = D                        # number of spatial dimensions
        ### Base units (in MKS) ###
        self.Lc   = L                                # length
        self.Tc   = tau0                             # time (can be same tau0 from Siegel)
        self.Mc   = rhp*(4./3.)*PI*R**3              # mass
        self.Tec  = Te if Te != 0.0 else 1.0         # temperature
        ### Derived units ###
        self.Vc   = self.Lc/self.Tc                  # velocity
        self.Ec   = self.Mc*(self.Lc/self.Tc)**2     # energy
        self.Nc   = self.Mc/self.Lc**3               # density
        self.Fc   = self.Ec/self.Lc                  # force
        # self.kB  = self.kBol*Te/self.Ec             # Boltzmann constant
        ### Physical parameters ###
        self.Te   = Te/self.Tec                     # temperature [K]
        self.L    = L/self.Lc                       # washboard period [m]
        self.R    = R/self.Lc                       # particle radius [m]
        self.rhp = rhp/self.Nc                      # particle density [kg/m^3]
        self.rhf = rhf/self.Nc                      # fluid density [kg/m^3]
        self.eta  = eta*self.Lc*self.Tc/self.Mc     # dynamic viscosity [kg/m/s]
        self.gam0 = gam0*self.Tc                    # collision frequency [1/s]
        self.nu1  = nu1*self.Tc                     # relaxation time [1/s]
        self.m    = m_eff(R, rhp, rhf)/self.Mc      # effective mass [kg]
        ### Simulation parameters ###
        self.dt   = dt/self.Tc                      # time step [s]
        ### Convenience parameters ###
        self.kT = self.kBol*Te/self.Ec              # Boltzmann constant * T (kJ/mol)

        self.seed = seed
        self.method = method
        self.trj_flags = (xtrj, vtrj, strj)
        self.maxboltz = maxboltz

        self.force_catalog = {}   # set by calling add_force
        self.integrator = None
        self._update = None  # set by calling add_integrator
        self.io = io.GLEInputOutput(self, outname, self.trj_flags,
                                    Nrep, Naux, D, self.dt)
        self.io.setup()


    # def add_force(self, ForceObject, name, **kwargs):
    #     '''Add a new force to the system.
    #
    #         A dictionary is used to keep track of the forces in the system
    #         by name.
    #     '''
    #     self.force_catalog[name] = ForceObject(name, **kwargs)
    #     print("Registered force \"{}\" to the system.".format(name))
    #     if self.integrator:
    #         self.integrator.add_force(self.force_catalog[name], name, **kwargs)


    def set_integrator(self, name):
        '''Set the integration scheme.

            The integrator is selected by the name of the scheme (`name`)
        '''
        self.method = name
        self.integrator = gli.GLEIntegrator(self.Nrep, self.Naux,
                                            self.D, self.kT, self.m, self.gam0, self.nu1,
                                            self.dt, self.seed)
        self._update = self.integrator.ELI_step
        # self._update = gli.ELI_step_numba


    # def is_valid_system(self, flag=True):
    #     if self.integrator is None:
    #         print("Warning: an integrator is not defined for this system.")
    #         print("    >>> Add an integrator to the system with add_integrator().")
    #         flag = False
    #     if self._update is None:
    #         print("Warning: no update() method has been defined for this system.")
    #         print("    >>> Add an integrator with add_integrator() to add an update() method.")
    #         flag = False
    #     if not self.integrator.forces_exist():
    #         print("Warning: no forces are currently defined for this system.")
    #         print("    >>> Register at least one force using integrator.add_force().")
    #         flag = False
    #     return flag


    def run(self, method=None, nsteps=1000, tmpsize=1000,
            ntout=1, printout=100, seed=(None, None)):
        """Perform Langevin MD.

        :Arguments:
            *method*
                integration method [string]
            *nsteps*
                number of steps to run for [int]
            *tmpsize*
                number of steps to store before writing to file [int]
            *ntout*
                store a frame every ntout steps [int]
            *printout*
                number of steps between progress updates
        """
        # if not self.is_valid_system():
        #     print("Error: system validation failed. Terminating run...")

        if ntout == 1: print 'Note: ntout=1 is broken'

        tmpsize = min(tmpsize, int(nsteps/ntout))
        self.io.open_trj_files()      # open trajectory HDF5 files for writing
        self.io.init_tmp_trj(tmpsize) # init trajs for temp storage pos/vel

        t = 0.0
        Xn = self.init_position()
        Vn = self.init_velocity()
        Sn = self.init_auxiliary()
        FDn = np.zeros_like(Sn)
        FTn = np.zeros_like(Sn)
        # Fn = self.integrator.calc_forces(X)          # TODO: this is a cheat
        # print self.integrator.force_catalog.keys()[0]
        # self.integrator.set_single_force(self.integrator.force_catalog.keys()[0])   # TODO: a hack
        Fn = self.integrator.calc_single_force(Xn)     # TODO: this is a cheat

        for ts in xrange(1, nsteps+1):
            ntidx = (ts % ntout) - 1
            idx   = (ts % (tmpsize*ntout)) - 1
            pidx  = (ts % printout) - 1
            perfidx  = (ts % 100*printout) - 1
            if ntidx == 0:
                tidx = ts/ntout
                self.io.itraj[tidx]       = self.dt*(ts-1)
                self.io.Xtraj[tidx][:]    = Xn
                self.io.Vtraj[tidx][:]    = Vn
                self.io.Ftraj[tidx][:]    = Fn
                self.io.FDtraj[tidx][:]   = FDn
                self.io.FTtraj[tidx][:]   = FTn
                self.io.Straj[tidx][:][:] = Sn
            if idx + 1 == 0:
                self.io.dump_tmp_trj(tmpsize)
            if (pidx + 1) == 0:
                self.io.step_print(ts, nsteps)
                # if (perfidx + 1) == 0:
                #     self.io.print_perf(ts, nsteps)

            Xn[:,:,:], Vn[:,:,:], Sn[:,:,:], Fn[:,:,:], FDn[:,:,:], FTn[:,:,:] = self.update(Xn, Vn, Sn, Fn)

        self.io.close_trj_files()     # close HDF5 trajectory files
        return self


    def init_position(self):
        return np.zeros((self.Nrep, 1, self.D))

    def init_velocity(self):
        if self.maxboltz:
            return self.maxwell_boltzmann_velocities()
        else:
            return 0.0*np.ones((self.Nrep, 1, self.D))

    def init_auxiliary(self):
        return np.zeros((self.Nrep, self.Naux, self.D))


    def maxwell_boltzmann_velocities(self):
        sigma = (self.kT/self.m)**0.5 * np.ones((self.Nrep, 1, self.D))
        return npr.normal(loc=0.0, scale=sigma)


    def update(self, X, V, S, F):
        """Step the dynamical variables to the next time level.

            This method is set by selecting the integrator with ``
        """
        return self._update(X, V, S, F)


    @property
    def times(self):
        """Get sequence of time steps."""
        out = pd.HDFStore(self.io.it_fname, 'r')
        out.open()
        traj = out['times']  # the indices of each of the trajectories
        out.close()
        return traj

    @property
    def positions(self):
        """Get position trajectory."""
        out = pd.HDFStore(self.io.Xt_fname, 'r')
        out.open()
        traj = out['positions']  # must match names given to columns in run()
        out.close()
        return traj

    @property
    def velocities(self):
        """Get velocity trajectory."""
        out = pd.HDFStore(self.io.Vt_fname, 'r')
        out.open()
        traj = out['velocities']  # must match names given to columns in run()
        out.close()
        return traj

    @property
    def aux_velocities(self):
        """Get auxiliary variable trajectory."""
        out = pd.HDFStore(self.io.St_fname, 'r')
        out.open()
        traj = out['aux_velocities']  # must match names given to columns in run()
        out.close()
        return traj

    @property
    def drag_forces(self):
        """Get drag force trajectory."""
        out = pd.HDFStore(self.io.FDt_fname, 'r')
        out.open()
        traj = out['drag_forces']  # must match names given to columns in run()
        out.close()
        return traj

    @property
    def thermal_forces(self):
        """Get thermal force trajectory."""
        out = pd.HDFStore(self.io.FTt_fname, 'r')
        out.open()
        traj = out['thermal_forces']  # must match names given to columns in run()
        out.close()
        return traj


    def close_trj_files(self):
        self.io.close_trj_files()


    # def quick_plot_MSD(self, **kwargs):
    #     """Alias for GLEInputOutput.quick_plot_MSD() method."""
    #
    #     return self.io.quick_plot_MSD(**kwargs)
    #
    # def quick_plot_SD(self, **kwargs):
    #     """Alias for GLEInputOutput.quick_plot_SD() method."""
    #
    #     return self.io.quick_plot_SD(**kwargs)
    #
    # def quick_plot_2D(self, **kwargs):
    #     """Alias for GLEInputOutput.quick_plot_2D() method."""
    #
    #     return self.io.quick_plot_2D(**kwargs)
    #
    # def quick_plot_MSV(self, **kwargs):
    #     """Alias for GLEInputOutput.quick_plot_MSV() method."""
    #
    #     return self.io.quick_plot_MSV(**kwargs)
    #
    # def plot_potential(self, **kwargs):
    #     """Alias for GLEInputOutput.plot_potential() method."""
    #
    #     return self.io.plot_potential(**kwargs)
    #
    # def plot_force(self, **kwargs):
    #     """Alias for GLEInputOutput.plot_force() method."""
    #
    #     return self.io.plot_force(**kwargs)
