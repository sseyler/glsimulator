# coding=utf-8

import numpy as np
import pandas as pd
import itertools as it
import matplotlib.pyplot as plt

import numpy.random as npr
import time

PI = np.pi


class Force(object):

    def __init__(self, k, L=1.0, V0=1.0):
        """*k in kJ/mol/nm^2
        """
        self.k  = k   # (harmonic) force constant
        self.L  = L   # washboard period
        self.V0 = V0  # washboard pot oscilliatory amplitude

        self.twopiiL = 2.0*PI/L       # wavenumber of washboard potential
        self.W0      = V0*(2.0*PI/L)  # coefficient of oscilliatory component of washboard force


    def harmonic(self, X):
        """Harmonic force for extension x.
            *X* in nm
        """
        return -self.k*X

    def washboard(self, X):
        """Force derived for washboard potential.
            V(x) = -V0*cos(2*pi*x/x0) - k*x
            F(x) = -(V0*2*pi/x0)*sin(2*pi*x/x0) + k
        """
        return -self.W0*np.sin(self.twopiiL*X) + self.k

    def V_washboard(self, X):
        """Washboard potential.
            V(x) = -V0*cos(2*pi*x/x0) - k*x
            F(x) = -(V0*2*pi/x0)*sin(2*pi*x/x0) + k
        """
        return -self.V0*np.cos(self.twopiiL*X) - self.k*X


class StochasticForce(Force):

    isqrt3 = 1./3.0**0.5           # 1/sqrt(3)

    def __init__(self, N, D, seed=123456):
        self.N = N
        self.D = D
        self.dimension = self.N*self.D

        np.random.seed(int(seed))
        # self.seed, self.tclk = self.reseed(rseed=seed)

    def GWN(self, sigma=1):
        return sigma*npr.randn(self.dimension)

    def GWN_multi(self, sigma):
        return npr.normal(loc=0.0, scale=sigma)

    @staticmethod
    def reseed(rseed=(123456, time.clock())):
        try:
            if type(rseed) is tuple and len(rseed) == 2:
                for s1, s2 in rseed:
                    if type(s1) is int and type(s2) is int:
                        seed1, seed2 = s1, s2
                    elif s1 is None:
                        print('Got \'None\' for rseed. Using time.clock()' +\
                              ' as RNG seed.\n')
                        seed1, seed2 = time.clock(), 1
            elif type(rseed) is int:
                seed1, seed2 = rseed, 1
        except:
            print('Something went wrong with reseed, Using time.clock()' +\
                  ' as RNG seed.\n')
            seed1, seed2 = time.clock(), 1

        seed1, seed2 = time.clock(), 1
        print('Initializing NumPy RNG: seed1={}, seed2={}'.format(seed1, seed2))
        np.random.seed(int(seed1/seed2))
        return seed1, seed2

# class Force(object):
#
#     """Define the external forces acting on the particle.
#
#     """
#
#     def __init__(self):
#
#
#     def potential(self, pos):
#         """Value of 3D potential at a given position
#
#         """
#         return self.amplitude*(1 - np.cos((np.pi/self.basinwidth)*pos)) + self.a*pos**4 + self.b*pos**2 + self.c
#
#     def potential2(self, pos):
#         """Value of 3D potential at a given position
#
#         """
#         x, y, z = pos
#         x2 = x*x
#         y2 = y*y
#         return self.f*(self.zmax-z) + self.a*x2 + self.b*y2*(y2-self.c22)
#
#     def force(self):
#         """Compute the net force on each particle
#             D = 2*X - ( np.roll(X,1,axis=0) + np.roll(X,-1,axis=0) )
#             return np.array([0,0,mixidt]) + kixidt*D
#
#             Get inter-particle forces from mutual distances:
#             1) In the one particle case, there are no interparticle forces to
#               compute.
#             2) For small N < 16, compute the entire distance matrix. For N >= 16,
#               compute only the upper triangle of the distance matrix.
#               f = # force scaling factor (can be gravitational acceleration)
#               list(permutations(...)) generates all possible particle coordinate
#               pairs. Each coordinate in a pair is a 1x3 numpy array specifying the
#               3D Euclidean coordinates of one particle. Each pair is a tuple of
#               two 1x3 numpy arrays. All pairs are generated so that the order of
#               a pair matters. Taking the difference of all pairs, we have every
#               off-diagonal element of the distance matrix from which the forces
#               can be calculated.
#             3) In the case with more than 16 particles, it is more efficient to
#               only generate the unique particle pairs to compute the distances.
#               This only generates the upper triangle of the distance matrix,
#               so that forces are obtained by utilizing matrix's symmetry.
#         """
#         X = np.reshape(self.pos,(self.N, 3))
#         if self.N == 1:
#             D = np.zeros((1,3))
#         elif self.N < 16:
#             D = np.diff(list(it.permutations(X, 2)), axis=1)
#             D = np.sum(np.reshape(D, (int(self.N), int(self.N-1), 3)), axis=1)
#         else:
#             D = np.squeeze(np.diff(list(it.combinations(X, 2)), axis=1))
#             Dx = sp.squareform(D[:,0], force='tomatrix')
#             Dx = -np.tril(Dx) + np.triu(Dx)
#             Dy = sp.squareform(D[:,1], force='tomatrix')
#             Dy = -np.tril(Dy) + np.triu(Dy)
#             Dz = sp.squareform(D[:,2], force='tomatrix')
#             Dz = -np.tril(Dz) + np.triu(Dz)
#             D = np.sum(np.dstack((Dx,Dy,Dz)), axis=1)
#         # 2) Compute the inter-particle forces
#         F = self.k*D
#
#         x = X[:,0]
#         y = X[:,1]
#         r2 = x*x + y*y
#         if self.N == 1:
#             # Need singleton dimension to maintain numpy array compatibility
#             # with multi-particle cases
#             r2 = r2*np.array([[1,1]])
#         else:
#             r2 = np.column_stack((r2, r2))
#         # Apply force from global W-potential
#         F[:,0:2] += 2*self.a*X[:,0:2]*(self.b - 2*r2)
#         return np.reshape(np.array([0,0,self.f]) + F, (self.total_dims))
#
#
#     def force2(self):
#         """Compute the net force on each particle
#             D = 2*X - ( np.roll(X,1,axis=0) + np.roll(X,-1,axis=0) )
#             return np.array([0,0,mixidt]) + kixidt*D
#
#             Get inter-particle forces from mutual distances:
#             1) In the one particle case, there are no interparticle forces to
#               compute.
#             2) For small N < 16, compute the entire distance matrix. For N >= 16,
#               compute only the upper triangle of the distance matrix.
#               f = # force scaling factor (can be gravitational acceleration)
#               list(permutations(...)) generates all possible particle coordinate
#               pairs. Each coordinate in a pair is a 1x3 numpy array specifying the
#               3D Euclidean coordinates of one particle. Each pair is a tuple of
#               two 1x3 numpy arrays. All pairs are generated so that the order of
#               a pair matters. Taking the difference of all pairs, we have every
#               off-diagonal element of the distance matrix from which the forces
#               can be calculated.
#             3) In the case with more than 16 particles, it is more efficient to
#               only generate the unique particle pairs to compute the distances.
#               This only generates the upper triangle of the distance matrix,
#               so that forces are obtained by utilizing matrix's symmetry.
#         """
#         X = np.reshape(self.pos,(self.N, 3))
#         if self.N == 1:
#             D = np.zeros((1,3))
#         elif self.N < 16:
#             D = np.diff(list(it.permutations(X, 2)), axis=1)
#             D = np.sum(np.reshape(D, (int(self.N), int(self.N-1), 3)), axis=1)
#         else:
#             D = np.squeeze(np.diff(list(it.combinations(X, 2)), axis=1))
#             Dx = sp.squareform(D[:,0], force='tomatrix')
#             Dx = -np.tril(Dx) + np.triu(Dx)
#             Dy = sp.squareform(D[:,1], force='tomatrix')
#             Dy = -np.tril(Dy) + np.triu(Dy)
#             Dz = sp.squareform(D[:,2], force='tomatrix')
#             Dz = -np.tril(Dz) + np.triu(Dz)
#             D = np.sum(np.dstack((Dx,Dy,Dz)), axis=1)
#         # 2) Compute the inter-particle forces
#         F = self.k*D
#
#         y = X[:,1]
#         y2 = y*y
#
#         F[:,0] -= self.a2*X[:,0]
#         F[:,1] -= self.b4*y*(y2-self.c2)
#         F[:,2] += self.f
#         return np.reshape(F, (self.total_dims))
