import numba

import numpy as np
import pandas as pd
import itertools as it
import matplotlib.pyplot as plt

import numpy.random as npr
import time

import pyximport; pyximport.install()
import force



# noise = force.StochasticForce(Nrep, Naux, D, seed=seed)
ext_force = force.ConstantUniformForce('noforce', C0=0.0)

def calc_single_force(X):
    return ext_force.apply(X)

###############################################################
# Extended system Langevin Impulse method (Baczewski version)
###############################################################
def ELI_half_velocity(V0, F0, S0, dti2, im):
    return V0 + dti2*(im*F0 - S0)

def ELI_impulse_velocity(Vh, W0, the0, sig0):
    # return -(1. - self.the0)*Vh + self.alp0*self.sqrt2kTim*W0
    return -ELI_Stokes_drag(Vh, the0) + ELI_noise(W0, sig0)

def ELI_Stokes_drag(Vh, the0):
    return (1. - the0)*Vh

def ELI_noise(W0, sig0):
    return sig0*W0

def ELI_position(X0, Vh, Vi, dt):
    return X0 + (Vh + 0.5*Vi)*dt

def ELI_auxiliary(S0, Vh, W0, gam0, the1, sig1):
    return the1*S0 - (1. - the1)*gam0*Vh + sig1*W0

def ELI_velocity(Vh, Vi, F0, S0, dti2, im):
    return Vh + Vi + dti2*(im*F0 - S0)

def ELI_step(X0, V0, S0, F0, W0, m, im, dt, idt, dti2, gam0, the0, alp0, sig0, the1, alp1, sig1):
    # Wn  = noise.GWN(sigma=1)
    Vh  = ELI_half_velocity(V0, F0, S0, dti2, im)
    # Vi  = ELI_impulse_velocity(Vh, W0, the0, sig0)
    Vdrag = ELI_Stokes_drag(Vh, the0)
    Vther = ELI_noise(W0, sig0)
    Vi  = -Vdrag + Vther
    Xn  = ELI_position(X0, Vh, Vi, dt)
    Sn  = ELI_auxiliary(S0, Vh, W0, gam0, the1, sig1)
    # Fn  = calc_forces(Xn)
    Fn  = calc_single_force(Xn)     # TODO: this is a cheat...
    Vn  = ELI_velocity(Vh, Vi, Fn, Sn, dti2, im)

    return Xn, Vn, Sn, Fn, Vdrag*m*idt, Vther*m*idt



class GLEIntegrator(object):


    def __init__(self, forces, Nrep, Naux, D, kT, m, gam0, nu1, dt, seed=123456):

        # Input parameters (non-dimensionalized)
        self.kT   = kT                      # temperature
        self.m    = m                       # particle mass
        self.gam0 = gam0                    # collision frequency
        self.nu1  = nu1                     # relaxation time
        self.dt   = dt                      # time step

        # Convenient shortcuts and definitions (combined parameters)
        self.im     = 1./m                  # inverse of mass
        self.idt    = 1./dt                 # inverse of the timestep
        self.dti2   = 0.5*dt                # timestep div by 2
        self.dti2m  = 0.5*dt/m              # timestep div by (2*m)
        self.dtsqi2 = 0.5*dt**2             # Used in Vanden-Eijnden Ciccotti
        self.gam0dt = gam0*dt               # unitless
        self.kTim   = kT/m                  # kT/m

        self.the0 = 0
        self.alp0 = 0
        self.sig0 = 0
        self.the1 = 0
        self.alp1 = 0
        self.sig1 = 0

        self.thetak = np.zeros((Nrep, Naux))
        self.alphak = np.zeros((Nrep, Naux))
        self.sigmak = np.zeros((Nrep, Naux))

        self.integrators = ['ELI', 'MELI', 'LI', 'GJF', 'VEC', None]
        self.method = None

        self.force_catalog = forces
        self.noise  = force.StochasticForce(Nrep, Naux, D, seed=seed)

        self.force = None         # TODO: this is a hack


    def set_integrator_params(self, method):
        if method in {'ELI', 'ELInc'} or method is None:
            self.sqrt2kTim = np.sqrt(2.*self.kTim)

            self.the0 = np.exp(-self.gam0*self.dt)
            self.alp0 = ((1. - self.the0**2) / 2.)**0.5
            self.sig0 = self.alp0*self.sqrt2kTim

            self.the1 = np.exp(-self.nu1*self.dt)
            self.alp1 = ((1. - self.the1**2) * self.nu1 / 2.)**0.5
            self.sig1 = self.alp1*self.sqrt2kTim
        if method == "MELI" or method is None:
            self.sqrt2kTim = np.sqrt(2.*self.kTim)
            self.the0 = np.exp(-self.gam0*self.dt)
            self.alp0 = ((1. - self.the0)**2 / (self.gam0*self.dt))**0.5
            self.the1 = np.exp(-self.nu1*self.dt)
            self.alp1 = ((1. - self.the1)**2 / self.dt)**0.5
        elif method == "LI" or method is None:
            self.sqrt2kTim = np.sqrt(2.*self.kTim)
            self.alpha = 1. - np.exp(-self.gam0dt)
            self.sigma = (self.kTim*self.alpha*(2.-self.alpha))**0.5
        elif method == "GJF":
            self.gam0dt = self.gam0*self.dt
            self.alp0 = 1./(1. + 0.5*self.gam0dt)

            self.alp0dt = self.alp0*self.dt
        print("Setting parameters for {} integrator.".format(method))
        print 'sqrt2kTim: {:9.3e}'.format(self.sqrt2kTim)
        print 'the0:      {:9.3e}'.format(self.the0)
        print 'alp0:      {:9.3e}'.format(self.alp0)
        print 'sig0:      {:9.3e}'.format(self.sig0)
        print 'the1:      {:9.3e}'.format(self.the1)
        print 'alp1:      {:9.3e}'.format(self.alp1)
        print 'sig1:      {:9.3e}'.format(self.sig1)


    def select(self, method):
        """Select integrator at runtime

            Dynamically bind methods ending with suffix "_step"; a generic
            "update" method is used by the main class (langevin.LangevinParticle)
            in the computational loop to advance trajectory.
        """
        try:
            self.set_integrator_params(method)
            return getattr(self, '{}_step'.format(method))
        except KeyError as key:
            print("Integrator \"{}\" not found. Valid selections: ".format(key))
            for m in self.integrators.keys(): print("  \"{}\"".format(m))

    def select_duo(self, method):
        try:
            self.set_integrator_params(method)
            return getattr(self, '{}_position'.format(method)), \
                    getattr(self, '{}_velocity'.format(method))
        except KeyError as key:
            print("Integrator {} not found. Valid selections: ".format(key))
            for m in self.integrators.keys(): print("  \"{}\"".format(m))


    def add_force(self, ForceObject, name, **kwargs):
        '''Register a new force for the selected integrator.

             This method allows one to manually add forces to the system *after*
             an integrator has been defined.

             A dictionary is used to keep track of the forces in the system.
        '''
        self.force_catalog[name] = ForceObject(name, **kwargs)


    def forces_exist(self, flag=True):
        if not self.force_catalog:
            flag = False
        return flag


    def calc_forces(self, X):
        F = 0
        for name, force in self.force_catalog.items():
            F += force.apply(X)
        return F

    # TODO: stupid hac of a method that aliases self.force to avoid looping
    def set_single_force(self, name):
        self.force = self.force_catalog[name]

    def calc_single_force(self, X):
        return self.force.apply(X)


    ###############################################################
    # Extended system Langevin Impulse method (Baczewski version)
    ###############################################################
    def ELI_half_velocity(self, V0, F0, S0):
        """Deterministic half velocity update (leap-frog)
            *V0* - velocity at current timestep [nm/ps]
            *F0* - force at current timestep [kJ/mol/nm]
        """
        return V0 + self.dti2*(self.im*F0 - S0)

    def ELI_impulse_velocity(self, Vh, W0):
        """Velocity impulse for a given white Gaussian noise W0
            *Vd* - deterministic velocity update [nm/ps]
            *xi* - stochastic force [kJ/mol/nm]
        """
        # return -(1. - self.the0)*Vh + self.alp0*self.sqrt2kTim*W0
        return -self.ELI_Stokes_drag(Vh) + self.ELI_noise(W0)

    def ELI_Stokes_drag(self, Vh):
        return (1. - self.the0)*Vh

    def ELI_noise(self, W0):
        return self.sig0*W0

    def ELI_position(self, X0, Vh, Vi):
        """LI position update.
            *X0* - position at current timestep [nm]
            *Vh* - deterministic velocity update [nm/ps]
            *Vi* - impulsive velocity component [nm/ps]
        """
        return X0 + (Vh + 0.5*Vi)*self.dt

    def ELI_auxiliary(self, S0, Vh, W0):
        """LI position update.
            *X0* - position at current timestep [nm]
            *Vh* - deterministic velocity update [nm/ps]
            *Vi* - impulsive velocity component [nm/ps]
        """
        return self.the1*S0 - (1. - self.the1)*self.gam0*Vh         \
                + self.sig1*W0

    def ELI_velocity(self, Vh, Vi, F0, S0):
        """LI (full) velocity update.
            *Vh* - deterministic velocity update [nm/ps]
            *Vi* - impulsive velocity component [nm/ps]
            *F0* - force at current timestep [kJ/mol/nm]
        """
        return Vh + Vi + self.dti2*(self.im*F0 - S0)

    def ELI_step(self, ts, X0, V0, S0, F0):
        """Langevin Impulse advance of positions and velocities
            *X0* - position at current timestep [nm]
            *V0* - velocity at current timestep [nm/ps]
            *F0* - force at current timestep [kJ/mol/nm]
        """
        Wn  = self.noise.GWN(sigma=1)
        # Wn  = self.noise.GWN_prealloc(ts)
        Vh  = self.ELI_half_velocity(V0, F0, S0)
        # Vi  = self.ELI_impulse_velocity(Vh, Wn)
        Vd  = self.ELI_Stokes_drag(Vh)
        Vt  = self.ELI_noise(Wn)
        Vi  = -Vd + Vt
        Xn  = self.ELI_position(X0, Vh, Vi)
        Sn  = self.ELI_auxiliary(S0, Vh, Wn)
        # Fn  = self.calc_forces(Xn)
        Fn  = self.calc_single_force(Xn)     # TODO: this is a cheat...
        Vn  = self.ELI_velocity(Vh, Vi, Fn, Sn)

        return Xn, Vn, Sn, Fn, Vd, Vt


    ###############################################################
    # Extended system Langevin Impulse method (Baczewski version)
    ###############################################################
    def ELInc_half_velocity(self, F0, S0):
        """Deterministic half velocity update (leap-frog)
            *V0* - velocity at current timestep [nm/ps]
            *F0* - force at current timestep [kJ/mol/nm]
        """
        return self.dti2*(self.im*F0 - S0)

    def ELInc_impulse_velocity(self, Vh, W0):
        """Velocity impulse for a given white Gaussian noise W0
            *Vd* - deterministic velocity update [nm/ps]
            *xi* - stochastic force [kJ/mol/nm]
        """
        return -(1 - self.the0)*Vh + self.sig0*W0
        # return -self.ELInc_Stokes_drag(Vh) + self.ELInc_noise(W0)

    def ELInc_Stokes_drag(self, Vh):
        return (1 - self.the0)*Vh

    def ELInc_noise(self, W0):
        return self.sig0*W0

    def ELInc_position(self, Vh, Vi):
        """LI position update.
            *X0* - position at current timestep [nm]
            *Vh* - deterministic velocity update [nm/ps]
            *Vi* - impulsive velocity component [nm/ps]
        """
        return (Vh + 0.5*Vi)*self.dt

    def ELInc_auxiliary(self, S0, Vh, W0):
        """LI position update.
            *X0* - position at current timestep [nm]
            *Vh* - deterministic velocity update [nm/ps]
            *Vi* - impulsive velocity component [nm/ps]
        """
        return self.the1*S0 - (1-self.the1)*self.gam0*Vh + self.sig1*W0

    def ELInc_velocity(self, F0, S0):
        """LI (full) velocity update.
            *Vh* - deterministic velocity update [nm/ps]
            *Vi* - impulsive velocity component [nm/ps]
            *F0* - force at current timestep [kJ/mol/nm]
        """
        return self.dti2*(self.im*F0 - S0)

    def ELInc_step(self, ts, Xn, Vn, Sn, Fn):
        """Langevin Impulse advance of positions and velocities
            *Xn* - position at current timestep [nm]
            *Vn* - velocity at current timestep [nm/ps]
            *Fn* - force at current timestep [kJ/mol/nm]
        """
        Wn  = self.noise.GWN(sigma=1)
        # Wn  = self.noise.GWN_prealloc(ts)
        Vn  += self.ELInc_half_velocity(Fn, Sn)
        # Vim  = self.ELInc_impulse_velocity(Vn, Wn)
        Vd  = self.ELI_Stokes_drag(Vn)
        Vt  = self.ELI_noise(Wn)
        Vim  = -Vd + Vt
        Xn  += self.ELInc_position(Vn, Vim)
        Sn  = self.ELInc_auxiliary(Sn, Vn, Wn)
        # Fn  = self.calc_forces(Xn)
        Fn  = self.calc_single_force(Xn)     # TODO: this is a cheat...
        Vim  += self.ELInc_velocity(Fn, Sn)
        Vn  += Vim

        return Xn, Vn, Sn, Fn, Vd, Vt



    ###############################################################
    # Modified Extended Langevin Impulse method (Baczewski version)
    ###############################################################
    def MELI_half_velocity(self, V0, F0, S0):
        """Deterministic half velocity update (leap-frog)
            *V0* - velocity at current timestep [nm/ps]
            *F0* - force at current timestep [kJ/mol/nm]
        """
        return V0 + self.dti2*(self.im*F0 - S0)

    def MELI_impulse_velocity(self, Vh, W0):
        """Velocity impulse for a given white Gaussian noise W0
            *Vd* - deterministic velocity update [nm/ps]
            *xi* - stochastic force [kJ/mol/nm]
        """
        return -(1. - self.the0)*Vh + self.alp0*self.sqrt2kTim*W0

    def MELI_position(self, X0, Vh, Vi):
        """LI position update.
            *X0* - position at current timestep [nm]
            *Vh* - deterministic velocity update [nm/ps]
            *Vi* - impulsive velocity component [nm/ps]
        """
        return X0 + (Vh + 0.5*Vi)*self.dt

    def MELI_auxiliary(self, S0, Vh, W0):
        """LI position update.
            *X0* - position at current timestep [nm]
            *Vh* - deterministic velocity update [nm/ps]
            *Vi* - impulsive velocity component [nm/ps]
        """
        return self.the1*S0 - (1. - self.the1)*self.gam0*Vh         \
                + self.alp1*self.sqrt2kTim*W0

    def MELI_velocity(self, Vh, Vi, F0, S0):
        """LI (full) velocity update.
            *Vh* - deterministic velocity update [nm/ps]
            *Vi* - impulsive velocity component [nm/ps]
            *F0* - force at current timestep [kJ/mol/nm]
        """
        return Vh + Vi + self.dti2*(self.im*F0 - S0)

    def MELI_step(self, X0, V0, S0, F0):
        """Langevin Impulse advance of positions and velocities
            *X0* - position at current timestep [nm]
            *V0* - velocity at current timestep [nm/ps]
            *F0* - force at current timestep [kJ/mol/nm]
        """
        Wn  = self.noise.GWN(sigma=1)
        Vh  = self.ELI_half_velocity(V0, F0, S0)
        Vi  = self.ELI_impulse_velocity(Vh, Wn)
        Xn  = self.ELI_position(X0, Vh, Vi)
        Sn  = self.ELI_auxiliary(S0, Vh, Wn)
        # Fn  = self.calc_forces(Xn)
        Fn  = self.calc_single_force(Xn)     # TODO: this is a cheat...
        Vn  = self.ELI_velocity(Vh, Vi, Fn, Sn)

        return Xn, Vn, Sn, Fn, Wn



    ###############################################################
    # Extended system Langevin Impulse method (Baczewski version)
    ###############################################################
    # def EGJF_position(self, X0, V0, F0, W):
    #     """GJF position update.
    #         *X0* - position at current timestep [nm]
    #         *V0* - velocity at current timestep [nm/ps]
    #         *F0* - force at current timestep [kJ/mol/nm]
    #         *W* - stochastic force [kJ/mol/nm]
    #     """
    #     return X0 + self.alp0dt*(V0 + self.dti2m*F0 + 0.5*self.sqrt2kTim*self.gam0dt*W)
    #
    # def EGJF_auxiliary(self, S0, Vh, W0):
    #     """LI position update.
    #         *X0* - position at current timestep [nm]
    #         *Vh* - deterministic velocity update [nm/ps]
    #         *Vi* - impulsive velocity component [nm/ps]
    #     """
    #     return self.the1*S0 - (1. - self.the1)*self.gam0*Vh         \
    #             + self.alp1*self.sqrt2kTim*W0
    #
    # def EGJF_velocity(self, X0, Xn, V0, F0, Fn, S0, Sn, W):
    #     """GJF velocity update.
    #         *X0* - position at current timestep [nm]
    #         *Xn* - position at next timestep [nm]
    #         *V0* - velocity at current timestep [nm/ps]
    #         *F0* - force at current timestep [kJ/mol/nm]
    #         *Fn* - force at next timestep [kJ/mol/nm]
    #         *W* - stochastic force [kJ/mol/nm]
    #     """
    #     return V0 + self.dti2*self.im*(F0 + Fn) - self.gam0*(Xn - X0) + W  # see above to include S0
    #
    # def EGJF_auxiliary(self, S0, Vh, W0):
    #     """EGJF auxiliary update.
    #         *X0* - position at current timestep [nm]
    #         *Vh* - deterministic velocity update [nm/ps]
    #         *Vi* - impulsive velocity component [nm/ps]
    #     """
    #     return self.the1*S0 - (1. - self.the1)*self.gam0*Vh         \
    #             + self.alp1*self.sqrt2kTim*W0
    #
    # def EGJF_step(self, X0, V0, S0, F0):
    #     """Gronbech-Jensen-Farago advance of positions and velocities
    #         *X0* - position at current timestep [nm]
    #         *V0* - velocity at current timestep [nm/ps]
    #         *F0* - force at current timestep [kJ/mol/nm]
    #     """
    #     Wn = self.noise.GWN(sigma=1)
    #     Xn = self.GJF_position(X0, V0, F0, Wn)
    #     Fn = self.calc_single_force(Xn)     # TODO: this is a cheat...
    #     Vn = self.GJF_velocity(X0, Xn, V0, F0, Fn, Wn)
    #
    #     return Xn, Vn, Sn, Fn, Wn
