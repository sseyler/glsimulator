import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

import numpy.random as npr
import time
import sympy
import pint
ur = pint.UnitRegistry()

import force
import GLE_io_v1 as io
import GLE_integrator_v1 as gli

PI = np.pi

def per_mol(Q, units=None):
    units = Q.units if not units else units
    return (Q * ur.avogadro_number).to(units/ur.mol)

def mass_particle(r, rhop, unit=ur.kg):
    return rhop*(4./3.)*PI*r**3

def effective_mass(r, rhop, rhof, unit=ur.kg):
    return (rhop+0.5*rhof)*(4./3.)*PI*r**3

def basset_coeff(r, eta, rhof, Cb):
    return Cb * 3*r**2*(PI*eta*rhof)**0.5

def t_units(m, r, eta, rhof, unit=ur.s):
    return ((m / (3*PI*r**2*(eta*rhof)**0.5))**2).to(unit)

def x_units(x, unit=ur.m):
    return (x).to(unit)

def v_units(x, t, unit=ur.m/ur.s):
    return (x/t).to(unit)

def F0_units(m, x, t, unit=ur.N):
    return (m*(x/t**2)).to(unit)

def E_units(m, x, t, unit=ur.J):
    return (m*(x/t)**2).to(unit)


class GLESimulation(object):
    """Engine for simulating Langevin dynamics of a particle.

    """
    # Physical constants
    kBol = 1.3806485e-23              # Boltzmann constant (J/K)
    NAvo = 6.022e23                   # Avogadro's number: # particles/mole
    kB2 = 1e-3*kBol*NAvo              # Boltzmann constant (kJ/mol/K)

    def __init__(self, method='ELI', outname='traj', init='origin',
                D=3, N=1, T=300.0, L=1.0e-7, R=1.0e-7, rhop=1.0e3, rhof=1.0e3, eta=1.0e-3,
                gam0=1.0e11, nu1=2.5e10, k=1.0e-6, dt=1.0e-15,
                xtrj=True, vtrj=True, strj=True, seed=123456):
        """Generate system with custom parameters.

        :Arguments:
            *outfile*
                base name of file to output results to
            *dt*
                time step of integration (ps)
            *m*
                mass of particle (amu)
            *T*
                temperature (K)
            *gamma*
                collision frequency (1/ps)
            *k*
                spring constant (kJ/mol/nm^2)
        """
        ### Base units (in MKS) ###
        self.Lc  = L                                # length
        self.Tc  = 1./gam0                          # time (can make same timescale tau from Siegel)
        self.Mc  = rhop*(4./3.)*PI*R**3             # mass
        self.Tec = T if T != 0.0 else 1.0           # temperature
        ### Derived units ###
        self.Vc  = self.Lc/self.Tc                  # velocity
        self.Ec  = self.Mc*(self.Lc/self.Tc)**2     # energy
        self.Nc  = self.Mc/self.Lc**3               # density
        self.Fc  = self.Ec/self.Lc                  # force
        self.kB  = self.kBol/self.Ec*self.Tec       # Boltzmann constant
        ### Physical parameters ###
        self.D    = D                               # number of spatial dims
        self.N    = N                               # number of particles
        self.T    = T/self.Tec                      # temperature [K]
        self.L    = L/self.Lc                       # washboard period [m]
        self.R    = R/self.Lc                       # particle radius [m]
        self.rhop = rhop/self.Nc                    # particle density [kg/m^3]
        self.rhof = rhof/self.Nc                    # fluid density [kg/m^3]
        self.eta  = eta*self.Lc*self.Tc/self.Mc     # dynamic viscosity [kg/m/s]
        self.gam0 = gam0*self.Tc                    # collision frequency [1/s]
        self.nu1  = nu1*self.Tc                     # relaxation time [1/s]
        # self.k    = k/(self.Fc/self.Lc)             # OU spring constant
        self.k    = k                               # washboard force constant
        self.m    = effective_mass(R, rhop, rhof)   # effective mass [kg]
        ### Simulation parameters ###
        self.dt   = dt/self.Tc              # time step [s]
        self.init = init                    # type of position initialization
        ### Convenience parameters ###
        self.kT = self.kB*self.T            # Boltzmann constant * T (kJ/mol)
        self.ND = self.N*self.D             # num configuation space dimensions

        # Set up forces and integrator
        self.force = force.Force(self.k)
        self.integrator = gli.GLEIntegrator(self.N, self.D, self.T, self.L, self.R,
                                    self.rhop, self.rhof, self.eta, self.gam0,
                                    self.nu1, self.m, self.k, self.dt, self.kB,
                                    seed=seed)
        self.update = self.integrator.select(method)
        self.io = io.GLEInputOutput(self, outname, (xtrj, vtrj, strj), N, N, D)
        self.io.setup()


    def run(self, method=None, nsteps=1000, tmpsize=1000,
            ntout=1, printout=100, seed=(None, None)):
        """Perform Langevin MD.

        :Arguments:
            *method*
                integration method [string]
            *nsteps*
                number of steps to run for [int]
            *tmpsize*
                number of steps to store before writing to file [int]
            *ntout*
                store a frame every ntout steps [int]
            *printout*
                number of steps between progress updates
        """
        self.io.open_trj_files()      # open trajectory HDF5 files for writing
        self.io.init_tmp_trj(tmpsize) # init trajs for temp storage pos/vel

        X = self.init_position()
        V = self.init_velocity()
        S = self.init_auxiliary()
        # F = self.force.harmonic(X)     # Don't want to hardcode this...
        F = self.force.washboard(X)     # Don't want to hardcode this...

        for ts in xrange(1, nsteps+1):
            ntidx = (ts % ntout) - 1
            idx   = (ts % tmpsize) - 1
            pidx  = (ts % printout) - 1
            if (ntidx + 1) == 0:
                self.io.itraj[idx]       = ts
                self.io.Xtraj[idx][:]    = X
                self.io.Vtraj[idx][:]    = V
                self.io.Straj[idx][:][:] = S
            if (idx + 1) == 0:
                self.io.dump_tmp_trj(tmpsize)
            if (pidx + 1) == 0:
                self.io.step_print(ts, nsteps)

            X[:], V[:], S[:,:], F[:] = self.update(X, V, S, F)

        self.io.close_trj_files()     # close HDF5 trajectory files


    def init_position(self):
        return np.zeros(self.D)

    def init_velocity(self):
        return np.zeros(self.D)

    def init_auxiliary(self):
        return np.zeros((self.N, self.D))


    @property
    def positions(self):
        """Get position trajectory."""
        out = pd.HDFStore(self.io.Xt_fname, 'r')
        out.open()
        traj = out['positions']  # must match names given to columns in run()
        out.close()
        return traj

    @property
    def velocities(self):
        """Get velocity trajectory."""
        out = pd.HDFStore(self.io.Vt_fname, 'r')
        out.open()
        traj = out['velocities']  # must match names given to columns in run()
        out.close()
        return traj

    @property
    def auxiliary(self):
        """Get auxiliary variable trajectory."""
        out = pd.HDFStore(self.io.St_fname, 'r')
        out.open()
        traj = out['auxiliary']  # must match names given to columns in run()
        out.close()
        return traj

    def quick_plot_MSD(self, **kwargs):
        """Alias for GLEInputOutput.quick_plot_MSD() method."""

        return self.io.quick_plot_MSD(**kwargs)

    def quick_plot_2D(self, **kwargs):
        """Alias for GLEInputOutput.quick_plot_2D() method."""

        return self.io.quick_plot_2D(**kwargs)

    def plot_potential(self, **kwargs):
        """Alias for GLEInputOutput.plot_potential() method."""

        return self.io.plot_potential(**kwargs)

    def plot_force(self, **kwargs):
        """Alias for GLEInputOutput.plot_force() method."""

        return self.io.plot_force(**kwargs)
