import numpy as np
import pandas as pd
import itertools as it
import matplotlib.pyplot as plt

import numpy.random as npr
import time


class Langevin(object):
    """Engine for simulating particle hydrodynamics with the BBO equation.

    """

    def __init__(self, outfile='traj', dt=1.0,
        xtraj=True, vtraj=True, straj=False, init='origin',
        N=1, D=3, m=1.0, T=1.0, gamma=10.0, k=1.0,
        method=None, rseed=7612592):
        """Generate system with custom parameters.

        :Arguments:
            *outfile*
                name of file to output results to
            *dt*
                time step of integration (ps)
            *m*
                mass of particle (amu)
            *T*
                temperature (K)
            *gamma*
                collision frequency (1/ps)
            *fric*
                friction coefficient ( (kJ/(mol*nm))/(nm/ps) )
            *k*
                spring constant (kJ/mol/nm^2)
        """

        self.outfile = outfile              # name of trajectory file

        # physical parameters
        self.m = m                          # particle mass (amu)
        self.N = N                          # number of particles
        self.D = D                          # number of spatial dimensions
        self.T = T                          # temperature (K)
        self.gamma = gamma                  # collision frequency (1/ps)
        self.k = k                          # OU spring constant

        # simulation parameters
        self.dt = dt                        # time step in ps
        self.init = init                    # type of position initialization

        self.Xn, self.Vn = self.initialize()
        # self.X0, self.V0 = self.initialize()
        self.F0 = np.empty_like(self.Xn)
        self.Fn = np.empty_like(self.Xn)

        # physical constants
        self.permol = 6.022E23              # Number of particles per mole
        self.kB = 8.314E-3                  # Boltzmann constant (kJ/mol/K)

        # Convenience parameters
        self.kT   = self.kB*T               # Boltzmann constant * T (kJ/mol)
        self.kTim = self.kT/m               # kT/m (kJ/mol/amu)
        self.idt  = 1./self.dt              # inverse timestep (1/ps)
        self.ND   = self.N*self.D           # num configuation space dimensions
        self.dtim = self.dt/self.m

        # Langevin impulse
        # self.alpha = 1. - np.exp(-self.gamma*self.dt) # vel reduction factor (Goga 2012)
        # self.sigma = (self.kTim*self.alpha*(2.-self.alpha))**0.5

        # GJF integrator
        self.alpha = 1./(1. + 0.5*self.gamma*self.dt)
        self.sigma = (2.*self.gamma*self.kTim*self.dt)**0.5
        self.alpdt = self.alpha*self.dt
        self.dti2m = 0.5*self.dtim

        # initialize trajectory column names
        self.X_columns, self.V_columns, self.scalar_columns =               \
               self.gen_column_names(N, D, xtraj, vtraj, straj)

        # random seed
        np.random.seed(int(rseed))
        # np.random.seed(int(rseed/time.clock()))

        self.Xt_name = self.outfile + '.pos.h5'
        self.Vt_name = self.outfile + '.vel.h5'
        self.Xout = pd.HDFStore(self.Xt_name, 'w')
        self.Vout = pd.HDFStore(self.Vt_name, 'w')


    def step_LI(self):
        xi = self.sigma*npr.randn(self.ND)
        self.F0 = self.force()

        Vp = self.Vn + self.dtim*self.F0
        dV = -self.alpha*Vp + xi
        self.Xn += (Vp + 0.5*dV)*self.dt
        self.Vn = Vp + dV


    def step_GJF(self):
        xi = self.sigma*npr.randn(self.ND)
        self.dX = self.alpdt*(self.Vn + self.dti2m*self.F0 + 0.5*xi)
        self.Xn[:] += self.dX
        self.Fn  = self.force()
        self.Vn += self.dti2m*(self.F0 + self.Fn) - self.gamma*self.dX + xi
        self.F0[:] = self.Fn


    def run(self, method=None, nsteps=1000, writeout=100):
        """Perform Langevin MD.

        :Arguments:
            *method*
                integration method [string]
            *nsteps*
                number of steps to run for [int]
            *writeout*
                number of steps to store before writeout [int]

        """
        # create storage
        self.Xout.open()
        self.Vout.open()

        # trajectory of scalars:
        # step, time, CoM z-position, net force, potential
        # traj = np.zeros((writeout, 5))

        # positions and velocities trajectories
        itraj = xtraj = np.zeros(writeout)
        Xtraj = np.zeros((writeout, self.ND))
        Vtraj = np.zeros((writeout, self.ND))

        for istep in xrange(1, nsteps+1):

            # For the time being, every step is stored; change for efficiency
            idx = (istep % writeout) - 1
            itraj[idx] = istep
            Xtraj[idx][:] = self.Xn
            Vtraj[idx][:] = self.Vn

            if (idx + 1) == 0:
                self.Xout.append('positions', pd.DataFrame(Xtraj[:][:],
                                 columns=self.X_columns, index=itraj[:]))
                self.Vout.append('velocities', pd.DataFrame(Vtraj[:][:],
                                 columns=self.V_columns, index=itraj[:]))
                Xtraj = np.zeros((writeout, self.ND))
                Vtraj = np.zeros((writeout, self.ND))
                print "\rStep {} ({}% complete)".format(istep, istep*100.0/nsteps),

            # self.step_LI()
            self.step_GJF()

        self.Xout.close()
        self.Vout.close()


    def force(self):
        # return 0
        return -self.k*(self.Xn)

    @property
    def positions(self):
        """Get position trajectory.
        """
        out = pd.HDFStore(self.Xt_name, 'r')
        out.open()
        traj = out['positions']  # must match names given to columns in run()
        out.close()
        return traj

    @property
    def velocities(self):
        """Get velocity trajectory.
        """
        out = pd.HDFStore(self.Vt_name, 'r')
        out.open()
        traj = out['velocities']  # must match names given to columns in run()
        out.close()
        return traj


    @staticmethod
    def gen_column_names(N, D, xtraj, vtraj, straj):
        pos_columns, vel_columns, scalar_columns = [], [], []

        if D == 1:
            position_vars = 'x'
            velocity_vars = ['vx']
        elif D == 2:
            position_vars = 'xy'
            velocity_vars = ['vx','vy']
        elif D == 3:
            position_vars = 'xyz'
            velocity_vars = ['vx','vy','vz']

        if xtraj:
            # column names
            pos_columns =                                                       \
                    [''.join(i[::-1])
                        for i in
                    it.product([str(j+1) for j in xrange(N)], position_vars)]
        if vtraj:
            vel_columns =                                                       \
                [''.join(i[::-1])
                    for i in
                it.product([str(j+1) for j in xrange(N)], velocity_vars)]
        if straj:
            scalar_columns = ['position', 'force', 'potential']
        return pos_columns, vel_columns, scalar_columns


    def initialize(self):
        if self.init is 'origin':
            return np.zeros((self.N, self.D)), np.zeros((self.N, self.D))
        else:
            print('Error selecting initial conditions. Choose from \"default\",\"split-y\" or \"gauss\".')


    def quick_plot(self, projection='xy', start=0, end=None, skip=1,
        xlim=[-1.0,1.0], ylim=[-1.0,1.0], zlim=[-1.0,1.0], plotscale=None, figsize=10, ptsize=2):
        """Plot the center of mass trajectory.

        """
        xdim = xlim[1]-xlim[0]
        ydim = ylim[1]-ylim[0]
        zdim = zlim[1]-zlim[0]

        steps, ncoords = self.positions.shape
        X = np.reshape(self.positions.values, (steps, self.N, 3))
        X = X[start:end:skip]

        x,y,z = X[:,:,0], X[:,:,1], X[:,:,2]
        # x_com, y_com, z_com = np.mean(x,axis=1), np.mean(y,axis=1), np.mean(z,axis=1)
        if projection == None:
            fig = plt.figure(figsize=(figsize, figsize))
            ax = fig.add_subplot(111)
            ax.set_xlabel("x position (nm)")
            ax.set_ylabel("y position (nm)")
            ax.set_xlim(xlim)
            ax.set_ylim(ylim)
            for i in xrange(self.N):
                ax.plot(x[:,i], y[:,i], alpha=0.7)
                ax.scatter(x[:,i], y[:,i], marker='o', s=ptsize, alpha=0.7)

        elif projection == 'xy':
            plotscale = 1 if plotscale is None else plotscale
            figsize = plotscale*np.array([2*xdim, ydim])
            fig = plt.figure(figsize=figsize)
            for i in xrange(1):
                ax = fig.add_subplot(1,2,i+1)
                ax.set_xlabel("x position (nm)")
                ax.set_ylabel("y position (nm)")
                ax.set_xlim(xlim)
                ax.set_ylim(ylim)
                if i == 0:
                    for i in xrange(self.N):
                        ax.plot(x[:,i], y[:,i], alpha=0.7)
                        ax.scatter(x[:,i], y[:,i], marker='o', s=ptsize, alpha=0.7)
                else:
                    ax.plot(x_com, y_com)

        elif projection == 'yz':
            plotscale = 1 if plotscale is None else plotscale
            figsize = plotscale*np.array([2*zdim, ydim])
            fig = plt.figure(figsize=figsize)
            for i in xrange(1):
                ax = fig.add_subplot(1,2,i+1)
                ax.set_xlabel("z position (nm)")
                ax.set_ylabel("y position (nm)")
                ax.set_xlim(zlim)
                ax.set_ylim(ylim)
                if i == 0:
                    for i in xrange(self.N):
                        ax.plot(z[:,i], y[:,i], alpha=0.7)
                        ax.scatter(z[:,i], y[:,i], marker='o', s=ptsize, alpha=0.7)
                else:
                    ax.plot(z_com, y_com)
        plt.tight_layout()

        return fig, fig.get_axes()
