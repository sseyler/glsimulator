'''
For advanced usage, make Integrators take Update objects (or some similar
name), the latter being a thin class or extension type that simply
performs an update step for a given kind of integration. The Update
class would only have to take the special parameters it needs from the
Integrator class that manages/instantiates it. The Update class can
furthermore be made callable and probably called directly from the
GLESimulation class.

The advantage of this setup is that setting up different integrators
and forces at runtime is clearer (to the user) and it's easier to
interface with both numba (simpler typing in Update) and Cython
(see cdef types in Extension Types page). It may also make it easier
to parallelize shit using ipyparallel or similar since classes can
be made callable, picklable (I hope...), and usable with map functions.
'''

# import numba
from numba import jit, jitclass, vectorize
from numba import int32, float32, void

import numpy as np
import pandas as pd
import itertools as it
import matplotlib.pyplot as plt

import numpy.random as npr
import time

import force_v3_numba as force

spec = [
    ('Nrep', int32),
    ('Naux', int32),
    ('D', int32),
    ('kT', float32),
    ('m', float32),
    ('gam0', float32),
    ('nu1', float32),
    ('dt', float32),
    ('seed', int32),
    ('im', float32),
    ('idt', float32),
    ('dti2', float32),
    ('dti2m', float32),
    ('dtsqi2', float32),
    ('gam0dt', float32),
    ('kTim', float32),
    ('sqrt2kTim', float32),
    ('the0', float32),
    ('alp0', float32),
    ('sig0', float32),
    ('the1', float32),
    ('alp1', float32),
    ('sig1', float32),
]


# @jit(nopython=True, parallel=True)
# def ELI_step_numba(*arg, **kwarg):
#     return GLEIntegrator.ELI_step(*arg, **kwarg)

# @jit
# def ELI_step_numba(*args):
#     return GLEIntegrator.ELI_step(*args)


@jitclass(spec)
class GLEIntegrator(object):

    def __init__(self, Nrep, Naux, D, kT, m, gam0, nu1, dt, seed):

        self.Nrep = Nrep
        self.Naux = Naux
        self.D = D

        # Input parameters (non-dimensionalized)
        self.kT   = kT                      # temperature
        self.m    = m                       # particle mass
        self.gam0 = gam0                    # collision frequency
        self.nu1  = nu1                     # relaxation time
        self.dt   = dt                      # time step

        # Convenient shortcuts and definitions (combined parameters)
        self.im     = float32(1./m)                  # inverse of mass
        self.idt    = float32(1./dt)                 # inverse of the timestep
        self.dti2   = float32(0.5*dt)                # timestep div by 2
        self.dti2m  = float32(0.5*dt/m)              # timestep div by (2*m)
        self.dtsqi2 = float32(0.5*dt**2)             # Used in Vanden-Eijnden Ciccotti
        self.gam0dt = float32(gam0*dt)               # unitless
        self.kTim   = float32(kT/m)                  # kT/m

        self.sqrt2kTim = float32(0.0)
        self.the0 = float32(0.0)
        self.alp0 = float32(0.0)
        self.sig0 = float32(0.0)
        self.the1 = float32(0.0)
        self.alp1 = float32(0.0)
        self.sig1 = float32(0.0)

        # self.thetak = np.zeros((Nrep, Naux))
        # self.alphak = np.zeros((Nrep, Naux))
        # self.sigmak = np.zeros((Nrep, Naux))

        # self.integrators = ['ELI', 'MELI', 'LI', 'GJF', 'VEC', None]
        # self.method = None

        # self.force_catalog = forces
        # self.noise  = force.StochasticForce(Nrep, Naux, D, seed=seed)

        # self.force = force.ConstantUniformForce('noforce', C0=1.0)  # TODO: this is a hack


    def set_integrator_params(self):
        self.sqrt2kTim = np.sqrt(2.*self.kTim)

        self.the0 = np.exp(-self.gam0*self.dt)
        self.alp0 = ((1. - self.the0**2) / 2.)**0.5
        self.sig0 = self.alp0*self.sqrt2kTim

        self.the1 = np.exp(-self.nu1*self.dt)
        self.alp1 = ((1. - self.the1**2) * self.nu1 / 2.)**0.5
        self.sig1 = self.alp1*self.sqrt2kTim
        # if method == "MELI" or method is None:
        #     self.sqrt2kTim = np.sqrt(2.*self.kTim)
        #     self.the0 = np.exp(-self.gam0*self.dt)
        #     self.alp0 = ((1. - self.the0)**2 / (self.gam0*self.dt))**0.5
        #     self.the1 = np.exp(-self.nu1*self.dt)
        #     self.alp1 = ((1. - self.the1)**2 / self.dt)**0.5
        # elif method == "LI" or method is None:
        #     self.sqrt2kTim = np.sqrt(2.*self.kTim)
        #     self.alpha = 1. - np.exp(-self.gam0dt)
        #     self.sigma = (self.kTim*self.alpha*(2.-self.alpha))**0.5
        # elif method == "GJF":
            # self.gam0dt = self.gam0*self.dt
            # self.alp0 = 1./(1. + 0.5*self.gam0dt)
            #
            # self.alp0dt = self.alp0*self.dt
        # print("Setting parameters for {} integrator.".format(method))
        print 'sqrt2kTim: {:9.3e}'.format(self.sqrt2kTim)
        print 'the0:      {:9.3e}'.format(self.the0)
        print 'alp0:      {:9.3e}'.format(self.alp0)
        print 'sig0:      {:9.3e}'.format(self.sig0)
        print 'the1:      {:9.3e}'.format(self.the1)
        print 'alp1:      {:9.3e}'.format(self.alp1)
        print 'sig1:      {:9.3e}'.format(self.sig1)


    # def select(self, method):
    #     """Select integrator at runtime
    #
    #         Dynamically bind methods ending with suffix "_step"; a generic
    #         "update" method is used by the main class (langevin.LangevinParticle)
    #         in the computational loop to advance trajectory.
    #     """
    #     try:
    #         self.set_integrator_params(method)
    #         return getattr(self, '{}_step'.format(method))
    #     except KeyError as key:
    #         print("Integrator \"{}\" not found. Valid selections: ".format(key))
    #         for m in self.integrators.keys(): print("  \"{}\"".format(m))


    # def add_force(self, ForceObject, name, **kwargs):
    #     '''Register a new force for the selected integrator.
    #
    #          This method allows one to manually add forces to the system *after*
    #          an integrator has been defined.
    #
    #          A dictionary is used to keep track of the forces in the system.
    #     '''
    #     self.force_catalog[name] = ForceObject(name, **kwargs)


    # def forces_exist(self, flag=True):
    #     if not self.force_catalog:
    #         flag = False
    #     return flag


    # def calc_forces(self, X):
    #     F = 0
    #     for name, force in self.force_catalog.items():
    #         F += force.apply(X)
    #     return F

    # TODO: stupid hac of a method that aliases self.force to avoid looping
    # def set_single_force(self, name):
    #     self.force = self.force_catalog[name]

    def calc_single_force(self, X):
        return 0.0*np.ones(X.shape) #self.force.apply(X)


    ###############################################################
    # Extended system Langevin Impulse method (Baczewski version)
    ###############################################################
    def ELI_half_velocity(self, V0, F0, S0):
        """Deterministic half velocity update (leap-frog)
            *V0* - velocity at current timestep [nm/ps]
            *F0* - force at current timestep [kJ/mol/nm]
        """
        return V0 + self.dti2*(self.im*F0 - S0)

    def ELI_impulse_velocity(self, Vh, W0):
        """Velocity impulse for a given white Gaussian noise W0
            *Vd* - deterministic velocity update [nm/ps]
            *xi* - stochastic force [kJ/mol/nm]
        """
        return -(1. - self.the0)*Vh + self.alp0*self.sqrt2kTim*W0
        # return -self.ELI_Stokes_drag(Vh) + self.ELI_noise(W0)

    def ELI_Stokes_drag(self, Vh):
        return (1. - self.the0)*Vh

    def ELI_noise(self, W0):
        return self.sig0*W0

    def ELI_position(self, X0, Vh, Vi):
        """LI position update.
            *X0* - position at current timestep [nm]
            *Vh* - deterministic velocity update [nm/ps]
            *Vi* - impulsive velocity component [nm/ps]
        """
        return X0 + (Vh + 0.5*Vi)*self.dt

    def ELI_auxiliary(self, S0, Vh, W0):
        """LI position update.
            *X0* - position at current timestep [nm]
            *Vh* - deterministic velocity update [nm/ps]
            *Vi* - impulsive velocity component [nm/ps]
        """
        return self.the1*S0 - (1. - self.the1)*self.gam0*Vh         \
                + self.alp1*self.sqrt2kTim*W0

    def ELI_velocity(self, Vh, Vi, F0, S0):
        """LI (full) velocity update.
            *Vh* - deterministic velocity update [nm/ps]
            *Vi* - impulsive velocity component [nm/ps]
            *F0* - force at current timestep [kJ/mol/nm]
        """
        return Vh + Vi + self.dti2*(self.im*F0 - S0)

    def ELI_step(self, X0, V0, S0, F0):
        """Langevin Impulse advance of positions and velocities
            *X0* - position at current timestep [nm]
            *V0* - velocity at current timestep [nm/ps]
            *F0* - force at current timestep [kJ/mol/nm]
        """
        Wn  = npr.randn(self.Nrep, self.Naux, self.D) #0.0*np.ones(X0.shape) #self.noise.GWN(sigma=1)
        Vh  = self.ELI_half_velocity(V0, F0, S0)
        # Vi  = self.ELI_impulse_velocity(Vh, Wn)
        Vdrag = self.ELI_Stokes_drag(Vh)
        Vther = self.ELI_noise(Wn)
        Vi  = -Vdrag + Vther
        Xn  = self.ELI_position(X0, Vh, Vi)
        Sn  = self.ELI_auxiliary(S0, Vh, Wn)
        # Fn  = self.calc_forces(Xn)
        Fn  = self.calc_single_force(Xn)     # TODO: this is a cheat...
        Vn  = self.ELI_velocity(Vh, Vi, Fn, Sn)

        return Xn, Vn, Sn, Fn, Vdrag*self.m*self.idt, Vther*self.m*self.idt
