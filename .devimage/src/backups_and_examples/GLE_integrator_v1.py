import numpy as np
import pandas as pd
import itertools as it
import matplotlib.pyplot as plt

import numpy.random as npr
import time

import force

class GLEIntegrator(object):

    # Physical constants
    kBol = 1.3806485e-23              # Boltzmann constant (J/K)
    NAvo = 6.022e23                   # Avogadro's number: # particles/mole
    kB2 = 1e-3*kBol*NAvo              # Boltzmann constant (kJ/mol/K)
    isqrt3 = 1./3.0**0.5           # 1/sqrt(3)

    def __init__(self, N, D, T, L, R, rhop, rhof, eta,
                 gam0, nu1, m, k, dt, kB, seed=123456):

        # Input parameters (non-dimensionalized)
        self.D    = D                       # number of spatial dimensions
        self.N    = N                       # number of auxiliary particles
        self.T    = T                       # temperature
        self.L    = L                       # periodicity of washboard potential
        self.R    = R                       # particle radius
        self.rhop = rhop                    # particle density
        self.rhof = rhof                    # fluid density
        self.eta  = eta                     # dynamic viscosity
        self.gam0 = gam0                    # collision frequency
        self.nu1  = nu1                     # relaxation time
        self.k    = k                       # OU spring constant
        self.m    = m                       # particle mass
        self.dt   = dt                      # time step
        self.kB   = kB                      # Boltzmann constant
        self.NA   = 1                       # Avogadro's number

        # Convenient shortcuts and definitions (combined parameters)
        self.im     = 1./m                  # inverse of mass
        self.idt    = 1./dt                 # inverse of the timestep
        self.dti2   = 0.5*dt                # timestep div by 2
        self.dti2m  = self.dti2/m           # timestep div by (2*m)
        self.dtsqi2 = 0.5*dt**2             # Used in Vanden-Eijnden Ciccotti
        self.gam0dt = gam0*dt               # unitless
        self.kT     = kB*T                  # Boltzmann constant * T (kJ/mol)
        self.kTim   = self.kT/m             # kT/m
        self.ND     = N*D                   # num config space dims

        self.the0 = 0
        self.alp0 = 0
        self.sig0 = 0
        self.the1 = 0
        self.alp1 = 0
        self.sig1 = 0

        self.thetak = np.zeros(N)
        self.alphak = np.zeros(N)
        self.sigmak = np.zeros(N)

        self.integrators = ['ELI', 'LI', 'GJF', 'VEC', None]
        self.method = None

        self.force = force.Force(self.k)
        self.noise = force.StochasticForce(self.N, self.D, seed=seed)


    def set_integrator_params(self, method):
        if method == "ELI" or method is None:
            self.the0 = np.exp(-self.gam0*self.dt)
            self.alp0 = np.sqrt((1. - self.the0**2) / 2.)
            # self.sig0 = self.alp0 * np.sqrt(2.*self.c0*self.kT)

            self.the1 = np.exp(-self.nu1*self.dt)
            self.alp1 = np.sqrt((1. - self.the1**2) * self.nu1 / 2.)
            # self.sig1 = self.alp1 * np.sqrt(2.*self.ck*self.kT)
            # self.omtkck = (1. - self.the1)*self.ck

            self.sqrt2kTim = np.sqrt(2.*self.kTim)

        elif method == "LI" or method is None:
            self.alpha = 1. - np.exp(-self.gamdt)
            self.sigma = (self.kTim*self.alpha*(2.-self.alpha))**0.5
        elif method == "GJF":
            self.alpha = 1./(1. + 0.5*self.gamdt)
            self.sigma = (2.*self.gamdt*self.kTim)**0.5
            self.alpdt = self.alpha*self.dt

        print("Setting parameters for {} integrator".format(method))


    def select(self, method):
        """Select integrator at runtime

            Dynamically bind methods ending with suffix "_step"; a generic
            "step" method is used by the main class (langevin.LangevinParticle)
            in the computational loop to advance trajectory.
        """
        try:
            self.set_integrator_params(method)
            return getattr(self, '{}_step'.format(method))
        except KeyError as key:
            print("Integrator \"{}\" not found. Valid selections: ".format(key))
            for m in self.integrators.keys(): print("  \"{}\"".format(m))

    def select_duo(self, method):
        try:
            self.set_integrator_params(method)
            return getattr(self, '{}_position'.format(method)), \
                    getattr(self, '{}_velocity'.format(method))
        except KeyError as key:
            print("Integrator {} not found. Valid selections: ".format(key))
            for m in self.integrators.keys(): print("  \"{}\"".format(m))


    ###############################################################
    # Extended system Langevin Impulse method (Baczewski version)
    ###############################################################
    def ELI_half_velocity(self, V0, F0, S0):
        """Deterministic half velocity update (leap-frog)
            *V0* - velocity at current timestep [nm/ps]
            *F0* - force at current timestep [kJ/mol/nm]
        """
        return V0 + self.dti2*(self.im*F0 - S0)

    def ELI_impulse_velocity(self, Vh, W0):
        """Velocity impulse for a given white Gaussian noise W0
            *Vd* - deterministic velocity update [nm/ps]
            *xi* - stochastic force [kJ/mol/nm]
        """
        return -(1. - self.the0)*Vh + self.alp0*self.sqrt2kTim*W0

    def ELI_position(self, X0, Vh, Vi):
        """LI position update.
            *X0* - position at current timestep [nm]
            *Vh* - deterministic velocity update [nm/ps]
            *Vi* - impulsive velocity component [nm/ps]
        """
        return X0 + (Vh + 0.5*Vi)*self.dt

    def ELI_auxiliary(self, S0, Vh, W0):
        """LI position update.
            *X0* - position at current timestep [nm]
            *Vh* - deterministic velocity update [nm/ps]
            *Vi* - impulsive velocity component [nm/ps]
        """
        return self.the1*S0 - (1. - self.the1)*self.gam0*Vh         \
                + self.alp1*self.sqrt2kTim*W0

    def ELI_velocity(self, Vh, Vi, F0, S0):
        """LI (full) velocity update.
            *Vh* - deterministic velocity update [nm/ps]
            *Vi* - impulsive velocity component [nm/ps]
            *F0* - force at current timestep [kJ/mol/nm]
        """
        return Vh + Vi + self.dti2*(self.im*F0 - S0)

    def ELI_step(self, X0, V0, S0, F0):
        """Langevin Impulse advance of positions and velocities
            *X0* - position at current timestep [nm]
            *V0* - velocity at current timestep [nm/ps]
            *F0* - force at current timestep [kJ/mol/nm]
        """
        # Wk = self.noise.GWN_multi(self.sigmak)
        # W0 = np.average(xik, axis=0, weights=self.ck) # TODO: sum aux noise
        # S0 = Sk0.sum(axis=0)                          # TODO: sum aux vars
        W   = self.noise.GWN(sigma=1)
        Vh  = self.ELI_half_velocity(V0, F0, S0)
        Vi  = self.ELI_impulse_velocity(Vh, W)
        Xn  = self.ELI_position(X0, Vh, Vi)
        Sn  = self.ELI_auxiliary(S0, Vh, W)  # TODO: should return a vector?
        # Fn  = self.force.harmonic(Xn)
        Fn  = self.force.washboard(Xn)
        Vn  = self.ELI_velocity(Vh, Vi, Fn, Sn)
        return Xn, Vn, Sn, Fn




    ###############################################################
    # Langevin Impulse method
    ###############################################################
    # def LI_leapfrog_velocity(self, V0, F0):
    #     """Deterministic velocity update (leap-frog)
    #         *V0* - velocity at current timestep [nm/ps]
    #         *F0* - force at current timestep [kJ/mol/nm]
    #     """
    #     return V0 + self.dtim*F0
    #
    # def LI_impulse_velocity(self, Vd, xi):
    #     """Velocity impulse for a given white Gaussian noise xi
    #         *Vd* - deterministic velocity update [nm/ps]
    #         *xi* - stochastic force [kJ/mol/nm]
    #     """
    #     return xi - self.alpha*Vd
    #
    # def LI_position(self, X0, Vd, Vi):
    #     """LI position update.
    #         *X0* - position at current timestep [nm]
    #         *Vd* - deterministic velocity update [nm/ps]
    #         *Vi* - impulsive velocity component [nm/ps]
    #     """
    #     return X0 + (Vd + 0.5*Vi)*self.dt
    #
    # def LI_velocity(self, Vd, Vi):
    #     """LI (full) velocity update.
    #         *Vd* - deterministic velocity update [nm/ps]
    #         *Vi* - impulsive velocity component [nm/ps]
    #         *F0* - force at current timestep [kJ/mol/nm]
    #     """
    #     return Vd + Vi
    #
    # def LI_step(self, X0, V0, F0):
    #     """Langevin Impulse advance of positions and velocities
    #         *X0* - position at current timestep [nm]
    #         *V0* - velocity at current timestep [nm/ps]
    #         *F0* - force at current timestep [kJ/mol/nm]
    #     """
    #     xi = self.noise.GWN(self.sigma)
    #     Vd = self.LI_leapfrog_velocity(V0, F0)
    #     Vi = self.LI_impulse_velocity(Vd, xi)
    #     Xn = self.LI_position(X0, Vd, Vi)
    #     Vn = self.LI_velocity(Vd, Vi)
    #     Fn = self.force.harmonic(Xn)
    #     return Xn, Vn, Fn

    ###############################################################
    # Gronbech-Jensen-Farago integration
    ###############################################################
    # def GJF_position(self, X0, V0, F0, xi):
    #     """GJF position update.
    #         *X0* - position at current timestep [nm]
    #         *V0* - velocity at current timestep [nm/ps]
    #         *F0* - force at current timestep [kJ/mol/nm]
    #         *xi* - stochastic force [kJ/mol/nm]
    #     """
    #     return X0 + self.alpdt*(V0 + self.dti2m*F0 + 0.5*xi)
    #
    # def GJF_velocity(self, X0, Xn, V0, F0, Fn, xi):
    #     """GJF velocity update.
    #         *X0* - position at current timestep [nm]
    #         *Xn* - position at next timestep [nm]
    #         *V0* - velocity at current timestep [nm/ps]
    #         *F0* - force at current timestep [kJ/mol/nm]
    #         *Fn* - force at next timestep [kJ/mol/nm]
    #         *xi* - stochastic force [kJ/mol/nm]
    #     """
    #     return V0 + self.dti2m*(F0 + Fn) - self.gamma*(Xn - X0) + xi
    #
    # def GJF_step(self, X0, V0, F0):
    #     """Gronbech-Jensen-Farago advance of positions and velocities
    #         *X0* - position at current timestep [nm]
    #         *V0* - velocity at current timestep [nm/ps]
    #         *F0* - force at current timestep [kJ/mol/nm]
    #     """
    #     xi = self.noise.GWN(self.sigma)
    #     Xn = self.GJF_position(X0, V0, F0, xi)
    #     Fn = self.force.harmonic(Xn)           # annoying b/c GJF must calc force itself...
    #     Vn = self.GJF_velocity(X0, Xn, V0, F0, Fn, xi)
    #     return Xn, Vn, Fn
