import itertools as it
import numpy as np
import pandas as pd

import matplotlib.pyplot as plt


class GLEInputOutput(object):

    def __init__(self, sim, basename, trj_flags, Nrep, Naux, D, dt, **kwargs):

        self.sim = sim                     # Generalized Langevin sim object
        self.basename = basename
        self.filepath = None
        self.trj_flags = trj_flags

        self.Nrep = Nrep                   # number of simulation replicas
        self.Naux = Naux                   # number of auxiliary variables
        self.D  = D                        # number of spatial dimensions
        self.dt = dt

        self.itraj = None
        self.Xtraj = None
        self.Vtraj = None
        self.Straj = None

        self.Xt_fname = None
        self.Vt_fname = None
        self.St_fname = None

        self.Xt_hdf5 = None
        self.Vt_hdf5 = None
        self.St_hdf5 = None

        self.use_seaborn = kwargs.pop('use_seaborn', True)

    def setup(self):
        """Set up HDF5 trajectory I/O and DataFrame column names."""
        self._gen_col_names(self.trj_flags)
        self._setup_trj_filenames(self.basename)
        self._init_trj_files()

    def init_tmp_trj(self, tmpsize):
        self.itraj = np.zeros(tmpsize)
        self.Xtraj = np.zeros((tmpsize, self.Nrep, 1, self.D))
        self.Vtraj = np.zeros((tmpsize, self.Nrep, 1, self.D))
        self.Straj = np.zeros((tmpsize, self.Nrep, self.Naux, self.D))

    def dump_tmp_trj(self, tmpsize):
        self.Xtraj = self.Xtraj.reshape((tmpsize, self.Nrep*self.D))
        self.Vtraj = self.Vtraj.reshape((tmpsize, self.Nrep*self.D))
        self.Xt_hdf5.append('positions', pd.DataFrame(self.Xtraj[:][:],
                         columns=self.X_cols, index=self.itraj[:]))
        self.Vt_hdf5.append('velocities', pd.DataFrame(self.Vtraj[:][:],
                         columns=self.V_cols, index=self.itraj[:]))

        self.Straj = self.Straj.reshape((tmpsize, self.Nrep*self.Naux*self.D))
        self.St_hdf5.append('aux_velocities', pd.DataFrame(self.Straj[:][:],
                         columns=self.S_cols, index=self.itraj[:]))
        self.init_tmp_trj(tmpsize)

    def step_print(self, n, nsteps):
        print "\rStep {} ({}% complete)".format(n, n*100./nsteps),

    def open_trj_files(self):
        self.Xt_hdf5.open()
        self.Vt_hdf5.open()
        self.St_hdf5.open()
        self.filepath = self.Xt_hdf5.filename.split('/')[:-1][0]


    def close_trj_files(self):
        try:
            print('Closing trajectory files. Data stored ' + \
                    'in:\n\t{}'.format(self.filepath))
            self.Xt_hdf5.close()
            self.Vt_hdf5.close()
            self.St_hdf5.close()
        except:
            print('Unable to close all trajectory files.')


    def _setup_trj_filenames(self, basename):
        self.Xt_fname = '{}.pos.h5'.format(basename)
        self.Vt_fname = '{}.vel.h5'.format(basename)
        self.St_fname = '{}.aux.h5'.format(basename)

    def _init_trj_files(self):
        self.Xt_hdf5 = pd.HDFStore(self.Xt_fname, 'w')
        self.Vt_hdf5 = pd.HDFStore(self.Vt_fname, 'w')
        self.St_hdf5 = pd.HDFStore(self.St_fname, 'w')

    def _gen_col_names(self, trj_flags, scalars=False):

        if type(trj_flags) is tuple:
            try:
                xtraj, vtraj, straj = trj_flags
            except TypeError:
                print("Requested trajectory output not understood;  \
                       defauling to positions and velocities only.")
        else:
            xtraj, vtraj, straj = True, True, False

        pos_cols, vel_cols, aux_cols = [], [], []
        if self.D == 1:
            pos_vars = 'x'
            vel_vars = ['vx']
            aux_vars = ['sx']
        elif self.D == 2:
            pos_vars = 'xy'
            vel_vars = ['vx','vy']
            aux_vars = ['sx','sy']
        elif self.D == 3:
            pos_vars = 'xyz'
            vel_vars = ['vx','vy','vz']
            aux_vars = ['sx','sy','sz']

        if xtraj:
            pos_cols =                                                       \
                ['{} ({})'.format(var, num) for num, var in
                it.product([str(j+1) for j in xrange(self.Nrep)], pos_vars)]
        if vtraj:
            vel_cols =                                                       \
                ['{} ({})'.format(var, num) for num, var in
                it.product([str(j+1) for j in xrange(self.Nrep)], vel_vars)]
        if straj:
            aux_cols =                                                       \
                [''.join(i[::-1]) for i in
                    it.product([' {}'.format(j+1) for j in xrange(self.Nrep)],
                               ['{}'.format(k+1)  for k in xrange(self.Naux)],
                               aux_vars)
                ]
        if scalars:
            sca_cols = ['position', 'force', 'potential']
        self.X_cols, self.V_cols, self.S_cols = pos_cols, vel_cols, aux_cols


    @property
    def positions(self):
        """Get position trajectory."""
        out = pd.HDFStore(self.Xt_fname, 'r')
        out.open()
        traj = out['positions']  # must match names given to columns in run()
        out.close()
        return traj

    @property
    def velocities(self):
        """Get velocity trajectory."""
        out = pd.HDFStore(self.Vt_fname, 'r')
        out.open()
        traj = out['velocities']  # must match names given to columns in run()
        out.close()
        return traj

    @property
    def auxiliary(self):
        """Get auxiliary variable trajectory."""
        out = pd.HDFStore(self.St_fname, 'r')
        out.open()
        traj = out['aux_velocities']  # must match name given to columns in run()
        out.close()
        return traj


    def setup_seaborn(self):
        import seaborn as sns
        sns.set()
        sns.set_style("ticks")
        sns.set_context("notebook", font_scale=1.5, rc={"lines.linewidth": 2.5})


    def quick_plot_MSD(self, xlims=None, tlims=None, step=1,
                       figscale=1, aspect=1.5, ax=None):
        """Plot the mean squared distance.

        """
        if self.use_seaborn: self.setup_seaborn()

        nsteps, ncoords = self.positions.shape
        assert ncoords == self.Nrep*self.D
        The_Xi = [self.positions.values[:,i*self.D:(i+1)*self.D] for i in xrange(self.Nrep)]
        MSD = (self.positions.values[:,:]**2).sum(axis=1)/self.Nrep

        # if tlims is not None:
        #     xlims = xlims[:]
        #     tlims = tlims[:]
        # else:
        #     xlims = np.array([1.0e0, 1.0e1])
        #     tlims = [0, nsteps]

        # t = self.dt*np.linspace()
        # start, stop = tlims[0], tlims[1]
        # t = self.dt*np.arange(start, stop, step)
        t = self.positions.index.values

        if ax is None:
            figsize = figscale * plt.figaspect(float(aspect))
            fig = plt.figure(figsize=figsize)
            ax = fig.add_subplot(111)
        ax.set_xlabel(r"$t$ time (scaled by $T_c$)")
        ax.set_ylabel(r"$\left\langle \Delta x^2\right\rangle$ (scaled by $L_c$)")
        ax.set_xlim(tlims)
        ax.set_ylim(xlims)

        ax.loglog(t[1:], MSD[1:], lw=1.0, marker='.', markersize=0.5, alpha=0.75)

        plt.tight_layout()
        return ax


    def quick_plot_SD(self, xlims=None, tlims=None, step=1,
                       i=0, figscale=1, aspect=1.5, ax=None):
        """Plot the mean squared distance.

        """
        if self.use_seaborn: self.setup_seaborn()

        nsteps, ncoords = self.positions.shape
        assert ncoords == self.Nrep*self.D
        q1_i, qD_i = i*self.D, (i+1)*self.D
        X_i = self.positions.values[:,q1_i:qD_i]
        X = np.reshape(X_i, (nsteps, self.D))

        if xlims is not None:
            xlims = xlims[:]
            tlims = tlims[:]
        else:
            xlims = np.array([1.0e0, 1.0e1])
            tlims = [0, nsteps]
        start, stop = tlims[0], tlims[1]
        t = self.dt*np.arange(start, stop, step)
        X = (X[start:stop:step]-X[0])**2

        if ax is None:
            figsize = figscale * plt.figaspect(float(aspect))
            fig = plt.figure(figsize=figsize)
            ax = fig.add_subplot(111)
        ax.set_xlabel(r"$t$ time (scaled by $T_c$)")
        ax.set_ylabel(r"$\left\langle \Delta x^2\right\rangle$ (scaled by $L_c$)")
        ax.set_xlim(tlims)
        ax.set_ylim(xlims)
        if logscale:
            ax.loglog(t[:], X[:], lw=1.0, marker='.', markersize=0.5, alpha=0.75)
        else:
            ax.plot(t[:], X[:], lw=1.0, marker='.', alpha=0.75)

        plt.tight_layout()
        return ax


    def quick_plot_2D(self, projection='XY', start=0, stop=None, step=1,
                      n=0, lims=None, figscale=1, aspect=1.5, ax=None):
        """Plot the center of mass trajectory.

        """
        if self.use_seaborn: self.setup_seaborn()

        if lims is not None:
            if self.D == 1:
                xlims = lims[0,:]
            elif self.D == 2:
                xlims, ylims = lims[0,:], lims[1,:]
            elif self.D == 3:
                xlims, ylims, zlims = lims[0,:], lims[1,:], lims[2,:]
        else:
            lims = np.vstack((-np.ones(3), np.ones(3))).T

        nsteps, ncoords = self.positions.shape
        assert ncoords == self.Nrep*self.D
        q1_i, qD_i = n*self.D, (n+1)*self.D
        X_i = self.positions.values[:,q1_i:qD_i]
        X = np.reshape(X_i, (nsteps, self.D))
        X = X[start:stop:step]

        if projection == 'XY':
            if ax is None:
                figsize = figscale * plt.figaspect(float(aspect))
                fig = plt.figure(figsize=figsize)
                ax = fig.add_subplot(111)
            ax.set_xlabel(r"$x$ position (in units of $L_c$)")
            ax.set_ylabel(r"$y$ position (in units of $L_c$)")
            ax.set_xlim(xlims)
            ax.set_ylim(ylims)
            ax.plot(X[:,0], X[:,1], color='k', lw=1.0, alpha=0.5)
            ax.scatter(X[:,0], X[:,1], marker='.', s=30, alpha=0.75)

        plt.tight_layout()
        return ax#fig.get_axes()


    def quick_plot_MSV(self, vlims=None, tlims=None, step=1,
                       figscale=1, aspect=1.5, ax=None):
        """Plot the mean squared distance.

        """
        if self.use_seaborn: self.setup_seaborn()

        nsteps, ncoords = self.positions.shape
        assert ncoords == self.Nrep*self.D
        The_Vi = [self.velocities.values[:,i*self.D:(i+1)*self.D] for i in xrange(self.Nrep)]
        MSV = (self.velocities.values[:,:]**2).mean(axis=1)

        if vlims is not None:
            vlims = vlims[:]
            tlims = tlims[:]
        else:
            vlims = np.array([1.0e0, 1.0e1])
            tlims = [0, nsteps]
        start, stop = tlims[0], tlims[1]
        t = self.dt*np.arange(start, stop, step)

        if ax is None:
            figsize = figscale * plt.figaspect(float(aspect))
            fig = plt.figure(figsize=figsize)
            ax = fig.add_subplot(111)
        ax.set_xlabel(r"$t$ time (scaled by $T_c$)")
        ax.set_ylabel(r"$\left\langle \Delta v^2\right\rangle$ (scaled by $V_c$)")
        ax.set_xlim(tlims)
        ax.set_ylim(vlims)

        ax.loglog(t[1:], MSV[1:], lw=1.0, marker='.', markersize=0.5, alpha=0.75)

        plt.tight_layout()
        return ax



    def plot_potential(self, projection='X', xlims=(0,1), Ulims=None, xsteps=200,
                       n=0, figscale=1, aspect=1.5, ax=None):
        """Plot the potential energy function.

        """
        if self.use_seaborn: self.setup_seaborn()

        if ax is None:
            figsize = figscale * plt.figaspect(float(aspect))
            fig = plt.figure(figsize=figsize)
            ax = fig.add_subplot(111)

        ax.set_xlabel(r"$x$ position (in units of $L_c$)")
        ax.set_ylabel(r"$U(x)$ (in units of $E_c$)")
        if xlims is not None:
            ax.set_xlim(xlims)
            xlo, xhi = float(xlims[0]), float(xlims[1])
        else:
            print "Error: need to specify x-axis limits"
        if Ulims is not None:
            ax.set_ylim(Vlims)

        x = np.linspace(xlo, xhi, xsteps)
        U = self.sim.forces['washboard'].U(x)
        ax.plot(x[:], U[:], color='k', lw=1.0, alpha=1.0)
        plt.tight_layout()
        return ax


    def plot_force(self, projection='X', xlims=(0.0,1.0), Flims=None, xsteps=100,
                   n=0, figscale=1, aspect=1.5, ax=None):
        """Plot the global force function.

        """
        if self.use_seaborn: self.setup_seaborn()

        if ax is None:
            figsize = figscale * plt.figaspect(float(aspect))
            fig = plt.figure(figsize=figsize)
            ax = fig.add_subplot(111)

        ax.set_xlabel(r"$x$ position (in units of $L_c$)")
        ax.set_ylabel(r"$F(x)$ (in units of $F_c$)")
        if xlims is not None:
            ax.set_xlim(xlims)
            xlo, xhi = float(xlims[0]), float(xlims[1])
        else:
            print "Error: need to specify x-axis limits"
        if Flims is not None:
            ax.set_ylim(Flims)

        x = np.linspace(xlo, xhi, xsteps)
        F = self.sim.force.washboard(x)
        ax.plot(x[:], F[:], color='k', lw=1.0, alpha=1.0)
        plt.tight_layout()
        return ax
