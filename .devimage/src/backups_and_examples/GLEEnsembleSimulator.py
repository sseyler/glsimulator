from __future__ import with_statement, division

import sys
import signal
import time

import itertools as it
from pathos.pools import ProcessPool

from math import ceil
from collections import OrderedDict

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import pint
ur = pint.UnitRegistry()

# sys.path.append('../util')
from progressbar_simple import ProgressBar

import GLE_io as io
import force_v2
import GLE_integrator_v2 as gli
from GLE_simulation_v2 import GLESimulation


PI = np.pi


def effective_mass(r, rhop, rhof, unit=ur.kg):
    return (rhop+0.5*rhof)*(4./3.)*PI*r**3

def mass_particle(r, rhop, unit=ur.kg):
    return rhop*(4./3.)*PI*r**3

def effective_mass(r, rhop, rhof, unit=ur.kg):
    return (rhop+0.5*rhof)*(4./3.)*PI*r**3

def basset_coeff(r, eta, rhof, Cb):
    return Cb * 3*r**2*(PI*eta*rhof)**0.5



class GLEEnsembleSimulator(object):
    """Engine for simulating an ensemble of particles undergoing generalized Langevin dynamics.

        This class also allows running many independent GLESimulations in parallel
        for quickly generating statistical data.

    """

    # Physical constants
    kBol = 1.3806485e-23              # Boltzmann constant (J/K)
    NAvo = 6.022e23                   # Avogadro's number: # particles/mole
    kB2 = 1e-3*kBol*NAvo              # Boltzmann constant (kJ/mol/K)

    def __init__(self, N_repeats, method='ELI', outname='traj', init='origin',
                D=3, N=1, T=300.0, L=1.0e-7, R=1.0e-7, rhop=1.0e3, rhof=1.0e3, eta=1.0e-3,
                gam0=1.0e11, nu1=2.5e10, k=1.0e-6, dt=1.0e-15,
                xtrj=True, vtrj=True, strj=True, seed=123456):

        self.N_repeats = N_repeats
        # self.init_kwargs = init_kwargs
        self.run_kwargs = None
        self.sim_ids = range(N_repeats)

        self.sims = None
        # self.active_runs = {}
        self.ensemble = {}


        ### Base units (in MKS) ###
        self.Lc  = L                                # length
        self.Tc  = 1./gam0                          # time (can make same timescale tau from Siegel)
        self.Mc  = rhop*(4./3.)*PI*R**3             # mass
        self.Tec = T                                # temperature
        ### Derived units ###
        self.Vc  = self.Lc/self.Tc                  # velocity
        self.Ec  = self.Mc*(self.Lc/self.Tc)**2     # energy
        self.Nc  = self.Mc/self.Lc**3               # density
        self.Fc  = self.Ec/self.Lc                  # force
        self.kB  = self.kBol/self.Ec*self.Tec       # Boltzmann constant
        ### Physical parameters ###
        self.D    = D                               # number of spatial dims
        self.N    = N                               # number of particles
        self.T    = T/self.Tec                      # temperature [K]
        self.L    = L/self.Lc                       # washboard period [m]
        self.R    = R/self.Lc                       # particle radius [m]
        self.rhop = rhop/self.Nc                    # particle density [kg/m^3]
        self.rhof = rhof/self.Nc                    # fluid density [kg/m^3]
        self.eta  = eta*self.Lc*self.Tc/self.Mc     # dynamic viscosity [kg/m/s]
        self.gam0 = gam0*self.Tc                    # collision frequency [1/s]
        self.nu1  = nu1*self.Tc                     # relaxation time [1/s]
        self.m    = effective_mass(R, rhop, rhof)   # effective mass [kg]
        ### Simulation parameters ###
        self.dt   = dt/self.Tc              # time step [s]
        self.init = init                    # type of position initialization
        ### Convenience parameters ###
        self.kT = self.kB*self.T            # Boltzmann constant * T (kJ/mol)
        self.ND = self.N*self.D             # num configuation space dimensions

        self.seed = seed
        self.method = method

        self.forces = {}   # set by calling add_force
        self.integrator = None
        self.update = None  # set by calling add_integrator
        self.io = io.GLEInputOutput(self, outname, (xtrj, vtrj, strj), D, N)
        self.io.setup()

    def add_force(self, force, name, **kwargs):
        '''Add a new force to the system.

            A dictionary is used to keep track of the forces in the system.
        '''
        self.forces[name] = force(name, **kwargs)
        print("Registered force \"{}\" to the system.".format(name))
        if self.integrator:
            self.integrator.add_force(self.forces[name], name, **kwargs)


    def add_integrator(self, method):
        '''Select an integrator.
        '''
        self.method = method
        self.integrator = gli.GLEIntegrator(self.forces, self.N, self.D, self.T, self.L, self.R,
                                    self.rhop, self.rhof, self.eta, self.gam0, self.nu1, self.m,
                                    self.dt, self.kB, seed=self.seed)
        self.update = self.integrator.select(method)


    def valid_system(self, flag=True):
        if self.integrator is None:
            print("Warning: an integrator is not defined for this system.")
            print("    >>> Add an integrator to the system with add_integrator().")
            flag = False
        if self.update is None:
            print("Warning: no update() method has been defined for this system.")
            print("    >>> Add an integrator with add_integrator() to add an update() method.")
            flag = False
        if not self.integrator.forces_exist():
            print("Warning: no forces are currently defined for this system.")
            print("    >>> Register at least one force using integrator.add_force().")
            flag = False
        return flag


    def simulate(self, N_WORKERS=1, TASK_SIZE=1, interval=1, method='map', **run_kwargs):
        # global g_init_kwargs
        global g_run_kwargs
        global temp_results
        global g_pbi

        # g_init_kwargs = self.init_kwargs
        self.run_kwargs = run_kwargs
        g_run_kwargs    = run_kwargs

        g_temp_results = []
        g_pbi = 0

        orig_sigint_handler = signal.signal(signal.SIGINT, signal.SIG_IGN)

        pool = ProcessPool(N_WORKERS)
        signal.signal(signal.SIGINT, orig_sigint_handler)

        N_GROUPS = int(ceil(self.N_repeats/TASK_SIZE))

        with ProgressBar(N_GROUPS, interval=interval, unit='msec') as pb:
            try:
                rs = self._run(method, pool, pb, TASK_SIZE, **g_run_kwargs)
            except KeyboardInterrupt:
                print '\nCaught KeyboardInterrupt; terminating workers...'
                pool.terminate()
            except Exception, e:
                print '\nCaught exception: {}; terminating workers...'.format(e)
                pool.terminate()
            else:
                pool.close()
            pool.join()

        self._results = np.array(g_temp_results)


    def _run(self, method, pool, pb, chunksize, **kwargs):
        if method is 'map':
            f = pool.map
            return self._run_map(f, pb, chunksize, **kwargs)
        elif method is 'map_async':
            f = pool.map_async
            return self._run_map_async(f, pb, chunksize, **kwargs)
        elif method is 'imap':
            f = pool.imap
            return self._run_imap_func(f, pb, chunksize, **kwargs)
        elif method is 'imap_unordered':
            f = pool.imap_unordered
            return self._run_imap_func(f, pb, chunksize, **kwargs)
        elif method is None:
            print "method is 'None'; please specify a method."
        else:
            print "Don't know how we got here, but there's a problem"


    def _run_map(self, map_func, pb, chunksize, **kwargs):
        global g_init_kwargs
        global g_run_kwargs
        global g_temp_results
        global g_pbi

        r_it = map_func(self._run_single_sim, self.sim_ids, chunksize=chunksize)
        for i, item in enumerate(r_it):
            g_temp_results.append(item)
            if i % chunksize == 0:
                pb.update(g_pbi)
                g_pbi += 1
        return r_it


    def _run_single_sim(self, idx):#, **init_kwargs, **run_kwargs):
        global g_init_kwargs
        global g_run_kwargs

        sim = GLESimulation.__init__(self, method=self.method, outname=self.outname,
                                    init=self.init, D=self.D, N=self.N, T=self.T, L=self.L,
                                    R=self.R, rhop=self.rhop, rhof=self.rhof, eta=self.eta,
                                    gam0=self.gam0, nu1=self.nu1, dt=self.dt, xtrj=self.xtrj,
                                    vtrj=self.vtrj, strj=self.strj, seed=self.seed)
        self.active_runs[idx] = sim.run(**g_run_kwargs)
        return self.ensemble[idx]









# class ParallelRun(object):
#
#     def __init__(self, tasks, run_kwargs):
#         self.tasks = tasks  # try inputting a generator first
#         self.run_kwargs = run_kwargs
#
#         self.num_objs = len(self.tasks)
#         self.results = None
#
#
#     def _run_map_async(self, map_async, pb, chunksize, **kwargs):
#         def _callback_function(r):
#             global temp_results
#             global pbi
#             pb.update(pbi)
#             pbi += 1
#             return temp_results.extend(r)
#
#         callback = kwargs.pop('callback', _callback_function)
#         while True:
#             groups = [pairs for pairs in it.islice(self.data_it, chunksize)]
#             if groups:
#                 rs = map_async(self.func, groups, callback=callback)
#             else:
#                 break
#         return rs
#
#
#     def _run_imap_func(self, imap_func, pb, chunksize, **kwargs):
#         global temp_results
#         global pbi
#         r_it = imap_func(self.func, self.data_it, chunksize=chunksize)
#         for i, item in enumerate(r_it):
#             temp_results.append(item)
#             if i % chunksize == 0:
#                 pb.update(pbi)
#                 pbi += 1
#         return r_it
#
#
#     def _run(self, method, pool, pb, chunksize, **kwargs):
#         if method is 'map_async':
#             f = pool.map_async
#             return self._run_map_async(f, pb, chunksize, **kwargs)
#         elif method is 'imap':
#             f = pool.imap
#             return self._run_imap_func(f, pb, chunksize, **kwargs)
#         elif method is 'imap_unordered':
#             f = pool.imap_unordered
#             return self._run_imap_func(f, pb, chunksize, **kwargs)
#         elif method is None:
#             print "method is 'None'; please specify a method."
#         else:
#             print "Don't know how we got here, but there's a problem"
#
#
#     def run(self, N_PROCESSES=1, TASK_SIZE=1, interval=1, method='map_async', **kwargs):
#         global temp_results
#         global pbi
#
#         temp_results = []
#         pbi = 0
#
#         orig_sigint_handler = signal.signal(signal.SIGINT, signal.SIG_IGN)  # init_worker(signal.SIG_IGN)
#         pool = mp.ProcessPool(N_PROCESSES)
#         signal.signal(signal.SIGINT, orig_sigint_handler)  # init_worker(orig_sigint_handler)
#
#         N_GROUPS = int(ceil(self.data_size/TASK_SIZE))
#         with ProgressBar(N_GROUPS, interval=interval, unit='msec') as pb:
#             try:
#                 rs = self._run(method, pool, pb, TASK_SIZE, **kwargs)
#             except KeyboardInterrupt:
#                 print '\nCaught KeyboardInterrupt; terminating workers...'
#                 pool.terminate()
#             except Exception, e:
#                 print '\nCaught exception: {}; terminating workers...'.format(e)
#                 pool.terminate()
#             else:
#                 pool.close()
#             pool.join()
#
#         self._results = np.array(temp_results)
#
#
#     @property
#     def results(self):
#         if self._results is None:
#             err_str = "No results data; do 'Parallelizer.run()' first."
#             raise ValueError(err_str)
#         return self._results
