import numpy as np
import pandas as pd
import itertools as it
import matplotlib.pyplot as plt

import numpy.random as npr
import time

import force
import integrator


class LangevinParticle(object):
    """Engine for simulating Langevin dynamics of a particle.

    """

    kB = 8.314E-3                  # Boltzmann constant (kJ/mol/K)
    NA = 6.022E23                  # Avogadro's number: # particles/mole

    def __init__(self, method='LI', filename='traj', init='origin',
                dt=1.0, N=1, D=3, m=1.0, T=1.0, gamma=10.0, k=1.0,
                xtrj=True, vtrj=True, strj=False, rseed=7612592):
        """Generate system with custom parameters.

        :Arguments:
            *outfile*
                base name of file to output results to
            *dt*
                time step of integration (ps)
            *m*
                mass of particle (amu)
            *T*
                temperature (K)
            *gamma*
                collision frequency (1/ps)
            *k*
                spring constant (kJ/mol/nm^2)
        """

        # physical parameters
        self.m = m                          # particle mass (amu)
        self.N = N                          # number of particles
        self.D = D                          # number of spatial dimensions
        self.T = T                          # temperature (K)
        self.gamma = gamma                  # collision frequency (1/ps)
        self.k = k                          # OU spring constant

        # simulation parameters
        self.dt = dt                        # time step in ps
        self.init = init                    # type of position initialization

        # Convenience parameters
        self.kT = self.kB*T                 # Boltzmann constant * T (kJ/mol)
        self.ND = self.N*self.D             # num configuation space dimensions

        # Set up forces and integrator
        self.force = force.Force(self.k)
        self.LI = integrator.LangevinIntegrator(N, D, T, m, gamma, k, dt)
        self.step = self.LI.select(method)

        # random seed
        tclk = 1 #time.clock()
        np.random.seed(int(rseed/tclk))

        # Set up HDF5 trajectory I/O and column naming
        self._setup_trj_files(filename, xtrj, vtrj, strj)


    def run(self, method=None, nsteps=1000, tmpsize=1000, ntout=1, printout=100):
        """Perform Langevin MD.

        :Arguments:
            *method*
                integration method [string]
            *nsteps*
                number of steps to run for [int]
            *tmpsize*
                number of steps to store before writing to file [int]
            *ntout*
                store a frame every ntout steps [int]
            *printout*
                number of steps between progress updates
        """
        self._open_trj_files()       # open trajectory HDF5 files for writing
        self._init_tmp_trj(tmpsize)  # init trajs for temporary storage pos/vel

        X = self.init_position()
        V = self.init_velocity()
        F = self.force.harmonic(X)     # Don't want to hardcode this...

        for istep in xrange(1, nsteps+1):
            ntidx = (istep % ntout) - 1
            idx   = (istep % tmpsize) - 1
            pidx  = (istep % printout) - 1
            if (ntidx + 1) == 0:
                self.itraj[idx]    = istep
                self.Xtraj[idx][:] = X
                self.Vtraj[idx][:] = V
            if (idx + 1) == 0:
                self._dump_traj(tmpsize)
            if (pidx + 1) == 0:
                print "\rStep {} ({}% complete)".format(istep, istep*100./nsteps),

            X[:], V[:], F[:] = self.step(X, V, F)

        self._close_trj_files()     # close HDF5 trajectory files


    def _setup_trj_files(self, filename, xtrj, vtrj, strj):
        # initialize trajectory output column names
        self.X_cols, self.V_cols, self.S_cols = self._gen_column_names(xtrj, vtrj, strj)
        self.Xt_fname = '{}.pos.h5'.format(filename)
        self.Vt_fname = '{}.vel.h5'.format(filename)
        self.Xout = pd.HDFStore(self.Xt_fname, 'w')
        self.Vout = pd.HDFStore(self.Vt_fname, 'w')

    def _init_tmp_trj(self, tmpsize):
        self.itraj = np.zeros(tmpsize)
        self.Xtraj = np.zeros((tmpsize, self.ND))
        self.Vtraj = np.zeros((tmpsize, self.ND))

    def _dump_traj(self, tmpsize):
        self.Xout.append('positions', pd.DataFrame(self.Xtraj[:][:],
                         columns=self.X_cols, index=self.itraj[:]))
        self.Vout.append('velocities', pd.DataFrame(self.Vtraj[:][:],
                         columns=self.V_cols, index=self.itraj[:]))
        self.itraj = np.zeros(tmpsize)
        self.Xtraj = np.zeros((tmpsize, self.ND))
        self.Vtraj = np.zeros((tmpsize, self.ND))

    def _open_trj_files(self):
        self.Xout.open()
        self.Vout.open()

    def _close_trj_files(self):
        self.Xout.close()
        self.Vout.close()


    def _gen_column_names(self, xtraj, vtraj, straj):
        pos_cols, vel_cols, sca_cols = [], [], []
        if self.D == 1:
            pos_vars = 'x'
            vel_vars = ['vx']
        elif self.D == 2:
            pos_vars = 'xy'
            vel_vars = ['vx','vy']
        elif self.D == 3:
            pos_vars = 'xyz'
            vel_vars = ['vx','vy','vz']

        if xtraj:
            pos_cols =                                                       \
                [''.join(i[::-1]) for i in
                it.product([str(j+1) for j in xrange(self.N)], pos_vars)]
        if vtraj:
            vel_cols =                                                       \
                [''.join(i[::-1]) for i in
                it.product([str(j+1) for j in xrange(self.N)], vel_vars)]
        if straj:
            sca_cols = ['position', 'force', 'potential']
        return pos_cols, vel_cols, sca_cols


    def init_position(self):
        return np.zeros((self.N, self.D))

    def init_velocity(self):
        return np.zeros((self.N, self.D))


    @property
    def positions(self):
        """Get position trajectory."""
        out = pd.HDFStore(self.Xt_fname, 'r')
        out.open()
        traj = out['positions']  # must match names given to columns in run()
        out.close()
        return traj

    @property
    def velocities(self):
        """Get velocity trajectory."""
        out = pd.HDFStore(self.Vt_fname, 'r')
        out.open()
        traj = out['velocities']  # must match names given to columns in run()
        out.close()
        return traj

    def quick_plot(self, projection='xy', start=0, end=None, skip=1,
        xlim=[-1.0,1.0], ylim=[-1.0,1.0], zlim=[-1.0,1.0], plotscale=None, figsize=10, ptsize=2):
        """Plot the center of mass trajectory.

        """
        xdim = xlim[1]-xlim[0]
        ydim = ylim[1]-ylim[0]
        zdim = zlim[1]-zlim[0]

        steps, ncoords = self.positions.shape
        X = np.reshape(self.positions.values, (steps, self.N, 3))
        X = X[start:end:skip]

        x,y,z = X[:,:,0], X[:,:,1], X[:,:,2]
        # x_com, y_com, z_com = np.mean(x,axis=1), np.mean(y,axis=1), np.mean(z,axis=1)
        if projection == None:
            fig = plt.figure(figsize=(figsize, figsize))
            ax = fig.add_subplot(111)
            ax.set_xlabel("x position (nm)")
            ax.set_ylabel("y position (nm)")
            ax.set_xlim(xlim)
            ax.set_ylim(ylim)
            for i in xrange(self.N):
                ax.plot(x[:,i], y[:,i], alpha=0.7)
                ax.scatter(x[:,i], y[:,i], marker='o', s=ptsize, alpha=0.7)

        elif projection == 'xy':
            plotscale = 1 if plotscale is None else plotscale
            figsize = plotscale*np.array([2*xdim, ydim])
            fig = plt.figure(figsize=figsize)
            for i in xrange(1):
                ax = fig.add_subplot(1,2,i+1)
                ax.set_xlabel("x position (nm)")
                ax.set_ylabel("y position (nm)")
                ax.set_xlim(xlim)
                ax.set_ylim(ylim)
                if i == 0:
                    for i in xrange(self.N):
                        ax.plot(x[:,i], y[:,i], alpha=0.7)
                        ax.scatter(x[:,i], y[:,i], marker='o', s=ptsize, alpha=0.7)
                else:
                    ax.plot(x_com, y_com)

        elif projection == 'yz':
            plotscale = 1 if plotscale is None else plotscale
            figsize = plotscale*np.array([2*zdim, ydim])
            fig = plt.figure(figsize=figsize)
            for i in xrange(1):
                ax = fig.add_subplot(1,2,i+1)
                ax.set_xlabel("z position (nm)")
                ax.set_ylabel("y position (nm)")
                ax.set_xlim(zlim)
                ax.set_ylim(ylim)
                if i == 0:
                    for i in xrange(self.N):
                        ax.plot(z[:,i], y[:,i], alpha=0.7)
                        ax.scatter(z[:,i], y[:,i], marker='o', s=ptsize, alpha=0.7)
                else:
                    ax.plot(z_com, y_com)
        plt.tight_layout()

        return fig, fig.get_axes()
