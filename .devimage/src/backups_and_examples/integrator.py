import numpy as np
import pandas as pd
import itertools as it
import matplotlib.pyplot as plt

import numpy.random as npr
import time

import force

class LangevinIntegrator(object):

    # physical constants
    kB = 8.314E-3                  # Boltzmann constant (kJ/mol/K)
    NA = 6.022E23                  # Avogadro's number: # particles/mole
    isqrt3 = 1./3.0**0.5           # 1/sqrt(3)

    def __init__(self, N, D, T, m, gamma, k, dt):
        self.N = N                          # number of particles
        self.D = D                          # number of spatial dimensions
        self.m = m                          # particle mass (amu)
        self.T = T                          # temperature (K)
        self.gamma = gamma                  # collision frequency (1/ps)
        self.k = k                          # spring constant
        self.dt = dt                        # time step (ps)

        # Shortcut (combined) parameters
        self.idt   = 1./self.dt             # inverse of the timestep (1/ps)
        self.dtim  = self.dt/self.m         # timestep over mass (ps/amu)
        self.dti2m = 0.5*self.dtim          # timestep over 2*m (ps/amu)
        self.dtsqi2 = 0.5*self.dt**2        # Used in Vanden-Eijnden Ciccotti
        self.gamdt = self.gamma*self.dt     # unitless
        self.kT    = self.kB*T              # Boltzmann constant * T (kJ/mol)
        self.kTim  = self.kT/self.m         # kT/m (kJ/mol/amu)
        self.ND    = self.N*self.D          # num configuation space dimensions

        self.alpha = 0
        self.sigma = 0
        self.integrators = ['LI', 'GJF', 'VEC', None]
        self.method = None

        self.force = force.Force(self.k)
        self.noise = force.StochasticForce(self.ND)


    def set_integrator_params(self, method):
        # Langevin Impulse: Goga, et al. (2012)
        if method == "LI" or method is None:
            self.alpha = 1. - np.exp(-self.gamdt)
            self.sigma = (self.kTim*self.alpha*(2.-self.alpha))**0.5
        # Gronbech-Jensen & Farago (2013), Eq. (19-20)
        elif method == "GJF":
            self.alpha = 1./(1. + 0.5*self.gamdt)
            self.sigma = (2.*self.gamdt*self.kTim)**0.5
            self.alpdt = self.alpha*self.dt

        print("Settings parameters for {} integrator".format(method))

    def select(self, method):
        """Select integrator at runtime

            Dynamically bind methods ending with suffix "_step"; a generic
            "step" method is used by the main class (langevin.LangevinParticle)
            in the computational loop to advance trajectory.
        """
        try:
            self.set_integrator_params(method)
            return getattr(self, '{}_step'.format(method))
        except KeyError as key:
            print("Integrator \"{}\" not found. Valid selections: ".format(key))
            for m in self.integrators.keys(): print("  \"{}\"".format(m))

    def select_duo(self, method):
        try:
            self.set_integrator_params(method)
            return getattr(self, '{}_position'.format(method)), \
                    getattr(self, '{}_velocity'.format(method))
        except KeyError as key:
            print("Integrator {} not found. Valid selections: ".format(key))
            for m in self.integrators.keys(): print("  \"{}\"".format(m))


    def LI_leapfrog_velocity(self, V0, F0):
        """Deterministic velocity update (leap-frog)
            *V0* - velocity at current timestep [nm/ps]
            *F0* - force at current timestep [kJ/mol/nm]
        """
        return V0 + self.dtim*F0

    def LI_impulse_velocity(self, Vd, xi):
        """Velocity impulse for a given white Gaussian noise xi
            *Vd* - deterministic velocity update [nm/ps]
            *xi* - stochastic force [kJ/mol/nm]
        """
        return xi - self.alpha*Vd

    def LI_position(self, X0, Vd, Vi):
        """LI position update.
            *X0* - position at current timestep [nm]
            *Vd* - deterministic velocity update [nm/ps]
            *Vi* - impulsive velocity component [nm/ps]
        """
        return X0 + (Vd + 0.5*Vi)*self.dt

    def LI_velocity(self, Vd, Vi):
        """LI (full) velocity update.
            *Vd* - deterministic velocity update [nm/ps]
            *Vi* - impulsive velocity component [nm/ps]
            *F0* - force at current timestep [kJ/mol/nm]
        """
        return Vd + Vi

    def LI_step(self, X0, V0, F0):
        """Langevin Impulse advance of positions and velocities
            *X0* - position at current timestep [nm]
            *V0* - velocity at current timestep [nm/ps]
            *F0* - force at current timestep [kJ/mol/nm]
        """
        xi = self.noise.GWN(self.sigma)
        Vd = self.LI_leapfrog_velocity(V0, F0)
        Vi = self.LI_impulse_velocity(Vd, xi)
        Xn = self.LI_position(X0, Vd, Vi)
        Vn = self.LI_velocity(Vd, Vi)
        Fn = self.force.harmonic(Xn)     # annoying b/c LI must calc force itself...
        return Xn, Vn, Fn


    def GJF_position(self, X0, V0, F0, xi):
        """GJF position update.
            *X0* - position at current timestep [nm]
            *V0* - velocity at current timestep [nm/ps]
            *F0* - force at current timestep [kJ/mol/nm]
            *xi* - stochastic force [kJ/mol/nm]
        """
        return X0 + self.alpdt*(V0 + self.dti2m*F0 + 0.5*xi)

    def GJF_velocity(self, X0, Xn, V0, F0, Fn, xi):
        """GJF velocity update.
            *X0* - position at current timestep [nm]
            *Xn* - position at next timestep [nm]
            *V0* - velocity at current timestep [nm/ps]
            *F0* - force at current timestep [kJ/mol/nm]
            *Fn* - force at next timestep [kJ/mol/nm]
            *xi* - stochastic force [kJ/mol/nm]
        """
        return V0 + self.dti2m*(F0 + Fn) - self.gamma*(Xn - X0) + xi

    def GJF_step(self, X0, V0, F0):
        """Gronbech-Jensen-Farago advance of positions and velocities
            *X0* - position at current timestep [nm]
            *V0* - velocity at current timestep [nm/ps]
            *F0* - force at current timestep [kJ/mol/nm]
        """
        xi = self.noise.GWN(self.sigma)
        Xn = self.GJF_position(X0, V0, F0, xi)
        Fn = self.force.harmonic(Xn)           # annoying b/c GJF must calc force itself...
        Vn = self.GJF_velocity(X0, Xn, V0, F0, Fn, xi)
        return Xn, Vn, Fn
