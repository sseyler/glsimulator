from __future__ import division
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

import numpy.random as npr
import time
import sympy
import pint
ur = pint.UnitRegistry()

import sys
sys.path.append('util')
from progressbar_simple import ProgressBar

# import pyximport; pyximport.install()
import gle_io as io           # used explicitly by GLESimulation TODO: changethis?
import gle_integrator as gli  # used explicitly by GLESimulation TODO: changethis?

# from types import MethodType

PI = np.pi


def m_eff(r, rhp, rhf, unit=ur.kg):
    return (rhp+0.5*rhf)*(4/3)*PI*r**3

def mass_particle(r, rhp, unit=ur.kg):
    return rhp*(4/3)*PI*r**3

def effective_mass(r, rhp, rhf, unit=ur.kg):
    return (rhp+0.5*rhf)*(4/3)*PI*r**3

def basset_coeff(r, eta, rhf, Cb):
    return Cb * 3*r**2*(PI*eta*rhf)**0.5


class GLESimulation(object):
    """Engine for simulating Langevin dynamics of a particle.

    """
    # Physical constants
    kBol = 1.3806485e-23              # Boltzmann constant (J/K)
    NAvo = 6.022e23                   # Avogadro's number: # particles/mole

    def __init__(self, method='ELI', outname='traj', outdir='data',
                 NREP=1, NAUX=1, NDIM=3,
                 Te=303.15, R=1.0e-7, rhp=1.0e3, rhf=1.0e3, eta=1.0e-3,
                 tau0=2.5e-9, gam0=1.0e11, nu1=2.5e10, dt=1.0e-15,
                 maxboltz=True, seed=1234567):
        """Generate system with custom parameters.

        :Arguments:
            *outfile*
                base name of file to output results to
            *dt*
                time step of integration (ps)
            *m*
                mass of particle (amu)
            *T*
                temperature (K)
            *gamma*
                collision frequency (1/ps)
            *k*
                spring constant (kJ/mol/nm^2)
        """
        # Define system dimensionality
        self.NREP = NREP                     # number of simulation replicas
        self.NAUX = NAUX                     # number of auxiliary variables
        self.NDIM = NDIM                     # number of spatial dimensions
        ### Base units (in MKS) ###
        self.Lc   = R                                # length
        self.Tc   = tau0                             # time (can be same tau0 from Siegel)
        self.Mc   = rhp*(4/3)*PI*R**3              # mass
        self.Tec  = Te if Te != 0.0 else 1.0         # temperature
        ### Derived units ###
        self.Vc   = self.Lc/self.Tc                  # velocity
        self.Ec   = self.Mc*(self.Lc/self.Tc)**2     # energy
        self.Nc   = self.Mc/self.Lc**3               # density
        self.Fc   = self.Ec/self.Lc                  # force
        ### Physical parameters ###
        self.Te   = Te/self.Tec                     # temperature [K]
        self.R    = R/self.Lc                       # particle radius [m]
        self.rhp  = rhp/self.Nc                     # particle density [kg/m^3]
        self.rhf  = rhf/self.Nc                     # fluid density [kg/m^3]
        self.eta  = eta*self.Lc*self.Tc/self.Mc     # dynamic viscosity [kg/m/s]
        self.gam0 = gam0*self.Tc                    # collision frequency [1/s]
        self.nu1  = nu1*self.Tc                     # relaxation time [1/s]
        self.m    = 1 #m_eff(R, rhp, rhf)/self.Mc      # effective mass [kg]
        ### Simulation parameters ###
        self.dt   = dt/self.Tc                      # time step [s]
        ### Convenience parameters ###
        self.kT = self.kBol*Te/self.Ec              # Boltzmann constant * T (kJ/mol)

        self.seed = seed
        self.method = method
        self.maxboltz = maxboltz

        self.outname = outname
        self.outdir = outdir
        self.basename = '{}/{}'.format(outdir, outname)
        self.force_catalog = {}   # set by calling add_force
        self.integrator = None
        self._update = None  # set by calling add_integrator
        self.io = io.GLEInputOutput(self, NREP, NAUX, NDIM)
        self.io.setup(complevel=5, complib='bzip2')


    def add_force(self, ForceObject, name, **kwargs):
        '''Add a new force to the system.

            A dictionary is used to keep track of the forces in the system
            by name.
        '''
        self.force_catalog[name] = ForceObject(name, **kwargs)
        print("Registered force \"{}\" to the system.".format(name))
        if self.integrator:
            self.integrator.add_force(self.force_catalog[name], name, **kwargs)


    def set_integrator(self, name):
        '''Set the integration scheme.

            The integrator is selected by the name of the scheme (`name`)
        '''
        self.method = name
        self.integrator = gli.GLEIntegrator(self.force_catalog, self.NREP, self.NAUX,
                                            self.NDIM, self.kT, self.m, self.gam0, self.nu1,
                                            self.dt, seed=self.seed)
        self._update = self.integrator.select(name)


    def is_valid_system(self, flag=True):
        if self.integrator is None:
            print("Warning: an integrator is not defined for this system.")
            print("    >>> Add an integrator to the system with add_integrator().")
            flag = False
        if self._update is None:
            print("Warning: no update() method has been defined for this system.")
            print("    >>> Add an integrator with add_integrator() to add an update() method.")
            flag = False
        if not self.integrator.forces_exist():
            print("Warning: no forces are currently defined for this system.")
            print("    >>> Register at least one force using integrator.add_force().")
            flag = False
        return flag


    def run(self, method=None, NSTEPS=1000, tmpsize=1000,
            NTOUT=1, printout=1000, seed=None):
        """Perform Langevin MD.

        :Arguments:
            *method*
                integration method [string]
            *NSTEPS*
                number of steps to run for [int]
            *tmpsize*
                number of steps to store before writing to file [int]
            *NTOUT*
                store a frame every ntout steps [int]
            *printout*
                number of steps between progress updates
        """
        if not self.is_valid_system():
            print("Error: system validation failed. Terminating run...")

        tmpsize = min(tmpsize, int(NSTEPS/NTOUT))
        self.io.open_trj_files()      # open trajectory HDF5 files for writing
        self.io.init_tmp_trj(tmpsize) # init trajs for temp storage pos/vel

        t = 0.0
        X = self.init_position(tmpsize)
        V = self.init_velocity(tmpsize)
        S = self.init_auxiliary(tmpsize)
        FD = np.zeros_like(S)
        FT = np.zeros_like(S)
        # F = self.integrator.calc_forces(X)          # TODO: this is a cheat
        # print self.integrator.force_catalog.keys()[0]
        self.integrator.set_single_force(self.integrator.force_catalog.keys()[0])   # TODO: a hack
        F = self.integrator.calc_single_force(X)     # TODO: this is a cheat

        # W = np.zeros_like(X)
        # Vim = np.zeros_like(X)
        # self.integrator.noise.prealloc(NSTEPS, self.NREP, self.NAUX, self.NDIM)
        with ProgressBar(NSTEPS, print_interval=0.05) as pb:
            for n in xrange(NSTEPS):
                ntidx = n % NTOUT
                idx   = (n+1) % (tmpsize*NTOUT)
                pidx  = n % printout
                if ntidx == 0:
                    tidx = int(n/NTOUT)
                    self.io.itraj[tidx]       = self.dt*n
                    self.io.Xtraj[tidx][:]    = X[n]
                    self.io.Vtraj[tidx][:]    = V[n]
                    self.io.Ftraj[tidx][:]    = F[n]
                    self.io.Straj[tidx][:][:] = S[n]
        #             FDn *= m/dt
        #             FTn *= m/dt
        #             io.FDtraj[tidx][:]   = FDn
        #             io.FTtraj[tidx][:]   = FTn
                # if idx == 0:
                #     print 'Dumping traj at step {}'.format(n)
                #     self.io.dump_tmp_trj(tmpsize, n, NSTEPS)
                if pidx == 0:
                    # self.io.step_print(n, NSTEPS)
                    pb.update(n)

                # X[n+1], V[n+1], S[n+1], F[n+1], FD[n+1], FT[n+1] = self.update(n, X[n], V[n], S[n], F[n])
                X[n+1], V[n+1], S[n+1], F[n+1] = self.update(n, X[n], V[n], S[n], F[n])

        self.io.close_trj_files()     # close HDF5 trajectory files
        return pb


    def init_position(self, tmpsize):
        return np.zeros((tmpsize+1, self.NREP, 1, self.NDIM))

    def init_velocity(self, tmpsize):
        if self.maxboltz:
            V = np.zeros((tmpsize+1, self.NREP, self.NAUX, self.NDIM))
            V[0] = self.maxwell_boltzmann_velocities()
        else:
            V = 0.0*np.ones((tmpsize+1, self.NREP, 1, self.NDIM))
        return V

    def init_auxiliary(self, tmpsize):
        return np.zeros((tmpsize+1, self.NREP, self.NAUX, self.NDIM))


    def maxwell_boltzmann_velocities(self):
        sigma = (self.kT/self.m)**0.5 * np.ones((self.NREP, 1, self.NDIM))
        return npr.normal(loc=0.0, scale=sigma)


    def update(self, ts, X, V, S, F):
        """Step the dynamical variables to the next time level.

            This method is set by selecting the integrator with ``
        """
        return self._update(ts, X, V, S, F)


    @property
    def times(self):
        """Get sequence of time steps."""
        out = pd.HDFStore(self.io.it_fname, 'r')
        out.open()
        traj = out['times']  # the indices of each of the trajectories
        out.close()
        return traj

    @property
    def positions(self):
        """Get position trajectory."""
        out = pd.HDFStore(self.io.Xt_fname, 'r')
        out.open()
        traj = out['positions']  # must match names given to columns in run()
        out.close()
        return traj

    @property
    def velocities(self):
        """Get velocity trajectory."""
        out = pd.HDFStore(self.io.Vt_fname, 'r')
        out.open()
        traj = out['velocities']  # must match names given to columns in run()
        out.close()
        return traj

    @property
    def aux_velocities(self):
        """Get auxiliary variable trajectory."""
        out = pd.HDFStore(self.io.St_fname, 'r')
        out.open()
        traj = out['aux_velocities']  # must match names given to columns in run()
        out.close()
        return traj

    @property
    def forces(self):
        """Get force trajectory."""
        out = pd.HDFStore(self.Ft_fname, 'r')
        out.open()
        traj = out['forces']  # must match names given to columns in run()
        out.close()
        return traj

    @property
    def drag_forces(self):
        """Get drag force trajectory."""
        out = pd.HDFStore(self.io.FDt_fname, 'r')
        out.open()
        traj = out['drag_forces']  # must match names given to columns in run()
        out.close()
        return traj

    @property
    def thermal_forces(self):
        """Get thermal force trajectory."""
        out = pd.HDFStore(self.io.FTt_fname, 'r')
        out.open()
        traj = out['thermal_forces']  # must match names given to columns in run()
        out.close()
        return traj


    def close_trj_files(self):
        self.io.close_trj_files()


    # def quick_plot_MSD(self, **kwargs):
    #     """Alias for GLEInputOutput.quick_plot_MSD() method."""
    #
    #     return self.io.quick_plot_MSD(**kwargs)
    #
    # def quick_plot_SD(self, **kwargs):
    #     """Alias for GLEInputOutput.quick_plot_SD() method."""
    #
    #     return self.io.quick_plot_SD(**kwargs)
    #
    # def quick_plot_2D(self, **kwargs):
    #     """Alias for GLEInputOutput.quick_plot_2D() method."""
    #
    #     return self.io.quick_plot_2D(**kwargs)
    #
    # def quick_plot_MSV(self, **kwargs):
    #     """Alias for GLEInputOutput.quick_plot_MSV() method."""
    #
    #     return self.io.quick_plot_MSV(**kwargs)
    #
    # def plot_potential(self, **kwargs):
    #     """Alias for GLEInputOutput.plot_potential() method."""
    #
    #     return self.io.plot_potential(**kwargs)
    #
    # def plot_force(self, **kwargs):
    #     """Alias for GLEInputOutput.plot_force() method."""
    #
    #     return self.io.plot_force(**kwargs)
