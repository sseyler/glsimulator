import numpy as np
import pandas as pd
import itertools as it
import matplotlib.pyplot as plt

import numpy.random as npr
import time

import force

class GLEIntegrator(object):

    # physical constants
    kB = 8.314E-3                  # Boltzmann constant (kJ/mol/K)
    NA = 6.022E23                  # Avogadro's number: # particles/mole
    isqrt3 = 1./3.0**0.5           # 1/sqrt(3)

    def __init__(self, N, D, T, m, gamma, tau0, tauk, c0, ck, k, dt):
        self.N = N                          # number of auxiliary particles
        self.D = D                          # number of spatial dimensions
        self.m = m                          # particle mass (kDa)
        self.T = T                          # temperature (K)
        self.gamma = gamma                  # collision frequency (1/ps)
        self.k = k                          # spring constant
        self.dt = dt                        # time step (ps)

        # Shortcut (combined) parameters
        self.idt   = 1./self.dt             # inverse of the timestep (1/ps)
        self.dtim  = self.dt/self.m         # timestep over mass (ps/kDa)
        self.dti2m = 0.5*self.dtim          # timestep over 2*m (ps/kDa)
        self.dtsqi2 = 0.5*self.dt**2        # Used in Vanden-Eijnden Ciccotti
        self.gamdt = self.gamma*self.dt     # unitless
        self.kT    = self.kB*T*1e-3              # Boltzmann constant * T (kJ/mol)
        self.kTim  = self.kT/self.m         # kT/m (kJ/mol/kDa)
        self.ND    = self.N*self.D          # num configuation space dimensions

        self.alpha = 0
        self.sigma = 0

        self.tau0   = tau0
        self.c0     = c0
        self.theta0 = 0
        self.alpha0 = 0
        self.sigma0 = 0

        self.tauk   = tauk
        self.ck     = ck
        self.thetak = np.zeros(N)
        self.alphak = np.zeros(N)
        self.sigmak = np.zeros(N)

        self.integrators = ['ELI', 'LI', 'GJF', 'VEC', None]
        self.method = None

        self.force = force.Force(self.k)
        self.noise = force.StochasticForce(self.N, self.D)


    def set_integrator_params(self, method):
        if method == "ELI" or method is None:
            self.theta0 = np.exp(-self.dt/self.tau0)
            self.alpha0 = np.sqrt((1. - self.theta0**2) / (2*self.tau0))
            self.sigma0 = self.alpha0 * np.sqrt(2.*self.c0*self.kT)

            self.mk     = self.ck*self.tauk  # mass of kth aux particle
            self.thetak = np.exp(-self.dt/self.tauk)
            self.alphak = np.sqrt((1. - self.thetak**2) / (2*self.tauk))
            self.sigmak = self.alphak * np.sqrt(2.*self.ck*self.kT)
            self.omtkck = (1. - self.thetak)*self.ck

            self.thetak = np.broadcast_to(self.thetak, (self.D, self.N)).T
            self.alphak = np.broadcast_to(self.alphak, (self.D, self.N)).T
            self.sigmak = np.broadcast_to(self.sigmak, (self.D, self.N)).T
            self.omtkck = np.broadcast_to(self.omtkck, (self.D, self.N)).T

            print 'tau0:  ', self.tau0
            print 'c0:    ', self.c0
            print 'theta0:', self.theta0
            print 'alpha0:', self.alpha0
            print 'sigma0:', self.sigma0
        elif method == "LI" or method is None:
            self.alpha = 1. - np.exp(-self.gamdt)
            self.sigma = (self.kTim*self.alpha*(2.-self.alpha))**0.5
        elif method == "GJF":
            self.alpha = 1./(1. + 0.5*self.gamdt)
            self.sigma = (2.*self.gamdt*self.kTim)**0.5
            self.alpdt = self.alpha*self.dt

        print("Settings parameters for {} integrator".format(method))


    def select(self, method):
        """Select integrator at runtime

            Dynamically bind methods ending with suffix "_step"; a generic
            "step" method is used by the main class (langevin.LangevinParticle)
            in the computational loop to advance trajectory.
        """
        try:
            self.set_integrator_params(method)
            return getattr(self, '{}_step'.format(method))
        except KeyError as key:
            print("Integrator \"{}\" not found. Valid selections: ".format(key))
            for m in self.integrators.keys(): print("  \"{}\"".format(m))

    def select_duo(self, method):
        try:
            self.set_integrator_params(method)
            return getattr(self, '{}_position'.format(method)), \
                    getattr(self, '{}_velocity'.format(method))
        except KeyError as key:
            print("Integrator {} not found. Valid selections: ".format(key))
            for m in self.integrators.keys(): print("  \"{}\"".format(m))


    ###############################################################
    # Extended system Langevin Impulse method
    ###############################################################
    def ELI_leapfrog_velocity(self, V0, F0, S0):
        """Deterministic velocity update (leap-frog)
            *V0* - velocity at current timestep [nm/ps]
            *F0* - force at current timestep [kJ/mol/nm]
        """
        return V0 + self.dtim*(F0 - S0)

    def ELI_impulse_velocity(self, Vd, xi0):
        """Velocity impulse for a given white Gaussian noise xi
            *Vd* - deterministic velocity update [nm/ps]
            *xi* - stochastic force [kJ/mol/nm]
        """
        return xi0 - self.sigma0*Vd

    def ELI_position(self, X0, Vd, Vi):
        """LI position update.
            *X0* - position at current timestep [nm]
            *Vd* - deterministic velocity update [nm/ps]
            *Vi* - impulsive velocity component [nm/ps]
        """
        return X0 + (Vd + 0.5*Vi)*self.dt

    def ELI_auxiliary(self, Sk0, Vi, xik):
        """LI position update.
            *X0* - position at current timestep [nm]
            *Vd* - deterministic velocity update [nm/ps]
            *Vi* - impulsive velocity component [nm/ps]
        """
        return self.thetak*Sk0 + self.omtkck*Vi.T + xik

    def ELI_velocity(self, Vd, Vi):
        """LI (full) velocity update.
            *Vd* - deterministic velocity update [nm/ps]
            *Vi* - impulsive velocity component [nm/ps]
            *F0* - force at current timestep [kJ/mol/nm]
        """
        return Vd + Vi

    def ELI_step(self, X0, V0, Sk0, F0):
        """Langevin Impulse advance of positions and velocities
            *X0* - position at current timestep [nm]
            *V0* - velocity at current timestep [nm/ps]
            *F0* - force at current timestep [kJ/mol/nm]
        """
        xik = self.noise.GWN_multi(self.sigmak)
        xi0 = np.average(xik, axis=0, weights=self.ck) # TODO: sum aux noise
        S0  = Sk0.sum(axis=0)                      # TODO: sum aux vars

        Vd  = self.ELI_leapfrog_velocity(V0, F0, S0)
        Vi  = self.ELI_impulse_velocity(Vd, xi0)
        Xn  = self.ELI_position(X0, Vd, Vi)
        Skn = self.ELI_auxiliary(Sk0, Vi, xik)  # TODO: should return a vector?
        Vn  = self.ELI_velocity(Vd, Vi)
        Fn  = self.force.harmonic(Xn)
        return Xn, Vn, Skn, Fn




    ###############################################################
    # Langevin Impulse method
    ###############################################################
    def LI_leapfrog_velocity(self, V0, F0):
        """Deterministic velocity update (leap-frog)
            *V0* - velocity at current timestep [nm/ps]
            *F0* - force at current timestep [kJ/mol/nm]
        """
        return V0 + self.dtim*F0

    def LI_impulse_velocity(self, Vd, xi):
        """Velocity impulse for a given white Gaussian noise xi
            *Vd* - deterministic velocity update [nm/ps]
            *xi* - stochastic force [kJ/mol/nm]
        """
        return xi - self.alpha*Vd

    def LI_position(self, X0, Vd, Vi):
        """LI position update.
            *X0* - position at current timestep [nm]
            *Vd* - deterministic velocity update [nm/ps]
            *Vi* - impulsive velocity component [nm/ps]
        """
        return X0 + (Vd + 0.5*Vi)*self.dt

    def LI_velocity(self, Vd, Vi):
        """LI (full) velocity update.
            *Vd* - deterministic velocity update [nm/ps]
            *Vi* - impulsive velocity component [nm/ps]
            *F0* - force at current timestep [kJ/mol/nm]
        """
        return Vd + Vi

    def LI_step(self, X0, V0, F0):
        """Langevin Impulse advance of positions and velocities
            *X0* - position at current timestep [nm]
            *V0* - velocity at current timestep [nm/ps]
            *F0* - force at current timestep [kJ/mol/nm]
        """
        xi = self.noise.GWN(self.sigma)
        Vd = self.LI_leapfrog_velocity(V0, F0)
        Vi = self.LI_impulse_velocity(Vd, xi)
        Xn = self.LI_position(X0, Vd, Vi)
        Vn = self.LI_velocity(Vd, Vi)
        Fn = self.force.harmonic(Xn)
        return Xn, Vn, Fn

    ###############################################################
    # Gronbech-Jensen-Farago integration
    ###############################################################
    def GJF_position(self, X0, V0, F0, xi):
        """GJF position update.
            *X0* - position at current timestep [nm]
            *V0* - velocity at current timestep [nm/ps]
            *F0* - force at current timestep [kJ/mol/nm]
            *xi* - stochastic force [kJ/mol/nm]
        """
        return X0 + self.alpdt*(V0 + self.dti2m*F0 + 0.5*xi)

    def GJF_velocity(self, X0, Xn, V0, F0, Fn, xi):
        """GJF velocity update.
            *X0* - position at current timestep [nm]
            *Xn* - position at next timestep [nm]
            *V0* - velocity at current timestep [nm/ps]
            *F0* - force at current timestep [kJ/mol/nm]
            *Fn* - force at next timestep [kJ/mol/nm]
            *xi* - stochastic force [kJ/mol/nm]
        """
        return V0 + self.dti2m*(F0 + Fn) - self.gamma*(Xn - X0) + xi

    def GJF_step(self, X0, V0, F0):
        """Gronbech-Jensen-Farago advance of positions and velocities
            *X0* - position at current timestep [nm]
            *V0* - velocity at current timestep [nm/ps]
            *F0* - force at current timestep [kJ/mol/nm]
        """
        xi = self.noise.GWN(self.sigma)
        Xn = self.GJF_position(X0, V0, F0, xi)
        Fn = self.force.harmonic(Xn)           # annoying b/c GJF must calc force itself...
        Vn = self.GJF_velocity(X0, Xn, V0, F0, Fn, xi)
        return Xn, Vn, Fn
