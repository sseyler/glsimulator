


# %%cython --cplus -c=-march=native -c=-Ofast -c=-ffast-math -c=-fopenmp --link-args=-fopenmp -I/home/sseyler/Library/miniconda2/include -lgsl -lgslcblas

#cython: cdivision=True
import numpy as np
cimport numpy as np
import cython
cimport cython
from cython.parallel cimport parallel
from cython.parallel import prange, threadid
cimport openmp

from cython_gsl cimport *
from libc.math cimport round
from libcpp.string cimport string

from c_gle_io import GLEInputOutput

import sys
sys.path.append('util')
from progressbar_simple import ProgressBar

# from time import time as pytime
from libc.time cimport time, time_t
from posix.time cimport clock_gettime, timespec, CLOCK_REALTIME

cdef int c_time():
    '''Cast type time_t to int'''
    return <int>time(NULL)

cpdef double c_time_f():
    cdef timespec ts
    clock_gettime(CLOCK_REALTIME, &ts)
    return (<double> ts.tv_sec) + (<double> ts.tv_nsec) * 1.0e-9

#############################################################################################

cdef class Force:
    cdef float evaluate(self, float X) nogil:
        return 0

cdef class VelocityAdvance:
    cdef float mass
    cdef float imass

    def __cinit__(self, float mass):
        self.mass = mass
        self.imass = 1/mass

    cdef float rhs(self, float F, float S) nogil:
        return 0

cdef class ImpulseVelocityAdvance:
    cdef float theta, sigma
    cdef float theta_minus_one

    def __cinit__(self, float theta, float sigma):
        self.theta = theta
        self.sigma = sigma
        self.theta_minus_one = -(1 - theta)

    cdef float rhs(self, float V, float W) nogil:
        return 0

cdef class PositionAdvance:
    cdef float rhs(self, float Vh, float Vi) nogil:
        return 0

cdef class AuxiliaryAdvance:
    cdef float gamma, theta, sigma
    cdef float one_m_theta_x_gamma

    def __cinit__(self, float gamma, float theta, float sigma):
        self.gamma = gamma
        self.theta = theta
        self.sigma = sigma
        self.one_m_theta_x_gamma = (1 - theta)*gamma

    cdef float rhs(self, float S, float Vh, float W) nogil:
        return 0

@cython.wraparound(False)
@cython.boundscheck(False)
cdef class LangevinIntegrator:
    cdef float dt

    cdef float advance(self, int n, float[:,::1] X, float[:,::1] V, \
                         float[:,::1] S, float[:,::1] F) nogil:
        return 0

cdef class LIVelocityAdvance(VelocityAdvance):
    cdef float rhs(self, float F, float S) nogil:
        return self.imass*F - S

cdef class LIImpulseVelocityAdvance(ImpulseVelocityAdvance):
    cdef float rhs(self, float V, float W) nogil:
        return self.theta_minus_one*V + self.sigma*W

cdef class LIPositionAdvance(PositionAdvance):
    cdef float rhs(self, float Vh, float Vi) nogil:
        return Vh + 0.5*Vi

cdef class LIAuxiliaryAdvance(AuxiliaryAdvance):
    cdef float rhs(self, float S, float Vh, float W) nogil:
        return self.theta*S - self.one_m_theta_x_gamma*Vh + self.sigma*W

cdef class LangevinImpulseIntegrator(LangevinIntegrator):
    cdef Force force
    cdef VelocityAdvance va
    cdef ImpulseVelocityAdvance iva
    cdef PositionAdvance pa
    cdef AuxiliaryAdvance sa
    cdef float mass, gamma, theta0, sigma0, theta1, sigma1
    cdef float dti2

    def __cinit__(self, Force force, float mass, float gamma, float theta0, \
                  float sigma0, float theta1, float sigma1, float dt):
        self.mass = mass
        self.gamma  = gamma
        self.theta0 = theta0
        self.sigma0 = sigma0
        self.theta1 = theta1
        self.sigma1 = sigma1

        self.va  = LIVelocityAdvance(mass)
        self.iva = LIImpulseVelocityAdvance(theta0, sigma0)
        self.pa  = LIPositionAdvance()
        self.sa  = LIAuxiliaryAdvance(gamma, theta1, sigma1)
        self.force = force

        self.dt   = dt
        self.dti2 = dt/2

    @cython.wraparound(False)
    @cython.boundscheck(False)
    cdef float advance(self, int n, float[:,::1] X, float[:,::1] V, \
                         float[:,::1] S, float[:,::1] F) nogil:
        cdef float Vh, Vi, W
        cdef int NDIM = X.shape[1]

        for dim in range(NDIM):
            W = gsl_ran_gaussian(r, 1)
            Vh = V[n,dim] + self.dti2*self.va.rhs(F[n,dim], S[n,dim])
            Vi = self.iva.rhs(V[n,dim], W)
            X[n+1,dim] = X[n,dim] + self.dt*self.pa.rhs(Vh, Vi)
            S[n+1,dim] = self.sa.rhs(S[n,dim], Vh, W)
            F[n+1,dim] = self.force.evaluate(X[n,dim])
            V[n+1,dim] = Vh + Vi + self.dti2*self.va.rhs(F[n+1,dim], S[n+1,dim])
        return W


cdef class ConstantForce(Force):
    cdef float C0

    def __cinit__(self, float C0):
        self.C0 = C0

    cdef float evaluate(self, float X) nogil:
        return self.C0

cdef class LinearForce(Force):
    cdef float C0, C1

    def __cinit__(self, float C0, float C1):
        self.C0 = C0
        self.C1 = C1

    cdef float evaluate(self, float X) nogil:
        return self.C0 + self.C1*X


@cython.wraparound(False)
@cython.boundscheck(False)
cpdef run_fast(float[:,:,::1] X, float[:,:,::1] V, float[:,:,::1] S, float[:,:,::1] F, \
               LangevinIntegrator integrator, inout,                          \
               int NREP, int NSTEPS, int NTDUMP, int NTOUT, int NDIM,  \
               int seed, int printdelay=100000, double print_interval=0.05, int workers=2, no_store=False):

    cdef int ts, n, dim, i, num_threads
    cdef double t_now = c_time_f()
    cdef double t_nextout = t_now
    cdef float dt = integrator.dt

    inout.open_traj_files()      # open trajectory HDF5 files for writing
    inout.init_io_storage(NSTEPS, NTOUT, NTDUMP) # init trajs for temp storage pos/vel
#     np.random.seed(seed)
    gsl_rng_set(r, seed)

#     print openmp.omp_get_thread_num()
#     print 'OMP_NUM_THREADS={}'.format(num_threads)
#     with ProgressBar(NSTEPS+1, print_interval) as pb:

    with nogil, parallel(num_threads=workers):
        openmp.omp_set_num_threads(workers)
        num_threads = openmp.omp_get_num_threads()
#         with gil:
#             print cython.parallel.threadid()

        for i in prange(NREP, schedule='dynamic'):
            for ts in range(NSTEPS+1):
                n = (ts % NTDUMP)
                if n == 0:
    #                 terminate = inout.dump_traj(ts, X, V, F, S, no_store=no_store)
                    if ts == NSTEPS:
                        break
                    for dim in range(NDIM):
                        X[i,0,dim] = X[i,-1,dim]
                        V[i,0,dim] = V[i,-1,dim]
                        F[i,0,dim] = F[i,-1,dim]
                        S[i,0,dim] = S[i,-1,dim]
    #             if (ts % printdelay) == 0:
    #                 t_now = c_time_f()
    #                 if t_now >= t_nextout:
    #                     pb.update(ts, t_now)
    #                     t_nextout += print_interval
                integrator.advance(n, X[i,:,:], V[i,:,:], S[i,:,:], F[i,:,:])

    inout.close_traj_files()
#     return pb
