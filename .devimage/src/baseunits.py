import pint
from pint import UnitRegistry


class CustomBaseUnitGroup(object):
    '''Define a group of base units for numerical simulations.

        CustomBaseUnitGroup is constructed from a (minimal) set of dimensional
        units represented as CustomBaseUnit objects. The group of base
        units can be used to form a nondimensional representation that can be
        directly inputted into a numerical algorithm written in
        nondimensionalized form. This allows a user to specify a basal unit
        set using whatever unit system (e.g., MKS, CGS, Imperial, etc.) is
        most convenient.
    '''

    _dimensional_IDs = ['L', 'T', 'M', 'A', 'Te']

    len_key  = ('length',      'L_ND',  'len_cbu')
    time_key = ('time',        'T_ND',  'time_cbu')
    mass_key = ('mass',        'M_ND',  'mass_cbu')
    amt_key  = ('amount',      'A_ND',  'amt_cbu')
    temp_key = ('temperature', 'Te_ND', 'temp_cbu')

    id2key_map = {'L'  : len_key,
                  'T'  : time_key,
                  'M'  : mass_key,
                  'A'  : amt_key,
                  'Te' : temp_key}

    def __init__(self, ureg, **kwargs):
        self._ur = ureg          # existing unit registry
        self._unit_map = kwargs.pop('unit_map', dict())

        self.QL  = kwargs.pop('L',  None)  # quantity in dimensions of length
        self.QT  = kwargs.pop('T',  None)  # quantity in dimensions of time
        self.QM  = kwargs.pop('M',  None)  # quantity in dimensions of mass
        self.QA  = kwargs.pop('A', ureg.N_A)  # dimensions of 'amount'/'substance'
        self.QTe = kwargs.pop('Te',  None)  # quantity in dimensions of mass
        self.QTe *= ureg.boltzmann_constant  # convert T to dimensions of kT (energy)

        self.register_units()


    def register_units(self):
        self._len_cbu  = CustomBaseUnit(self._ur, self.QL,  *self.len_key)
        self._time_cbu = CustomBaseUnit(self._ur, self.QT,  *self.time_key)
        self._mass_cbu = CustomBaseUnit(self._ur, self.QM,  *self.mass_key)
        self._amt_cbu  = CustomBaseUnit(self._ur, self.QA,  *self.amt_key)
        self._temp_cbu = CustomBaseUnit(self._ur, self.QTe, *self.temp_key)

        self._unit_map.update({self.id2key('L')  : self._len_cbu})
        self._unit_map.update({self.id2key('T')  : self._time_cbu})
        self._unit_map.update({self.id2key('M')  : self._mass_cbu})
        self._unit_map.update({self.id2key('A')  : self._amt_cbu})
        self._unit_map.update({self.id2key('Te') : self._temp_cbu})

    @property
    def dimensions(self):
        return self._dimensional_IDs

    def id2key(self, dim_id):
        '''Return the full key represented by *dim_id*.

          Note that the full key is a frozenset to provide protection for use
          as dictionary keys. The parameter *dim_id* can be one of the following
          1-/2-letter IDs corresponding to the following base dimensions:
            * L  -- length
            * T  -- time
            * M  -- mass
            * Te -- temperature
            * N  -- (amount of) substance
        '''
        return self.id2key_map[dim_id]

    @property
    def L(self):
        '''Alias for the length nondimensional unit.
        '''
        return self.__getitem__('L')

    @property
    def T(self):
        '''Alias for the time nondimensional unit.
        '''
        return self.__getitem__('T')

    @property
    def M(self):
        '''Alias for the mass nondimensional unit.
        '''
        return self.__getitem__('M')

    @property
    def A(self):
        '''Alias for the amount (substance) nondimensional unit.
        '''
        return self.__getitem__('A')

    @property
    def Te(self):
        '''Alias for the temperature*kB nondimensional unit.
        '''
        return self.__getitem__('Te')


    @property
    def ureg(self):
        '''Return the UnitRegistry being modified by this class.
        '''
        return self._ur

    @ureg.setter
    def ureg(self, value):
        if isinstance( value, UnitRegistry ):
            self._ur = value
        else:
            raise ValueError("Value must be of type pint.UnitRegistry.")

    def __repr__(self):
        return "<CustomBaseUnits:%s>" % self.__dict__

    def __len__(self):
        return len(self._unit_map)

    def __getitem__(self, dim_id):
        '''Return the CustomBaseUnit corresponding to the ID of the
        given dimensional unit (*dim_id*).

          See `CustomBaseUnitGroup.id2key()`
        '''
        return self._unit_map[self.id2key(dim_id)].unit

    def __setitem__(self, dim_id, custom_base_unit):
        self._unit_map[self.id2key(dim_id)] = custom_base_unit

    def __call__(self, dim_id):
        '''Effectively an alias for __getitem__
        '''
        return self.__getitem__(dim_id)

    def keys(self):
        return self._unit_map.keys()

    def values(self):
        return self._unit_map.values()

    def items(self):
        return self._unit_map.items()

    def pop(self, *args):
        return self._unit_map.pop(*args)

    def __contains__(self, item):
        return item in self._unit_map



class CustomBaseUnit(object):
    '''Define a nondimensional representation of a physical unit.
    '''

    def __init__(self, ureg, Q, dim, name, fullname=None):
        self._ur = ureg         # existing unit registry to which new base units will be added
        self._Qc = Q            # characteristic scale for quantity Q
        self._dim  = dim        # dimension of quantity Q
        self._name = name       # (short) name of units of quantity Q
        self._fname = fullname  # full name of units of quantity Q

        self._Qbu = self._create_base_unit()   # (new) base unit for quantity Q

    def __call__(self):
        '''CustomBaseUnit is callable.
        '''
        return self._ur

    def _define_new_unit(self):
        magnitude, unit_tuple = self._Qc.to_tuple()
        unit, power = unit_tuple[0]   # only get 1st item (only 1 element b/c it should be a base unit)
        if self._fname is not None:
            self._ur.define('{} = {} * {} = {}'.format(self._name, magnitude, unit, self._fname))
        else:
            self._ur.define('{} = {} * {}'.format(self._name, magnitude, unit))

    def _create_base_unit(self):
        '''Get the custom base unit.

            Same as w/ ur as callable: ur(self._name)
        '''
        self._define_new_unit()
        return self._ur.parse_expression(self._name)

    def convert_to(self, unit):
        '''Convert the CustomBaseUnit to unit.

            *unit* -- pint.unit.Unit
        '''
        if not self._Qbu:
            raise ValueError("Base unit for quantity is None and must be set first.")
        return self._Qbu.to(unit)

    @property
    def ureg(self):
        '''Return the UnitRegistry being modified by this class.
        '''
        return self._ur

    @ureg.setter
    def ureg(self, value):
        if isinstance( value, UnitRegistry ):
            self._ur = value
        else:
            raise ValueError("Value must be of type pint.UnitRegistry.")

    @property
    def dimension(self):
        return self._dim

    @dimension.setter
    def dimension(self, value):
        if isinstance( value, str ):
            self._dim = value
        else:
            raise ValueError("Dimension for base unit must be type string.")

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, value):
        if isinstance( value, str ):
            self._name = value
        else:
            raise ValueError("(Short) name for base unit must be type string.")

    @property
    def fullname(self):
        return self._fname

    @fullname.setter
    def fullname(self, value):
        if isinstance( value, str ):
            self._fname = value
        else:
            raise ValueError("Full name for base unit must be type string.")

    @property
    def unit(self):
        if not self._Qbu:
            raise ValueError("The unit for quantity is None and must be set first.")
        return self._Qbu

    @unit.setter
    def unit(self, value):
        if True:  # some test for val is needed here
            self._Qbu = value
        else:
            raise ValueError("Value needs to be ____.")
