# coding=utf-8

# import abc

import numpy as np
import pandas as pd
import itertools as it
import matplotlib.pyplot as plt

import numpy.random as npr
import time

import codetimer

PI = np.pi


cdef class Force:
    cpdef float[:,:,::1] apply(self, float[:,:,::1] X) except *:
        return 0

cdef class ConstantUniformForce(Force):

    cdef public float[:,:,::1] C0

    cpdef float[:,:,::1] apply(self, float[:,:,::1] X) except *:
        Nrep = X.shape[0]
        Naux = X.shape[1]
        D    = X.shape[2]
        cdef int i = 0
        cdef int j = 0
        cdef int k = 0
        for i in range(Nrep):
            for j in range(Nrep):
                for k in range(Nrep):
                    X[i,j,k] *= self.C0
        return X


class MathFunction(object):
    def __init__(self, name, operator):
        self.name = name
        self.operator = operator

    def __call__(self, *operands):
        return self.operator(*operands)


class Force(object):

    def __init__(self, name, style=None, substyles=None):
        self.name = name
        self.style = style
        self.substyles = []
        if substyles is not None:
            if isinstance(substyles, str):
                self.substyles.append(substyles)
            elif isinstance(substyles, list):
                self.substyles.extend(substyles)

    def apply(self, X):
        return 0

    def get_name(self):
        return self.name

    def get_style(self):
        return self.style

    def get_substyles(self):
        return self.substyles



class ExternalForce(Force):
    '''Create an external force.
    '''
    def __init__(self, name, style='external', substyles=None):
        Force.__init__(self, name, style=style, substyles=substyles)


class UniformForce(ExternalForce):
    '''Create an external force applied uniformly to all particles.
    '''
    def __init__(self, name):
        ExternalForce.__init__(self, name, substyles='uniform')


class ConstantUniformForce(UniformForce):
    '''Create a constant external force
    '''
    def __init__(self, name, C0=1.0):
        UniformForce.__init__(self, name)
        self.substyles.append('constant')
        self.C0 = C0

    def apply(self, X):
        return self.C0*np.ones(X.shape)


class LinearUniformForce(ConstantUniformForce):

    def __init__(self, name, C1=1.0, C0=0.0):
        ConstantUniformForce.__init__(self, name, C0=C0)
        self.substyles.append('linear')
        self.C1 = C1

    def apply(self, X):
        return self.C1*X + self.C0


class QuadraticUniformForce(LinearUniformForce):

    def __init__(self, name, C2=1.0, C1=0.0, C0=0.0):
        LinearUniformForce.__init__(self, name, C1=C1, C0=C0)
        self.substyles.append('quadratic')
        self.C2 = C2

    def apply(self, X):
        return self.C2*X**2 + self.C1*X + self.C0


class WashboardForce(ConstantUniformForce):

    def __init__(self, name, V0=1.0, L0=1.0, C0=1.0):
        # Set force parameters
        ConstantUniformForce.__init__(self, name, C0=C0)
        self.substyles.append('washboard')

        self.V0 = V0                # amplitude of sinusoidal part
        self.L0 = L0                # period of sinusoidal part

        # Set derived parameters
        self.k   = 2*PI/L0          # wavenumber
        self.V0k = V0*self.k        # amplitude of derivative

    def apply(self, X):
        return -self.V0k*np.sin(self.k*X) + self.C0

    def U(self, X):
        return -self.V0*np.cos(self.k*X) - self.C0*X





class PairwiseForce(Force):

    def __init__(self, name):
        Force.__init__(self, name, "pairwise")


class StochasticForce(Force):

    isqrt3 = 1./3.0**0.5           # 1/sqrt(3)

    def __init__(self, Nrep, Naux, D, seed=123456):
        self.Nrep = Nrep
        self.Naux = Naux
        self.D = D

        self.allnoise = None

        np.random.seed(int(seed))


    def prealloc(self, nsteps, Nrep, Naux, D):
        print('Preallocating Gaussian random samples for noise terms...')
        with codetimer.Timer(unit='secs') as t:
            self.allnoise = npr.randn(nsteps+1, Nrep, Naux, D)
        print('Generated samples for {} timesteps in {:.1f} s'.format(nsteps+1, t.secs))


    def GWN_prealloc(self, ts):
        """Return new noise terms for time step ts
            *ts* int -- time step
        """
        return self.allnoise[ts,:,:,:]
        # return self.allnoise.take()[ts,:,:,:]


    def GWN(self, sigma=1):
        return sigma*npr.randn(self.Nrep, self.Naux, self.D)

    def GWN_multi(self, sigma):
        return npr.normal(loc=0.0, scale=sigma)




class DumbForce(object):

    def __init__(self, k, L0=1.0, V0=1.0):
        """*k in kJ/mol/nm^2
        """
        self.k  = k   # (harmonic) force constant
        self.L0 = L0  # washboard period
        self.V0 = V0  # washboard pot oscilliatory amplitude

        self.twopiiL = 2.0*PI/L       # wavenumber of washboard potential
        self.W0      = V0*(2.0*PI/L)  # coefficient of oscilliatory component of washboard force


    def harmonic(self, X):
        """Harmonic force for extension x.
            *X* in nm
        """
        return -self.k*X

    def washboard(self, X):
        """Force derived for washboard potential.
            V(x) = -V0*cos(2*pi*x/x0) - k*x
            F(x) = -(V0*2*pi/x0)*sin(2*pi*x/x0) - k
        """
        return -self.W0*np.sin(self.twopiiL*X) + self.k

    def V_washboard(self, X):
        """Washboard potential.
            V(x) = -V0*cos(2*pi*x/x0) - k*x
            F(x) = -(V0*2*pi/x0)*sin(2*pi*x/x0) - k
        """
        return -self.V0*np.cos(self.twopiiL*X) - self.k*X



# @staticmethod
# def reseed(rseed=(123456, time.clock())):
#     try:
#         if type(rseed) is tuple and len(rseed) == 2:
#             for s1, s2 in rseed:
#                 if type(s1) is int and type(s2) is int:
#                     seed1, seed2 = s1, s2
#                 elif s1 is None:
#                     print('Got \'None\' for rseed. Using time.clock()' +\
#                           ' as RNG seed.\n')
#                     seed1, seed2 = time.clock(), 1
#         elif type(rseed) is int:
#             seed1, seed2 = rseed, 1
#     except:
#         print('Something went wrong with reseed, Using time.clock()' +\
#               ' as RNG seed.\n')
#         seed1, seed2 = time.clock(), 1
#
#     seed1, seed2 = time.clock(), 1
#     print('Initializing NumPy RNG: seed1={}, seed2={}'.format(seed1, seed2))
#     np.random.seed(int(seed1/seed2))
#     return seed1, seed2
