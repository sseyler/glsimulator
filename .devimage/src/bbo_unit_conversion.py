import numpy as np
import baseunits as bu
import pint
ur = pint.UnitRegistry()

PI = np.pi

def per_mol(Q, units=None):
    units = Q.units if not units else units
    return (Q * ur.avogadro_number).to(units/ur.mol)

def T_units(m, r, eta, rhof, unit=ur.s):
    return ((m / (3*PI*r**2*(eta*rhof)**0.5))**2).to(unit)

def X_units(x, unit=ur.m):
    return (x).to(unit)

def V_units(x, t, unit=ur.m/ur.s):
    return (x/t).to(unit)

def F_units(m, x, t, unit=ur.N):
    return (m*(x/t**2)).to(unit)

def E_units(m, x, t, unit=ur.J):
    return (m*(x/t)**2).to(unit)

def vol_sphere(r):
    return (4./3.)*PI*r**3

def mass_parti(r, rhop, unit=ur.kg):
    return rhop*vol_sphere(r)

def mass_fluid(r, rhof, unit=ur.kg):
    return rhof*vol_sphere(r)

def calc_mstar(r, rhop, rhof, unit=ur.kg):
    return (rhop + 0.5*rhof)*vol_sphere(r)

def calc_gamma(r, eta, rhop, rhof, unit=1/ur.s):
    return (( (3*PI*r**2*(eta*rhof)**0.5) / mass_parti(r, rhop) )**2).to(unit)


class StochasticBBOUnitConverter(object):

    kBol = 1.3806485e-23              # Boltzmann constant (J/K)
    NAvo = 6.022e23

    def __init__(self, ureg, **kwargs):

        self._ur = ureg  # UnitRegistry

        Te  = kwargs.get('temperature', 303.15 * ur.K)
        R   = kwargs.get('radius', 100.0 * ur.nm)
        rhp = kwargs.get('density_cargo', 0.9956502 * ur.g/ur.cm**3)
        rhf = kwargs.get('density_fluid', 0.9956502 * ur.g/ur.cm**3)
        eta = kwargs.get('dynamic_viscosity', 0.8007e-3 * ur.Pa*ur.s)
        dt  = kwargs.get('relative_timestep', 1.0e-4)
        Lw  = kwargs.get('washboard_period', 10.0 * ur.nm)

        m    = mass_parti(R, rhp)
        mstr = calc_mstar(R, rhp, rhf)
        gam0 = calc_gamma(R, eta, rhp, rhf)

        self.inputs = {}
        self.inputs.update({'temperature' : Te})
        self.inputs.update({'radius' : R})
        self.inputs.update({'density_cargo' : rhp})
        self.inputs.update({'density_fluid' : rhf})
        self.inputs.update({'dynamic_viscosity' : eta})
        self.inputs.update({'relative_timestep' : dt})
        self.inputs.update({'washboard_period' : Lw})
        self.inputs.update({'mass_particle' : m})
        self.inputs.update({'mass_effective' : mstr})
        self.inputs.update({'collision_frequency' : gam0})

        self.baseunits = {}
        self.baseunits.update({'L' : R})
        self.baseunits.update({'T' : 1./gam0})
        self.baseunits.update({'M' : m})
        self.baseunits.update({'Te' : Te})

        self._cbg = self.nondimensionalize()
        L  = self._cbg('L')
        T  = self._cbg('T')
        M  = self._cbg('M')
        Te = self._cbg('Te')
        A  = self._cbg('A')

        self.m = self.nondimize(m, M)
        self.gam0 = gam0.to(1/T)

        # Convenient shortcuts and definitions (combined parameters)
        self.im     = 1./m                  # inverse of mass
        self.idt    = 1./dt                 # inverse of the timestep
        self.dti2   = 0.5*dt                # timestep div by 2
        self.dti2m  = 0.5*dt/m              # timestep div by (2*m)
        self.dtsqi2 = 0.5*dt**2             # Used in Vanden-Eijnden Ciccotti
        self.gam0dt = gam0*dt               # unitless
        self.kTim   = kT/m                  # kT/m



        self.the0 = 0
        self.alp0 = 0
        self.sig0 = 0
        self.the1 = 0
        self.alp1 = 0
        self.sig1 = 0


    def nondimensionalize(self):
        return bu.CustomBaseUnitGroup(self._ur, **self.baseunits)


    @property
    def ur(self):
        return self._ur

    @property
    def cbg(self):
        return self._cbg

    def nondimize(self, Q, dim):
        return Q.to(dim)

    @property
    def V(self):
        '''Nondimensional velocity
        '''
        return self.L / self.T

    @property
    def E(self):
        '''Nondimensional energy
        '''
        return self.M * (self.L / self.T)**2

    @property
    def F(self):
        '''Nondimensional force
        '''
        return self.M * self.L / self.T**2

    @property
    def N(self):
        '''Nondimensional number density
        '''
        return 1. / self.L**3




# class BBOUnitConverter(object):
