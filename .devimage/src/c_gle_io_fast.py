import sys
import itertools as it
import numpy as np
import pandas as pd

import matplotlib.pyplot as plt


class GLEInputOutput(object):

    def __init__(self, dt, NREP, NAUX, NDIM, NSTEPS, NTOUT, NTDUMP, basename, **kwargs):

        self.traj_flags = (True, True, True)
        self.basename = basename
        self.filepath = None

        self.NREP = NREP                   # number of simulation replicas
        self.NAUX = NAUX                   # number of auxiliary variables
        self.NDIM  = NDIM                  # number of spatial dimensions
        self.dt = dt

        self.NSTEPS = None                 # number of simulation steps
        self.NTOUT  = None                 # number of steps between outputs
        self.NTDUMP = None                 # number of steps between traj dumps
        self.DUMPSIZE = None               # size of array to store trajectory dumps

        self.Xtraj  = None
        self.Vtraj  = None
        self.Ftraj  = None
        self.FDtraj = None
        self.FTtraj = None
        self.Straj  = None

        self.it_fname  = None
        self.Xt_fname  = None
        self.Vt_fname  = None
        self.Ft_fname  = None
        self.FDt_fname = None
        self.FTt_fname = None
        self.St_fname  = None

        self.it_hdf5  = None
        self.Xt_hdf5  = None
        self.Vt_hdf5  = None
        self.Ft_hdf5  = None
        self.FDt_hdf5 = None
        self.FTt_hdf5 = None
        self.St_hdf5  = None

        self.use_seaborn = kwargs.pop('use_seaborn', True)

    def setup(self, complevel=5, complib='bzip2'):
        """Set up HDF5 trajectory I/O and DataFrame column names."""
        self._gen_col_names(self.traj_flags)
        self._setup_traj_filenames(self.basename, complib)
        self._init_traj_files(complevel, complib)

    def init_io_storage(self, nsteps, ntout, ntdump):
        self.NSTEPS = nsteps
        self.NTOUT  = ntout
        self.NTDUMP = ntdump
        self.DUMPSIZE = int(ntdump/ntout)
        self._alloc_temp_storage()

    def _alloc_temp_storage(self):
        self.Xtraj  = np.zeros((self.DUMPSIZE, self.NREP, 1, self.NDIM))
        self.Vtraj  = np.zeros((self.DUMPSIZE, self.NREP, 1, self.NDIM))
        self.Ftraj  = np.zeros((self.DUMPSIZE, self.NREP, 1, self.NDIM))
        self.FDtraj = np.zeros((self.DUMPSIZE, self.NREP, 1, self.NDIM))
        self.FTtraj = np.zeros((self.DUMPSIZE, self.NREP, 1, self.NDIM))
        self.Straj  = np.zeros((self.DUMPSIZE, self.NREP, self.NAUX, self.NDIM))

    def dump_traj(self, ts, X, V, F, S, no_store=False):
        if ts == 0:
            start = 0
            stop  = 1
            step  = None
            offset = start
            indices = np.zeros(1)
            # Reshape temporary storage to 2D arrays to fit into DataFrames
            Xtraj_out  = np.zeros((1, self.NREP*self.NDIM))
            Vtraj_out  = np.zeros((1, self.NREP*self.NDIM))
            Straj_out  = np.zeros((1, self.NREP*self.NAUX*self.NDIM))
            Ftraj_out  = np.zeros((1, self.NREP*self.NDIM))
            FDtraj_out = np.zeros((1, self.NREP*self.NDIM))
            FTtraj_out = np.zeros((1, self.NREP*self.NDIM))
        else:
            start = self.NTOUT
            stop  = start + self.NTDUMP
            step  = self.NTOUT
            istart = start + ts - self.NTDUMP
            istop  = istart + self.NTDUMP
            indices = self.dt * np.arange(istart, istop, step)
            # Reshape temporary storage to 2D arrays to fit into DataFrames
            Xtraj_out  = self.Xtraj.reshape((self.DUMPSIZE, self.NREP*self.NDIM))
            Vtraj_out  = self.Vtraj.reshape((self.DUMPSIZE, self.NREP*self.NDIM))
            Straj_out  = self.Straj.reshape((self.DUMPSIZE, self.NREP*self.NAUX*self.NDIM))
            Ftraj_out  = self.Ftraj.reshape((self.DUMPSIZE, self.NREP*self.NDIM))
            FDtraj_out = self.FDtraj.reshape((self.DUMPSIZE, self.NREP*self.NDIM))
            FTtraj_out = self.FTtraj.reshape((self.DUMPSIZE, self.NREP*self.NDIM))

        # Store the selected traj steps in temporary arrays
        Xtraj_out[:][:]    = X[start:stop:step,:]
        Vtraj_out[:][:]    = V[start:stop:step,:]
        Ftraj_out[:][:]    = F[start:stop:step,:]
        Straj_out[:][:][:] = S[start:stop:step,:]

        # Store reshaped trajectory data in DataFrames
        df_pos = pd.DataFrame(Xtraj_out[:][:],  index=indices, columns=self.x_cols)
        df_vel = pd.DataFrame(Vtraj_out[:][:],  index=indices, columns=self.v_cols)
        df_for = pd.DataFrame(Ftraj_out[:][:],  index=indices, columns=self.f_cols)
        df_aux = pd.DataFrame(Straj_out[:][:],  index=indices, columns=self.s_cols)
        df_fdr = pd.DataFrame(FDtraj_out[:][:], index=indices, columns=self.fd_cols)
        df_fth = pd.DataFrame(FTtraj_out[:][:], index=indices, columns=self.ft_cols)

        # convert to hierarchical columns
        # df.columns = pd.MultiIndex.from_tuples([tuple(c.split('_')) for c in df.columns])

        if not no_store:
            group = 'test'#'chunk{}'.format(str(ts//self.NTDUMP))
            # Append the new DataFrames containing the dumped trajectory steps
            self.it_hdf5.append('times', pd.Series(indices[:]))
            df_pos.to_hdf(self.Xt_hdf5, group,format='table',mode='a',append=True,complevel=9,complib='blosc')
            df_vel.to_hdf(self.Vt_hdf5, group,format='table',mode='a',append=True,complevel=9,complib='blosc')
            df_for.to_hdf(self.Ft_hdf5, group,format='table',mode='a',append=True,complevel=9,complib='blosc')
            df_aux.to_hdf(self.FDt_hdf5,group,format='table',mode='a',append=True,complevel=9,complib='blosc')
            df_fdr.to_hdf(self.FTt_hdf5,group,format='table',mode='a',append=True,complevel=9,complib='blosc')
            df_fth.to_hdf(self.St_hdf5, group,format='table',mode='a',append=True,complevel=9,complib='blosc')

        if ts < self.NSTEPS:
            self._alloc_temp_storage()
            return False
        else:
            return True


    def step_print(self, n, nsteps):
        print 'Step {:9d} ({:6.2f}% complete)\r'.format(n, n*100./nsteps),
        # sys.stdout.write('\rStep {:9d} ({:6.2f}% complete)'.format(n, n*100./nsteps))
        # sys.stdout.flush()

    # def print_perf(self, n):


    def open_traj_files(self, mode='w'):
        pass
        self.it_hdf5.open(mode=mode)
        self.Xt_hdf5.open(mode=mode)
        self.Vt_hdf5.open(mode=mode)
        self.Ft_hdf5.open(mode=mode)
        self.FDt_hdf5.open(mode=mode)
        self.FTt_hdf5.open(mode=mode)
        self.St_hdf5.open(mode=mode)
        self.filepath = self.Xt_hdf5.filename.split('/')[:-1][0]


    def close_traj_files(self):
        try:
            print('Closing trajectory files. Data stored ' + \
                    'in:\n\t{}'.format(self.filepath))
            self.it_hdf5.close()
            self.Xt_hdf5.close()
            self.Vt_hdf5.close()
            self.Ft_hdf5.close()
            self.FDt_hdf5.close()
            self.FTt_hdf5.close()
            self.St_hdf5.close()
        except:
            print('Unable to close all trajectory files.')


    def _setup_traj_filenames(self, basename, complib='bzip2'):
        ext = '.{}'.format(complib) if complib is not None else ''

        self.it_fname  = '{}.idx.h5{}'.format(basename, ext)
        self.Xt_fname  = '{}.pos.h5{}'.format(basename, ext)
        self.Vt_fname  = '{}.vel.h5{}'.format(basename, ext)
        self.Ft_fname  = '{}.for.h5{}'.format(basename, ext)
        self.FDt_fname = '{}.fdr.h5{}'.format(basename, ext)
        self.FTt_fname = '{}.fth.h5{}'.format(basename, ext)
        self.St_fname  = '{}.aux.h5{}'.format(basename, ext)

    def print_traj_filenames(self):
        return self.it_fname, self.Xt_fname, self.Vt_fname, self.Ft_fname, self.FDt_fname, self.FTt_fname, self.St_fname

    def _init_traj_files(self, complevel=5, complib='bzip2'):
        # self.hdf_kwargs = {'complevel' : complevel, 'complib' : complib}
        self.it_hdf5  = pd.HDFStore(self.it_fname,  mode='w')#, **kwargs)
        self.Xt_hdf5  = pd.HDFStore(self.Xt_fname,  mode='w')#, **kwargs)
        self.Vt_hdf5  = pd.HDFStore(self.Vt_fname,  mode='w')#, **kwargs)
        self.Ft_hdf5  = pd.HDFStore(self.Ft_fname,  mode='w')#, **kwargs)
        self.FDt_hdf5 = pd.HDFStore(self.FDt_fname, mode='w')#, **kwargs)
        self.FTt_hdf5 = pd.HDFStore(self.FTt_fname, mode='w')#, **kwargs)
        self.St_hdf5  = pd.HDFStore(self.St_fname,  mode='w')#, **kwargs)

    def _compress_traj_files(self, complevel=5, complib='bzip2'):
        self.hdf_kwargs = {'complevel' : complevel, 'complib' : complib}
        self.it_hdf5  = pd.HDFStore(self.it_fname,  mode='w')#, **kwargs)
        self.Xt_hdf5  = pd.HDFStore(self.Xt_fname,  mode='w')#, **kwargs)
        self.Vt_hdf5  = pd.HDFStore(self.Vt_fname,  mode='w')#, **kwargs)
        self.Ft_hdf5  = pd.HDFStore(self.Ft_fname,  mode='w')#, **kwargs)
        self.FDt_hdf5 = pd.HDFStore(self.FDt_fname, mode='w')#, **kwargs)
        self.FTt_hdf5 = pd.HDFStore(self.FTt_fname, mode='w')#, **kwargs)
        self.St_hdf5  = pd.HDFStore(self.St_fname,  mode='w')#, **kwargs)

    def _gen_col_names(self, traj_flags, scalars=False):

        run_names = ['Run {}'.format(p+1) for p in xrange(self.NREP)]
        if self.NDIM == 1:
            pos_vars = ['x']
            vel_vars = ['vx']
            for_vars = ['fx']
            fdr_vars = ['fdx']
            fth_vars = ['ftx']
            aux_vars = ['sx']
        elif self.NDIM == 2:
            pos_vars = ['x','y']
            vel_vars = ['vx','vy']
            for_vars = ['fx','fy']
            fdr_vars = ['fdx','fdy']
            fth_vars = ['ftx','fty']
            aux_vars = ['sx','sy']
        elif self.NDIM == 3:
            pos_vars = ['x','y','z']
            vel_vars = ['vx','vy','vz']
            for_vars = ['fx','fy','fz']
            fdr_vars = ['fdx','fdy','fdz']
            fth_vars = ['ftx','fty','ftz']
            aux_vars = ['sx','sy','sz']

        x_iterables  = [run_names, pos_vars]
        v_iterables  = [run_names, vel_vars]
        f_iterables  = [run_names, for_vars]
        s_iterables  = [run_names, aux_vars]
        fd_iterables = [run_names, fdr_vars]
        ft_iterables = [run_names, fth_vars]

        x_index_names  = ['Run #', 'Coordinates']
        v_index_names  = ['Run #', 'Velocities']
        f_index_names  = ['Run #', 'Forces']
        s_index_names  = ['Run #', 'Auxiliary Velocities']
        fd_index_names = ['Run #', 'Stokes Drag Forces']
        ft_index_names = ['Run #', 'Thermal Forces']

        self.x_cols  = pd.MultiIndex.from_product(x_iterables, names=x_index_names)
        self.v_cols  = pd.MultiIndex.from_product(v_iterables, names=v_index_names)
        self.f_cols  = pd.MultiIndex.from_product(f_iterables, names=f_index_names)
        self.s_cols  = pd.MultiIndex.from_product(s_iterables, names=s_index_names)
        self.fd_cols = pd.MultiIndex.from_product(fd_iterables, names=fd_index_names)
        self.ft_cols = pd.MultiIndex.from_product(ft_iterables, names=ft_index_names)

        # if type(traj_flags) is tuple:
        #     try:
        #         xtraj, vtraj, straj = traj_flags
        #     except TypeError:
        #         print("Requested trajectory output not understood;  \
        #                defauling to positions and velocities only.")
        # else:
        #     xtraj, vtraj, straj = True, True, False
        #
        # pos_cols, vel_cols, aux_cols = [], [], []
        # if xtraj:
        #     self.pos_cols =                                                       \
        #         ['{} ({})'.format(var, num) for num, var in
        #         it.product([str(j+1) for j in xrange(self.NREP)], pos_vars)]
        # if vtraj:
        #     self.vel_cols =                                                       \
        #         ['{} ({})'.format(var, num) for num, var in
        #         it.product([str(j+1) for j in xrange(self.NREP)], vel_vars)]
        # if True: # TODO this is a hack
        #     self.for_cols =                                                       \
        #         ['{} ({})'.format(var, num) for num, var in
        #         it.product([str(j+1) for j in xrange(self.NREP)], for_vars)]
        # if True: # TODO this is a hack
        #     self.fdr_cols =                                                       \
        #         ['{} ({})'.format(var, num) for num, var in
        #         it.product([str(j+1) for j in xrange(self.NREP)], fdr_vars)]
        # if True: # TODO this is a hack
        #     self.fth_cols =                                                       \
        #         ['{} ({})'.format(var, num) for num, var in
        #         it.product([str(j+1) for j in xrange(self.NREP)], fth_vars)]
        # if straj:
        #     self.aux_cols =                                                       \
        #         [''.join(i[::-1]) for i in
        #             it.product([' {}'.format(j+1) for j in xrange(self.NREP)],
        #                        ['{}'.format(k+1)  for k in xrange(self.NAUX)],
        #                        aux_vars)
        #         ]
        # if scalars:
        #     sca_cols = ['position', 'force', 'potential']
        #
        # self.X_cols, self.V_cols, self.S_cols = pos_cols, vel_cols, aux_cols

    @property
    def times(self):
        """Get sequence of time steps."""
        out = pd.HDFStore(self.it_fname, 'r')
        out.open()
        traj = out['times']  # must match names given to columns in run()
        out.close()
        return traj

    @property
    def positions(self):
        """Get position trajectory."""
        out = pd.HDFStore(self.Xt_fname, 'r')
        out.open()
        traj = out['positions']  # must match names given to columns in run()
        out.close()
        return traj

    @property
    def velocities(self):
        """Get velocity trajectory."""
        out = pd.HDFStore(self.Vt_fname, 'r')
        out.open()
        traj = out['velocities']  # must match names given to columns in run()
        out.close()
        return traj

    @property
    def aux_velocities(self):
        """Get auxiliary variable trajectory."""
        out = pd.HDFStore(self.St_fname, 'r')
        out.open()
        traj = out['aux_velocities']  # must match name given to columns in run()
        out.close()
        return traj

    @property
    def forces(self):
        """Get force trajectory."""
        out = pd.HDFStore(self.Ft_fname, 'r')
        out.open()
        traj = out['forces']  # must match names given to columns in run()
        out.close()
        return traj

    @property
    def drag_forces(self):
        """Get drag force trajectory."""
        out = pd.HDFStore(self.FDt_fname, 'r')
        out.open()
        traj = out['drag_forces']  # must match names given to columns in run()
        out.close()
        return traj

    @property
    def thermal_forces(self):
        """Get thermal force trajectory."""
        out = pd.HDFStore(self.FTt_fname, 'r')
        out.open()
        traj = out['thermal_forces']  # must match names given to columns in run()
        out.close()
        return traj


    def setup_seaborn(self):
        import seaborn as sns
        sns.set(context="notebook", style="ticks",
                font_scale=1.5, rc={"lines.linewidth": 2.0})
