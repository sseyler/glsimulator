import sys
import itertools as it
import numpy as np
import pandas as pd

import matplotlib.pyplot as plt


class GLEInputOutput(object):

    def __init__(self, dt, Nrep, Naux, D, basename, **kwargs):

        self.trj_flags = (True, True, True)
        self.basename = basename
        self.filepath = None

        self.Nrep = Nrep                   # number of simulation replicas
        self.Naux = Naux                   # number of auxiliary variables
        self.D  = D                        # number of spatial dimensions
        self.dt = dt

        self.itraj  = None
        self.Xtraj  = None
        self.Vtraj  = None
        self.Ftraj  = None
        self.FDtraj = None
        self.FTtraj = None
        self.Straj  = None

        self.it_fname  = None
        self.Xt_fname  = None
        self.Vt_fname  = None
        self.Ft_fname  = None
        self.FDt_fname = None
        self.FTt_fname = None
        self.St_fname  = None

        self.it_hdf5  = None
        self.Xt_hdf5  = None
        self.Vt_hdf5  = None
        self.Ft_hdf5  = None
        self.FDt_hdf5 = None
        self.FTt_hdf5 = None
        self.St_hdf5  = None

        self.use_seaborn = kwargs.pop('use_seaborn', True)

    def setup(self, complevel=5, complib='bzip2'):
        """Set up HDF5 trajectory I/O and DataFrame column names."""
        self._gen_col_names(self.trj_flags)
        self._setup_trj_filenames(self.basename, complib)
        self._init_trj_files(complevel, complib)

    def init_tmp_trj(self, tmpsize):
        self.itraj  = np.zeros(tmpsize)
        self.Xtraj  = np.zeros((tmpsize, self.Nrep, 1, self.D))
        self.Vtraj  = np.zeros((tmpsize, self.Nrep, 1, self.D))
        self.Ftraj  = np.zeros((tmpsize, self.Nrep, 1, self.D))
        self.FDtraj = np.zeros((tmpsize, self.Nrep, 1, self.D))
        self.FTtraj = np.zeros((tmpsize, self.Nrep, 1, self.D))
        self.Straj  = np.zeros((tmpsize, self.Nrep, self.Naux, self.D))

    def dump_tmp_trj(self, tmpsize, ts, nsteps):
        self.Xtraj  = self.Xtraj.reshape((tmpsize, self.Nrep*self.D))
        self.Vtraj  = self.Vtraj.reshape((tmpsize, self.Nrep*self.D))
        self.Straj  = self.Straj.reshape((tmpsize, self.Nrep*self.Naux*self.D))
        self.Ftraj  = self.Ftraj.reshape((tmpsize, self.Nrep*self.D))
        self.FDtraj = self.FDtraj.reshape((tmpsize, self.Nrep*self.D))
        self.FTtraj = self.FTtraj.reshape((tmpsize, self.Nrep*self.D))

        run_names = ['Run {}'.format(p+1) for p in xrange(self.Nrep)]
        x_iterables  = [run_names, self.pos_vars] #, 'z']]
        v_iterables  = [run_names, self.vel_vars] #, 'vz']]
        f_iterables  = [run_names, self.for_vars] #, 'vz']]
        s_iterables  = [run_names, self.aux_vars] #, 'vz']]
        fd_iterables = [run_names, self.fdr_vars] #, 'vz']]
        ft_iterables = [run_names, self.fth_vars] #, 'vz']]

        x_index_names  = ['Run #', 'Coordinates']
        v_index_names  = ['Run #', 'Velocities']
        f_index_names  = ['Run #', 'Forces']
        s_index_names  = ['Run #', 'Auxiliary Velocities']
        fd_index_names = ['Run #', 'Stokes Drag Forces']
        ft_index_names = ['Run #', 'Thermal Forces']

        x_cols  = pd.MultiIndex.from_product(x_iterables, names=x_index_names)
        v_cols  = pd.MultiIndex.from_product(v_iterables, names=v_index_names)
        f_cols  = pd.MultiIndex.from_product(f_iterables, names=f_index_names)
        s_cols  = pd.MultiIndex.from_product(s_iterables, names=s_index_names)
        fd_cols = pd.MultiIndex.from_product(fd_iterables, names=fd_index_names)
        ft_cols = pd.MultiIndex.from_product(ft_iterables, names=ft_index_names)

        df_pos = pd.DataFrame(self.Xtraj[:][:], index=self.itraj[:], columns=x_cols)
        df_vel = pd.DataFrame(self.Vtraj[:][:], index=self.itraj[:], columns=v_cols)
        df_for = pd.DataFrame(self.Ftraj[:][:], index=self.itraj[:], columns=f_cols)
        df_aux = pd.DataFrame(self.Straj[:][:], index=self.itraj[:], columns=s_cols)
        df_fdr = pd.DataFrame(self.FDtraj[:][:], index=self.itraj[:], columns=fd_cols)
        df_fth = pd.DataFrame(self.FTtraj[:][:], index=self.itraj[:], columns=ft_cols)

        # convert to hierarchical columns
        # df.columns = pd.MultiIndex.from_tuples([tuple(c.split('_')) for c in df.columns])

        self.it_hdf5.append('times', pd.Series(self.itraj[:]))
        self.Xt_hdf5.append('positions', df_pos)
        self.Vt_hdf5.append('velocities', df_vel)
        self.Ft_hdf5.append('forces', df_for)
        self.FDt_hdf5.append('drag_forces', df_fdr)
        self.FTt_hdf5.append('thermal_forces', df_fth)
        self.St_hdf5.append('aux_velocities', df_aux)
        # self.St_hdf5.append('aux_velocities', pd.DataFrame(self.Straj[:][:],
        #                  columns=self.S_cols, index=self.itraj[:]))

        if ts < nsteps:
            self.init_tmp_trj(tmpsize)


    def step_print(self, n, nsteps):
        print 'Step {:9d} ({:6.2f}% complete)\r'.format(n, n*100./nsteps),
        # sys.stdout.write('\rStep {:9d} ({:6.2f}% complete)'.format(n, n*100./nsteps))
        # sys.stdout.flush()

    # def print_perf(self, n):


    def open_trj_files(self, mode='a'):
        self.it_hdf5.open(mode=mode)
        self.Xt_hdf5.open(mode=mode)
        self.Vt_hdf5.open(mode=mode)
        self.Ft_hdf5.open(mode=mode)
        self.FDt_hdf5.open(mode=mode)
        self.FTt_hdf5.open(mode=mode)
        self.St_hdf5.open(mode=mode)
        self.filepath = self.Xt_hdf5.filename.split('/')[:-1][0]


    def close_trj_files(self):
        try:
            print('Closing trajectory files. Data stored ' + \
                    'in:\n\t{}'.format(self.filepath))
            self.it_hdf5.close()
            self.Xt_hdf5.close()
            self.Vt_hdf5.close()
            self.Ft_hdf5.close()
            self.FDt_hdf5.close()
            self.FTt_hdf5.close()
            self.St_hdf5.close()
        except:
            print('Unable to close all trajectory files.')


    def _setup_trj_filenames(self, basename, complib='bz2'):
        ext = '.{}'.format(complib) if complib is not None else ''

        self.it_fname  = '{}.idx.h5{}'.format(basename, ext)
        self.Xt_fname  = '{}.pos.h5{}'.format(basename, ext)
        self.Vt_fname  = '{}.vel.h5{}'.format(basename, ext)
        self.Ft_fname  = '{}.for.h5{}'.format(basename, ext)
        self.FDt_fname = '{}.fdr.h5{}'.format(basename, ext)
        self.FTt_fname = '{}.fth.h5{}'.format(basename, ext)
        self.St_fname  = '{}.aux.h5{}'.format(basename, ext)

    def print_trj_filenames(self):
        return self.it_fname, self.Xt_fname, self.Vt_fname, self.Ft_fname, self.FDt_fname, self.FTt_fname, self.St_fname

    def _init_trj_files(self, complevel=5, complib='bzip2'):
        kwargs = {'complevel' : complevel, 'complib' : complib}
        self.it_hdf5  = pd.HDFStore(self.it_fname,  mode='w', **kwargs)
        self.Xt_hdf5  = pd.HDFStore(self.Xt_fname,  mode='w', **kwargs)
        self.Vt_hdf5  = pd.HDFStore(self.Vt_fname,  mode='w', **kwargs)
        self.Ft_hdf5  = pd.HDFStore(self.Ft_fname,  mode='w', **kwargs)
        self.FDt_hdf5 = pd.HDFStore(self.FDt_fname, mode='w', **kwargs)
        self.FTt_hdf5 = pd.HDFStore(self.FTt_fname, mode='w', **kwargs)
        self.St_hdf5  = pd.HDFStore(self.St_fname,  mode='w', **kwargs)

    def _gen_col_names(self, trj_flags, scalars=False):

        if type(trj_flags) is tuple:
            try:
                xtraj, vtraj, straj = trj_flags
            except TypeError:
                print("Requested trajectory output not understood;  \
                       defauling to positions and velocities only.")
        else:
            xtraj, vtraj, straj = True, True, False

        pos_cols, vel_cols, aux_cols = [], [], []
        if self.D == 1:
            self.pos_vars = ['x']
            self.vel_vars = ['vx']
            self.for_vars = ['fx']
            self.fdr_vars = ['fdx']
            self.fth_vars = ['ftx']
            self.aux_vars = ['sx']
        elif self.D == 2:
            self.pos_vars = ['x','y']
            self.vel_vars = ['vx','vy']
            self.for_vars = ['fx','fy']
            self.fdr_vars = ['fdx','fdy']
            self.fth_vars = ['ftx','fty']
            self.aux_vars = ['sx','sy']
        elif self.D == 3:
            self.pos_vars = ['x','y','z']
            self.vel_vars = ['vx','vy','vz']
            self.for_vars = ['fx','fy','fz']
            self.fdr_vars = ['fdx','fdy','fdz']
            self.fth_vars = ['ftx','fty','ftz']
            self.aux_vars = ['sx','sy','sz']

        if xtraj:
            pos_cols =                                                       \
                ['{} ({})'.format(var, num) for num, var in
                it.product([str(j+1) for j in xrange(self.Nrep)], self.pos_vars)]
        if vtraj:
            vel_cols =                                                       \
                ['{} ({})'.format(var, num) for num, var in
                it.product([str(j+1) for j in xrange(self.Nrep)], self.vel_vars)]
        if True: # TODO this is a hack
            for_cols =                                                       \
                ['{} ({})'.format(var, num) for num, var in
                it.product([str(j+1) for j in xrange(self.Nrep)], self.for_vars)]
        if True: # TODO this is a hack
            fdr_cols =                                                       \
                ['{} ({})'.format(var, num) for num, var in
                it.product([str(j+1) for j in xrange(self.Nrep)], self.fdr_vars)]
        if True: # TODO this is a hack
            fth_cols =                                                       \
                ['{} ({})'.format(var, num) for num, var in
                it.product([str(j+1) for j in xrange(self.Nrep)], self.fth_vars)]
        if straj:
            aux_cols =                                                       \
                [''.join(i[::-1]) for i in
                    it.product([' {}'.format(j+1) for j in xrange(self.Nrep)],
                               ['{}'.format(k+1)  for k in xrange(self.Naux)],
                               self.aux_vars)
                ]
        if scalars:
            sca_cols = ['position', 'force', 'potential']
        self.X_cols, self.V_cols, self.S_cols = pos_cols, vel_cols, aux_cols

    @property
    def times(self):
        """Get sequence of time steps."""
        out = pd.HDFStore(self.it_fname, 'r')
        out.open()
        traj = out['times']  # must match names given to columns in run()
        out.close()
        return traj

    @property
    def positions(self):
        """Get position trajectory."""
        out = pd.HDFStore(self.Xt_fname, 'r')
        out.open()
        traj = out['positions']  # must match names given to columns in run()
        out.close()
        return traj

    @property
    def velocities(self):
        """Get velocity trajectory."""
        out = pd.HDFStore(self.Vt_fname, 'r')
        out.open()
        traj = out['velocities']  # must match names given to columns in run()
        out.close()
        return traj

    @property
    def aux_velocities(self):
        """Get auxiliary variable trajectory."""
        out = pd.HDFStore(self.St_fname, 'r')
        out.open()
        traj = out['aux_velocities']  # must match name given to columns in run()
        out.close()
        return traj

    @property
    def forces(self):
        """Get force trajectory."""
        out = pd.HDFStore(self.Ft_fname, 'r')
        out.open()
        traj = out['forces']  # must match names given to columns in run()
        out.close()
        return traj

    @property
    def drag_forces(self):
        """Get drag force trajectory."""
        out = pd.HDFStore(self.FDt_fname, 'r')
        out.open()
        traj = out['drag_forces']  # must match names given to columns in run()
        out.close()
        return traj

    @property
    def thermal_forces(self):
        """Get thermal force trajectory."""
        out = pd.HDFStore(self.FTt_fname, 'r')
        out.open()
        traj = out['thermal_forces']  # must match names given to columns in run()
        out.close()
        return traj


    def setup_seaborn(self):
        import seaborn as sns
        sns.set(context="notebook", style="ticks",
                font_scale=1.5, rc={"lines.linewidth": 2.0})
