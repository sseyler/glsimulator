{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Assumed dynamical form\n",
    "\n",
    "The generalized Langevin dynamics (GLD) equation of motion is represented in the extended/auxiliary variable formalism by the following set of coupled, first-order SDEs:\n",
    "\n",
    "$$\n",
    "\\dot{x} = v(t)\n",
    "$$\n",
    "\n",
    "$$\n",
    "\\dot{v} = \\frac{1}{m}F(x(t)) - \\frac{1}{m}\\sum_{k=1}^N \\sigma_k(t) - \\frac{c_0}{m}\\:\\! v(t) + \\hat{\\xi}_0(t),\n",
    "$$\n",
    "\n",
    "$$\n",
    "\\dot{\\sigma}_k = -\\frac{1}{\\tau_k}\\sigma_k(t) - \\frac{c_k}{\\tau_k}v(t) + \\frac{1}{\\tau_k}\\hat{\\xi}_k(t)\n",
    "$$\n",
    "\n",
    "where $x$, $v$, and $m$ are, respectively, the main particle's position, velocity and mass, $c_0 = \\sum c_k$ with $\\gamma_0 = c_0/m = 1/\\tau_0$ being the collision frequency for the main particle, $\\gamma_k = c_k/m = 1/\\tau_k$ is the collision frequency for auxiliary particle $k$, and $\\hat{\\xi}_0$ and $\\hat{\\xi}_k$ are thermal noise terms for the main and $k^\\text{th}$ particle, respectively. There are $N$ auxiliary particles, each subject to an independent noise process, $\\xi_k$; $\\hat{\\xi}_0$ and $\\hat{\\xi}_k$ describe $N+1$ zero-mean, white Gaussian noise processes with autocorrelations given by\n",
    "\n",
    "$$\n",
    "\\left\\langle\\hat{\\xi}_0(t)\\hat{\\xi}_0(t')\\right\\rangle = \\sum_{k=1}^N \\frac{2k_BT c_k}{m}\\delta(t-t').\n",
    "$$\n",
    "\n",
    "$$\n",
    "\\left\\langle\\hat{\\xi}_k(t)\\hat{\\xi}_k(t')\\right\\rangle = \\frac{2k_BT c_k}{\\tau_k^2}\\delta(t-t').\n",
    "$$\n",
    "\n",
    "Since $\\xi_0$ is calculated from a weighted sum of the $\\xi_k$, the $N$ auxiliary noise processes, there are $N$ independent stochastic samples required per timestep."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Numerical discretization based on modified Langevin Impulse integration\n",
    "\n",
    "An LI-like numerical algorithm can be written in five steps<sup>[1](#footnote1)</sup>:\n",
    "\n",
    "**Step 1**: Update velocity by $\\Delta t/2$\n",
    "\n",
    "$\\qquad\n",
    "v^{n+1/2} = v^n + \\frac{\\Delta t}{2}\\left[\\frac{1}{m}F(x^n) - \\sum_{k=1}^N s_k^n\\right]\n",
    "$\n",
    "\n",
    "**Step 2**: Compute impulse velocity at $\\Delta t(n+1/2)$ (dissipative + noise terms)\n",
    "\n",
    "$\\qquad\n",
    "\\Delta v^{n+1/2} = -(1-\\theta_0)\\:\\! v^{n+1/2} + \\alpha_0\\sqrt{\\frac{k_BT}{m}} \\,\\mathcal{W}_0^n\n",
    "$\n",
    "\n",
    "**Step 3**: Update position by $\\Delta t$\n",
    "\n",
    "$\\qquad\n",
    "x^{n+1} = x^n + \\Delta t\\left(v^{n+1/2} + \\frac{1}{2}\\Delta v^{n+1/2}\\right)\n",
    "$\n",
    "\n",
    "**Step 4**: Update auxiliary variables (velocities) by $\\Delta t$\n",
    "\n",
    "$\\qquad\n",
    "s_k^{n+1} = \\theta_k s_k^n - (1-\\theta_k)\\:\\!\\gamma_k v^{n+1/2} + \\alpha_k\\sqrt{\\frac{k_BT}{m}\\gamma_k} \\,\\mathcal{W}_k^n\n",
    "$\n",
    "\n",
    "**Step 5**: Update velocity by $\\Delta t/2$\n",
    "\n",
    "$\\qquad\n",
    "v^{n+1} = \\left[v^{n+1/2} + \\Delta v^{n+1/2}\\right] + \\frac{\\Delta t}{2}\\left[\\frac{1}{m}F(x^{n+1}) - \\sum_{k=1}^N s_k^{n+1}\\right]\n",
    "$\n",
    "\n",
    "where\n",
    "\n",
    "$\\quad\n",
    "\\theta_0 = e^{-\\gamma_0 \\Delta t} \\qquad\\text{and}\\qquad \\alpha_0 = \\sqrt{\\frac{(1-\\theta_0^2)}{2}}\n",
    "$\n",
    "\n",
    "$\\quad\n",
    "\\theta_k = e^{-\\nu_k \\Delta t}  \\qquad\\text{and}\\qquad \\alpha_k = \\sqrt{\\frac{(1-\\theta_k^2)\\:\\!\\nu_k}{2}} \\quad\\text{or}\\quad \\alpha_k = \\sqrt{\\frac{(1-\\theta_k)^2}{\\Delta t}}\n",
    "$"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 2",
   "language": "python",
   "name": "python2"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.14"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
