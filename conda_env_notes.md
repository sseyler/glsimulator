## `glsimulator` basic packages

From base environment, I ran

```sh
conda install blosc cycler hdf5 gsl ipyparallel lz4-c matplotlib pandas seaborn numexpr cython pytables
```


## `randomgen`

Next, I need `randomgen` (also `cythongsl`), which has been update to v1.17 as of late (which also doesn't officially support Python 2.7). Nonetheless, I can try the latest version first using PyPi:

```sh
pip install randomgen cythongsl
```

This gave me `randomgen-1.16.6` and the latest (dev) version can be obtained from github:

```sh
pip install git+https://github.com/bashtage/randomgen.git
```


To gen randomgen working from way long ago, here's the instructions from the [issue I raised on github](https://github.com/bashtage/randomgen/issues/20):

```sh
git clone https://github.com/bashtage/randomgen
cd randomgen
python setup.py develop
cd randomgen/examples/cython
python setup.py build_ext --inplace
```

I can combine these older instructions with the newer ones to get things working (in case they don't using default package management approaches)


## `xarray`

To get `xarray` going, followed [instructions in the `xarray` docs](http://xarray.pydata.org/en/stable/installing.html):

Installing with recommended dependencies:

```sh
conda install xarray dask netCDF4 bottleneck
```

* `cyordereddict`: (optional) speeds up most internal operations with xarray data structures
* `h5netcdf`: (optional) an alternative library for reading and writing netCDF4 files that does not use the netCDF-C libraries


## Necessary optional niceties: `DotMap`, `pint`


```sh
pip install dotmap pint
```




---

## Getting new virtual environment to work with Hydrogen:

```sh
source activate thisenv
python -m ipykernel install --user --name thisenv
```

You can run "Hydrogen: Update Kernels" or restart Atom for the changes to take effect.



---

## Installation output from each command:


### 1. `glsimulation` basic packages

```sh
[sseyler:~/Repositories ... on/glsimulator/src] [glsim2] bbo(+260/-220)* ± conda install blosc cycler hdf5 gsl ipyparallel lz4-c matplotlib pandas seaborn numexpr
Collecting package metadata (current_repodata.json): done
Solving environment: done

\#\# Package Plan \#\#

  environment location: /home/sseyler/Library/miniconda2/envs/glsim2

  added / updated specs:
    - blosc
    - cycler
    - gsl
    - hdf5
    - ipyparallel
    - lz4-c
    - matplotlib
    - numexpr
    - pandas
    - seaborn


The following packages will be downloaded:

    package                    |            build
    ---------------------------|-----------------
    blosc-1.16.3               |       hd408876_0          71 KB
    cycler-0.10.0              |           py27_0          13 KB
    gsl-2.4                    |       h14c3975_4         1.8 MB
    hdf5-1.10.4                |       hb1b8bf9_0         3.9 MB
    ipyparallel-6.2.4          |           py27_0         306 KB
    kiwisolver-1.1.0           |   py27he6710b0_0          84 KB
    matplotlib-2.2.3           |   py27hb69df0a_0         4.8 MB
    numexpr-2.6.9              |   py27h9e4a6bb_0         183 KB
    pandas-0.24.2              |   py27he6710b0_0         8.4 MB
    patsy-0.5.1                |           py27_0         274 KB
    pyparsing-2.4.0            |             py_0          58 KB
    pytz-2019.1                |             py_0         236 KB
    seaborn-0.9.0              |           py27_0         360 KB
    snappy-1.1.7               |       hbae5bb6_3          35 KB
    statsmodels-0.10.1         |   py27hdd07704_0         9.6 MB
    subprocess32-3.5.4         |   py27h7b6447c_0          51 KB
    ------------------------------------------------------------
                                           Total:        30.1 MB

The following NEW packages will be INSTALLED:

  blosc              pkgs/main/linux-64::blosc-1.16.3-hd408876_0
  cycler             pkgs/main/linux-64::cycler-0.10.0-py27_0
  gsl                pkgs/main/linux-64::gsl-2.4-h14c3975_4
  hdf5               pkgs/main/linux-64::hdf5-1.10.4-hb1b8bf9_0
  ipyparallel        pkgs/main/linux-64::ipyparallel-6.2.4-py27_0
  kiwisolver         pkgs/main/linux-64::kiwisolver-1.1.0-py27he6710b0_0
  matplotlib         pkgs/main/linux-64::matplotlib-2.2.3-py27hb69df0a_0
  numexpr            pkgs/main/linux-64::numexpr-2.6.9-py27h9e4a6bb_0
  pandas             pkgs/main/linux-64::pandas-0.24.2-py27he6710b0_0
  patsy              pkgs/main/linux-64::patsy-0.5.1-py27_0
  pyparsing          pkgs/main/noarch::pyparsing-2.4.0-py_0
  pytz               pkgs/main/noarch::pytz-2019.1-py_0
  seaborn            pkgs/main/linux-64::seaborn-0.9.0-py27_0
  snappy             pkgs/main/linux-64::snappy-1.1.7-hbae5bb6_3
  statsmodels        pkgs/main/linux-64::statsmodels-0.10.1-py27hdd07704_0
  subprocess32       pkgs/main/linux-64::subprocess32-3.5.4-py27h7b6447c_0


Proceed ([y]/n)? y


Downloading and Extracting Packages
matplotlib-2.2.3     | 4.8 MB    | ######################################################### | 100%
patsy-0.5.1          | 274 KB    | ######################################################### | 100%
ipyparallel-6.2.4    | 306 KB    | ######################################################### | 100%
seaborn-0.9.0        | 360 KB    | ######################################################### | 100%
snappy-1.1.7         | 35 KB     | ######################################################### | 100%
gsl-2.4              | 1.8 MB    | ######################################################### | 100%
numexpr-2.6.9        | 183 KB    | ######################################################### | 100%
cycler-0.10.0        | 13 KB     | ######################################################### | 100%
subprocess32-3.5.4   | 51 KB     | ######################################################### | 100%
kiwisolver-1.1.0     | 84 KB     | ######################################################### | 100%
hdf5-1.10.4          | 3.9 MB    | ######################################################### | 100%
pyparsing-2.4.0      | 58 KB     | ######################################################### | 100%
blosc-1.16.3         | 71 KB     | ######################################################### | 100%
pandas-0.24.2        | 8.4 MB    | ######################################################### | 100%
pytz-2019.1          | 236 KB    | ######################################################### | 100%
statsmodels-0.10.1   | 9.6 MB    | ######################################################### | 100%
Preparing transaction: done
Verifying transaction: done
Executing transaction: done
```


**Note**: I forgot to install cython in the first batch above, so here it is separately!

```sh
[sseyler:~/Repositories ... on/glsimulator/src] [glsim2] bbo(+260/-220)* ± conda install cython
Collecting package metadata (current_repodata.json): done
Solving environment: done

\#\# Package Plan \#\#

  environment location: /home/sseyler/Library/miniconda2/envs/glsim2

  added / updated specs:
    - cython


The following packages will be downloaded:

    package                    |            build
    ---------------------------|-----------------
    cython-0.29.12             |   py27he6710b0_0         2.2 MB
    ------------------------------------------------------------
                                           Total:         2.2 MB

The following NEW packages will be INSTALLED:

  cython             pkgs/main/linux-64::cython-0.29.12-py27he6710b0_0


Proceed ([y]/n)? y


Downloading and Extracting Packages
cython-0.29.12       | 2.2 MB    | ######################################################### | 100%
Preparing transaction: done
Verifying transaction: done
Executing transaction: done
```


### 2. `randomgen`

```sh
[sseyler:~/Repositories ... on/glsimulator/src] [glsim2] bbo(+260/-220)* ± pip install randomgen
DEPRECATION: Python 2.7 will reach the end of its life on January 1st, 2020. Please upgrade your Python as Python 2.7 won't be maintained after that date. A future version of pip will drop support for Python 2.7.
Collecting randomgen
  Using cached https://files.pythonhosted.org/packages/f1/06/c06c618d6bf6491422c1a4bf9f943fa3840ee4353dffb85bc8071375e9de/randomgen-1.16.6-cp27-cp27mu-manylinux1_x86_64.whl
Requirement already satisfied: numpy>=1.13 in /home/sseyler/Library/miniconda2/envs/glsim2/lib/python2.7/site-packages (from randomgen) (1.16.4)
Requirement already satisfied: setuptools in /home/sseyler/Library/miniconda2/envs/glsim2/lib/python2.7/site-packages (from randomgen) (41.0.1)
Requirement already satisfied: cython>=0.26 in /home/sseyler/Library/miniconda2/envs/glsim2/lib/python2.7/site-packages (from randomgen) (0.29.12)
Requirement already satisfied: wheel in /home/sseyler/Library/miniconda2/envs/glsim2/lib/python2.7/site-packages (from randomgen) (0.33.4)
Installing collected packages: randomgen
Successfully installed randomgen-1.16.6
```


### 3. `xarray`

```sh
[sseyler:~/Repositories ... on/glsimulator/src] [xglsim] bbo(+261/-221)* ± conda install xarray
Collecting package metadata (current_repodata.json): done
Solving environment: done

## Package Plan ##

  environment location: /home/sseyler/Library/miniconda2/envs/xglsim

  added / updated specs:
    - xarray


The following packages will be downloaded:

    package                    |            build
    ---------------------------|-----------------
    xarray-0.11.3              |           py27_0         770 KB
    ------------------------------------------------------------
                                           Total:         770 KB

The following NEW packages will be INSTALLED:

  xarray             pkgs/main/linux-64::xarray-0.11.3-py27_0


Proceed ([y]/n)? y


Downloading and Extracting Packages
xarray-0.11.3        | 770 KB    | ######################################################### | 100%
Preparing transaction: done
Verifying transaction: done
Executing transaction: done
```


### 4. `dask`

```sh
[sseyler:~/Repositories ... on/glsimulator/src] [xglsim] bbo(+261/-221)* ± conda install dask
Collecting package metadata (current_repodata.json): done
Solving environment: done

\#\# Package Plan \#\#

  environment location: /home/sseyler/Library/miniconda2/envs/xglsim

  added / updated specs:
    - dask


The following packages will be downloaded:

    package                    |            build
    ---------------------------|-----------------
    bokeh-1.3.0                |           py27_0         4.0 MB
    click-7.0                  |           py27_0         120 KB
    cloudpickle-1.2.1          |             py_0          28 KB
    cytoolz-0.10.0             |   py27h7b6447c_0         422 KB
    distributed-1.28.1         |           py27_0         813 KB
    heapdict-1.0.0             |           py27_2           8 KB
    libtiff-4.0.10             |       h2733197_2         435 KB
    locket-0.2.0               |           py27_1           9 KB
    msgpack-python-0.6.1       |   py27hfd86e86_1          85 KB
    olefile-0.46               |           py27_0          49 KB
    packaging-19.0             |           py27_0          38 KB
    partd-1.0.0                |             py_0          19 KB
    psutil-5.6.3               |   py27h7b6447c_0         311 KB
    pyyaml-5.1.1               |   py27h7b6447c_0         169 KB
    sortedcontainers-2.1.0     |           py27_0          44 KB
    tblib-1.4.0                |             py_0          14 KB
    toolz-0.10.0               |             py_0          50 KB
    zict-1.0.0                 |             py_0          12 KB
    ------------------------------------------------------------
                                           Total:         6.6 MB

The following NEW packages will be INSTALLED:

  bokeh              pkgs/main/linux-64::bokeh-1.3.0-py27_0
  click              pkgs/main/linux-64::click-7.0-py27_0
  cloudpickle        pkgs/main/noarch::cloudpickle-1.2.1-py_0
  cytoolz            pkgs/main/linux-64::cytoolz-0.10.0-py27h7b6447c_0
  dask               pkgs/main/linux-64::dask-1.1.4-py27_1
  dask-core          pkgs/main/linux-64::dask-core-1.1.4-py27_1
  distributed        pkgs/main/linux-64::distributed-1.28.1-py27_0
  heapdict           pkgs/main/linux-64::heapdict-1.0.0-py27_2
  libtiff            pkgs/main/linux-64::libtiff-4.0.10-h2733197_2
  locket             pkgs/main/linux-64::locket-0.2.0-py27_1
  msgpack-python     pkgs/main/linux-64::msgpack-python-0.6.1-py27hfd86e86_1
  olefile            pkgs/main/linux-64::olefile-0.46-py27_0
  packaging          pkgs/main/linux-64::packaging-19.0-py27_0
  partd              pkgs/main/noarch::partd-1.0.0-py_0
  pillow             pkgs/main/linux-64::pillow-6.1.0-py27h34e0f95_0
  psutil             pkgs/main/linux-64::psutil-5.6.3-py27h7b6447c_0
  pyyaml             pkgs/main/linux-64::pyyaml-5.1.1-py27h7b6447c_0
  sortedcontainers   pkgs/main/linux-64::sortedcontainers-2.1.0-py27_0
  tblib              pkgs/main/noarch::tblib-1.4.0-py_0
  toolz              pkgs/main/noarch::toolz-0.10.0-py_0
  zict               pkgs/main/noarch::zict-1.0.0-py_0


Proceed ([y]/n)? y


Downloading and Extracting Packages
toolz-0.10.0         | 50 KB     | ######################################################### | 100%
distributed-1.28.1   | 813 KB    | ######################################################### | 100%
psutil-5.6.3         | 311 KB    | ######################################################### | 100%
pyyaml-5.1.1         | 169 KB    | ######################################################### | 100%
sortedcontainers-2.1 | 44 KB     | ######################################################### | 100%
heapdict-1.0.0       | 8 KB      | ######################################################### | 100%
olefile-0.46         | 49 KB     | ######################################################### | 100%
msgpack-python-0.6.1 | 85 KB     | ######################################################### | 100%
bokeh-1.3.0          | 4.0 MB    | ######################################################### | 100%
zict-1.0.0           | 12 KB     | ######################################################### | 100%
click-7.0            | 120 KB    | ######################################################### | 100%
partd-1.0.0          | 19 KB     | ######################################################### | 100%
tblib-1.4.0          | 14 KB     | ######################################################### | 100%
packaging-19.0       | 38 KB     | ######################################################### | 100%
locket-0.2.0         | 9 KB      | ######################################################### | 100%
libtiff-4.0.10       | 435 KB    | ######################################################### | 100%
cytoolz-0.10.0       | 422 KB    | ######################################################### | 100%
cloudpickle-1.2.1    | 28 KB     | ######################################################### | 100%
Preparing transaction: done
Verifying transaction: done
Executing transaction: done
```



### 5. `netCDF4`

```sh
[sseyler:~/Repositories ... on/glsimulator/src] [xglsim] bbo(+261/-221)* 130 ± conda install netCDF4
Collecting package metadata (current_repodata.json): done
Solving environment: done

\#\# Package Plan \#\#

  environment location: /home/sseyler/Library/miniconda2/envs/xglsim

  added / updated specs:
    - netcdf4


The following packages will be downloaded:

    package                    |            build
    ---------------------------|-----------------
    cftime-1.0.3.4             |   py27hdd07704_1         278 KB
    curl-7.65.2                |       hbc83047_0         141 KB
    hdf4-4.2.13                |       h3ca952b_2         714 KB
    libcurl-7.65.2             |       h20c2e04_0         588 KB
    libnetcdf-4.6.1            |       h11d0813_2         833 KB
    libssh2-1.8.2              |       h1ba5d50_0         226 KB
    netcdf4-1.4.2              |   py27h808af73_0         453 KB
    ------------------------------------------------------------
                                           Total:         3.2 MB

The following NEW packages will be INSTALLED:

  cftime             pkgs/main/linux-64::cftime-1.0.3.4-py27hdd07704_1
  curl               pkgs/main/linux-64::curl-7.65.2-hbc83047_0
  hdf4               pkgs/main/linux-64::hdf4-4.2.13-h3ca952b_2
  krb5               pkgs/main/linux-64::krb5-1.16.1-h173b8e3_7
  libcurl            pkgs/main/linux-64::libcurl-7.65.2-h20c2e04_0
  libnetcdf          pkgs/main/linux-64::libnetcdf-4.6.1-h11d0813_2
  libssh2            pkgs/main/linux-64::libssh2-1.8.2-h1ba5d50_0
  netcdf4            pkgs/main/linux-64::netcdf4-1.4.2-py27h808af73_0


Proceed ([y]/n)? y


Downloading and Extracting Packages
curl-7.65.2          | 141 KB    | ######################################################### | 100%
libssh2-1.8.2        | 226 KB    | ######################################################### | 100%
netcdf4-1.4.2        | 453 KB    | ######################################################### | 100%
hdf4-4.2.13          | 714 KB    | ######################################################### | 100%
cftime-1.0.3.4       | 278 KB    | ######################################################### | 100%
libnetcdf-4.6.1      | 833 KB    | ######################################################### | 100%
libcurl-7.65.2       | 588 KB    | ######################################################### | 100%
Preparing transaction: done
Verifying transaction: done
Executing transaction: done
```


### 6. `bottleneck`

```sh
[sseyler:~/Repositories ... on/glsimulator/src] [xglsim] bbo(+261/-221)* ± conda install bottleneck
Collecting package metadata (current_repodata.json): done
Solving environment: done

\#\# Package Plan \#\#

  environment location: /home/sseyler/Library/miniconda2/envs/xglsim

  added / updated specs:
    - bottleneck


The following packages will be downloaded:

    package                    |            build
    ---------------------------|-----------------
    bottleneck-1.2.1           |   py27h035aef0_1         121 KB
    ------------------------------------------------------------
                                           Total:         121 KB

The following NEW packages will be INSTALLED:

  bottleneck         pkgs/main/linux-64::bottleneck-1.2.1-py27h035aef0_1


Proceed ([y]/n)? y


Downloading and Extracting Packages
bottleneck-1.2.1     | 121 KB    | ######################################################### | 100%
Preparing transaction: done
Verifying transaction: done
Executing transaction: done

```


7. Optional: `py.test` and `mock` for testing, `airspeed-velocity` for performance regression testing of python packages over their lifetime




### Necessary optional niceties

```sh
[sseyler:~/Repositories … on/glsimulator/src] [glsim2] bbo(+261/-221)* ± pip install dotmap
DEPRECATION: Python 2.7 will reach the end of its life on January 1st, 2020. Please upgrade your Python as Python 2.7 won't be maintained after that date. A future version of pip will drop support for Python 2.7.
Collecting dotmap
  Downloading https://files.pythonhosted.org/packages/a0/f0/0fbb822ce82a9c50681531def5289f517628029307eafc781549dfd3b24e/dotmap-1.3.8.tar.gz
Building wheels for collected packages: dotmap
  Building wheel for dotmap (setup.py) ... done
  Stored in directory: /home/sseyler/.cache/pip/wheels/bc/87/e6/ee9a8713eee623d97db2440e7643ec8bc3f91bd0d67d857044
Successfully built dotmap
Installing collected packages: dotmap
Successfully installed dotmap-1.3.8
```


```sh
[sseyler:~/Repositories … on/glsimulator/src] [glsim2] bbo(+261/-221)* ± pip install pint
DEPRECATION: Python 2.7 will reach the end of its life on January 1st, 2020. Please upgrade your Python as Python 2.7 won't be maintained after that date. A future version of pip will drop support for Python 2.7.
Collecting pint
  Downloading https://files.pythonhosted.org/packages/15/9d/bf177ebbc57d25e9e296addc14a1303d1e34d7964af5df428a8332349c42/Pint-0.9-py2.py3-none-any.whl (138kB)
     |████████████████████████████████| 143kB 3.0MB/s
Collecting funcsigs; python_version == "2.7" (from pint)
  Using cached https://files.pythonhosted.org/packages/69/cb/f5be453359271714c01b9bd06126eaf2e368f1fddfff30818754b5ac2328/funcsigs-1.0.2-py2.py3-none-any.whl
Installing collected packages: funcsigs, pint
Successfully installed funcsigs-1.0.2 pint-0.9
```
