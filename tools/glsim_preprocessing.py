
from __future__ import division
import numpy as np
import pandas as pd
import itertools as it

from tools.physical_quantities import *
from tools.physical_quantities import PI, kBol, Navo
from tools.physical_quantities import mass_e, mass_p, gamma_s, gamma_0, gamma_b, nu_b

from tools.simulation_metadata import store_parameter_metadata, store_simulation_metadata

from glsimulator.glsim      import Simulation
from glsimulator.sim_state  import State
from glsimulator.system     import System
from glsimulator.integrator import GLEulerIntegratorLE, GLEulerIntegratorFBBO
from glsimulator.integrator import (GLImpulseIntegratorLE, GLImpulseIntegratorFBBO,
                                      GLImpulseIntegratorStableFBBO)
from glsimulator.force import (ConstantForce, LinearForce, LinearlyGrowingForce,
                                 PeriodicSquareWaveForce, TranslatingSpringForce,
                                 PeriodicTranslatingSpringForce)

###############################################################################
# Class to enable direct access of variables with dot operator
###############################################################################
class Bunch(object):
    def __init__(self, d={}):
        self.__dict__.update(d)

    def get_dict(self):
        return self.__dict__

    def __getitem__(self, key):
        return self.__dict__[key]

    def __setitem__(self, key, value):
        self.__dict__[key] = value

    def __repr__(self):
        return 'Bunch(' + self.__dict__.__repr__() + ')'

def round_to_int(number):
    return int(round(number))

#- Seeding ---------------
SEED = 23061989

def select_keyset(i):
    if   i == 1:
        KEYS = np.array([0, 14702096011762217365, 14636641319537817119,
                         3351689648718967237], dtype=np.uint64)
    elif i == 2:
        KEYS = np.array([0, 14559322926498757700, 3632739549066318548,
                         10755600028138268439], dtype=np.uint64)
    elif i == 3:
        KEYS = np.array([0, 5533886795991833713, 4758535347328213817,
                         11818842473267243275], dtype=np.uint64)
    elif i == 4:
        KEYS = np.array([0, 2462089494503843304, 12584081913381477329,
                         834753877517719638], dtype=np.uint64)
    elif i == 5:
        KEYS = np.array([0, 2289790832165283079, 3697694132300942476,
                         15101728960709935653], dtype=np.uint64)
    return list(KEYS.astype(str))

###############################################################################
# Class to enable easier simulation post-processing
###############################################################################
class SimulationCapsule(object):
    trj_col_map = {'pos': 'X',
                   'vel': 'V',
                   'fex': 'FE',
                   'ths': 'NS',
                   'thb': 'NB',
                   'aux': 'S'}

    def __init__(self, sim_data, forcing, directories):
        self.sim_data = sim_data
        self.forcing = forcing
        self.dirs = directories
        self.trj = Bunch()
        self.rng = Bunch()

        self.dynamics = sim_data.dynamics
        self.integrator = sim_data.integrator.name
        self.dt = sim_data.integrator.dt
        self.id = sim_data.id

        self.TSIM = sim_data.TSIM
        self.dtout = sim_data.dtout
        self.rng.lib = sim_data.rng.lib
        self.rng.name = sim_data.rng.name

        self.NAUX = sim_data.integrator.NAUX
        self.NPAR = sim_data.dims.NPAR
        self.NDIM = sim_data.dims.NDIM
        self.NRUN = sim_data.dims.NRUN
        self.NSETS = sim_data.dims.meta.NSETS
        self.NTASKS = sim_data.dims.meta.NTASKS
        self.NWORKERS = sim_data.dims.meta.NWORKERS

        self.Q = sim_data.Q
        self.Qnd = Bunch()
        self.io = Bunch()
        #------------------------------------
        params_force = '{}'.format(forcing.style)
        if self.forcing.style != 'free':
            self.DURA = forcing.Qnd.DURA  # pulse duration
            params_force += '/{}'.format(forcing.strength)
            params_force += '/{}'.format(forcing.shape)
            params_force += '/DURA={}'.format(self.DURA)
            if any(s in forcing.style for s in ['pulse_delay','freq_doubling']):
                self.NCYC = forcing.Qnd.NCYC  # num pulses
                self.PMUL = forcing.Qnd.PMUL  # period = PMUL*DURA
                self.FMUL = forcing.Qnd.FMUL  # frequency multiplier
                params_force += 'NCYC={}'.format(self.NCYC)
                params_force += '_PMUL={}'.format(self.PMUL)
                params_force += '_FMUL={:01e}'.format(self.FMUL)
        #------------------------------------
        params_sim = 'dt={}'.format(self.fmt_time(self.dt))
        if any(s in self.dynamics for s in ['LE', 'Langevin', 'Stokes']):
            self.Qnd.nu0 = 0
            self.NAUX = 1
            self.dynamics = 'Langevin' if self.dynamics == 'LE' else self.dynamics
            self.trj_fname_base = '_'.join([self.integrator, self.rng.name])
        elif any(s in self.dynamics for s in ['FBBO', 'BBO']):
            self.nu0 = sim_data.integrator.nu0
            self.trj_fname_base = '_'.join([self.integrator, self.rng.name])
            params_sim += '_nu0={:.1e}_NAUX={}'.format(self.nu0, self.NAUX)
        #------------------------------------
        self.dirs.trj = '/'.join([self.dirs.trjbase, params_force, self.dynamics,
                                  params_sim, self.id])
        self.dirs.analysis = '/'.join([directories.analysis, self.dirs.trj])
        self.dirs.figs = '/'.join([directories.figs, self.dirs.trj])
        self.trj_fpath_base = '/'.join([self.dirs.trj, self.trj_fname_base])

        if any(s == self.dynamics for s in ['BBO', 'Stokes']):
            self.Q.Te = 0.0
            self.equilibrate = False
        else:
            self.equilibrate = True

        self.compute_derived_quantities()
        self.set_forcing_params()
        self.non_dimensionalize()
        self.set_run_params()
        self.store_metadata()

    def compute_derived_quantities(self, force_effective_mass=False):
        q = self.Q
        #------------------------------------
        Te  = q.Te
        mu  = q.mu
        rhf = q.rhf
        rhp = q.rhp
        R   = q.R
        #------------------------------------
        kTe = kBol*Te
        knu = mu/rhf
        Me  = mass_e(R, rhf, rhp)
        Mp  = mass_p(R, rhp)
        if any(s in self.dynamics for s in ['FBBO', 'BBO']) or force_effective_mass:
            dyn_flag = 1
            M = Me
            if force_effective_mass:
                print('NOTE: this simulation is set up to use the effective' + \
                      ' mass instead of the particle mass.')
        elif any(s in self.dynamics for s in ['LE', 'Langevin', 'Stokes']):
            dyn_flag = 0
            M = Mp
        #------------------------------------
        self.Q.kTe = kTe
        self.Q.knu = knu
        self.Q.Me  = Me
        self.Q.Mp  = Mp
        self.Q.M   = M
        self.set_characteristic_scales()
        #------------------------------------
        self.Q.gams = gamma_s(R, knu, rhf, rhp, M)
        self.Q.gam0 = gamma_0(R, knu, rhf, rhp, M, self.NAUX, self.nu0, self.T_c)
        self.Q.gamb = dyn_flag * gamma_b(R, knu, rhf, rhp, M, self.NAUX, self.nu0, self.T_c)
        self.Q.nub  = dyn_flag * nu_b(self.NAUX, self.nu0, self.T_c)
        self.tsim = self.TSIM*self.T_c

    def set_characteristic_scales(self):
        q = self.Q
        qnd = self.Qnd
        #------------------------------------
        qnd.RH   = q.rhp/q.rhf
        qnd.BETA = (9.0/2)/(qnd.RH+0.5)
        #------------------------------------
        self.T_c = q.rhf*q.R**2/(qnd.BETA*q.mu)  # tau_s(R,knu,rhf,rhp,M)
        self.V_c = (kBol*303.15/q.Me)**0.5
        self.F_c = 6*PI*(q.R)*(q.mu)*(self.V_c)
        #------------------------------------
        self.L_c = self.V_c*self.T_c
        self.E_c = self.F_c*self.L_c
        self.M_c = self.E_c/self.V_c**2
        #------------------------------------
        self.Qnd = qnd

    def set_forcing_params(self):
        q = self.forcing.Q
        qnd = self.forcing.Qnd
        #------------------------------------
        q.F = 0
        qnd.DURA = 0
        q.duration = 0
        q.period = 0
        q.J = 0
        qnd.F_steps = 0
        qnd.period_steps = 0
        qnd.NCYC_TOTAL = 0
        if self.forcing.style != 'free':
            if self.forcing.strength == 'soft':
                q.F = 5.0e-12
            elif self.forcing.strength == 'hard':
                q.F = 5.0e-10
            qnd.DURA = 1e1
            q.duration = qnd.DURA * self.T_c
            q.period = qnd.PMUL * q.duration
            q.J = q.F * q.duration
            qnd.F_steps = round_to_int(q.duration / self.dt)
            qnd.period_steps = round_to_int(q.period / self.dt)
            if any(s in self.forcing.style for s in ['pulse_delay','freq_doubling']):
                qnd.NCYC_TOTAL = qnd.NCYC*qnd.FMUL # FMUL pulses per 1 NCYC
                qnd.F_steps *= 1./qnd.FMUL
                qnd.period_steps *= 1./qnd.FMUL
            else:
                qnd.NCYC_TOTAL = 0
        #------------------------------------
        self.forcing.Q = q
        self.forcing.Qnd = qnd

    def non_dimensionalize(self):
        q = self.Q
        qnd = self.Qnd
        force = self.forcing
        #------------------------------------
        qnd.TSIM   = self.tsim   / self.T_c
        qnd.DT     = self.dt     / self.T_c
        qnd.DTOUT  = self.dtout  / self.T_c
        qnd.MASSp  = q.Mp   / self.M_c
        qnd.MASSe  = q.Me   / self.M_c
        qnd.MASS   = q.M    / self.M_c
        qnd.RADIUS = q.R    / self.L_c
        qnd.GAMMAs = q.gams * self.T_c
        qnd.GAMMA0 = q.gam0 * self.T_c
        qnd.GAMMAb = q.gamb * self.T_c
        qnd.NUb    = q.nub  * self.T_c
        qnd.kT     = q.kTe  / self.E_c
        force.Qnd.F = force.Q.F / self.F_c
        #------------------------------------
        self.Qnd = qnd

    def set_run_params(self):
        qnd = self.Qnd
        io = self.io
        #------------------------------------
        io.NTOUT   = round_to_int(qnd.DTOUT / qnd.DT)  # output a frame every NTOUT steps
        io.NCHUNK  = 10
        NSTEP_MULT = io.NCHUNK*io.NTOUT
        NSTEP = int(np.around(qnd.TSIM / qnd.DT, decimals=-3))
        io.NSTEP = NSTEP + NSTEP_MULT - (NSTEP % NSTEP_MULT)
        io.NTDMP    = round_to_int(io.NSTEP/io.NCHUNK)
        io.NSTEP_EQ = round_to_int(io.NSTEP / 1)
        io.NTOUT_EQ = io.NTOUT
        #------------------------------------
        if (io.NTDMP % io.NTOUT) != 0:
            print('WARNING: NTDMP is not a divisor of NTOUT')
            sys.exit(-1)
        print 'TSIM:', qnd.TSIM
        print 'NTOUT:', io.NTOUT
        print 'NSTEP:', io.NSTEP
        print 'NCHUNK:', io.NCHUNK
        print 'NTDMP:', io.NTDMP
        print 'dump size:', io.NTDMP//io.NTOUT
        print 'NSTEP_EQ:', io.NSTEP_EQ
        print 'NTOUT_EQ:', io.NTOUT_EQ
        #------------------------------------
        self.Qnd = qnd
        self.io = io

    def fmt_time(self, t):
        if t < 1e-12:
            return '{}fs'.format(round_to_int(t*1e15))
        elif t < 1e-9:
            return '{}ps'.format(round_to_int(t*1e12))
        elif t < 1e-6:
            return '{}ns'.format(round_to_int(t*1e9))
        elif t < 1e-3:
            return u'{}μs'.format(round_to_int(t*1e6))
        elif t < 1e0:
            return '{}ms'.format(round_to_int(t*1e3))
        else:
            return '{}s'.format(round_to_int(t))

    def store_metadata(self):
        store_parameter_metadata(self.Q.R, self.Q.knu, self.Q.rhf, self.Q.rhp, self.Q.Te,
                                 self.dt, self.L_c, self.T_c, self.E_c, self.M_c,
                                 self.V_c, self.F_c, self.NAUX, self.dynamics,
                                 self.integrator, self.rng.name, self.dirs.trj,
                                 verbose=False)
        store_simulation_metadata(self.NRUN, self.NAUX, self.NPAR, self.NDIM,
                                  self.io.NSTEP, self.io.NTOUT, self.io.NTDMP,
                                  self.dynamics, self.integrator, self.rng.name, SEED,
                                  self.dirs.trj, verbose=False)

def run_wrapper(i, sim_capsule, keyset=1, seed=SEED):

    sc = sim_capsule
    Q = sc.Qnd
    FQ = sc.forcing.Qnd
    ##### Initialize seed & I/O names ####
    keys = np.asarray(select_keyset(keyset), dtype=np.uint64)
    keys[0] = task_id
    fname = '.'.join(sc.trj_fname_base, i+1)
    ##### Instantiate and initialize System ####
    sys = System(sc.NDIM, sc.NAUX, sc.Qnd.kT)
    sys.add_N_particles(sc.NPAR, Q.MASS, Q.RADIUS, Q.GAMMAs,
                        Q.GAMMA0, Q.GAMMAb, Q.NUb)
    if sc.forcing.style == 'free':
        pass
    elif sc.forcing.style == 'impulse':
        sys.add_force( ConstantForce(F0=FQ.F, max_steps=FQ.period_steps) )
    elif any(s in sc.forcing.style for s in ['pulse_delay','freq_doubling']):
        sys.add_force(
            PeriodicSquareWaveForce(F0=FQ.F, period=FQ.F_steps, duration=FQ.period_steps,
                                    n_cycles=FQ.NCYC_TOTAL))
    else:
        print("Error: a proper forcing protocol hasn't been specified.")
        sys.exit()
    ############################################
    if sc.integrator == 'LI':
        if sc.dynamics == 'LE':
            ing = GLImpulseIntegratorLE(sys, sc.rng.lib, sc.rng.name, sc.dt,
                                          seed=seed, keys=keys)
        elif sc.dynamics == 'FBBO':
            ing = GLImpulseIntegratorFBBO(sys, sc.rng.lib, sc.rng.name, sc.dt,
                                            seed=seed, keys=keys)
    elif sc.integrator == 'EM':
        if sc.dynamics == 'LE':
            ing = GLEulerIntegratorLE(sys, sc.rng.lib, sc.rng.name, sc.dt,
                                        seed=seed, keys=keys)
        elif sc.dynamics == 'FBBO':
            ing = GLEulerIntegratorFBBO(sys, sc.rng.lib, sc.rng.name, sc.dt,
                                          seed=seed, keys=keys)
    # SIMULATION ###############################
    sim = Simulation(sys, ing, fname, sc.dirs.trj)
    # Equilibration ----------------------
    if sc.equilibrate:
        sim.initialize(equilibration=True, continuation=False, kT=Q.kT,
                       mass=mass, init_aux=False, seed=seed)
        sim.run(sc.io.NSTEP_EQ, sc.io.NTOUT_EQ, equilibration=True,
                NTDUMP=sc.io.NTDMP_EQ, print_step_delay=25000, print_interval=0.05)
        cont = True
    else:
        cont = False
    # Production -------------------------
    sim.initialize(equilibration=False, continuation=cont, init_aux=False, seed=seed)
    sim.run(sc.io.NSTEP, sc.io.NTOUT, NTDUMP=sc.io.NTDMP, print_step_delay=25000,
            print_interval=0.05)
    return i
#################################################################################
