import pandas as pd


###############################################################################
# Quick change max number of displayed rows/columns for pandas DataFrames
def set_pd_max_rows(rows):
    pd.set_option('display.max_rows', rows)


def set_pd_max_cols(columns):
    pd.set_option('display.max_columns', columns)
