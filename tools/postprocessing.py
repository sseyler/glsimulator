
# from __future__ import division
import numpy as np
import pandas as pd
import itertools as it

from tools.misc import Bunch, round_to_int
from .physical_quantities import *
from .glsim_preprocessing import round_to_int


###############################################################################
# Class to enable easier simulation post-processing
###############################################################################
class SimContainer(object):
    trj_col_map = {'pos': 'X',
                   'vel': 'V',
                   'fex': 'FE',
                   'ths': 'NS',
                   'thb': 'NB',
                   'aux': 'S'}

    def __init__(self, sim_data, forcing, directories):
        self.sim_data = sim_data
        self.forcing = forcing
        self.dirs = directories
        #------------------------------------
        self.inputs = sim_data.inputs
        self.dims = sim_data.dims
        #------------------------------------
        self.dynamics = self.inputs.dynamics
        self.method = self.inputs.method
        self.sid = '{:04d}'.format(int(self.inputs.sid)) # min 4-char numerical string
        self.dt = self.inputs.dt
        self.TSIM = self.inputs.TSIM
        self.dtout = self.inputs.dtout
        #------------------------------------
        self.NAUX = self.inputs.NAUX
        self.NPAR = self.dims.NPAR
        self.NDIM = self.dims.NDIM
        self.NRUN = self.dims.meta.NRUN
        self.NSETS = self.dims.meta.NSETS
        self.NTASKS = self.dims.meta.NTASKS
        self.NWORKERS = self.dims.meta.NWORKERS
        #=======================================================================
        self.Q = Bunch()
        self.Qnd = Bunch()
        self.rng = Bunch()
        self.io = Bunch()
        #------------------------------------
        self.Q.extend(R=self.inputs.R, Te=self.inputs.Te, mu=self.inputs.mu,
                      rhf=self.inputs.rhf, rhp=self.inputs.rhp)
        self.rng.extend(lib=sim_data.rng.lib, name=sim_data.rng.name)
        #------------------------------------
        params_force = '{}'.format(forcing.style)
        if self.forcing.style != 'free':
            self.DURA = forcing.Qnd.DURA  # pulse duration
            params_force += '/{}'.format(forcing.strength)
            params_force += '/{}'.format(forcing.shape)
            params_force += '/DURA={}'.format(self.DURA)
            if any(s in forcing.style for s in ['pulse_delay','freq_doubling']):
                self.NCYC = forcing.Qnd.NCYC  # num pulses
                self.PMUL = forcing.Qnd.PMUL  # period = PMUL*DURA
                self.FMUL = forcing.Qnd.FMUL  # frequency multiplier
                params_force += 'NCYC={}'.format(self.NCYC)
                params_force += '_PMUL={}'.format(self.PMUL)
                params_force += '_FMUL={:01e}'.format(self.FMUL)
        #------------------------------------
        params_sim = 'dt={}'.format(self.fmt_time(self.dt))
        if self.is_langevin_dynamics(self.dynamics):
            self.NU0 = 0
            self.NAUX = 1
            self.dynamics = 'Langevin' if self.dynamics == 'LE' else self.dynamics
            self.trj_fname_base = '_'.join([self.method, self.rng.name])
        elif self.is_bbo_dynamics(self.dynamics):
            self.NU0 = self.inputs.NU0
            self.trj_fname_base = '_'.join([self.method, self.rng.name])
            params_sim += '_nu0={:.1e}_NAUX={}'.format(self.NU0, self.NAUX)
        #------------------------------------
        self.dirs.trj = '/'.join([self.dirs.trjbase, params_force, self.dynamics,
                                  params_sim, self.sid])
        self.dirs.analysis = '/'.join([directories.analysis, self.dirs.trj])
        self.dirs.figs = '/'.join([directories.figs, self.dirs.trj])
        self.trj_fpath_base = '/'.join([self.dirs.trj, self.trj_fname_base])
        #=======================================================================

        self.load_metadata()


    def load_full_trajectories(self, var_list, ext='h5', reindex=True, format='wide',
                               store=True):
        for var in var_list:
            trj_df_list = []
            for i in xrange(self.NTASKS):
                trj_name = '.'.join([self.trj_fpath_base, i+1, var, ext])
                df_trj = pd.read_hdf(trj_name, format='table')
                trj_df_list.append(df_trj)
            #--------------------------------
            df = self.combine_trajectories(trj_df_list)
            if reindex:
                df = self.reindex_parallel_trj(df, self.NRUNS, self.NAUX, self.NDIMS, var)
            if format == 'wide':
                df = self.wide_to_long(df, col_name=trj_col_map[var])
            self.trj[var] = df
        #------------------------------------
        if not store:
            df_combined = self.trj[var_list[0]].copy()
            self.trj[var_list[0]] = None
            for var in var_list[1:]:
                df_combined[var] = self.trj[var].copy()
                self.trj[var] = None
            return df_combined


    def load_metadata(self):
        '''Alias for load_physical_metadata() and load_simulation_metadata().
        '''
        self.load_physical_metadata()
        self.load_simulation_metadata()
        self.tsim = self.NSTEPS*self.dt

    def load_physical_metadata(self):
        physical_params = '{}/{}.par.h5'.format(self.dirs.trj, self.trj_fname_base)
        self.metadata.phys = pd.read_hdf(physical_params, 'par')
        self.Lc = self.extract_param(df, 'Lc')
        self.Tc = self.extract_param(df, 'Tc')
        self.Ec = self.extract_param(df, 'Ec')
        self.Mc = self.extract_param(df, 'Mc')
        self.Vc = self.extract_param(df, 'Vc')
        self.Fc = self.extract_param(df, 'Fc')
        # mass_e = self.extract_param(df, 'mass_e')
        # mass_p = self.extract_param(df, 'mass_p')
        # mass = self.extract_param(df, 'mass')
        # radius = self.extract_param(df, 'radius')
        # kBT = self.extract_param(df, 'kBT')
        # tau_s = self.extract_param(df, 'tau_s')
        # tau_h = self.extract_param(df, 'tau_h')
        # tau_0 = self.extract_param(df, 'tau_0')
        # gamma_s = self.extract_param(df, 'gamma_s')
        # gamma_0 = self.extract_param(df, 'gamma_0')
        # zeta_s = self.extract_param(df, 'zeta_s')
        # zeta_b = self.extract_param(df, 'zeta_b')
        # dt = self.extract_param(df, 'dt')
        # Re_s = self.extract_param(df, 'Re_s', 'Unitless')
        #-----------------
        # kT = kBT / Ec
        # DT = dt / Tc
        # MASSe = mass_e / Mc
        # MASSp = mass_p / Mc
        # MASS = mass / Mc
        # GAMMAs = gamma_s * Tc
        # GAMMA0 = gamma_0 * Tc
        # RADIUS = radius / Lc

    def load_simulation_metadata(self):
        simulation_params = '{}/{}.sim.h5'.format(self.dirs.trj, self.trj_fname_base)
        self.metadata.sim = pd.read_hdf(simulation_params, 'sim')
        self.NSTEPS = self.extract_param(df, 'n_steps')
        # NRUN = self.extract_param(df, 'n_runs', col='Value')
        # NAUX = self.extract_param(df, 'n_aux', col='Value')
        # NPAR = self.extract_param(df, 'n_par', col='Value')
        # NDIMS = self.extract_param(df, 'n_dim', col='Value')
        # NTOUT = self.extract_param(df, 'ntout', col='Value')
        # NOUT = int(NSTEPS/NTOUT)
        # NTASKS = int(NRUN / NPAR)  # int(NSETS * NWORKERS)
        # NSETS = int(NTASKS / NWORKERS)

    def extract_param(self, df, param, col='Units'):
        return df.loc[param, col]

    def combine_trajectories(self, df_list):
        '''Take a list of pandas DataFrames and return a combined one'''
        return pd.concat(df_list, axis=1)

    def reindex_parallel_traj(df, n_runs, n_aux, n_dims, var='pos'):
        runs = ['Run{:03d}'.format(r+1) for r in xrange(n_runs)]
        if n_dims == 1:
            coords = ['x']
        elif n_dims == 2:
            coords = ['x', 'y']
        elif n_dims == 3:
            coords = ['x', 'y', 'z']
        if (var == 'pos' or var == 'vel' or var == 'fex'
                or var == 'ths' or var == 'thb'):
            names = ['Run', 'Coordinate']
            headr = list(it.product(runs, coords))
        elif var == 'aux':
            names = ['Run', 'Coordinate', 'S']
            index = ['S{:02d}'.format(i+1) for i in xrange(n_aux)]
            headr = list(it.product(runs, coords, index))
        elif var == 'noi':
            names = ['Run', 'Coordinate', 'G']
            index = ['G{:02d}'.format(i+1) for i in xrange(n_aux)]
            headr = list(it.product(runs, coords, index))
        new_columns = pd.MultiIndex.from_tuples(headr, names=names)
        df.columns = new_columns
        return df

    def wide_to_long(self, df, col_name='X', dt=None):
        if 'Time' not in df.columns:
            if not isinstance(dt, float):
                print('Warning: timestep, dt, not specified or isn\'t type \
                        "float".')
                print(' --> setting dt to 1.0')
                dt = 1.0
            columns = ['Time'] + [i+1 for i in xrange(df.shape[1])]
            data = np.concatenate((dt * df.index.values[:, np.newaxis], df.values),
                                  axis=1)
        else:
            if dt is not None:
                df['Time'] *= dt
            columns = df.columns
            data = df.values
        df_flat = pd.DataFrame(data=data, columns=columns)  # Removes MultiIndexing
        df_flat['Step'] = df.index  # New col 'Step' via old index (before melting)
        # Specify 'identifier' variables; all unspecified columns will be unpivoted
        return df_flat.melt(id_vars=['Step', 'Time'],
                            var_name='Run',
                            value_name=col_name).infer_objects()

    def fmt_time(self, t):
        if t < 1e-12:
            return '{}fs'.format(round_to_int(t*1e15))
        elif t < 1e-9:
            return '{}ps'.format(round_to_int(t*1e12))
        elif t < 1e-6:
            return '{}ns'.format(round_to_int(t*1e9))
        elif t < 1e-3:
            return u'{}μs'.format(round_to_int(t*1e6))
        elif t < 1e0:
            return '{}ms'.format(round_to_int(t*1e3))
        else:
            return '{}s'.format(round_to_int(t))


###############################################################################
# NOTE: These are legacy functions!
#    For extracting simulation metadata (stored in DataFrames)
###############################################################################
def get_param_metadata(df, param, units='Units'):
    return df.loc[param, units]


def get_sim_metadata(df, param):
    return df.loc[param, 'Value']


###############################################################################
# Trajectory postprocessing: extraction, aggregation, manipulation
###############################################################################
def load_traj_full(trajdir, basename, n_tasks, n_runs, n_aux, n_par, n_dims,
                   var='pos', ext='h5', reindex=True):
    traj_df_list = []
    for i in xrange(n_tasks):
        traj_name = '{}/{}.{}.{}.{}'.format(trajdir, basename, i+1, var, ext)
        df_traj = pd.read_hdf(traj_name, format='table')
        traj_df_list.append(df_traj)

    df = combine_trajs(traj_df_list)
    if reindex:
        # n_sets = int(n_tasks / n_workers)
        # n_runs = n_workers * n_sets * n_par
        df = reindex_parallel_traj(df, n_runs, n_aux, n_dims, var=var)
    return df


def load_traj_part(tid, trajdir, basename, var='pos', ext='h5'):
    traj_name = '{}/{}.{}.{}.{}'.format(trajdir, basename, tid+1, var, ext)
    return pd.read_hdf(traj_name, format='table')


def get_run_from_long_df(df, run):
    return df[df['Run'] == run]


def generate_XVF_dataframe(dfX, dfF, dfV=None, dfF_name='External',
                           dtype='float32'):
    df_to_join = [dfV['Velocity'], dfF[dfF_name]] if dfV else [dfF[dfF_name]]
    df = dfX.join(df_to_join)
    dfX, df_to_join = None, None
    if dtype == 'float32':
        col_types = {'Step': 'uint32',
                     'Time': 'float32',
                     'Run': 'uint16'}
        return df.astype(col_types)
    return df


def wide_to_long_df(df, dt=None, value_name='Position', baseunit=None):
    if baseunit is not None:
        df *= baseunit
    if 'Time' not in df.columns:
        if not isinstance(dt, float):
            print('Warning: timestep, dt, not specified or isn\'t type \
                    "float".')
            print(' --> setting dt to 1.0')
            dt = 1.0
        columns = ['Time'] + [i+1 for i in xrange(df.shape[1])]
        data = np.concatenate((dt * df.index.values[:, np.newaxis], df.values),
                              axis=1)
    else:
        if dt is not None:
            df['Time'] *= dt
        columns = df.columns
        data = df.values
    df_flat = pd.DataFrame(data=data, columns=columns)  # Removes MultiIndexing
    df_flat['Step'] = df.index  # New col 'Step' via old index (before melting)
    # Specify 'identifier' variables; all unspecified columns will be unpivoted
    return df_flat.melt(id_vars=['Step', 'Time'],
                        var_name='Run',
                        value_name=value_name).infer_objects()


'''`melt` the `DataFrame` after having "time" as a separate column (should map
    to the index, which enumerates the time steps, but not be a separate index,
    just a column).
'''


def read_traj(basename, directory, var='pos', ext='h5'):
    '''Read a trajectory from disk and return the pandas DataFrame.

        Note: unlike the functions `reload_raw_trajs()` and
        `convert_to_long_format()` (see below), the `basename` should EXCLUDE
        the worker number in the name; e.g. if the names are
        "LE_EM_xoroshiro128.1.pos.h5", "LE_EM_xoroshiro128.2.pos.h5", etc, then
        specify `basename` as "LE_EM_xoroshiro128", not as
        "LE_EM_xoroshiro128.1", etc.
    '''
    fullname = '{}/{}.{}.{}'.format(directory, basename, var, ext)
    return pd.read_hdf(fullname, format='table')


def combine_trajs(list_of_dfs, var='pos'):
    '''Take a list of pandas DataFrames and return a combined one'''
    return pd.concat(list_of_dfs, axis=1)


def reindex_parallel_traj(df, n_runs, n_aux, n_dims, var='pos'):
    runs = ['Run{:03d}'.format(r+1) for r in xrange(n_runs)]
    if n_dims == 1:
        coords = ['x']
    elif n_dims == 2:
        coords = ['x', 'y']
    elif n_dims == 3:
        coords = ['x', 'y', 'z']
    if (var == 'pos' or var == 'vel' or var == 'fex'
            or var == 'ths' or var == 'thb'):
        names = ['Run', 'Coordinate']
        headr = list(it.product(runs, coords))
    elif var == 'aux':
        names = ['Run', 'Coordinate', 'S']
        index = ['S{:02d}'.format(i+1) for i in xrange(n_aux)]
        headr = list(it.product(runs, coords, index))
    elif var == 'noi':
        names = ['Run', 'Coordinate', 'G']
        index = ['G{:02d}'.format(i+1) for i in xrange(n_aux)]
        headr = list(it.product(runs, coords, index))
    new_columns = pd.MultiIndex.from_tuples(headr, names=names)
    df.columns = new_columns
    return df


def wide_format_from_async_result(async_result, n_threads, reindex=True,
                                  from_filenames=False):
    # solves the problem where a new list object (to store dataframes that
    # represent trajectories) will be created if the given key isn't found
    from collections import defaultdict
    appended_trajs = defaultdict(list)
    for sim_result, _ in async_result.get(0):
        for v in sim_result.keys():
            if from_filenames:
                temp = pd.read_hdf(sim_result[v], format='table')
            else:
                temp = sim_result[v]
            appended_trajs[v].append(temp)

    df_pos = combine_trajs(appended_trajs['positions'])
    df_vel = combine_trajs(appended_trajs['velocities'])
    df_for = combine_trajs(appended_trajs['forces'])
    df_ths = combine_trajs(appended_trajs['stokes_noises'])
    df_thb = combine_trajs(appended_trajs['basset_noises'])
    df_aux = combine_trajs(appended_trajs['auxiliaries'])
#     df_noi = combine_trajs(appended_trajs['noises'])
    if reindex:
        # The second item in following tuple has the size parameters
        n_aux, n_par, n_dim = async_result.get(0)[0][1]
        df_pos = reindex_parallel_traj(df_pos, n_threads, n_aux, n_par, n_dim,
                                       var='pos')
        df_vel = reindex_parallel_traj(df_vel, n_threads, n_aux, n_par, n_dim,
                                       var='vel')
        df_for = reindex_parallel_traj(df_for, n_threads, n_aux, n_par, n_dim,
                                       var='fex')
        df_ths = reindex_parallel_traj(df_ths, n_threads, n_aux, n_par, n_dim,
                                       var='ths')
        df_thb = reindex_parallel_traj(df_thb, n_threads, n_aux, n_par, n_dim,
                                       var='thb')
        df_aux = reindex_parallel_traj(df_aux, n_threads, n_aux, n_par, n_dim,
                                       var='aux')
    return df_pos, df_vel, df_for, df_ths, df_thb, df_aux


def reload_raw_trajs(basename, directory, var='pos', ext='h5', sep='.'):
    '''`basename` should EXCLUDE the worker number in the name; e.g. if the
        names are "LE_EM_xoroshiro128.1.pos.h5", "LE_EM_xoroshiro128.2.pos.h5",
        etc., then specify `basename` as "LE_EM_xoroshiro128".
    '''
    import os.path

    def _get_fullname(run):
        filename = '{}{}{}'.format(basename, sep, run)
        return '{}/{}.{}.{}'.format(directory, filename, var, ext)

    trajectories = []
    run = 1
    fullname = _get_fullname(run)
    while os.path.isfile(fullname):
        trajectories.append(pd.read_hdf(fullname, format='table'))
        run += 1
        fullname = _get_fullname(run)
    return combine_trajs(trajectories)


def temporary_wide_format_converter(df, dt=None, variable='position'):
    '''Create a DataFrame from the raw DataFrame representing a trajectory
        ensemble (each run separate column).

        Note: automatically adds a "time" column.
    '''
    if not isinstance(dt, float):
        print('Warning: timestep, dt, not specified or isn\'t type "float".')
        print(' --> setting dt to 1.0')
        dt = 1.0
    columns = ['Time'] + [i+1 for i in xrange(df.shape[1])]
    data = np.concatenate((dt * df.index.values[:, np.newaxis], df.values),
                          axis=1)

    # MultiIndexing is removed below since new DataFrame is constructed
    return pd.DataFrame(data=data, columns=columns, index=df.index)


def rewrite_to_wide_format(basename, outdir, outname=None, variable='position',
                           dt=None, ext='h5', reload_traj=True, clean=False,
                           sep='.', hdf_kwargs=None):
    '''Similar to `traj_to_long_format()`, but replaces the separate `h5` files
        (from individual workers) stored on disk with a combined, wide format
        file.

        `basename` should EXCLUDE the worker number in the name; e.g. if the
        names are "LE_EM_xoroshiro128.1.pos.h5", "LE_EM_xoroshiro128.2.pos.h5",
        etc, then specify `basename` as "LE_EM_xoroshiro128".

        If `reload_traj == True`, return the newly-built long-form DataFrame.
    '''
    short_var_name = variable[0:3]
    if outname is None:
        outname = '_'.join([basename, 'wide'])
    outname = '{}.{}'.format(outname, 'all')
    full_outname = '{}/{}.{}.{}'.format(outdir, outname, short_var_name, ext)

    if hdf_kwargs is None:
        hdf_kwargs = {'complib':   'blosc:lz4',
                      'complevel': 9,
                      'format':    'table',
                      'append':    True,
                      'mode':      'a'}

    df = reload_raw_trajs(basename, outdir, var=short_var_name, ext=ext,
                          sep=sep)
    df_wide = temporary_wide_format_converter(df, dt=dt, variable=variable)
    df_wide.to_hdf(full_outname, variable, **hdf_kwargs)

    if clean:
        print('Cleaning up raw trajectories...')
        import os

        def _get_fullname(run):
            filename = '{}{}{}'.format(basename, sep, run)
            return '{}/{}.{}.{}'.format(outdir, filename, short_var_name, ext)

        run = 1
        fullname = _get_fullname(run)
        while os.path.isfile(fullname):
            os.remove(fullname)
            run += 1
            fullname = _get_fullname(run)

        n_runs = run - 1
        for i in xrange(n_runs):
            run = i + 1
            assert not os.path.isfile(_get_fullname(run))
        print('Cleaned {} trajectories.'.format(n_runs))

    if reload_traj:
        return read_traj(outname, outdir, var=short_var_name, ext=ext)


###############################################################################
# Original versions here
###############################################################################
def load_traj_full_v0(trajdir, basename, n_tasks, n_dim, n_aux, n_par,
                      var='pos', ext='h5', reindex=True):
    traj_df_list = []
    for i in xrange(n_tasks):
        traj_name = '{}/{}.{}.{}.{}'.format(trajdir, basename, i+1, var, ext)
        df_traj = pd.read_hdf(traj_name, format='table')
        traj_df_list.append(df_traj)

    df = combine_trajs(traj_df_list)
    if reindex:
        df = reindex_parallel_traj(df, n_tasks, n_aux, n_par, n_dim, var=var)
    return df


def reindex_parallel_traj_v0(df, n_workers, n_aux, n_par, n_dims, var='pos'):
    runs = ['Run{:03d}'.format(p+1) for worker in xrange(n_workers) for p
            in xrange(worker*n_par, (worker+1)*n_par)]
    if n_dims == 1:
        coords = ['x']
    elif n_dims == 2:
        coords = ['x', 'y']
    elif n_dims == 3:
        coords = ['x', 'y', 'z']
    if (var == 'pos' or var == 'vel' or var == 'fex'
            or var == 'ths' or var == 'thb'):
        names = ['Run', 'Coordinate']
        headr = list(it.product(runs, coords))
    elif var == 'aux':
        names = ['Run', 'Coordinate', 'S']
        index = ['S{:02d}'.format(i) for i in xrange(1, n_aux+1)]
        headr = list(it.product(runs, coords, index))
    elif var == 'noi':
        names = ['Run', 'Coordinate', 'G']
        index = ['G{:02d}'.format(i) for i in xrange(1, n_aux+1)]
        headr = list(it.product(runs, coords, index))
    new_columns = pd.MultiIndex.from_tuples(headr, names=names)
    df.columns = new_columns
    return df


def wide_to_long_df_v0(df, dt=None, variable='Position', baseunit=None):
    '''Create a DataFrame from trajectory ensemble (each run separate column).
        Note: automatically adds a "time" column if there isn't one already.
    '''
    if baseunit is not None:
        df *= baseunit
    if 'Time' not in df.columns:
        if not isinstance(dt, float):
            print('Warning: timestep, dt, not specified or isn\'t type \
                    "float".')
            print(' --> setting dt to 1.0')
            dt = 1.0
        columns = ['Time'] + [i+1 for i in xrange(df.shape[1])]
        data = np.concatenate((dt * df.index.values[:, np.newaxis], df.values),
                              axis=1)
    else:
        if dt is not None:
            df['Time'] *= dt
        columns = df.columns
        data = df.values
    df_flat = pd.DataFrame(data=data, columns=columns)  # Removes MultiIndexing
    df_flat['Step'] = df.index  # New col 'Step' via old index (before melting)
    # Specify 'identifier' variables; all unspecified columns will be unpivoted
    return df_flat.melt(id_vars=['Step', 'Time'], var_name='Run',
                        value_name=variable)
