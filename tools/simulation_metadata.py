
import os
import shutil
import datetime
import pandas as pd
import numpy as np
from IPython.display import display
from .physical_quantities import (mass_p, mass_e, tau_s, tau_h, tau_0, gamma_s,
                                  gamma_0, zeta_s, zeta_b, Re_s, kBol, Navo,
                                  PI)


def back_up_file(fullpath, create_separate_dir=True, log_ext='log'):
    new_filepath = fullpath
    bdir, fname = os.path.split(fullpath)
    log_fname = fullpath.split('.', 1)[0] + '.' + log_ext
    f = open(log_fname, 'a+')
    f.write('[{}]\n'.format(datetime.datetime.now()))
    f.write('  >>> {} already exists in {}\n'.format(fname, bdir))

    i = 1
    if create_separate_dir:
        def _new_dir(i, bdir=bdir, name=fname.split('.', 1)[0]):
            return os.path.join(bdir, '#{}-{}#'.format(i, name))

        while os.path.exists(new_filepath):
            new_filepath = os.path.join(_new_dir(i), fname)
            i += 1
        new_dir, _ = os.path.split(new_filepath)
        if not os.path.isdir(new_dir):
            f.write('  >>> Making backup directory: {}\n'.format(new_dir))
            os.makedirs(new_dir)
        f.write('  >>> Moving file to {}\n'.format(new_dir))
        shutil.move(fullpath, new_dir)
    else:
        while os.path.exists(new_filepath):
            new_fname = '#{}-{}#'.format(i, fname)
            new_filepath = os.path.join(bdir, new_fname)
            i += 1
        f.write('  >>> Renaming file to {}\n'.format(new_fname))
        os.rename(fullpath, new_filepath)
    f.write('\n')
    f.close()


def store_parameter_metadata(R, knu, rhf, rhp, Te, dt, Lc, Tc, Ec, Mc, Vc, Fc, Pc,
                             NAUX, dynamics, integrator, rng_name, outdir,
                             unitless, verbose=True):

    filename = '_'.join([integrator, rng_name])
    fullname = '{}/{}.{}.{}'.format(outdir, filename, 'par', 'h5')
    if not os.path.exists(outdir):
        os.makedirs(outdir)

    MASS_P = mass_p(R, rhp)
    MASS_E = mass_e(R, rhf, rhp)
    MASS = MASS_P if dynamics == 'LE' else MASS_E
    TAU_S = tau_s(R, knu, rhf, rhp, MASS)
    TAU_H = tau_h(R, knu)
    TAU_0 = tau_0(R, knu, rhf, rhp, MASS)
    GAMMA_S = gamma_s(R, knu, rhf, rhp, MASS)
    GAMMA_0 = gamma_0(R, knu, rhf, rhp, MASS, NAUX, Tc)
    ZETA_S = zeta_s(R, knu, rhf)
    ZETA_B = zeta_b(R, knu, rhf)
    RE_S = Re_s(Vc, R, knu)
    kBT = kBol*Te

    variable_names = ['mass_e', 'mass_p', 'mass', 'radius', 'kBT', 'tau_s',
                      'tau_h', 'tau_0', 'gamma_s', 'gamma_0', 'zeta_s',
                      'zeta_b', 'dt', 'Lc', 'Tc', 'Ec', 'Mc', 'Vc', 'Fc', 'Pc',
                      'gam0dt', 'Re_s', 'beta']

    param_dict = {}
    param_dict['Name'] = [
        r'$\mathsf{M_e}$', r'$\mathsf{M_p}$', r'$\mathsf{M}$', r'$\mathsf{R}$',
        r'$\mathsf{k_BT}$', r'$\tau_s$', r'$\tau_h$', r'$\tau_0$',
        r'$\gamma_s$', r'$\gamma_0$', r'$\zeta_s$', r'$\zeta_b$',
        r'$\Delta t$', r'$\mathsf{L_c}$', r'$\mathsf{T_c}$', r'$\mathsf{E_c}$',
        r'$\mathsf{M_c}$', r'$\mathsf{V_c}$', r'$\mathsf{F_c}$', r'$\mathsf{P_c}$',
        r'$\gamma_0\Delta t$', r'$\mathsf{Re_s}$', r'$\beta$'
    ]
    param_dict['Units'] = [MASS_E, MASS_P, MASS, R, kBT, TAU_S, TAU_H, TAU_0,
                           GAMMA_S, GAMMA_0, ZETA_S, ZETA_B, dt, Lc, Tc, Ec,
                           Mc, Vc, Fc, Pc, -1, -1, -1
                           ]
    param_dict['Units-nice'] = [
        '{:9.2e} kg'.format(MASS_E),
        '{:9.2e} kg'.format(MASS_P), '{:9.2e} kg'.format(MASS),
        '{:9.2e} nm'.format(R*1e9), '{:9.2e} kJ/mol'.format(kBT),
        '{:9.2e} ps'.format(TAU_S*1e12), '{:9.2e} ps'.format(TAU_H*1e12),
        '{:9.2e} ps'.format(TAU_0*1e12), '{:9.2e} 1/ps'.format(GAMMA_S*1e-12),
        '{:9.2e} 1/ps'.format(GAMMA_0*1e-12), '{:9.2e} kg/s'.format(ZETA_S),
        '{:9.2e} kg/s'.format(ZETA_B), '{:9.2e} ps'.format(dt*1e12),
        '{:9.2e} nm'.format(Lc*1e9), '{:9.2e} ps'.format(Tc*1e12),
        '{:9.2e} kJ/mol'.format(Ec*1e-3*Navo),
        '{:9.2e} kDa'.format(Mc*1e3*Navo), '{:9.2e} nm/ps'.format(Vc*1e-3),
        '{:9.2e} pN'.format(Fc*1e12),
        u'{:9.2e} pN nm/μs'.format(Pc*1e12*1e9*1e-6).encode('utf-8'),
        'N/A', 'N/A', 'N/A'
    ]
    param_dict['Unitless'] = [MASS_E/Mc, MASS_P/Mc, MASS/Mc, R/Lc, kBT/Ec,
                              TAU_S/Tc, TAU_H/Tc, TAU_0/Tc, GAMMA_S*Tc,
                              GAMMA_0*Tc, ZETA_S*Tc/Mc, ZETA_B*Tc/Mc, dt/Tc,
                              Lc/Lc, Tc/Tc, Ec/Ec, Mc/Mc, Vc/Vc, Fc/Fc, Pc/Pc,
                              GAMMA_0*dt, RE_S, np.sqrt(TAU_H/TAU_S/PI)
                              ]

    df = pd.DataFrame(param_dict, index=variable_names)

    if verbose:
        pd.set_option('display.max_rows', 25)
        pd.options.display.float_format = '{:9.2e}'.format
        display(df)
    if os.path.exists(fullname):
        back_up_file(fullname)
    df.to_hdf(fullname, 'par', format='table', append=False)


def store_simulation_metadata(n_runs, n_aux, n_par, n_dim, n_steps, ntout,
                              ntdump, dynamics, integrator, rng_name, seed,
                              outdir, verbose=False):
    filename = '_'.join([integrator, rng_name])
    fullname = '{}/{}.{}.{}'.format(outdir, filename, 'sim', 'h5')
    if not os.path.exists(outdir):
        os.makedirs(outdir)

    param_dict = {}
    variable_names = ['n_runs', 'n_aux', 'n_par', 'n_dim',
                      'n_steps', 'ntout', 'ntdump', 'seed']
    param_dict['Value'] = [n_runs, n_aux, n_par, n_dim,
                           n_steps, ntout, ntdump, seed]
    df = pd.DataFrame(param_dict, index=variable_names)

    if verbose:
        pd.set_option('display.max_rows', 25)
        pd.options.display.float_format = '{:9.2e}'.format
        display(df)
    if os.path.exists(fullname):
        back_up_file(fullname)
    df.to_hdf(fullname, 'sim', format='table', append=False)
