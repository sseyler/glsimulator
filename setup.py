"""setuptools installation of GLSimulator
Copyright (c) 2018 Sean L Seyler <slseyler@asu.com>
Released under the GNU Public License 3 (or higher, your choice)

For a basic installation just type the command::
    python setup.py install
See the files INSTALL and README for details
"""
# Set this to True to enable building extensions using Cython.
# Set it to False to build extensions from the C file (that
# was previously created using Cython).
# Set it to 'auto' to build with Cython if available, otherwise
# from the C file.
# rm -rf ./*.so ./*.c && python setup_default.py build_ext --inplace --force

# from __future__ import with_statement#, unicode_literals

USE_CYTHON = True
CYTHON_LINETRACE = False
CYTHON_ANNOTATE = True
NTHREADS = 1

import numpy as np
# from scipy._build_utils import numpy_nodepr_api  # for silencing the numpy API warnings...
import os, sys
from os.path import join
import platform, socket
import cython_gsl

try:
    from setuptools import setup, find_packages
    from setuptools.extension import Extension
except ImportError:
    from distutils.core import setup
    from distutils.extension import Extension

################################################################################
## Check whether Cython compilation step is needed
#--------------------------------------
if USE_CYTHON:
    try:
        from Cython.Build import cythonize

        global_compiler_directives = {}
        global_macros = []

        if CYTHON_LINETRACE:
            global_compiler_directives['profile'] = True
            global_compiler_directives['linetrace'] = True
            global_compiler_directives['binding'] = True
            global_macros += ('CYTHON_TRACE', '1')
        if CYTHON_ANNOTATE:
            global_compiler_directives['annotate'] = True
    except:
        print("You don't seem to have Cython installed. Please get a")
        print("copy from www.cython.org and install it")
        sys.exit(1)

cmdclass = { }
ext_modules = [ ]
#-------------------------------------------------------------------------------


################################################################################
## Remove REALLY irritating -Wstrict-prototypes warnings for C++ compiling
##   See: https://tinyurl.com/y7tbdj6u
#--------------------------------------
import distutils.sysconfig
cfg_vars = distutils.sysconfig.get_config_vars()
for key, value in cfg_vars.items():
    if type(value) == str:
        cfg_vars[key] = value.replace("-Wstrict-prototypes", "")
#-------------------------------------------------------------------------------


################################################################################
## Set up compiler, environment, libraries, etc.
#--------------------------------------
COMPILER = 'gnu'
HOSTNAME = platform.node()  # socket.hostname() -- slower

if HOSTNAME == 'seylermoon':
    if COMPILER == 'gnu':
        os.environ["CC"] = "gcc-7"
        os.environ["CXX"] = "g++-7"
        COMPILE_ARGS = [
            '-march=native', '-O3', '-mavx', '-mprefer-avx128', '-ffast-math',
            '-ftree-vectorizer-verbose=2', '-Wno-cpp', '-Wno-unused-variable',
            '-Wno-unused-function', '-Wno-discarded-qualifiers']
    USER_DIR = '/home/sseyler'
    MINICONDA_INCLUDE_DIR = f'{USER_DIR}/Library/miniconda3/include'
elif HOSTNAME == 'seylerpluto':
    if COMPILER == 'gnu':
        os.environ["CC"] = "gcc"
        os.environ["CXX"] = "g++"
        COMPILE_ARGS = [
            '-march=native', '-O3', '-mavx', '-mprefer-avx128', '-ffast-math',
            '-ftree-vectorizer-verbose=2', '-Wno-cpp', '-Wno-unused-variable',
            '-Wno-unused-function', '-Wno-discarded-qualifiers']
    if COMPILER == 'intel':
        os.environ["CC"] = "icc"
        os.environ["CXX"] = "icpc"
        os.environ["LINKCC"] = "icc"
        os.environ["LDSHARED"] = "icc -shared"
        COMPILE_ARGS = ['-fast', '-qopt-report=4']
    USER_DIR = '/home/sseyler'
    MINICONDA_INCLUDE_DIR = f'{USER_DIR}/Library/miniconda3/include'
elif HOSTNAME == 'Seans-MBP':
    if COMPILER == 'gnu':
        os.environ["CC"] = "gcc"
        os.environ["CXX"] = "g++"
        COMPILE_ARGS = [
            '-march=native', '-O3', '-mavx', '-mprefer-avx128', '-ffast-math',
            '-ftree-vectorizer-verbose=2', '-Wno-cpp', '-Wno-unused-variable',
            '-Wno-unused-function', '-Wno-discarded-qualifiers']
    USER_DIR = '/Users/sseyler'
    MINICONDA_INCLUDE_DIR = f'{USER_DIR}/opt/miniconda3/include'
#'-std=c++11', -fopenmp, -shared -pthread -fPIC -fwrapv -fno-strict-aliasing


################################################################################
## Specify sources
#--------------------------------------
module_dir = './glsimulator'

## Specify module names (names of *.so files)
mod_glsim      = 'glsimulator.core.glsim'
mod_integrator = 'glsimulator.core.integrator'
mod_system     = 'glsimulator.core.system'
mod_sim_state  = 'glsimulator.core.sim_state'
mod_force      = 'glsimulator.core.force'
mod_glrandom   = 'glsimulator.core.glrandom'
mod_types      = 'glsimulator.core.types'
mod_timers     = 'glsimulator.core.timers'
mod_progressmeter = 'glsimulator.core.progressmeter'


RANDOMGEN_INCLUDE_DIRS = [
        join(f'{USER_DIR}', 'Repositories', 'python', 'randomgen'),
        join(f'{USER_DIR}', 'Repositories', 'python', 'randomgen', 'randomgen')]
src_randomgen = join(RANDOMGEN_INCLUDE_DIRS[1], 'src', 'distributions', 'distributions.c')

def pyx_name_from_mod_name(mod_name):
    return [join(*mod_name.split('.')) + '.pyx']

src_glsim      = pyx_name_from_mod_name(mod_glsim)
src_integrator = pyx_name_from_mod_name(mod_integrator)
src_system     = pyx_name_from_mod_name(mod_system)
src_sim_state  = pyx_name_from_mod_name(mod_sim_state)
src_force      = pyx_name_from_mod_name(mod_force)
src_glrandom   = pyx_name_from_mod_name(mod_glrandom) + [src_randomgen]
src_types      = pyx_name_from_mod_name(mod_types)
src_timers     = pyx_name_from_mod_name(mod_timers)
src_progressmeter = pyx_name_from_mod_name(mod_progressmeter)

sources = {mod_glsim : src_glsim,
           mod_integrator : src_integrator,
           mod_system : src_system,
           mod_sim_state : src_sim_state,
           mod_force : src_force,
           mod_glrandom : src_glrandom,
           mod_types : src_types,
           mod_timers : src_timers,
           mod_progressmeter : src_progressmeter}

GENERAL_INCLUDE_DIRS = ['.','...', os.path.join(os.getcwd(), 'include'), np.get_include(),
                        MINICONDA_INCLUDE_DIR] + [cython_gsl.get_include()]
GENERAL_LIBRARY_DIRS = [os.getcwd(),]  # path to .a or .so file(s   )
GENERAL_LIBRARIES = ['m']  # compile modules


################################################################################
## Set up and build extensions
#--------------------------------------
def ensure_list(str_or_list):
    if str_or_list is None:
        return []
    elif isinstance(str_or_list, list):
        return str_or_list
    elif isinstance(str_or_list, str):
        return [str_or_list]


def generate_extension(module, extra_libs=None, extra_lib_dirs=None, extra_inc_dirs=None,
                       extra_comp_args=None, macros=global_macros, language='c'):
    source = sources[module]
    # Default include directories, libraries, library directories, and compiler arguments
    inc_dirs  = GENERAL_INCLUDE_DIRS
    libs      = GENERAL_LIBRARIES
    lib_dirs  = GENERAL_LIBRARY_DIRS
    comp_args = COMPILE_ARGS

    inc_dirs += ensure_list(extra_inc_dirs)
    libs += ensure_list(extra_libs)
    lib_dirs += ensure_list(extra_lib_dirs)
    comp_args += ensure_list(extra_comp_args)

    ext = Extension(module,
        sources = source,
        include_dirs = inc_dirs,
        libraries    = libs,
        library_dirs = lib_dirs,
        extra_compile_args = comp_args,
        language = language,
        define_macros=macros
    )
    return [ext]


EXTENSIONS = []
EXTENSIONS += generate_extension(mod_progressmeter, language='c')
EXTENSIONS += generate_extension(mod_timers)
EXTENSIONS += generate_extension(mod_types)
EXTENSIONS += generate_extension(
    mod_glrandom,
    extra_inc_dirs=[cython_gsl.get_cython_include_dir()] + RANDOMGEN_INCLUDE_DIRS,
    extra_libs=cython_gsl.get_libraries(),
    extra_lib_dirs=cython_gsl.get_library_dir()
)
EXTENSIONS += generate_extension(mod_force)
EXTENSIONS += generate_extension(mod_sim_state)
EXTENSIONS += generate_extension(mod_system)
EXTENSIONS += generate_extension(
    mod_integrator,
    extra_inc_dirs=cython_gsl.get_cython_include_dir(),
    extra_libs=cython_gsl.get_libraries(),
    extra_lib_dirs=cython_gsl.get_library_dir()
)
EXTENSIONS += generate_extension(
    mod_glsim,
    extra_inc_dirs=cython_gsl.get_cython_include_dir(),
    extra_libs=cython_gsl.get_libraries(),
    extra_lib_dirs=cython_gsl.get_library_dir()
)


print('------------------------------------------------------------------------')
print('Running setup...')

def readme():
    with open("README.rst") as f:
        return f.read()

setup(name = 'glsimulator',
    version = '0.1.0.dev',
    description      = 'A simulation engine for generalized Langevin dynamics.',
    long_description = readme(),
    author           = 'Sean L Seyler',
    author_email     = 'slseyler@asu.com',
    url              = 'http://glsimulator.readthedocs.org/',
    project_urls = {
        'Source code' : 'https://bitbucket.org/sseyler/glsimulator/',
        'Documentation' : 'http://glsimulator.readthedocs.org/'
    },
    keywords = ("Langevin simulation stochastic SDE 'Markovian embedding'"
                "'generalized Langevin' 'Basset force' 'Basset-Boussinesq-Oseen'"),
    classifiers = [
        'Development Status :: 3 - Alpha',
        'Environment :: Console',
        'Intended Audience :: Science/Research',
        'License :: OSI Approved :: GNU General Public License (GPL)',
        'Operating System :: POSIX',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Cython :: 0.29.13',
        'Topic :: Scientific/Engineering :: Physics',
    ],
    packages = ['glsimulator', 'glsimulator.core', 'glsimulator.manager',
                'glsimulator.utils'],
    package_data = {
        '': ['*.pyx', '*.pxd'],
    },
    include_dirs = GENERAL_INCLUDE_DIRS,
    ext_modules = cythonize(EXTENSIONS, nthreads=NTHREADS, **global_compiler_directives),
    # install_requires=['cython>=0.28',
    #                   'numpy>=1.12',
    #                   'scipy>=1.0'],
    tests_require=['numpy', 'pandas'],
)
