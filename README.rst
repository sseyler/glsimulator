# -*- coding: utf-8 -*-

======================
GLSimulator
======================

GLSimulator is a numerical code written in Python/Cython for simulating the 3D
time evolution of particles moving according to generalized Langevin equations.

:Authors:      Sean L. Seyler
:Organization: Arizona State University
:Contact:      slseyler@asu.edu
:Year:         2018
:License:      GPLv3
:Copyright:    © 2018 Sean L. Seyler

|

Attribution and Disclaimer
==========================

The GLSimulator code, for the numerical simulation of the fluctuating Basset-Boussinesq-Oseen (FBBO)
equation, was used to generate the results published in
`Long-time persistence of hydrodynamic memory boosts microparticle transport`_ [1] and
`Surmounting potential barriers: Hydrodynamic memory hedges against thermal fluctuations in particle transport`_ [2].
If you use this work and any of the ideas therein, please cite one or both of these articles
as appropriate! The corresponding abstracts from my APS March Meeting talks,
`Hydrodynamic Brownian motion and nanoscale transport efficiency in liquids`_ (2019) and
`Hydrodynamic memory and driven microparticle transport: hedging against fluctuating sources of energy`_ (2020),
are also available.

It is my hope that in the near future, a fresh repository will be created for each article, which will not only
contain the core numerical code used to generate the data, but also the datasets themselves
along with drastically simplified scripts to reproduce those datasets. Ideally, these scripts should
be straightforward enough for a competent computational scientist to run; in the meantime, I can
provide the datasets upon reasonable request.




|

Overview
=========

See `README.ipynb`.

|

References
===========

[1] Seyler, S. L. and Pressé, Steve. Long-time persistence of hydrodynamic memory boosts microparticle transport. *Phys. Rev. Research* **1**, 032003(R) (2019). doi: `10.1103/PhysRevResearch.1.032003`_
  
[2] Seyler, S. L. and Pressé. Surmounting potential barriers: hydrodynamic memory hedges against thermal fluctuations in particle transport. *J. Chem. Phys.* **153**, 041102 (2020). doi: `10.1063/5.0013722`_

|

.. _`Long-time persistence of hydrodynamic memory boosts microparticle transport`: https://journals.aps.org/prresearch/abstract/10.1103/PhysRevResearch.1.032003
.. _`Surmounting potential barriers: Hydrodynamic memory hedges against thermal fluctuations in particle transport`: https://aip.scitation.org/doi/full/10.1063/5.0013722

.. _`10.1103/PhysRevResearch.1.032003`: https://doi.org/10.1103/PhysRevResearch.1.032003
.. _`10.1063/5.0013722`: https://doi.org/10.1063/5.0013722

.. _`Hydrodynamic Brownian motion and nanoscale transport efficiency in liquids`: https://meetings.aps.org/Meeting/MAR19/Session/S57.5
.. _`Hydrodynamic memory and driven microparticle transport: hedging against fluctuating sources of energy`: http://meetings.aps.org/Meeting/MAR20/Session/A24.10